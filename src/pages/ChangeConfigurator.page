<apex:page showHeader="true" sidebar="false" standardController="Extension__c" extensions="ChangeConfiguratorController">
	<apex:includeScript value="{!$Resource.AngularJS}"/>
	<apex:includeScript value="{!$Resource.BootstrapAngularDirectives}"/>

	<apex:stylesheet value="{!URLFOR($Resource.NormalizeCSS)}"/>

	<style>
		.error-msg .messageText {
			white-space: pre-line;
		}

		.table-view th {
    		padding: 0px 15px 0px 5px !important;
    		display: table-cell !important;
    		height: 35px !important;
    		background-color: white !important;
		}

		.table-view .bottom-row {
    		/*background-color: #539FC6 !important;*/
    		height: 35px !important;
    		font-weight: bold;
		}

		.table-view .label {
			text-align: right;
			/*color: white !important;*/
		}

		.table-view select, .table-view input {
    		padding: 3px;
    		border-radius: 4px;
    		border: 1px solid #c5c5c5;
    		outline: none !important;
		}

		.save-button, .cancel-button {
    		cursor: pointer;
    		padding: 7px;
    		background-color: white !important;
    		background: none;
		}
	</style>

	<script type="text/javascript">

		var currencyFilterFunction = function(input) {
            if (input == null) { return ''; }
            var string = input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ").replace('\.', ',');
            var split = string.split(',');
            if (split.length == 2 && split[1].length == 1) {
                string += '0';
            }

            return  string + ' {!currencySymbol}';
        };

		var changeConfigurator = angular.module('changeConfigurator', ['ui.bootstrap']);

		changeConfigurator.controller('changeConfiguratorCtrl', function($scope, $parse, $window, $q, $timeout) {
			$scope.retURL = ('{!retURL}' != '')? '{!retURL}' : null;
			$scope.standard = 'Standard';
			$scope.totalValue = 0;
			$scope.changeData = ('{!changeDataJSON}' != '')? JSON.parse('{!changeDataJSON}') : [];
			$scope.standardOptions = [];
			$scope.standardOptions.push.apply($scope.standardOptions, '{!standardOptionsJSON}' != ''? JSON.parse('{!standardOptionsJSON}') : []);
			$scope.errors = false;
			$scope.errorMsg = '';

            $scope.calculate = function(item) {
				if(item.isPriceListed){
					var result = item.ext.Price_of_change__c * item.ext.Amount__c;
					result = Math.round(result * 100) / 100;
					if(item.ext.Selected__c && item.ext.Standard_of_change__c != $scope.standard){
						item.valueOfChange = result;
					} else if( (item.ext.Selected__c && item.ext.Standard_of_change__c == $scope.standard) || !item.ext.Selected__c && item.ext.Standard_of_change__c != $scope.standard  ) {
                        item.valueOfChange = 0;
                    } 
                    else if(!item.ext.Selected__c && item.ext.Standard_of_change__c == $scope.standard) {
						item.valueOfChange = -1 * result;
					}
				} else {
					item.valueOfChange = item.ext.Price_of_change__c;
				}
            }

            $scope.sumUp = function() {
            	var result = 0;
				angular.forEach($scope.changeData, function(item) {
                	result += item.valueOfChange;
            	});
            	$scope.totalValue = Math.round(result * 100) / 100;
            }

            $scope.reCalculate = function(item) {
            	$scope.calculate(item);
            	$scope.sumUp();
           	}

            //----- INIT -----
			angular.forEach($scope.changeData, function(item) {
                $scope.calculate(item);
            });
            $scope.sumUp();

            $scope.saveChanges = function() {

                var changeElements = [];
                angular.forEach($scope.changeData, function(data) {
                    changeElements.push(data.ext);
                });

               	var changeElementsJSON = JSON.stringify(changeElements); 
                Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.ChangeConfiguratorController.saveData}', changeElementsJSON, function(result) {
                    if(result.success) {
                        $window.location.href = $scope.retURL;
                    } else {
                        $scope.errors = true;
                        $scope.errorMsg = '';
                        angular.forEach(result.errMsgs, function(err) {
                    		$scope.errorMsg += err + '\n';
                		});
                    }
                    $scope.$apply();
                });
            }

            $scope.cancelChanges = function() {
				$window.location.href = $scope.retURL;
            }
		})

		.filter('currencyFilter', function(){
            return currencyFilterFunction;
        })
	</script>

	<div class="body main-div" ng-class="{'show-div': displayReady}" ng-app="changeConfigurator" ng-controller="changeConfiguratorCtrl">
	<apex:PageBlock title="{!$Label.ChangeConfigureHeader}" id="changeConfigureBlock">
		<div class="error-msg" ng-if="errors">
        	<apex:pageMessage severity="Error" strength="3" summary="{{errorMsg}}"/>
        </div>
		<apex:pageBlockButtons location="bottom" >
        	<button class="save-button" ng-click="saveChanges();">{!$Label.site.save}</button>
        	<button class="cancel-button" ng-click="cancelChanges();">{!$Label.site.cancel}</button>
        </apex:pageBlockButtons>
		<div class="table-view-container">
			<table class="table-view list" >
				<thead>
                    <tr class="headerRow">
                        <th><span></span></th>
                        <th><span>{!$ObjectType.Extension__c.fields.Standard_of_change__c.Label}</span></th>
                        <th><span>{!$ObjectType.Extension__c.fields.Price_of_change__c.Label}</span></th>
                        <th><span>{!$ObjectType.Resource__c.fields.Unit__c.Label}</span></th>
                        <th><span>{!$ObjectType.Extension__c.fields.Amount__c.Label}</span></th>
                        <th><span>{!$ObjectType.Extension__c.fields.Value_of_change__c.Label}</span></th>
                        <th><span>{!$ObjectType.Extension__c.fields.Selected__c.Label}</span></th>
                    </tr>
                </thead>
                <tbody> 
                	<tr class="dataRow" ng-repeat="item in changeData" >
                        <td ng-if="item.isPriceListed"><span>{{item.res.Name}}</span></td>
                        <td ng-if="!item.isPriceListed"><span>{{item.ext.Name}}</span></td>
                        <td ng-if="item.isPriceListed">
                        	<select ng-model="item.ext.Standard_of_change__c" ng-options="opt.value as opt.label for opt in standardOptions" ng-change="reCalculate(item);"></select>
                        </td>
                        <td ng-if="!item.isPriceListed"></td>
                        <td ng-if="item.isPriceListed">
                        	<input type="number" ng-model="item.ext.Price_of_change__c" ng-change="reCalculate(item);"/>
                        </td>
                        <td ng-if="!item.isPriceListed"></td>
                        <td><span>{{item.res.Unit__c}}</span></td>
                        <td ng-if="item.isPriceListed">
                        	<input type="number" ng-model="item.ext.Amount__c" ng-change="reCalculate(item);"/>
                        </td>
                        <td ng-if="!item.isPriceListed"></td>
                        <td ng-if="item.isPriceListed"><span>{{item.valueOfChange | currencyFilter}}</span></td>
                        <td ng-if="!item.isPriceListed">
                        	<input type="number" ng-model="item.ext.Price_of_change__c" ng-change="reCalculate(item);"/>
                        </td>
                        <td ng-if="item.isPriceListed"><input type="checkbox" ng-model="item.ext.Selected__c" ng-change="reCalculate(item);"/></td>
                        <td ng-if="!item.isPriceListed"><input type="checkbox" ng-model="item.ext.Selected__c" disabled="true"/></td>
                	</tr>
                	<tr class="dataRow bottom-row">
                		<td></td>
                		<td></td>
                		<td></td>
                		<td></td>
                		<td class="label"><span>{!$ObjectType.Extension__c.fields.Quotation__c.Label}:</span></td>
                		<td><span>{{totalValue | currencyFilter}}</span></td>
                		<td></td>
                	</tr>
                </tbody>
			</table>
		</div>
	</apex:PageBlock>
	</div>
</apex:page>