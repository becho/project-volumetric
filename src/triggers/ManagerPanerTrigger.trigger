trigger ManagerPanerTrigger on Manager_Panel__c (before insert, after insert, before update, after update) {
	TriggerHandler.execute(new TH_ManagerPanelTrigger());
}