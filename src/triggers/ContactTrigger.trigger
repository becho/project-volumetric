trigger ContactTrigger on Contact (
    before insert,
    before update
) {
    TriggerHandler.execute(new TH_ContactTrigger());
}