<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Marketing Campaigns</relationshipLabel>
        <relationshipName>Marketing_Campaigns</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Campaign_Effectiveness__c</fullName>
        <externalId>false</externalId>
        <formula>Number_of_Leads__c / Planned_Number_of_Leads__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Campaign Effectiveness</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Company_Company_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Contact__r.Account.Name &lt;&gt; &quot;&quot;,  Contact__r.Account.Name ,  $Label.Company_Form )</formula>
        <label>Company</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Company__c</fullName>
        <externalId>false</externalId>
        <label>Company</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Marketing Campaigns</relationshipLabel>
        <relationshipName>Marketing_Campaigns</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>End_Date_Campaign_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>Marketing_Campaign__r.End_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Expenses__c</fullName>
        <externalId>false</externalId>
        <label>Expenses</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Marketing_Campaign__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Marketing Campaign</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Marketing_Campaign__c.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>Marketing_Campaign</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Marketing_Campaign__c</referenceTo>
        <relationshipLabel>Marketing Campaign Contacts</relationshipLabel>
        <relationshipName>Marketing_Campaigns</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Number_of_Leads__c</fullName>
        <externalId>false</externalId>
        <label>Number of Leads</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Planned_Budget__c</fullName>
        <externalId>false</externalId>
        <label>Planned Budget</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Planned_Number_of_Leads__c</fullName>
        <externalId>false</externalId>
        <label>Planned Number of Leads</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Publish_Date__c</fullName>
        <externalId>false</externalId>
        <label>Publish Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Publisher__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Publisher</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Marketing_Campaigns</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Start_Date_Camapign_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>Marketing_Campaign__r.Start_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Title_Campaign_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>Marketing_Campaign__r.Title__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Title</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Title__c</fullName>
        <externalId>false</externalId>
        <label>Title</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Use_of_the_Budget__c</fullName>
        <externalId>false</externalId>
        <formula>Expenses__c /  Planned_Budget__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Use of the Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <label>Marketing Campaign</label>
    <listViews>
        <fullName>Kampanie</fullName>
        <columns>NAME</columns>
        <columns>RECORDTYPE</columns>
        <columns>Title__c</columns>
        <columns>Contact__c</columns>
        <columns>OWNER.ALIAS</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>Marketing_Campaign__c.Marketing_Campaign</value>
        </filters>
        <label>Kampanie</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Wszystkie</fullName>
        <columns>NAME</columns>
        <columns>RECORDTYPE</columns>
        <columns>Title__c</columns>
        <columns>Contact__c</columns>
        <columns>OWNER.ALIAS</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>Wszystkie</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>MC-{000000}</displayFormat>
        <label>Marketing Campaign Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Marketing Campaigns</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Marketing_Campaign</fullName>
        <active>true</active>
        <label>Marketing Campaign</label>
    </recordTypes>
    <recordTypes>
        <fullName>Marketing_Campaign_Contact</fullName>
        <active>true</active>
        <label>Marketing Campaign Contact</label>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>RECORDTYPE</customTabListAdditionalFields>
        <customTabListAdditionalFields>Title__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Marketing_Campaign__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Start_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>End_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OWNER.ALIAS</customTabListAdditionalFields>
        <customTabListAdditionalFields>LAST_UPDATE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Title__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Start_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>End_Date__c</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <webLinks>
        <fullName>Add_to_Marketing_Campaign</fullName>
        <availability>online</availability>
        <description>Add a client to the Marketing Campaign (with choosing the Contact option )</description>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Add to Marketing Campaign</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/20.0/connection.js&quot;)} 
{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/apex.js&quot;)} 
{!REQUIRESCRIPT(&quot;/resource/jQuery&quot;)} 
{!REQUIRESCRIPT(&quot;/resource/jsModal&quot;)}

sforce.connection.defaultNamespace = &apos;&apos;;
var j$ = jQuery.noConflict();

var profileName = &quot;{!$Profile.Name}&quot;;
//if(profileName != &apos;Properto Broker Sales&apos; &amp;&amp; profileName != &apos;Properto Sales&apos; &amp;&amp; //profileName != &apos;Properto Rent Sales&apos;){

	j$.modal(&apos;&lt;style&gt;#simplemodal-overlay {background-color:rgba(0,0,0,0.5);}&lt;/style&gt;&lt;div style=&quot;height:70px; width:70px; color: black; background-color: white; border-radius: 10px; padding:20px 20px 20px 20px; -webkit-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75);&quot;&gt;&lt;div style=&quot;font-weight: bold; text-align: center;&quot;&gt;{!$Label.modal_Loading}...&lt;/div&gt;&lt;div style=&quot;padding: 15px 20px;&quot;&gt;&lt;div style=&quot; height: 31px; width: 31px; background: url(/resource/Ajax_Loader) no-repeat&quot; &gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&apos;);

	setTimeout(function() {

		var json = JSON.stringify({
			sObjectFromId : &apos;{!Marketing_Campaign__c.Id}&apos;,
			sObjectFromName : &apos;Marketing_Campaign__c&apos;,
			sObjectToName : &apos;Marketing_Campaign__c&apos;,
			recordTypeDevName : &apos;Marketing_Campaign_Contact&apos;,
			fieldFromApiNames : [&apos;Name&apos;],
			fieldToApiNames : [&apos;Marketing_Campaign__c&apos;]
		});

		var url = sforce.apex.execute(&apos;WebserviceUtilities&apos;, &apos;generateUrl&apos;, {jsonString: json, noOverride: true});

		if(url != null &amp;&amp; url != &apos;fault&apos;) {
			j$.modal.close();
			window.open(url + &quot;&amp;retURL=/{!Marketing_Campaign__c.Id}&amp;saveURL=/{!Marketing_Campaign__c.Id}&quot;, &quot;_self&quot;);
		}
	}, 200);
//}</url>
    </webLinks>
    <webLinks>
        <fullName>New_Marketing_Campaigns</fullName>
        <availability>online</availability>
        <description>Button for related list on Account</description>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Add to Marketing Campaign</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/20.0/connection.js&quot;)} 
{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/apex.js&quot;)} 
{!REQUIRESCRIPT(&quot;/resource/jQuery&quot;)} 
{!REQUIRESCRIPT(&quot;/resource/jsModal&quot;)}

sforce.connection.defaultNamespace = &apos;&apos;;
var j$ = jQuery.noConflict();

var profileName = &quot;{!$Profile.Name}&quot;;


j$.modal(&apos;&lt;style&gt;#simplemodal-overlay {background-color:rgba(0,0,0,0.5);}&lt;/style&gt;&lt;div style=&quot;height:70px; width:70px; color: black; background-color: white; border-radius: 10px; padding:20px 20px 20px 20px; -webkit-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75);&quot;&gt;&lt;div style=&quot;font-weight: bold; text-align: center;&quot;&gt;{!$Label.modal_Loading}...&lt;/div&gt;&lt;div style=&quot;padding: 15px 20px;&quot;&gt;&lt;div style=&quot; height: 31px; width: 31px; background: url(/resource/Ajax_Loader) no-repeat&quot; &gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&apos;);

setTimeout(function() {

  var contactId = (&apos;{!Contact.Id}&apos; != null &amp;&amp; &apos;{!Contact.Id}&apos; != &apos;&apos;) ? &apos;{!Contact.Id}&apos; : &apos;{!Account.PersonContactId_Formula__c}&apos;;

  console.log(&apos;contactId: &apos; + contactId);

  var json = JSON.stringify({
    sObjectFromId: contactId,
    sObjectFromName: &apos;Contact&apos;,
    sObjectToName: &apos;Marketing_Campaign__c&apos;,
    recordTypeDevName: &apos;Marketing_Campaign_Contact&apos;,
    fieldFromApiNames: [&apos;Name&apos;],
    fieldToApiNames: [&apos;Contact__c&apos;]
  });

  var url = sforce.apex.execute(&apos;WebserviceUtilities&apos;, &apos;generateUrl&apos;, {
    jsonString: json,
    noOverride: true
  });

  if (url != null &amp;&amp; url != &apos;fault&apos;) {
    j$.modal.close();
    window.open(url + &quot;&amp;retURL=/&quot; + contactId, &quot;_self&quot;);
  }
}, 200);</url>
    </webLinks>
    <webLinks>
        <fullName>New_Marketing_Campaigns_for_contact</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Add to Marketing Campaign</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/20.0/connection.js&quot;)} 
{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/apex.js&quot;)} 
{!REQUIRESCRIPT(&quot;/resource/jQuery&quot;)} 
{!REQUIRESCRIPT(&quot;/resource/jsModal&quot;)} 

sforce.connection.defaultNamespace = &apos;&apos;; 
var j$ = jQuery.noConflict(); 

// var profileName = &quot;{!$Profile.Name}&quot;; 

// if(profileName === &apos;Properto Broker Sales&apos; || profileName === &apos;Properto Sales&apos; || profileName === &apos;Properto Rent Sales&apos; ){ 
// alert(&quot;{!$Label.ErrorProfileHandOverFailure}&quot;); 
// } 
// else{ 
j$.modal(&apos;&lt;style&gt;#simplemodal-overlay {background-color:rgba(0,0,0,0.5);}&lt;/style&gt;&lt;div style=&quot;height:70px; width:70px; color: black; background-color: white; border-radius: 10px; padding:20px 20px 20px 20px; -webkit-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75);&quot;&gt;&lt;div style=&quot;font-weight: bold; text-align: center;&quot;&gt;{!$Label.modal_Loading}...&lt;/div&gt;&lt;div style=&quot;padding: 15px 20px;&quot;&gt;&lt;div style=&quot; height: 31px; width: 31px; background: url(/resource/Ajax_Loader) no-repeat&quot; &gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&apos;); 

setTimeout(function() { 

	var json = JSON.stringify({ 
	sObjectFromId : &apos;{!Contact.Id}&apos;, 
	sObjectFromName : &apos;Contact&apos;, 
	sObjectToName : &apos;Marketing_Campaign__c&apos;, 
	recordTypeDevName : &apos;Marketing_Campaign_Contact&apos;, 
	fieldFromApiNames : [&apos;Name&apos;], 
	fieldToApiNames : [&apos;Contact__c&apos;] 
	}); 

	var url = sforce.apex.execute(&apos;WebserviceUtilities&apos;, &apos;generateUrl&apos;, {jsonString: json, noOverride: true}); 

	if(url != null &amp;&amp; url != &apos;fault&apos;) { 
		j$.modal.close(); 
		window.open(url + &quot;&amp;retURL=/{!Contact.Id}&quot;, &quot;_self&quot;); 
	} 
	
}, 200); 
// }</url>
    </webLinks>
    <webLinks>
        <fullName>Publish_on_Internal_Portal</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Publish on Internal Portal</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/20.0/connection.js&quot;)} 
{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/apex.js&quot;)} 
{!REQUIRESCRIPT(&quot;/resource/jQuery&quot;)} 
{!REQUIRESCRIPT(&quot;/resource/jsModal&quot;)}

sforce.connection.defaultNamespace = &apos;&apos;;
var j$ = jQuery.noConflict();

j$.modal(&apos;&lt;style&gt;#simplemodal-overlay {background-color:rgba(0,0,0,0.5);}&lt;/style&gt;&lt;div style=&quot;height:70px; width:70px; color: black; background-color: white; border-radius: 10px; padding:20px 20px 20px 20px; -webkit-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75);&quot;&gt;&lt;div style=&quot;font-weight: bold; text-align: center;&quot;&gt;{!$Label.modal_Loading}...&lt;/div&gt;&lt;div style=&quot;padding: 15px 20px;&quot;&gt;&lt;div style=&quot; height: 31px; width: 31px; background: url(/resource/Ajax_Loader) no-repeat&quot; &gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&apos;);

setTimeout(function() {
	if(!{!ISNULL(Marketing_Campaign__c.Publish_Date__c)}){
		j$.modal.close();
		alert(&apos;{!$Label.MarketingAlreadyPublished}&apos;);
	} 
	else{
		var result = sforce.apex.execute(&quot;IntegrationService&quot;, &quot;publishCampaignOnDeveloperPortal&quot;, {campId: &quot;{!Marketing_Campaign__c.Name}&quot;}); 
		if (result == &apos;SUCCESS&apos;) {
			j$.modal.close();
			alert(&apos;{!$Label.InformationSentToDeveloperPortal}&apos;);
			window.location.reload();
		}
		else { 
			j$.modal.close();
			alert(result);
		}
	}
}, 200);</url>
    </webLinks>
    <webLinks>
        <fullName>Remove_From_Internal_Portal</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Remove From Internal Portal</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/20.0/connection.js&quot;)} 
{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/apex.js&quot;)} 
{!REQUIRESCRIPT(&quot;/resource/jQuery&quot;)} 
{!REQUIRESCRIPT(&quot;/resource/jsModal&quot;)}

sforce.connection.defaultNamespace = &apos;&apos;;
var j$ = jQuery.noConflict();

j$.modal(&apos;&lt;style&gt;#simplemodal-overlay {background-color:rgba(0,0,0,0.5);}&lt;/style&gt;&lt;div style=&quot;height:70px; width:70px; color: black; background-color: white; border-radius: 10px; padding:20px 20px 20px 20px; -webkit-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75);&quot;&gt;&lt;div style=&quot;font-weight: bold; text-align: center;&quot;&gt;{!$Label.modal_Loading}...&lt;/div&gt;&lt;div style=&quot;padding: 15px 20px;&quot;&gt;&lt;div style=&quot; height: 31px; width: 31px; background: url(/resource/Ajax_Loader) no-repeat&quot; &gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&apos;);

setTimeout(function() {
	var result = sforce.apex.execute(&quot;IntegrationService&quot;, &quot;removeCampaignFromDeveloperPortal&quot;, {campId: &quot;{!Marketing_Campaign__c.Name}&quot;}); 
	if (result == &apos;SUCCESS&apos;) {
		j$.modal.close();
		alert(&apos;{!$Label.InformationSentToDeveloperPortal}&apos;);
		window.location.reload();
	}
	else { 
		j$.modal.close();
		alert(result);
	}
}, 200);</url>
    </webLinks>
</CustomObject>
