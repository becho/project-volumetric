/**
* @author       Mariia Dobzhanska
* @description  Test class for OffersEmail
**/

@isTest
private class TestClassOffersEmail {

  public final static String testsubject = 'Test Subject';
  public final static String testbody = 'Test Body';

    @testsetup
  static void setuptestdata() {
      //Creating the sales process proposal 
      Sales_Process__c salesprocess = TestHelper.createSalesProcessProposal(null, true);
      //Creating investment and resource for the product line
      Resource__c tInvestment = TestHelper.createInvestment(null, true);
      Id invId = tInvestment.Id;
      Resource__c fltempl = new Resource__c (Investment_Flat__c = invId);
      Resource__c flat = TestHelper.createResourceFlatApartment(fltempl, true);
      Id flId = flat.Id;
      //Creating the product line related to the proposal created with the created flat resource
      Sales_Process__c prlinetempl = new Sales_Process__c (

                  Product_Line_to_Proposal__c = salesprocess.Id,
                  Offer_Line_Resource__c = flId
      );
      Sales_Process__c prline = TestHelper.createSalesProcessProductLine(prlinetempl, true);
      List<Attachment> atts = new List<Attachment>();
      //Creating the attachment for testing
      String attbody = 'TestAttBody1';
      Attachment testatt1 = new Attachment (

                 Body = Blob.valueOf(attbody),
                 Name = String.valueOf('testattach.pdf'),
                 Description = 'Testatt1 Descr',
                 ContentType = CommonUtility.FILETYPE_PDF,
                 ParentId = salesprocess.Id
      );
      //Creating the investment attachment for testing
      Attachment testatt2 = new Attachment (

                 Body = Blob.valueOf(attbody),
                 Name = String.valueOf('testattach2.pdf'),
                 Description = 'Testatt2 Descr',
                 ContentType = CommonUtility.FILETYPE_PDF,
                 ParentId = invId
      ); 
      atts.add(testatt1);
      atts.add(testatt2);
      insert atts;
      //Creating the metadata attachment
      Metadata__c md = new Metadata__c();
      md.Metadata__c = '"Category":"listing","originalFilename":"kitchen-673733_1280.jpg","URL":"http://static-mennica.properto-portal.enxoo.com/C.LU.I.01/fzzQyrnzaHSNSu4X_30Jun2016090238GMT_1467277358026.jpg","Main":"false","FileType":"IMG"';
      md.Metadata_type__c = CommonUtility.METADATA_TYPE_FILE_PDF;
      md.ObjectID__c = CommonUtility.SOBJECT_NAME_RESOURCE;
      md.RecordID__c = flId;
      insert md;
  }
    //In the test class there is an external class TestMockOfferEmail used to get the fake response to get
    //the file body by url
    
    /**
    **Testing if the appropriate error is received while the email and account reference are null
    **/
    
    static testmethod void WrEmailTest() {
      Sales_Process__c salesprocess = [
                                        SELECT Id, Contact__c
                                        FROM Sales_Process__c
      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
                                        LIMIT 1
      ];
      ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
      
      Contact contactperson = [
                                SELECT Id, Email, AccountId
                                FROM Contact
                                WHERE id = :salesprocess.Contact__c
      ];
      
      //Changing the existing values to get an error
      contactperson.Email = null;
      contactperson.AccountId = null;
      update contactperson;
      Test.startTest();
        OffersEmail testobj = new OffersEmail(stdController);
      Test.stopTest();
      List<Apexpages.Message> msgs = ApexPages.getMessages();
      Boolean errorfound = false;
      
      //Checking if an expected error is received
      for(ApexPages.Message message : msgs) {
          if (message.getDetail().contains(Label.SendOfferEmptyEmail))
              errorfound = true;      
      }
      System.assert(errorfound);
  }
  
  /**
    **Testing if the Task with the expected values has been created and if the message that
    **claims about the successful sending sent
    **/
    
   static testmethod void sendOfferTest() {
        //Selecting the salesprocess and creating the controller
        //Choosing the salesprocess and creating an instance of a class for receiving the actual results
        Sales_Process__c salesprocess = [
                                          SELECT Id, Contact__c
                                          FROM Sales_Process__c
        WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
                                          LIMIT 1
        ];
         
        ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
        OffersEmail testobj = new OffersEmail(stdController); 
        //Creating a contact for sending an offer
        Contact sendcont = TestHelper.CreatecontactPerson(null, true);
        testobj.checkcontact = sendcont;
        //Assigning the email body and subject
        testobj.mSubject = testsubject;
        testobj.mBody = testbody;
        //Creating the expected task and task description that are expected to be received after sending an offer
        String exptaskDescription = Label.emailSentTo + ': ' + sendcont.email + '\n' 
           + Label.Attachment + ': \n' 
           + '' + '\n\n'
           + Label.EmailSubject + ': ' + testobj.mSubject + '\n'
           + Label.EmailBody + ': ' + testobj.mBody;
        Task expactivityHistoryTask = new Task(
                  WhatId = salesprocess.Id,
                  WhoId = salesprocess.Contact__c,
                  IsReminderSet = false,
                  ActivityDate = Date.today() - 1,
                  Description = exptaskDescription,
                  Subject = testobj.mSubject,
                  Status = 'Completed'
        );              
            Test.startTest();
          testobj.sendOffers(null);
        Test.stopTest();
        // Selecting the actual task
        Task acttask = [
                          SELECT Id, WhatId, Whoid, IsReminderSet,ActivityDate, Description, Subject, Status
                          FROM Task
                          WHERE WhatId = :salesprocess.id
                          LIMIT 1
        ];
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        //Checking if the email was sent by reading the message
        Boolean errorfound = false;
            
        //Checking if expected error was found 
        for(ApexPages.Message message : msgs) {
            if (message.getDetail().contains(Label.EmailSend))
                errorfound = true;      
        }
            
        System.assert(errorfound);
        // Checking if the attachments were cleared after sending
        System.assertEquals(0, testobj.linkedAttachments.size());
        // Checking if the created task is the same to expected
        System.assertEquals(expactivityHistoryTask.Description, acttask.Description);
        System.assertEquals(expactivityHistoryTask.WhoId, acttask.WhoId);
        System.assertEquals(expactivityHistoryTask.WhatId, acttask.WhatId);
        System.assertEquals(expactivityHistoryTask.IsReminderSet, acttask.IsReminderSet);
        System.assertEquals(expactivityHistoryTask.Subject, acttask.Subject);
        System.assertEquals(expactivityHistoryTask.Status, acttask.Status);  
    }
  
  /**
    **Testing if the expected task description has been created when the metadata attachment is chosen
    **/
    
  static testmethod void loadAttachmentsMetadataTest() {
       //Choosing the salesprocess and creating an instance of a class for receiving the actual results 
      Sales_Process__c salesprocess = [
                                        SELECT Id, Contact__c
                                        FROM Sales_Process__c
      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
                                        LIMIT 1
      ];

      ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
      OffersEmail testobj = new OffersEmail(stdController);
      // Creating contact to send to
      Contact sendcont = TestHelper.CreatecontactPerson(null, true);
      testobj.checkcontact = sendcont;
      //Creating test mail's subject and body
      testobj.mSubject = testsubject;
      testobj.mBody = testbody;
      String attbody;
      // Choosing the metadata type attachment
      if (testobj.linkedAttachments.size() > 0) {

        for (Integer i = 0; i < testobj.linkedAttachments.size(); i++) { 
            if (testobj.linkedAttachments[i].fromFTP == true) {
                testobj.linkedAttachments[i].checked = true;
                attbody = testobj.linkedAttachments[i].name + '.pdf'+'\n';
            }
        }
      }
      //Creating the expected task description
      String exptaskDescription = Label.emailSentTo + ': ' + sendcont.email + '\n' 
                + Label.Attachment + ': \n' 
                + attbody + '\n\n'
                + Label.EmailSubject + ': ' + testobj.mSubject + '\n'
                + Label.EmailBody + ': ' + testobj.mBody;
      Test.startTest();
       // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
  
        testobj.loadAttachments();
      Test.stopTest();

      Task acttask = [
                      SELECT Id, WhatId, Whoid, IsReminderSet,ActivityDate, Description, Subject, Status
                      FROM Task
                      WHERE WhatId = :salesprocess.id
                      LIMIT 1
      ];

      //Checking if the task description is as expected
      System.assertequals(exptaskDescription, acttask.Description);       
 }
    
  /**
    **Testing if the expected task description has been created when the  attachment is chosen
    **/   
    
  static testmethod void loadAttachmentsAttTest() {
       //Choosing the salesprocess and creating an instance of a class for receiving the actual results 
      Sales_Process__c salesprocess = [
                                        SELECT Id, Contact__c FROM
                                        Sales_Process__c
      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
                                        LIMIT 1
      ];

      ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
      OffersEmail testobj = new OffersEmail(stdController);
      // Creating contact to send to
      Contact sendcont = TestHelper.CreatecontactPerson(null, true);
      testobj.checkcontact = sendcont;
      //Creating test mail's subject and body
      testobj.mSubject = testsubject;
      testobj.mBody = testbody;
      String attbody;
      //Choosing the test attachment
      if (testobj.linkedAttachments.size() > 0) {
         
        for (Integer i = 0; i <testobj.linkedAttachments.size(); i++) { 
          if (testobj.linkedAttachments[i].fromFTP == false && testobj.linkedAttachments[i].name == String.valueOf('testattach.pdf')) {
              testobj.linkedAttachments[i].checked = true;
              attbody = testobj.linkedAttachments[i].name +'\n';
          }
        }
      }
      //Creating the expected task description
      String exptaskDescription = Label.emailSentTo + ': ' + sendcont.email + '\n' 
                 + Label.Attachment + ': \n' 
                 + attbody + '\n\n'
                 + Label.EmailSubject + ': ' + testobj.mSubject + '\n'
                 + Label.EmailBody + ': ' + testobj.mBody;
      Test.startTest();
        testobj.loadAttachments();
      Test.stopTest();
      Task acttask = [
                      SELECT Id, WhatId, Whoid, IsReminderSet,ActivityDate, Description, Subject, Status
                      FROM Task
                      WHERE WhatId = :salesprocess.id
                      LIMIT 1
      ];
      //Checking if the task description is as expected
      System.assertequals(exptaskDescription, acttask.Description);        
 }

  /**
  **Testing if the appropriate email body and subject are created, for testing the template Accepted Approval Proces
  **is used
  **/ 
  
/*  static testmethod void loadTemplateTestOkTypeCont() {
      //Choosing the template and its values
      String tempName = 'AcceptedApprovalProces';
      EmailTemplate extemp = [
                                SELECT Id, Body, Subject
                                FROM EmailTemplate
                                WHERE Name = :tempName
                                LIMIT 1
      ];
      String tempbody = extemp.Body;
      String tempsubj = extemp.Subject;
      //Creating the instance of class to get actual results
      Sales_Process__c salesprocess = [
                                        SELECT Id, Name
                                        FROM Sales_Process__c
      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
                                        LIMIT 1
      ];

      Id salesprId = salesprocess.Id;
      String salesprName = salesprocess.Name;
      ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
      OffersEmail testobj = new OffersEmail(stdController);
      //Choosing the template
      testobj.pickTempId = extemp.Id;
      String RECIPIENT_TO_SUBSTITUTE = 'xxxrepiNamexxx';
      Messaging.SingleEmailMessage mail = testobj.prepareEmail();
      mail.setPlainTextBody(tempbody);
      mail.setSubject(tempsubj);
      String expmBody = mail.getPlainTextBody().replace('{!Sales_Process__c.Name}', salesprName);
      String expmSubject = mail.getSubject().replace(RECIPIENT_TO_SUBSTITUTE, salesprName);
      Test.startTest();
        testobj.loadTemplate();
      Test.stopTest();
      //Comparing the body and subject
      System.assertEquals(expmBody, testobj.mBody);
      System.assertEquals(expmSubject, testobj.mSubject);
  }
  */
  
  /**
  **Testing if the expected error appears if the email is wrong
  **/
  
  static testmethod void loadTemplateBadEmail() {
      //Choosing the salesprocess and creating an instance of a class for receiving the actual results
      Sales_Process__c salesprocess = [
                                        SELECT Id, Contact__c 
                                        From Sales_Process__c
      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
                                        LIMIT 1
      ];  
      ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
      OffersEmail testobj = new OffersEmail(stdController);
      //Creating the empty contact for the sales process
      Contact wrcont = new Contact();
      testobj.checkContact = wrcont;
      Test.startTest();
        testobj.loadTemplate();
      Test.stopTest();
      //Checking if the expected error appeared
      List<Apexpages.Message> msgs = ApexPages.getMessages();
      Boolean errorfound = false;

      for(ApexPages.Message message : msgs) {
          if (message.getDetail().contains(Label.SendOfferEmptyEmail))
              errorfound = true;      
      }

      System.assert(errorfound);
  }
    
  /**
    **Testing if the mbody is as expected if the template not chosen
    **/
  
  static testmethod void loadTemplateEmptyTempl() {
      //Choosing the salesprocess and creating an instance of a class for receiving the actual results
      Sales_Process__c salesprocess = [
                                        SELECT Id, Contact__c
                                        FROM Sales_Process__c
      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
                                        LIMIT 1
      ];

      ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
      OffersEmail testobj = new OffersEmail(stdController);
      //Setting the null template
      testobj.pickTempId = null;
      Test.startTest();
        testobj.loadTemplate();
      Test.stopTest();
      String expMbody = '';
      System.assertequals(expMbody, testobj.mbody);
  }
    
  /**
    **Testing if the checkEmail method returns false if the Contact data is right
    **/
  
  static testmethod void checkEmailTestOk() {
      //Choosing the salesprocess and creating an instance of a class for receiving the actual results
      Sales_Process__c salesprocess = [
                                        SELECT Id, Contact__c
                                        FROM Sales_Process__c
      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
                                        LIMIT 1
      ];
      ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
      OffersEmail testobj = new OffersEmail(stdController);
      Test.startTest();
        Boolean actres = testobj.checkEmail();
      Test.stopTest();
      //Checking if the email is appropriate
      System.assertEquals(true, actres);
  }
    
/**
**Testing if the checkEmail method returns false if the Contact data is inappropriate
**/
  
  static testmethod void checkEmailTestWr() {
      //Choosing the salesprocess and creating an instance of a class for receiving the actual results
      Sales_Process__c salesprocess = [
                                        SELECT Id, Contact__c
                                        From Sales_Process__c
      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
      LIMIT 1
      ];
      
      ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
      OffersEmail testobj = new OffersEmail(stdController);
      //Creating the empty contact for the sales process
      Contact wrcont = new Contact();
      testobj.checkContact = wrcont;
      Test.startTest();
        Boolean actres = testobj.checkEmail();
      Test.stopTest();
      System.assertEquals(false, actres);
  }
    
  /**
    **Testing if the email becomes changed to null if the fakecontact exists
    **/
    
  static testmethod void sendOffersFakeContact() {
      Contact fakecont = TestHelper.createContactPerson(null, true);
      //Choosing the salesprocess and creating an instance of a class for receiving the actual results
      Sales_Process__c salesprocess = [
                                        SELECT Id, Contact__c
                                        FROM Sales_Process__c
      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
                                        LIMIT 1
      ];
      ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
      OffersEmail testobj = new OffersEmail(stdController); 
      testobj.isFakeContact = true;
      testobj.fakecontact = fakecont;
      Test.startTest();
        testobj.sendOffers(null);
      Test.stopTest();
      //Checking if the fakecontact email is null
      System.assertEquals(null, testobj.fakecontact.email);
  }
  
  /**
    **Testing if the prepareEmail method returns the email message as expected
    **/  
/*    
  static testmethod void prepareEmailTest() {
      //Choosing the salesprocess and creating an instance of a class for receiving the actual results
      Sales_Process__c salesprocess = [
                                        SELECT Id 
                                        FROM Sales_Process__c
      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
                                        LIMIT 1
      ];

      ApexPages.StandardController stdController = new ApexPages.StandardController(salesprocess);
      //Choosing the template and assigning the template property
      OffersEmail testobj = new OffersEmail(stdController);
      EmailTemplate extemp = [
                              SELECT Id
                              FROM EmailTemplate
                              LIMIT 1
      ];
      testobj.pickTempId = extemp.Id; 
      //Creating the expected emailmessage instance
      Messaging.SingleEmailMessage expmail = new Messaging.SingleEmailMessage();
      expmail.setUseSignature(false);
      expmail.setTargetObjectId(testobj.contactId);
      expmail.setWhatId(salesprocess.Id);
      expmail.setTemplateId(testobj.pickTempId);
      Test.startTest();
        Messaging.SingleEmailMessage prepem = testobj.prepareEmail();
      Test.stopTest();
      //Comparing if all the properties are as expected
      System.assertEquals(expmail.getTargetObjectId(), prepem.getTargetObjectId());
      System.assertEquals(expmail.getTemplateId(), prepem.getTemplateId());
      System.assertEquals(expmail.getWhatId(), prepem.getWhatId());
      System.assertEquals(expmail.isUserMail(), prepem.isUserMail());
  }*/
     
}