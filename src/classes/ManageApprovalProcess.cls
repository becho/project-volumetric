/**
* @author 		Mateusz Pruszyński
* @description 	Class to manage approval process across the system
**/

public without sharing class ManageApprovalProcess {

	// manages discount propagation
	public static void manageDiscountPropagation(List<Sales_Process__c> mainProcess) {
		// get config from cs
		DiscountApprovalSwitch__c approvalSwitch = DiscountApprovalSwitch__c.getInstance('CurrentConfig');
		
		if(approvalSwitch.Switch_On__c) {

			for(Sales_Process__c mp : mainProcess) {
				Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
				req.setObjectId(mp.Id);
				req.setSubmitterId(mp.OwnerId);
				req.setProcessDefinitionNameOrId('DiscountAcceptanceProdLine');
				Approval.ProcessResult result = Approval.process(req);          
			}

		}
	}

	// if Manager does not accept the discount rate requested by sales rep, the discount percent comes to max value stored on record owner (User object)
	public static void rejectDiscounts(Map<Id, List<Sales_Process__c>> prodLinesWithDiscountsToReject) {
		List<User> owners = [
			SELECT Id, Maximum_Discount__c
			FROM User
			WHERE Id in :prodLinesWithDiscountsToReject.keySet()
		];

		for(User owner : owners) {
			for(Sales_Process__c pl : prodLinesWithDiscountsToReject.get(owner.Id)) {
				pl.Discount__c = owner.Maximum_Discount__c;
				pl.Price_With_Discount__c += pl.Discount_Amount_Offer__c;
				if(pl.Discount__c != null && pl.Discount__c > 0) {
					pl.Discount_Amount_Offer__c = (pl.Discount_Amount_Offer__c / 100) * pl.Discount__c;
				} else {
					pl.Discount_Amount_Offer__c = 0;
				}
				pl.Price_With_Discount__c = pl.Price_With_Discount__c - pl.Discount_Amount_Offer__c;
			}
		}
	}

}