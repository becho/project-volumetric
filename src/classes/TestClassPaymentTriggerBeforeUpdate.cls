/**
* @author      Krystian Bednarek
* @description  Test class for th_payment
*/

@isTest
private class TestClassPaymentTriggerBeforeUpdate {
    
   /* @isTest static void EditLookupsPaymentFor  () {
        Resource__c investment = TestHelper.createInvestment(null, true);
        // Insert flat
        Resource__c flat = TestHelper.createResourceFlatApartment(              
            new Resource__c(
                Investment_Flat__c = investment.Id
            ), 
            true
        );

        Resource__c parkingSpace = TestHelper.createResourceParkingSpace(               
            new Resource__c(
                Investment_Parking_Space__c = investment.Id
            ), 
            true
        );

        Resource__c[] resources = new Resource__c[] {flat, parkingSpace};
        Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);


        List<Sales_Process__c> productLines = new List<Sales_Process__c>();
        for(Resource__c res : resources) {
            productLines.add(TestHelper.createSalesProcessProductLine(
                new Sales_Process__c(
                    Product_Line_to_Proposal__c = proposal.Id,
                    Contact_from_Offer_Line__c = proposal.Contact__c,
                    Offer_Line_Resource__c = res.Id
                ),  
                false
            ));
        }
        insert productLines;

        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Offer__c = proposal.Id,
                Contact__c = proposal.Contact__c
            ), 
            true
        );

        // Trigger part that does not allow to edit particular lookups (see CannotEditLookupsSalesProcess.cls description)
        Test.startTest();
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)
            ,
            Agreements_Installment__c = saleTerms.Id,
            Due_Date__c = Date.today(),
            Payment_For__c = productLines[0].Offer_Line_Resource__c
            );
        insert newPayment;
        newPayment.Payment_For__c = productLines[1].Offer_Line_Resource__c;
        try {
            update newPayment;  
        } 
        catch(Exception e){
            System.assert(String.valueOf(e).Contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
        Test.stopTest();
    }*/

    @isTest static void EditLookupsAgreementsInstallment () { 
        Sales_Process__c  saleTerms =  TestHelper.createSalesProcessSaleTerms(null, true);

        Payment__c payment = TestHelper.createPaymentsInstallment(
            new Payment__c(
                Agreements_Installment__c = saleTerms.Id
            ),
            true
        );

        Test.startTest();
            Sales_Process__c  saleTermsToUpdate =  TestHelper.createSalesProcessSaleTerms(null, true);
            payment.Agreements_Installment__c = saleTermsToUpdate.Id;
            try {
               update payment;
               System.assert(false, 'DmlException expected!');
            } catch(System.DmlException e) {
                    String message = e.getMessage();
                    String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('Agreements_Installment__c').getDescribe().getLabel();
                    System.assert(message.contains(errorMessage), 'message=' + message);
            }
        Test.stopTest();
    }

    @isTest static void EditLookupsAfterSalesService () {
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(null, true);

        Payment__c payment = TestHelper.createPaymentsInstallment(
            new Payment__c(
                After_sales_Service__c  = afterSales.Id
            ),
            true
        );

        Test.startTest();
            Sales_Process__c afterSalesToUpdate = TestHelper.createSalesProcessAfterSalesService(null, true);
            payment.After_sales_Service__c = afterSalesToUpdate.Id;
            try {
               update payment;
               System.assert(false, 'DmlException expected!');
            } catch(System.DmlException e) {
                    String message = e.getMessage();
                    String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('After_sales_Service__c').getDescribe().getLabel();
                    System.assert(message.contains(errorMessage), 'message=' + message);
            }
        Test.stopTest(); 
    }

    @isTest static void TestCannotEditContactLookupOnPayment () {
        Account personAccount = TestHelper.createAccountIndividualClient(null, true);
        Contact contactPerson = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id LIMIT 1];

        Payment__c payment = TestHelper.createPaymentsInstallment(
            new Payment__c(
                Contact__c  = contactPerson.Id
            ),
            true
        );

        Test.startTest();
            Account personAccountToUpdate = TestHelper.createAccountIndividualClient(null, true);
            Contact contactPersonToUpdate = [SELECT Id FROM Contact WHERE AccountId = :personAccountToUpdate.Id LIMIT 1];
            payment.Contact__c = contactPersonToUpdate.Id;
            try {
               update payment;
               System.assert(false, 'DmlException expected!');
            } catch(System.DmlException e) {
                    String message = e.getMessage();
                    String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('Contact__c').getDescribe().getLabel();
                    System.assert(message.contains(errorMessage), 'message=' + message);
            }
        Test.stopTest(); 
    }

    @isTest static void ObtainBankAccount() {
        Resource__c investment = TestHelper.createInvestment(null, true);
        // Insert flat
        Resource__c flat = TestHelper.createResourceFlatApartment(              
            new Resource__c(
                Investment_Flat__c = investment.Id
            ), 
            true
        );

        Resource__c[] resources = new Resource__c[] {flat};
        Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);

        List<Sales_Process__c> productLines = new List<Sales_Process__c>();
        for(Resource__c res : resources) {
            productLines.add(TestHelper.createSalesProcessProductLine(
                new Sales_Process__c(
                    Product_Line_to_Proposal__c = proposal.Id,
                    Contact_from_Offer_Line__c = proposal.Contact__c,
                    Offer_Line_Resource__c = res.Id
                ),  
                false
            ));
        }
        insert productLines;

        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Offer__c = proposal.Id,
                Contact__c = proposal.Contact__c
            ), 
            true
        );      

        // Trigger part that does not allow to edit particular lookups (see CannotEditLookupsSalesProcess.cls description)
        Test.startTest();
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)
            ,
            Agreements_Installment__c = saleTerms.Id,
            Due_Date__c = Date.today(),
            Payment_For__c = NULL
            );
        insert newPayment;
        newPayment.Payment_For__c = productLines[0].Offer_Line_Resource__c;
        update newPayment;  

        // test regularPaymentsToGetBankAccount.add(newPayment);
    }
    @isTest static void changeSalesProcessReference() {

        Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);
        Sales_Process__c proposal2 = TestHelper.createSalesProcessProposal(null, true);
        Test.startTest();
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_COMMISION)
            ,
            Agreements_Comission__c = proposal.Id,
            Due_Date__c = Date.today()
            );
        insert newPayment;
        newPayment.Agreements_Comission__c = proposal2.Id;
        try {
            update newPayment;
        } catch(Exception e) {
             System.assert(String.valueOf(e).Contains(Label.cannotEditLookupsCommisionReward));
        }   
        Test.stopTest();
    }

}