/**
* @author       Krystian Bednarek
* @description  test class for Account Trigger
**/
@isTest
private class TestClassAccountTrigger{
    
    public static final String TEST_FIRST_NAME = 'testowy';
    public static final String TEST_PESEL      = '70062512052';
    public static final String TEST_LAST_NAME  = 'testowy';
    public static final String TEST_PHONE      = '949243871';
    public static final String TEST_EMAIL      = 'testuje@test.pl';
    public static final String TEST_MOBILE_PHONE = '66677766';
    public static final String DUPLICATED_MOBILE = '123456789';

    @isTest static void testAccountPhoneLongerThen9DigitsInsert() {

        Account testAccount = TestHelper.createAccountIndividualClient(NULL, false);
        testAccount.Mobile_Phone__c =  TEST_MOBILE_PHONE+'812';

        Test.startTest();

            insert testAccount;  
            DuplicateRuleForAccountAndContact__c duplicateRule = DuplicateRuleForAccountAndContact__c.getInstance('AccountRule');
            duplicateRule.Search_Over_Accounts__c = false;
            update duplicateRule;
            List<Account> accountPhone = [SELECT Mobile_Phone__C, Phone FROM Account WHERE (Id = :testAccount.Id)];  

        Test.stopTest();

        // (IF mobile phone is longer then 9 digits) swap Account.Phone with Account.Mobile_Phone__c
        //  Set Mobile_phone to null  
        System.assertEquals(null, accountPhone[0].Mobile_Phone__C);
        System.assertEquals(accountPhone[0].phone, testAccount.Mobile_Phone__c);
    }

    @isTest static void testAccountPhoneLongerThen9DigitsUpdate() {

        Account testAccount = TestHelper.createAccountIndividualClient(NULL, false);
        testAccount.Mobile_Phone__c =  null;

        Test.startTest();

            insert testAccount; 
            DuplicateRuleForAccountAndContact__c duplicateRule = DuplicateRuleForAccountAndContact__c.getInstance('AccountRule');
            duplicateRule.Search_Over_Accounts__c = false;
            update duplicateRule;
            testAccount.FirstName = TEST_FIRST_NAME;
            testAccount.PESEL__c  = TEST_PESEL;
            testAccount.Mobile_Phone__c =  TEST_MOBILE_PHONE+'812';
            update testAccount;
            List<Account> accountPhone = [SELECT Mobile_Phone__C, Phone FROM Account WHERE (Id = :testAccount.Id)];

        Test.stopTest();

        // (IF mobile phone is longer then 9 digits) swap Account.Phone with Account.Mobile_Phone__c
        //  Set Mobile_phone to null
        System.assertEquals(null, accountPhone[0].Mobile_Phone__C);
        System.assertEquals(accountPhone[0].phone, testAccount.Mobile_Phone__c);
    }

    @isTest static void testAccountSearchOverContacts() {

        Account testAccount = TestHelper.createAccountIndividualClient(NULL, true);      
        DuplicateRuleForAccountAndContact__c duplicateRule = DuplicateRuleForAccountAndContact__c.getInstance('AccountRule');   
        duplicateRule.Search_Over_Contacts__c = true;

        Test.startTest();

            update duplicateRule;       
            testAccount.FirstName = TEST_FIRST_NAME;
            testAccount.PESEL__c  = '70062512052';
            update testAccount;

        Test.stopTest();
    }

    @isTest static void testAccountAccountDuplicateNameEmail() {

        Account testAccount = TestHelper.createAccountIndividualClient(NULL, false);
        testAccount.FirstName = TEST_FIRST_NAME;
        testAccount.LastName  = TEST_LAST_NAME;
        testAccount.Email__c  = TEST_EMAIL;

        Test.startTest();

            insert  testAccount;
            Account testAccount2 = TestHelper.createAccountIndividualClient(NULL, true);
            testAccount2.FirstName = TEST_FIRST_NAME;    
            testAccount2.Email__c  = TEST_EMAIL;
        

            try {
                update testAccount2;
            } 
            catch (Exception e) {
                System.assert(String.valueOf(e).Contains(label.AccountDuplicateNameEmail));
            }

        Test.stopTest();
    }

    @isTest static void testAccountDuplicatePesel() {

        Account testAccount1 = new Account(PESEL__c  = TEST_PESEL);
        Account testAccount = TestHelper.createAccountIndividualClient(testAccount1, true);
        Account testAccount2 = TestHelper.createAccountIndividualClient(NULL, true);
        testAccount2.PESEL__c = TEST_PESEL;

        Test.startTest();

            try {
                update testAccount2;
            } 
            catch (Exception e) {
                System.assert(String.valueOf(e).Contains(Label.AccountDuplicatePesel));
            }

        Test.stopTest();
     }

    @isTest static void testAccountDuplicateNameMobilePhone() {

        Account testAccount = TestHelper.createAccountIndividualClient(NULL, true);
        testAccount.FirstName = TEST_FIRST_NAME;
        testAccount.LastName  = TEST_LAST_NAME;
        testAccount.Mobile_Phone__c  = TEST_PHONE;

        Test.startTest();

            update  testAccount;
            Account testAccount2 = TestHelper.createAccountIndividualClient(NULL, true);
            testAccount2.FirstName = TEST_FIRST_NAME;
            testAccount2.LastName  = TEST_LAST_NAME;
            testAccount2.Mobile_Phone__c  = TEST_PHONE;

            try {
                update testAccount2;
            } 
            catch (Exception e) {
                System.assert(String.valueOf(e).Contains(Label.AccountDuplicateNameMobilePhone));
            }

        Test.stopTest();
    }

    @isTest static void testAccountDuplicateNameEmail() {

        Account testAccount = TestHelper.createAccountIndividualClient(NULL, true);
        testAccount.FirstName = TEST_FIRST_NAME;
        testAccount.LastName  = TEST_LAST_NAME;
        testAccount.Email__c  = TEST_EMAIL;

        Test.startTest();

            update  testAccount;
            Account testAccount2 = TestHelper.createAccountIndividualClient(NULL, true);
            testAccount2.FirstName = TEST_FIRST_NAME;
            testAccount2.LastName  = TEST_LAST_NAME;
            testAccount2.Email__c  = TEST_EMAIL;

            try {
                update testAccount2;
            } 
            catch (Exception e) {
                System.assert(String.valueOf(e).Contains(Label.AccountDuplicateNameEmail));
            }

        Test.stopTest();


    }

    @isTest static void testAccountDuplicateNamePhone() {

        Account testAccount = TestHelper.createAccountIndividualClient(NULL, true);
        testAccount.FirstName = TEST_FIRST_NAME;
        testAccount.LastName  = TEST_LAST_NAME;
        testAccount.Phone     = TEST_PHONE;

        Test.startTest();

            update  testAccount;
            Account testAccount2 = TestHelper.createAccountIndividualClient(NULL, true);
            testAccount2.FirstName = TEST_FIRST_NAME;
            testAccount2.LastName  = TEST_LAST_NAME;
            testAccount2.Phone     = TEST_PHONE;
            
            try {
                update testAccount2;
            } 
            catch (Exception e) {
                System.assert(String.valueOf(e).Contains(Label.AccountDuplicateNamePhone));
            }

        Test.stopTest();
    }


    
    // Function used to set Search_Over_Contacts__c for duplicate contact testing   
    static void duplicateRuleSearchOverContacts() {

        DuplicateRuleForAccountAndContact__c duplicateRule = DuplicateRuleForAccountAndContact__c.getInstance('AccountRule');
        duplicateRule.Search_Over_Accounts__c = false;
        duplicateRule.Search_Over_Contacts__c = true;

        update duplicateRule;

    }

    @isTest static void testContactDuplicatePesel() { 

        Contact testContact  = TestHelper.createContactPerson(NULL, false);
        testContact.PESEL__c = TEST_PESEL;

        Test.startTest();

            insert testContact;
            Account testAccount = TestHelper.createAccountIndividualClient(NULL, true);  
            duplicateRuleSearchOverContacts();
            testAccount.PESEL__c = TEST_PESEL;

            
            try {
                update testAccount;
            } catch (Exception e) {
                System.assert(String.valueOf(e).Contains(Label.AccountDuplicatePesel));
            }

        Test.stopTest();
    }

    @isTest static void testContactDuplicateNameEmail() {  

        Contact testContact = TestHelper.createContactPerson(NULL, false);
        testContact.FirstName = TEST_FIRST_NAME;
        testContact.LastName  = TEST_LAST_NAME;
        testContact.Email     = TEST_EMAIL;

        Test.startTest();

            insert testContact;
            Account testAccount = TestHelper.createAccountIndividualClient(NULL, true);
            duplicateRuleSearchOverContacts();
            testAccount.FirstName = TEST_FIRST_NAME;
            testAccount.LastName  = TEST_LAST_NAME;
            testAccount.Email__c  = TEST_EMAIL;

            try {
                update testAccount;
            } catch (Exception e) {
                System.assert(String.valueOf(e).Contains(Label.AccountDuplicateNameEmail));
            }

        Test.stopTest();
    }

    @isTest static void testContactDuplicateNamePhone() {  

        Contact testContact = TestHelper.createContactPerson(NULL, false);
        testContact.FirstName = TEST_FIRST_NAME;
        testContact.LastName  = TEST_LAST_NAME;
        testContact.Phone     = TEST_PHONE;

        Test.startTest();

            insert testContact;
            Account testAccount = TestHelper.createAccountIndividualClient(NULL, true);
            duplicateRuleSearchOverContacts();
            testAccount.FirstName = TEST_FIRST_NAME;
            testAccount.LastName  = TEST_LAST_NAME;
            testAccount.Phone     = TEST_PHONE;
            
            try {
                update testAccount;
            } catch (Exception e) {
                System.assert(String.valueOf(e).Contains(Label.AccountDuplicateNamePhone));
            }

        Test.stopTest();
    }
    
    @isTest static void testContactDuplicateNameMobilePhone() {

        Contact testContact = TestHelper.createContactPerson(NULL, false);
        testContact.FirstName = TEST_FIRST_NAME;
        testContact.LastName  = TEST_LAST_NAME;
        testContact.MobilePhone = TEST_PHONE;

        Test.startTest();

            insert testContact;
            Account testAccount = TestHelper.createAccountIndividualClient(NULL, true); 
            duplicateRuleSearchOverContacts();
            testAccount.FirstName = TEST_FIRST_NAME;
            testAccount.LastName  = TEST_LAST_NAME;
            testAccount.Mobile_Phone__c = TEST_PHONE;
            
            try {
                update testAccount;
            } catch (Exception e) {
                System.assert(String.valueOf(e).Contains(Label.AccountDuplicateNameMobilePhone));
            }

        Test.stopTest();
    }
}