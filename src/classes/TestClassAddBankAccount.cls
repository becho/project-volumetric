/**
* @author 		Mateusz Pruszynski
* @description 	Test class for AddBankAccount
*/
@isTest
private class TestClassAddBankAccount{

	//static Map<Integer, Id> dataSetup(Boolean isBankAccount, String random4digit){
	//	Map<Integer, Id> idList = new Map<Integer, Id>();

	//	Contact customer = testHelper.createInddividualClient('Test_Client');
	//	idList.put(1, customer.Id);					//1
	//	Account investor = testHelper.createInvestor('Test_Investor');
	//	idList.put(2, investor.Id);					//2
	//	Resource__c building = testHelper.createBuilding('Test_Building');
	//	idList.put(3, building.Id);					//3
	//	Resource__c investment = testHelper.createInvestment('Test_Investment', investor);
	//	idList.put(4, investment.Id);				//4
	//	Resource__c property = testHelper.createFlat('Test_Flat', investment, building);
	//	idList.put(5, property.Id);					//5
	//	Listing__c listing = testHelper.createListingSale(property, 'Test_Listing', 76432);
	//	idList.put(6, listing.Id);					//6
	//	Sales_Process__c offer = testHelper.createOffer(customer);
	//	idList.put(7, offer.Id);					//7
	//	Sales_Process__c offerLine = testHelper.createOfferLine(offer, listing);
	//	idList.put(8, offerLine.Id);				//8
	//	if(isBankAccount == true){
	//		Extension__c bankAccount = testHelper.createBankAccount(investment, '15 '+random4digit+' 1910 1050 0000 1111 2222 4014', true);
	//		idList.put(9, bankAccount.Id);				//9	
	//	}
	//	Sales_Process__c preliminaryAgreement = testHelper.createPreliminaryAgreement(offer);
	//	idList.put(10, preliminaryAgreement.Id);		//10
		

	//	return idList;		
	//}

	//static testMethod void allDataInserted(){
	//	Map<Integer, Id> idList = dataSetup(true, '2222');
	//	Id bankAccountId = idList.get(9);
	//	Id preliminaryAgreementId = idList.get(10);
	//	Sales_Process__c preliminaryAgreement = [SELECT Id, Name FROM Sales_Process__c WHERE Id = :preliminaryAgreementId];

	//	Test.startTest();
	//	PageReference pageRef = Page.AddBankAccount;
	//	pageRef.getParameters().put('agreementId', preliminaryAgreementId);
	//	Test.setCurrentPage(pageRef);

	//	ApexPages.StandardController stdController = new ApexPages.standardController(preliminaryAgreement);
	//	AddBankAccount aba = new AddBankAccount(stdController);
	//	aba.selectedBankAccount = bankAccountId;
	//	PageReference reload = aba.reload();
	//	aba.save();
	//	test.stopTest();

	//	Sales_Process__c updatedPreliminaryAgreement = [SELECT Id, Bank_Account__c FROM Sales_Process__c WHERE Id = :preliminaryAgreementId];
	//	System.assertEquals(bankAccountId, updatedPreliminaryAgreement.Bank_Account__c);
	//	System.assertEquals(null, reload);
	//}

	//static testMethod void noBankAccount(){
	//	Map<Integer, Id> idList = dataSetup(false, null);
	//	Id preliminaryAgreementId = idList.get(10);
	//	Sales_Process__c preliminaryAgreement = [SELECT Id, Name FROM Sales_Process__c WHERE Id = :preliminaryAgreementId];

	//	Test.startTest();
	//	PageReference pageRef = Page.AddBankAccount;
	//	pageRef.getParameters().put('agreementId', preliminaryAgreementId);
	//	Test.setCurrentPage(pageRef);

	//	ApexPages.StandardController stdController = new ApexPages.standardController(preliminaryAgreement);
	//	AddBankAccount aba = new AddBankAccount(stdController);
	//	PageReference reload = aba.reload();
	//	test.stopTest();

	//	List<Apexpages.Message> msgs = ApexPages.getMessages();
	//	boolean b = false;
	//	for(Apexpages.Message msg:msgs){
	//	    if (msg.getDetail().contains(Label.noBankAccountWithSelectedInvestment)) b = true;
	//	}

 //  		System.assert(b);
	//}

	//static testMethod void oldBankAccount(){
	//	Map<Integer, Id> idList = dataSetup(true, '4598');
	//	Id oldBankAccountId = idList.get(9);
	//	Id preliminaryAgreementId = idList.get(10);
	//	Id investmentId = idList.get(4);
	//	Sales_Process__c preliminaryAgreement = [SELECT Id, Name FROM Sales_Process__c WHERE Id = :preliminaryAgreementId];
	//	preliminaryAgreement.Bank_Account__c = oldBankAccountId;
	//	update preliminaryAgreement;
	//	Resource__c investment = [SELECT Id FROM Resource__c WHERE Id = :investmentId];
	//	Extension__c newBankAccount = testHelper.createBankAccount(investment, '12 1010 1010 1050 0000 1124 2222 4013', true);

	//	Test.startTest();
	//	PageReference pageRef = Page.AddBankAccount;
	//	pageRef.getParameters().put('agreementId', preliminaryAgreementId);
	//	Test.setCurrentPage(pageRef);

	//	ApexPages.StandardController stdController = new ApexPages.standardController(preliminaryAgreement);
	//	AddBankAccount aba = new AddBankAccount(stdController);
	//	aba.selectedBankAccount = newBankAccount.Id;
	//	aba.save();
	//	test.stopTest();

	//	Extension__c oldBankAccountUpdated = [SELECT Id, Used__c FROM Extension__c WHERE Id = :oldBankAccountId];

	//	System.assertEquals(false, oldBankAccountUpdated.Used__c);
	//}
}