/**
* @author 		Mateusz Pruszynski
* @description 	This class is used to mass delete OfferLines records from within Offer Lines related list (for PROPERTO SALES profile).
*				This class is ran without sharing, since Sales Reps cannot delete records from Sales_Process__c.
*				deleteOfferLine.page + "Delete Offer Lines" list button
*/

public without sharing class deleteOfferLineController {

	public List<Sales_Process__c> selectedOfferLines {get; set;}
	public String retUrl {get; set;}
	public Boolean renderTable {get; set;}
	
	public deleteOfferLineController(ApexPages.StandardSetController stdSetController) {
		if(!test.isRunningTest()) {
            stdSetController.addFields(new String[] {'Name', 'Standard_of_Completion__c', 'Product_Line_to_Proposal__c', 'Offer_Line_to_After_sales_Service__c', 'Offer_Line_Resource__c', 'Offer_Line_to_Sale_Term__c', 'Offer_Line_Reservation_Expiration_Date__c', 'Product_Line_to_Proposal__c', 'Queue__c', 'Discount__c', 'Discount_Amount__c', 'Price_With_Discount__c', 'Offer_Line_Reservation_Date__c'});
    		selectedOfferLines = (List<Sales_Process__c>)stdSetController.getSelected();
		} else {
            selectedOfferLines = (List<Sales_Process__c>)stdSetController.getSelected();
            Set<Id> selectedOfferLinesIds = new Set<Id>();

            for(Sales_Process__c sp : selectedOfferLines) {
                selectedOfferLinesIds.add(sp.Id);
            }
            
            selectedOfferLines = [
                SELECT Name, Standard_of_Completion__c, Product_Line_to_Proposal__c, 
                       Offer_Line_to_After_sales_Service__c, Offer_Line_Resource__c, Offer_Line_to_Sale_Term__c, Offer_Line_Reservation_Expiration_Date__c,
                       Queue__c, Discount__c, Discount_Amount__c, Price_With_Discount__c, Offer_Line_Reservation_Date__c
                FROM Sales_Process__c
                WHERE Id =: selectedOfferLinesIds
            ];
        }

        retUrl = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL);
        String retu = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL).replace('/', '');
        Boolean spRetUrl = true;
        if(retu.containsIgnoreCase('null')) {
            if(!selectedOfferLines.isEmpty()) {
                retu = selectedOfferLines[0].Offer_Line_to_Sale_Term__c != null ? selectedOfferLines[0].Offer_Line_to_Sale_Term__c : selectedOfferLines[0].Product_Line_to_Proposal__c;
                retUrl = '/' + retu;
            }
        } else {
            if(retu.containsIgnoreCase(CommonUtility.URL_PARAM_NEWID)) {
                retu = retu.left(15);
            } 
            Id idRetU = retu;
            if(Schema.Resource__c.SObjectType == idRetU.getSobjectType()) {
                spRetUrl = false;
            }
        }    
        if(!spRetUrl) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, Label.cannotDeleteSPFromResource);
            ApexPages.addmessage(msg);
            renderTable = false;
        } else {   
    		// check wchich way a user choose to delete promotion(s) -> either 'delete' action or 'delete promotions' button
    		if(selectedOfferLines.isEmpty()) {
    			selectedOfferLines.add([SELECT Id, Standard_of_Completion__c, Offer_Line_Resource__c, Offer_Line_Reservation_Expiration_Date__c, Offer_Line_Reservation_Date__c,
    										   Product_Line_to_Proposal__c, Queue__c, Discount__c, Discount_Amount__c, Price_With_Discount__c, Offer_Line_to_After_sales_Service__c, Offer_Line_to_Sale_Term__c
    									FROM Sales_Process__c 
                                        WHERE Id = :ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ID)]);

                if(retu.containsIgnoreCase('null')) {
                    retu = selectedOfferLines[0].Offer_Line_to_Sale_Term__c != null ? selectedOfferLines[0].Offer_Line_to_Sale_Term__c : selectedOfferLines[0].Product_Line_to_Proposal__c;       
                    retUrl = '/' + retu;
                }
    		}
            if(selectedOfferLines[0].Offer_Line_to_After_sales_Service__c != null && selectedOfferLines[0].Offer_Line_to_After_sales_Service__c == retu) {
                // AFTER_SALES_SERVICE -> cannot delete any customer
                renderTable = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.YouCannotUseButton + ' ' + Label.AfterSalesServiceExists)); 
            } else if(selectedOfferLines[0].Offer_Line_to_Sale_Term__c != null && selectedOfferLines[0].Offer_Line_to_Sale_Term__c == retu) {
                // SALE_TERMS -> check if After-sales service exists;
                if(selectedOfferLines[0].Offer_Line_to_After_sales_Service__c != null) {
                    renderTable = false;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.YouCannotUseButton + ' ' + Label.AfterSalesServiceExists));
                } else {
                    // get sale terms record to check other conditions
                    Sales_Process__c saleTerms = [SELECT Id, Status__c, Date_of_signing__c 
                                                    FROM Sales_Process__c 
                                                    WHERE Id = :selectedOfferLines[0].Offer_Line_to_Sale_Term__c];
                    if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
                        renderTable = false;                        
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.SaleTermsAccepted)); // cannot edit sale terms accepted by manager
                    } else if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL) {
                        renderTable = false;                        
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.RecordIsWaitingForManager)); // cannot edit sale terms waiting for manager's approval                  
                    } else if(saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED && saleTerms.Date_of_signing__c != null) { 
                        renderTable = false;
                        apexpages.addmessage(new Apexpages.Message(ApexPages.Severity.Info, Label.YouCannotUseButton + ' ' + Label.SaleTermsAlreadySigned)); // agreement signed
                    } else { // OK
                        renderTable = true;
                    }
                }
            } else if(selectedOfferLines[0].Product_Line_to_Proposal__c != null && selectedOfferLines[0].Product_Line_to_Proposal__c == retu) {
                // PROPOSAL -> check if Sale Terms exists
                if(selectedOfferLines[0].Offer_Line_to_Sale_Term__c != null) {
                    renderTable = false;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.YouCannotUseButton + ' ' + Label.PraliminaryAgreementAlreadyExists)); // Sale terms already created
                } else { // OK
                    renderTable = true;
                }
            }	
        }	
	}

	public PageReference no(){
		return retUrl.contains('/') ? new PageReference(retUrl) : new PageReference('/' + retUrl);
	}

	public PageReference yes(){
		try{
			delete selectedOfferLines;
            return retUrl.contains('/') ? new PageReference(retUrl) : new PageReference('/' + retUrl);
		}
		catch(Exception e){
			renderTable = false;
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.YouCantEditOrDeleteOfferLine);
			apexpages.addmessage(msg);
			ErrorLogger.log(e);
			return null;		
		}
	}

}