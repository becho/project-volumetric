/**
* @author       Wojciech Słodziak
* @editor 		Przemysław Tustanowski
* @description  Manager class for various methods called from triggers. Mostly for update of SalesTarget caused by amount changes on offer/agreement
*/

public without sharing class ExtensionManager {

	//Marta Malinowska - add roll up for Change Elements
	public static void updateQuotationOnChanges(Set<Id> changeIds) {
		List<Extension__c> changesToUpdList = new List<Extension__c>();
		For(Extension__c change : [SELECT Id, Last_date_of_changed_change__c, Change_taken__c, Quotation__c, (SELECT Id, Value_of_change__c FROM Change_elements__r) FROM Extension__c WHERE Id IN :changeIds]){
			Decimal newQuotation = 0;
        	for(Extension__c changeElem : change.Change_elements__r){
        		if(changeElem.Value_of_change__c != null){
        			newQuotation += changeElem.Value_of_change__c;
        		}
        	}
        	if(change.Quotation__c != newQuotation && change.Change_taken__c){
        		change.Quotation__c= newQuotation;
        		change.Last_date_of_changed_change__c= Datetime.now();
        		changesToUpdList.add(change);
        	} else if (change.Quotation__c != newQuotation && !(change.Change_taken__c)) {
        		change.Quotation__c=newQuotation;
        		changesToUpdList.add(change);
        	} else if(change.Quotation__c ==newQuotation && change.Change_taken__c) {
        		change.Last_date_of_changed_change__c= Datetime.now();
        		changesToUpdList.add(change);
        	}
    	}
    	update changesToUpdList;
	}

	//Mateusz Wolak-Ksiazek
	public static void updateDateOfChangedChange(Set<Id> changeIds) {
		List<Extension__c> changesToUpdates = [
			SELECT Last_date_of_changed_change__c
			FROM Extension__c
			WHERE Id in: changeIds
		];

		List<Extension__c> listToUpdate = new List<Extension__c>();
		for(Extension__c change : changesToUpdates) {
			listToUpdate.add(new Extension__c(Id=change.Id, Last_date_of_changed_change__c = Datetime.now()));
		}
		if(!listToUpdate.isEmpty()) {
			update listToUpdate;
		}

	}
	//Mateusz Wolak-Ksiazek
	//update vat value on after sales (add/subtract quotation from changes)
	public static void updateValueOnAfterSales(Set<Id> afterSales) {
		List<Sales_Process__c> productLines = [
			SELECT Offer_Line_To_Sale_term__c, Offer_Line_to_After_sales_Service__c, 
				Offer_Line_To_Sale_term__r.Agreement_Value__c, Offer_Line_To_Sale_term__r.Net_Agreement_Price__c, Offer_Line_To_Sale_term__r.VAT_Amount__c
			FROM Sales_Process__c
			WHERE Offer_Line_to_After_sales_Service__c in: afterSales
			AND RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
		];

		List<Extension__c> changes = [
			SELECT Quotation__c, After_sales_Service__c, Status__c
			FROM Extension__c
			WHERE After_sales_Service__c =: afterSales
		];

		AreaAndVatPercentageDistribution__c currentDistributionChanges = AreaAndVatPercentageDistribution__c.getInstance('CurrentDistributionChanges');

		List<Sales_Process__c> procesesToUpdate = new List<Sales_Process__c>();
		for(Id key : afterSales) {
			Decimal valueToAdd = 0;
			Decimal netValue = 0;
			Decimal vatAmount = 0;
			for(Extension__c change : changes) {
				if(change.After_sales_Service__c == key
					&& (change.Status__c == CommonUtility.EXTENSION_STATUS_ACCEPTED
                    	|| change.Status__c == CommonUtility.EXTENSION_STATUS_COMPLETED
                    	|| change.Status__c == CommonUtility.EXTENSION_STATUS_IN_PROGRESS)
				) {
					if(change.Quotation__c != null && change.Quotation__c != 0) {
						valueToAdd += change.Quotation__c;
						netValue += (change.Quotation__c / (1 + (currentDistributionChanges.Low_Vat_Rate__c / 100))).setScale(2, RoundingMode.HALF_UP);
						vatAmount += (change.Quotation__c - (change.Quotation__c / (1 + (currentDistributionChanges.Low_Vat_Rate__c / 100))).setScale(2, RoundingMode.HALF_UP));
					}
				}
			}
			for(Sales_Process__c pl : productLines) {
				if(pl.Offer_Line_to_After_sales_Service__c == key) {
					valueToAdd += pl.Offer_Line_To_Sale_term__r.Agreement_Value__c != null ? pl.Offer_Line_To_Sale_term__r.Agreement_Value__c : 0;
					vatAmount += pl.Offer_Line_To_Sale_term__r.VAT_Amount__c != null ? pl.Offer_Line_To_Sale_term__r.VAT_Amount__c : 0;
					netValue += pl.Offer_Line_To_Sale_term__r.Net_Agreement_Price__c != null ? pl.Offer_Line_To_Sale_term__r.Net_Agreement_Price__c : 0; 
					
					procesesToUpdate.add(
						new Sales_Process__c(
							Id = key,
							Agreement_Value__c = valueToAdd,
							VAT_Amount__c = vatAmount,
							Net_Agreement_Price__c = netValue 
						)
					);
				}
				break;
			}
		}

		try {
			update procesesToUpdate;
		} catch(Exception e) {
			ErrorLogger.log(e);
    	}
	}

}