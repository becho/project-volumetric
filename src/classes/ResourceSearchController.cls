/**
* @author       Wojciech Słodziak
* @description  Controller for Resource Search 
*/


global without sharing class ResourceSearchController {

    private final static Integer UPD_TYPE_AMOUNT = 1;
    private final static Integer UPD_TYPE_PERCENT = 2;
    private final static Integer UPD_PTYPE_PRICE = 1;
    private final static Integer UPD_PTYPE_PPSQM = 2;

    public static Id FLAT_ID { get { return CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT); } }
    public static Id CP_ID { get { return CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY); } }
    public static Id HOUSE_ID { get { return CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_HOUSE); } }
    public static Id PS_ID { get { return CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE); } }
    public static Id STORAGE_ID { get { return CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE); } }

    public static String SIDE_NORTH { get { return CommonUtility.WORLDSIDE_NORTH; } }
    public static String SIDE_SOUTH { get { return CommonUtility.WORLDSIDE_SOUTH; } }
    public static String SIDE_EAST { get { return CommonUtility.WORLDSIDE_EAST; } }
    public static String SIDE_WEST { get { return CommonUtility.WORLDSIDE_WEST; } }

    public String retURL { get; set; }
    public String currencySymbol { get; set; }
    public String investmentListJSON { get; set; }
    public String recordTypeListJSON { get; set; }
    public String parkingSpaceTypeListJSON { get; set; }
    public String liningRoomWindowListJSON { get; set; }
    public String contactDataJSON { get; set; }
    public String requestDataJSON { get; set; }
    public String spDataJSON { get; set; }
    public String sliderLimitJSON { get; set; }
    public String columnPrefJSON { get; set; }
    /* Mateusz Pruszyński */
    // This object is used when new line is to be added to existing sales process
    public Boolean reserveOrOfferOnly { get; set; }

    public ResourceSearchController() {
        retURL = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL);
        currencySymbol = CommonUtility.getCurrencySymbolFromIso('PLN').unescapeHtml4();

        String contactId = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_CONTACTID);
        if (contactId != null) {
            List<Contact> contactList = [SELECT Id, Name, Email, Phone, MobilePhone, AccountId, Account.Name, Account.Email__c FROM Contact WHERE Id = :contactId];
            if (contactList.size() > 0) {
                contactDataJSON = JSON.serialize(contactList[0]);
                requestDataJSON = JSON.serialize(getRequestByContactId(contactList[0].Id));
            }
        }
        String requestId = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_REQUESTID);
        if (requestId != null) {
            requestDataJSON = JSON.serialize(getRequestById(requestId));
        }

        String spId = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_SPID);
        if (spId != null) {
            spDataJSON = JSON.serialize(getSalesProcessDetails(spId));
        } else {
            spDataJSON = JSON.serialize(getSalesProcessDetailsHelperObj(new SalesProcess_Wrapper())); /* Mateusz Pruszyński */ // helper variable for Offer_Line_Reservation_Expiration_Date__c init
        }

        String withoutReservation = Apexpages.currentPage().getParameters().get('withoutReservation');
        if(withoutReservation != null && withoutReservation == '1') {
            reserveOrOfferOnly = true;
        } else {
            reserveOrOfferOnly = false;
        }

        investmentListJSON = JSON.serialize(getInvestments());
        recordTypeListJSON = JSON.serialize(getRecordTypes());
        parkingSpaceTypeListJSON = JSON.serialize(getPicklistValues(Resource__c.Parking_Space_Type__c));
        liningRoomWindowListJSON = JSON.serialize(getPicklistValues(Resource__c.Saloon_Location__c));
        sliderLimitJSON = JSON.serialize(getSliderLimits());
    }

    private List<Resource__c> getInvestments() {
        List<Resource__c> investmentList = [SELECT Id, Name 
                                            FROM Resource__c 
                                            WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT) 
                                            AND Status__c = :CommonUtility.RESOURCE_STATUS_ACTIVE];

        return investmentList;
    }

    private List<RecordType_Wrapper> getRecordTypes() {
        List<String> rtDevNameList = new List<String>{ CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT, 
                                                       CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY, 
                                                       CommonUtility.RESOURCE_TYPE_HOUSE, 
                                                       CommonUtility.RESOURCE_TYPE_PARKING_SPACE, 
                                                       CommonUtility.RESOURCE_TYPE_STORAGE };

        List<RecordType> rtList = [SELECT Id, toLabel(Name) 
                                   FROM RecordType 
                                   WHERE sObjectType = :CommonUtility.SOBJECT_NAME_RESOURCE AND IsActive = true AND DeveloperName IN :rtDevNameList];
        Map<String, String> rtIdToNameMap = new Map<String, String>();
        for (RecordType rt : rtList) {
            rtIdToNameMap.put(rt.Id, rt.Name);
        }


        List<RecordType_Wrapper> rtwList = new List<RecordType_Wrapper>();
        for (String rtDN : rtDevNameList) {
            RecordType_Wrapper rtw = new RecordType_Wrapper();
            rtw.Id = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, rtDN);
            rtw.label = rtIdToNameMap.get(rtw.Id);

            rtwList.add(rtw);
        }

        columnPrefJSON = [SELECT ResSearchColumnPreference__c FROM User WHERE Id = :UserInfo.getUserId()].ResSearchColumnPreference__c;

        rtwList[0].checked = true;

        return rtwList;
    }

    private static List<PicklistValue_Wrapper> getPicklistValues(Schema.sObjectField field) {
        List<PicklistValue_Wrapper> options = new List<PicklistValue_Wrapper>();
        
        Schema.DescribeFieldResult fieldResult = field.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
        for(Schema.PicklistEntry f : ple)
        {
            PicklistValue_Wrapper pvw = new PicklistValue_Wrapper();
            pvw.value = f.getValue();
            pvw.label = f.getLabel();
            options.add(pvw);
        }       

        return options;
    }

    private static Map<String, String> getStatuses() {
        Map<String, String> statusesMap = new Map<String, String> ();
        
        Schema.DescribeFieldResult fieldResult = Resource__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
        for(Schema.PicklistEntry f : ple)
        {
            statusesMap.put(f.getValue(), f.getLabel());
        }       

        return statusesMap;
    }

    private Request__c getRequestByContactId(Id contacId) {
        List<Request__c> reqList = [SELECT Id, Air_Conditioning__c, Area_from__c, Area_to__c, Balcony__c, Entresol__c, Floor_from__c, Floor_to__c, Garage__c, 
                                    Garden__c, Number_of_rooms_from__c, Number_of_rooms_to__c, Price_from__c, Price_to__c, Saloon_Location__c, 
                                    Seperate_Kitchen__c, Terrace__c, Commercial_Property_Type__c, Flat_Apartment_Type__c, Parking_Space_Type__c, 
                                    Storage_Type__c, House_Type__c, Investment__c, Due_date__c, World_Side__c, Building__r.Name
                                    FROM Request__c 
                                    WHERE Contact__c = :contacId 
                                    ORDER BY CreatedDate DESC LIMIT 1];
        if (reqList.size() > 0) {
            return reqList[0];
        } else {
            return null;
        }
    }

    private Request__c getRequestById(Id requestId) {
        List<Request__c> reqList = [SELECT Id, Air_Conditioning__c, Area_from__c, Area_to__c, Balcony__c, Entresol__c, Floor_from__c, Floor_to__c, Garage__c, 
                                    Garden__c, Number_of_rooms_from__c, Number_of_rooms_to__c, Price_from__c, Price_to__c, Saloon_Location__c, Seperate_Kitchen__c, 
                                    Terrace__c, Commercial_Property_Type__c, Flat_Apartment_Type__c, Parking_Space_Type__c, Storage_Type__c, House_Type__c, 
                                    Investment__c, Due_date__c, World_Side__c, Building__r.Name
                                    FROM Request__c 
                                    WHERE Id = :requestId];
        if (reqList.size() > 0) {
            return reqList[0];
        } else {
            return null;
        }
    }

    private SalesProcess_Wrapper getSalesProcessDetails(String spId) {
        List<Sales_Process__c> spList = [SELECT Id, Name, toLabel(RecordType.Name), Contact__r.Name 
                                         FROM Sales_Process__c 
                                         WHERE Id = :spId];
        SalesProcess_Wrapper spw = new SalesProcess_Wrapper();
        if (spList.size() > 0) {


            spw.obj = spList[0];
            List<Sales_Process__c> lineList = [SELECT Id, Offer_Line_Resource__c
                                               FROM Sales_Process__c 
                                               WHERE Product_Line_to_Proposal__c = :spId OR Offer_Line_to_Sale_Term__c = :spId];
            List<Id> lineResIdList = new List<Id>();
            for (Sales_Process__c line : lineList) {
                lineResIdList.add(line.Offer_Line_Resource__c);
            }
            spw.lineResIdList = lineResIdList;
            //return spw;
        }

        return getSalesProcessDetailsHelperObj(spw);
    }

    private SalesProcess_Wrapper getSalesProcessDetailsHelperObj(SalesProcess_Wrapper spw) {
        // get default reservation date from custom settings      
        DefaultReservationDays2Add__c days2Add = DefaultReservationDays2Add__c.getInstance('currentConfig');        

        spw.helperObj = new Sales_Process__c(
            Offer_Line_Reservation_Expiration_Date__c = Date.today() + Integer.valueOf(days2Add.Number_Of_Days__c)
        );

        return spw;
    }

    public PageReference back() {
        if (retURL != null) {
            return new PageReference(retURL);
        }
        return null;
    }


    @RemoteAction
    public static SliderLimit_Wrapper getSliderLimits() {
        SliderLimit_Wrapper slw = new SliderLimit_Wrapper();


        List<Resource__c> mpList = [SELECT Price__c FROM Resource__c WHERE Price__c != null ORDER BY Price__c DESC NULLS LAST LIMIT 1];
        slw.maxPrice = !mpList.isEmpty()? Math.ceil(mpList[0].Price__c) : 100000;

        List<Resource__c> mppsqmList = [SELECT Price_Per_Square_Meter__c FROM Resource__c WHERE Price_Per_Square_Meter__c != null ORDER BY Price_Per_Square_Meter__c DESC NULLS LAST LIMIT 1];
        slw.maxPricePerSqMet = !mppsqmList.isEmpty()? Math.ceil(mppsqmList[0].Price_Per_Square_Meter__c) : 10000;

        List<Resource__c> mtaList = [SELECT Area__c FROM Resource__c WHERE Area__c != null ORDER BY Area__c DESC NULLS LAST LIMIT 1];
        slw.maxTotalArea = !mtaList.isEmpty()? Math.ceil(mtaList[0].Area__c) : 200;

        List<Resource__c> mRoomsList = [SELECT Number_of_Rooms__c FROM Resource__c WHERE Number_of_Rooms__c != null ORDER BY Number_of_Rooms__c DESC NULLS LAST LIMIT 1];
        slw.maxRooms = !mRoomsList.isEmpty()? Math.ceil(mRoomsList[0].Number_of_Rooms__c) : 5;

        List<Resource__c> mFloorList = [SELECT Floor__c FROM Resource__c WHERE Floor__c != null ORDER BY Floor__c DESC NULLS LAST LIMIT 1];
        slw.maxFloor = !mFloorList.isEmpty()? Math.ceil(mFloorList[0].Floor__c) : 5;

        return slw;
    }

    @RemoteAction @ReadOnly
    global static ResList_Wrapper getResources(Criteria_Wrapper criteria) {

        List<Resource_Wrapper> resourceList = new List<Resource_Wrapper>();

        ResList_Wrapper rlw = new ResList_Wrapper();
        rlw.timestamp = criteria.timestamp;

        List<String> activeStatues = new List<String> { CommonUtility.RESOURCE_STATUS_ACTIVE, CommonUtility.RESOURCE_STATUS_RESERVATION };

        String worldSide = getWorldSideString(criteria.worldSide);

        String query = 'SELECT ';
        query += 'Id, Name, RecordTypeId, toLabel(RecordType.Name), Status__c, Investment__c, Investment__r.Name, Completion_Date__c, Building__c, Building__r.Name, ';
        query += 'Price__c, Rent_Price__c, Price_Per_Square_Meter__c, Area__c, City__c, Number_of_Rooms__c, Floor__c, Resource_Commission_Rate__c ';
        query += 'FROM Resource__c ';

        String whereString = 'WHERE ';
        whereString += 'RecordTypeId IN ' + listToSOQLString(criteria.recordTypeIdList);

        if (criteria.properties.investment != null) {
            whereString += ' AND Investment__c =  \'' + criteria.properties.investment + '\'';
        }
        //Marta Malinowska: add Due Date
        if (criteria.properties.duedate != null) {
            Date dueDate = Date.valueOf(criteria.properties.duedate);
            whereString += ' AND Completion_Date__c <= :dueDate';
        }
        if (criteria.properties.active) {
            whereString += ' AND Status__c IN ' + listToSOQLString(activeStatues);
        }
        if (criteria.properties.withReservation) {
            whereString += ' AND Status__c = \'' + CommonUtility.RESOURCE_STATUS_RESERVATION + '\'';
        }
        if (criteria.properties.noReservation) {
            whereString += ' AND Status__c != \'' + CommonUtility.RESOURCE_STATUS_RESERVATION + '\'';
        }
        if (criteria.features.balcony) {
            whereString += ' AND Balcony__c = true';
        }
        if (criteria.features.garden) {
            whereString += ' AND Garden__c = true';
        }
        if (criteria.features.loggia) {
            whereString += ' AND Loggia__c = true';
        }
        if (criteria.features.airCond) {
            whereString += ' AND Air_Conditioning__c = true';
        }
        if (criteria.features.terrace) {
            whereString += ' AND Terrace__c = true';
        }
        if (criteria.features.entresol) {
            whereString += ' AND Entresol__c = true';
        }
        if (criteria.features.garage) {
            whereString += ' AND Garage__c = true';
        }
        if (criteria.features.sepKitchen) {
            whereString += ' AND Seperate_Kitchen__c = true';
        }
        if (criteria.features.parkingSpaceType != null) {
            whereString += ' AND (Parking_Space_Type__c = \'' + criteria.features.parkingSpaceType + '\' OR RecordTypeId != \'' 
                           + CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE) + '\')';
        }
        if (worldSide != null) {
            whereString += ' AND World_Side__c INCLUDES (\'' + worldSide + '\')';
        }
        if (criteria.features.livingRoomWindowLocation != null) {
            whereString += ' AND Saloon_Location__c =  \'' + criteria.features.livingRoomWindowLocation + '\'';
        }
        if (criteria.minPrice != null) {
            whereString += ' AND Price__c >= ' + criteria.minPrice;
        }
        if (criteria.maxPrice != null) {
            whereString += ' AND Price__c <= ' + criteria.maxPrice;
        }
        if (criteria.minPricePerSqMet != null) {
            whereString += ' AND ' + (criteria.minPricePerSqMet == 0? '(Price_Per_Square_Meter__c = null OR (' : '') + 'Price_Per_Square_Meter__c >= ' + criteria.minPricePerSqMet;
        }
        if (criteria.maxPricePerSqMet != null) {
            whereString += ' AND Price_Per_Square_Meter__c <= ' + criteria.maxPricePerSqMet + (criteria.minPricePerSqMet == 0? '))' : '');
        }
        if (criteria.minTotalArea != null) {
            whereString += ' AND ' + (criteria.minTotalArea == 0? '(Area__c = null OR (' : '') + 'Area__c >= ' + criteria.minTotalArea;
        }
        if (criteria.maxTotalArea != null) {
            whereString += ' AND Area__c <= ' + criteria.maxTotalArea + (criteria.minTotalArea == 0? '))' : '');
        }
        if (criteria.minRooms != null) {
            whereString += ' AND ' + (criteria.minRooms == 0? '(Number_of_Rooms__c = null OR ' : '') + '((Number_of_Rooms__c >= ' + criteria.minRooms;
        }
        if (criteria.maxRooms != null) {
            whereString += ' AND Number_of_Rooms__c <= ' + criteria.maxRooms + (criteria.minRooms == 0? ')))' : ') OR RecordTypeId != \'' 
                           + CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT) + '\')');
        }
        if (criteria.minFloor != null) {
            whereString += ' AND ' + (criteria.minFloor == 0? '(Floor__c = null OR (' : '') + 'Floor__c >= ' + criteria.minFloor;
        }
        if (criteria.maxFloor != null) {
            whereString += ' AND Floor__c <= ' + criteria.maxFloor + (criteria.minFloor == 0? '))' : '');
        }

        query += whereString;

        System.debug('query: ' + query);

        List<Resource__c> rList;
        try {
            rList = Database.query(query);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            return null;
        }
        
        List<Id> resourceIdList = new List<Id>();
        for (Resource__c r : rList) {
            resourceIdList.add(r.Id);
        }

        List<Metadata__c> metadataList = [SELECT Id, FileID__c, Metadata__c, RecordID__c 
                                          FROM Metadata__c 
                                          WHERE RecordID__c IN :resourceIdList AND Metadata_type__c = :CommonUtility.METADATA_TYPE_FILE_IMG];

        List<Sales_Process__c> reservationList = [SELECT Id, Offer_Line_Resource__c, Product_Line_to_Proposal__c, Product_Line_to_Proposal__r.Contact__r.Name,
                                                  Product_Line_to_Proposal__r.Contact__r.Email, Product_Line_to_Proposal__r.Contact__r.Account.Email__c, 
                                                  Product_Line_to_Proposal__r.Contact__r.Phone, Product_Line_to_Proposal__r.Contact__r.MobilePhone, Owner.Alias, Queue__c 
                                                  FROM Sales_Process__c 
                                                  WHERE Offer_Line_Resource__c IN :resourceIdList AND Queue__c != null];
        

        Map<String, String> statuses = getStatuses();

        for (Resource__c r : rList) {
            Resource_Wrapper rw = new Resource_Wrapper();
            rw.obj = r;

            rw.statusLabel = statuses.get(r.Status__c);

            for (Metadata__c met : metadataList){
                if (met.RecordID__c == r.Id){
                    if (CommonUtility.getMetadataValue(met.Metadata__c, CommonUtility.METADATA_TYPE_MAIN) == 'true') {
                        String url = CommonUtility.getMetadataValue(met.Metadata__c, CommonUtility.METADATA_METADATA_URL_URL);
                        rw.imgUrl = url.escapeHtml4();
                    }
                }
            }

            Integer resrvCount = 0;
            List<Sales_Process__c> subReservationList = new List<Sales_Process__c>();
            for (Sales_Process__c reservation : reservationList) {
                if (reservation.Offer_Line_Resource__c == r.Id) {
                    resrvCount++;
                    if (reservation.Queue__c == 1) {
                        rw.mainResrv = reservation;
                    }
                    subReservationList.add(reservation);
                }
            }
            rw.resrvCount = resrvCount;
            rw.resrvList = subReservationList;

            resourceList.add(rw);
        }

        rlw.resList = resourceList;
        return rlw;
    }

    @RemoteAction
    global static List<Contact> getContacts(String query) {
        query = String.escapeSingleQuotes(query);

        List<String> splitQueries = query.split(' ');
        if (splitQueries.isEmpty()) {
            splitQueries.add(query);
        }

        String soqlQuery = 'SELECT Id, Name, Email, Account.Email__c, Phone, MobilePhone, AccountId, Account.Name FROM Contact ';
        
        String whereString = 'WHERE';
        for (Integer i = 0; i < splitQueries.size(); i++) {
             whereString += (i != 0? ' AND ': ' ') + '(FirstName LIKE \'%' + splitQueries[i] + '%\' OR LastName LIKE \'%' + splitQueries[i] + '%\')';
        }
        whereString += + ' LIMIT 5';

        soqlQuery += whereString ;

        try {
            return Database.query(soqlQuery);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            return null;
        } 
    }

    /* Mateusz Pruszynski */
    @RemoteAction
    global static List<Manager_Panel__c> getPromotions(String query) {
        query = String.escapeSingleQuotes(query);

        List<String> splitQueries = query.split(' ');
        if (splitQueries.isEmpty()) {
            splitQueries.add(query);
        }

        String soqlQuery = 'SELECT Id, Name, toLabel(Discount_Kind__c), Discount_Value__c FROM Manager_Panel__c ';
        
        String whereString = 'WHERE';
        for (Integer i = 0; i < splitQueries.size(); i++) {
             whereString += (i != 0? ' AND ': ' ') + '(Name LIKE \'%' + splitQueries[i] + '%\')';
        }
        whereString += ' AND RecordTypeId = ' + '\'' + CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_PROMOTION) + '\'';
        whereString += + ' LIMIT 5';

        soqlQuery += whereString ;

        try {
            return Database.query(soqlQuery);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            return null;
        } 
    }    

    @RemoteAction
    global static Boolean addToFav(List<Id> resourceIdList, Id contactId) {
        return AddFavourite.massAddToFavorite(resourceIdList, contactId);
    }

    @RemoteAction
    global static ResourceReservation.DetailsResponse createReservation(List<Id> resourceIdList, Id contactId, String reservationExpirationDate) {
        return ResourceReservation.massResourceReservationWithDetails(resourceIdList, contactId, reservationExpirationDate);
    }

    @RemoteAction
    global static void updateColumnPreference(String preferenceJSON) {
        User u = new User();
        u.Id = UserInfo.getUserId();
        u.ResSearchColumnPreference__c = preferenceJSON;

        update u;
    }

    @RemoteAction
    global static Boolean updatePrice(PriceUpdate_Wrapper priceUpdateJSON) {
        List<Resource__c> resList = [SELECT Price__c, Price_Per_Square_Meter__c, Area__c FROM Resource__c WHERE Id IN :priceUpdateJSON.resourceIdList];
        for (Resource__c res : resList) {
            if (priceUpdateJSON.updateType == UPD_TYPE_AMOUNT) {
                if (priceUpdateJSON.priceToUpdate == UPD_PTYPE_PRICE) {
                    res.Price__c += priceUpdateJSON.updateValue.setScale(2);
                } else {
                    res.Price__c += (res.Area__c * priceUpdateJSON.updateValue).setScale(2);
                }
            } else {
                res.Price__c = (res.Price__c * (1 + priceUpdateJSON.updateValue / 100)).setScale(2);
            }
        }

        try {
            update resList;
            return true;
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            return false;
        } 
    }

    /* Mateusz Pruszynski */
    // method to assign defined promotion for bulk of resources
    @RemoteAction 
    global static Boolean massAssignDefinedPromotion(Id promoId, List<Id> resourceIdList) {

        String forbiddenResources = '';
        String resourcesPassed = '';

        List<Resource__c> rightResources = new List<Resource__c>();

        List<Resource__c> resourcesSelected = [
            SELECT Id, Name, Status__c, Price__c
            FROM Resource__c
            WHERE Id in :resourceIdList
        ];

        Integer forbiddenCount = 0;
        Integer passedCount = 0;

        for(Resource__c rSel : resourcesSelected) {

            if(rSel.Status__c == CommonUtility.RESOURCE_STATUS_SOLD_RESERVATION_AGREEMENT
                || rSel.Status__c == CommonUtility.RESOURCE_STATUS_SOLD_TRANSFERED
                || rSel.Status__c == CommonUtility.RESOURCE_STATUS_SOLD_FINAL_AGR
                || rSel.Status__c == CommonUtility.RESOURCE_STATUS_SOLD
            ) {

                forbiddenResources += '<tr><td><a href="/' + rSel.Id + '">' + rSel.Name + '</a><p> - ' + Label.CannotChangeOrAddPromoForSoldRes + '</p></td></tr>';
                forbiddenCount++;

            } else {

                if(rSel.Price__c > 0) {

                    resourcesPassed += '<tr><td><a href="/' + rSel.Id + '">' + rSel.Name + '</a><br /></td></tr>';
                    rSel.Defined_Promotion__c = promoId;
                    rightResources.add(rSel);
                    passedCount++;

                } else {

                    forbiddenResources += '<tr><td><a href="/' + rSel.Id + '">' + rSel.Name + '</a><p> - ' + Label.PleaseProvideResourcePrice + '</p></td></tr>';
                    forbiddenCount++;

                }

            }

        }

        if(forbiddenCount > passedCount) {

            for(Integer i = 0; i <= forbiddenCount; i++) {

                resourcesPassed += '<tr><td> </td></tr>';

            }

        } else if(forbiddenCount < passedCount) {

            for(Integer i = 0; i <= passedCount; i++) {

                forbiddenResources += '<tr><td> </td></tr>';

            }

        }

        try {
            update rightResources;
            return ManageDefinedPromotions.sendEmailWithPromoResult(resourcesPassed, forbiddenResources, UserInfo.getUserName(), UserInfo.getUserId(), promoId);
        } catch(Exception e) {
            ErrorLogger.log(e);
            return false;
        }

    }

    @RemoteAction
    global static Boolean addLine(Id spId, List<Id> resIdList, Boolean withoutReservation, String reservationExpirationDate) {

        List<String> reservationDateTimeSplitted = ((reservationExpirationDate.left(10).replaceAll(' ','\\.')).replaceAll('-','\\.')).split('\\.');

        Sales_Process__c mainSP = [SELECT Id, RecordTypeId, Offer__c FROM Sales_Process__c WHERE ID = :spId];
        Map<Id, String> proposalIdsMapWithStatus = new Map<Id, String>();
        List<Resource__c> resList = [SELECT Id, Price__c, Total_Area_Planned__c, Usable_Area_Planned__c FROM Resource__c WHERE Id IN :resIdList];
        List<Sales_Process__c> linesToInsert = new List<Sales_Process__c>();
        Map<Id, String> resourceIdsMapWithStatus = new Map<Id, String>();
        for(Resource__c res : resList) {
            Sales_Process__c line = new Sales_Process__c();
            line.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE); 
            if (mainSP.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)) {
                line.Product_Line_to_Proposal__c = spId;
                proposalIdsMapWithStatus.put(mainSP.Id, CommonUtility.SALES_PROCESS_RESERVATION_STATUS_RESERVED);
            } 
            if (mainSP.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)) {
                line.Offer_Line_to_Sale_Term__c = spId;
                line.Product_Line_to_Proposal__c = mainSP.Offer__c;
            }           
            line.Agreement_Value__c = res.Price__c;
            line.Discount__c = 0;
            line.Discount_Amount_Offer__c = 0;
            line.Price_With_Discount__c = res.Price__c;
            line.Offer_Line_Resource__c = res.Id;
            Line.Resource_Area__c = res.Total_Area_Planned__c != null ?  res.Usable_Area_Planned__c : res.Total_Area_Planned__c;
            if(withoutReservation) {
                line.Without_Reservation__c = true;
            } else {
                line.Offer_Line_Reservation_Date__c = System.now(); 
                line.Without_Reservation__c = false;               
                resourceIdsMapWithStatus.put(res.Id, CommonUtility.RESOURCE_STATUS_RESERVATION);
                Line.Offer_Line_Reservation_Expiration_Date__c = Date.newInstance(
                    Integer.valueOf(reservationDateTimeSplitted[0]), 
                    Integer.valueOf(reservationDateTimeSplitted[1]), 
                    Integer.valueOf(reservationDateTimeSplitted[2])
                );      
            }

            linesToInsert.add(line);
        }
        Savepoint sp = Database.setSavepoint();
        try{
            if(withoutReservation) {    
                insert linesToInsert;
           } else {
                ResourceReservation.updateQueue(linesToInsert, resIdList);
                ResourceReservation.updateResourceStatuse(resourceIdsMapWithStatus); 
           }
           return true;
        } catch(Exception e) {
            Database.rollback(sp);
            ErrorLogger.log(e);
        }
        return false;
    }

    /* Mateusz Pruszynski */
    // Create Offer without reservation
    @RemoteAction
    global static ResourceOffering.DetailsResponse createOffer(List<Id> resourceIdList, Id contactId) {
        return ResourceOffering.massCreateOfferWithDetails(resourceIdList, contactId);
    }

   @RemoteAction 
    global static Wrapper createClient(String clientFirstName, String clientLastName, String clientPhone, String clientMobilePhone, String clientEmail, String description ) {

      Wrapper resultToReturn = new Wrapper(new Contact(),new List<String>());

        Account client = new Account(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_ACCOUNT, CommonUtility.CONTACT_TYPE_INDIVIDUAL),
            FirstName =  clientFirstName,
            LastName = clientLastName,
            Phone = clientPhone,
            Mobile_Phone__c = clientMobilePhone,
            Email__c = clientEmail,
            Description = description
        );
               
        try {
            insert client;
            Contact con = [SELECT Id, Name, Email, Account.Email__c, Phone, MobilePhone, AccountId, Account.Name 
                                    FROM Contact WHERE accountId = :client.Id LIMIT 1];
            resultToReturn.contact = con;
            resultToReturn.errors = new String[] {'success'};


        } catch(Exception e) {
         resultToReturn.errors = new String[] {'error', String.valueOf(e.getMessage())};
        }

        return resultToReturn;
    }
    global class Wrapper{

      public Contact contact {get; set;}
      public String[] errors {get; set;}

      public Wrapper(Contact c, List<String> errors){
        this.contact = c;
        this.errors = errors;
      }
     }
     
    // Krystian Bednarek
    // class to check if current profile is Properto Manager or system admin PS-177
    @RemoteAction 
    global static Boolean IsProfileManger() {
    
        Id currentProfileId = userinfo.getProfileId();
        Id sysAdminId = CommonUtility.getProfileId(Label.SystemAdministrator);
        Id propertoManagerId = CommonUtility.getProfileId(CommonUtility.PROFILE_PROPERTO_MANAGER_NAME); // zmienne w common util
        Id systemAdminId = CommonUtility.getProfileId(CommonUtility.PROFILE_SYSTEM_ADMINISTRATOR);

        if(currentProfileId == sysAdminId || currentProfileId ==  propertoManagerId || currentProfileId == systemAdminId) {
            system.debug('true');
            return true;   
        } 
        else {
            system.debug('false');
            return false; 
        }

    }

    //helpers
    private static String listToSOQLString(List<String> stringList) {
        if (stringList == null) return null;

        String soqlString = '(\'\','; 
        for (String s : stringList) {
            soqlString += '\'' + s + '\',';
        }
        soqlString = soqlString.substring(0, soqlString.length()-1);
        soqlString += ')';

        return soqlString;
    }

    private static String getWorldSideString(WorldSide_Wrapper worldSide) {
        String result = '';

        if (worldSide.north) {
            result += CommonUtility.WORLDSIDE_NORTH + ';';
        }
        if (worldSide.south) {
            result += CommonUtility.WORLDSIDE_SOUTH + ';';
        }
        if (worldSide.west) {
            result += CommonUtility.WORLDSIDE_WEST + ';';
        }
        if (worldSide.east) {
            result += CommonUtility.WORLDSIDE_EAST + ';';
        }

        if (result == '') {
            return null;
        }
        return result;
    }


    //wrappers
    global class ResList_Wrapper {
        @testVisible List<Resource_Wrapper> resList;
        @testVisible Long timestamp;
    }
    
    global class Resource_Wrapper {
        @testVisible Resource__c obj;
        @testVisible String statusLabel;
        @testVisible Sales_Process__c mainResrv;
        @testVisible Integer resrvCount;
        @testVisible List<Sales_Process__c> resrvList;
        @testVisible String imgUrl;
    }
    
    global class Criteria_Wrapper {
        @testVisible Long timestamp;
        @testVisible Integer minPrice;
        @testVisible Integer maxPrice;
        @testVisible Integer minPricePerSqMet;
        @testVisible Integer maxPricePerSqMet;
        @testVisible Integer minTotalArea;
        @testVisible Integer maxTotalArea;
        @testVisible Integer minRooms;
        @testVisible Integer maxRooms;
        @testVisible Integer minFloor;
        @testVisible Integer maxFloor;
        @testVisible Properties_Wrapper properties;
        @testVisible Features_Wrapper features;
        @testVisible WorldSide_Wrapper worldSide;
        @testVisible List<Id> recordTypeIdList;
    }
    
    global class Properties_Wrapper {
        @testVisible Boolean active;
        @testVisible Boolean noReservation;
        @testVisible Boolean withReservation;
        @testVisible Id investment;
        @testVisible String duedate; //Marta Malinowska: add Due Date
    }
    
    global class Features_Wrapper {
        @testVisible Boolean balcony;
        @testVisible Boolean garden;
        @testVisible Boolean loggia;
        @testVisible Boolean airCond;
        @testVisible Boolean terrace;
        @testVisible Boolean entresol;
        @testVisible Boolean garage;
        @testVisible Boolean sepKitchen;
        @testVisible String parkingSpaceType;
        @testVisible String livingRoomWindowLocation;
    }
    
    global class WorldSide_Wrapper {
        @testVisible Boolean north;
        @testVisible Boolean south;
        @testVisible Boolean west;
        @testVisible Boolean east;
    }
    
    global class SliderLimit_Wrapper {
        @testVisible Decimal maxPrice;
        @testVisible Decimal maxPricePerSqMet;
        @testVisible Decimal maxTotalArea;
        @testVisible Decimal maxRooms;
        @testVisible Decimal maxFloor;
    }
    
    global class RecordType_Wrapper {
        @testVisible String Id;
        @testVisible String label;
        @testVisible Boolean checked;
    }
    
    global class PicklistValue_Wrapper {
        @testVisible String value;
        @testVisible String label;
    }
    
    global class PriceUpdate_Wrapper {
        @testVisible List<Id> resourceIdList;
        @testVisible Decimal updateValue;
        @testVisible Integer updateType;
        @testVisible Integer priceToUpdate;
    }
    
    global class SalesProcess_Wrapper {
        @testVisible Sales_Process__c obj;
        @testVisible List<Id> lineResIdList;
        @testVisible Sales_Process__c helperObj; /* Mateusz Pruszyński */ // helper variable for Offer_Line_Reservation_Expiration_Date__c init
    }
}