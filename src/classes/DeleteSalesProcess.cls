/**
* @author 		Mateusz Pruszyński
* @description 	Class used to check if a user can delete a particular record (namely: Offer or Offer Line). If there is an agreement related to the offer, it cannot be deleted.
*/

public with sharing class DeleteSalesProcess {

	public Sales_Process__c process {get; set;}
	public Boolean disable {get; set;}
	public String retURL {get; set;}
	public Id currentUserId {get; set;}

	// Lists to be deleted
	public List<Sales_Process__c> relatedLists2delete {get; set;}
	public List<Payment__c> relatedPayments2delete {get; set;}
	public List<Event> events2delete {get; set;}
	public List<Task> tasks2delete {get; set;}
	public List<Attachment> attachments2delete {get; set;}

	public DeleteSalesProcess(ApexPages.StandardController stdController){
		if(!test.isRunningTest()) {
			String[] fields = getFields();	
			stdController.addFields(fields); 
			process = (Sales_Process__c)stdController.getRecord();
			}else{
			Id recId = stdController.getRecord().Id;
			Sales_Process__c res = [SELECT Id, RecordTypeId, Status__c, OwnerId, Proposal_from_Customer_Group__c, Proposal_from_Promotions__c, Date_of_signing__c,
					Product_Line_to_Proposal__c, After_sales_from_Customer_Group__c, Developer_Agreement_from_Customer_Group__c,
					Sale_Terms_from_Promotions__c, Offer_Line_to_Sale_Term__c
            FROM Sales_Process__c
            WHERE Id =: recId LIMIT 1];
			process = res;
			}
			currentUserId = UserInfo.getUserId();
			retURL = Sales_Process__c.sObjectType.getDescribe().getKeyPrefix();
			initialize();
		
	}

	public String[] getFields() {
		String[] result = new String[] {
			'RecordTypeId',
			'Status__c',
			'OwnerId',
			'Proposal_from_Customer_Group__c',
			'Proposal_from_Promotions__c',
			'Date_of_signing__c',
			'Product_Line_to_Proposal__c',
			'After_sales_from_Customer_Group__c',
			'Developer_Agreement_from_Customer_Group__c',
			'Sale_Terms_from_Promotions__c',
			'Offer_Line_to_Sale_Term__c'			
		};
		return result;
	}	
	
	// Initialize lists and variables
	public void initialize() {
		disable = true;
		relatedLists2delete = new List<Sales_Process__c>();
		relatedPayments2delete = new List<Payment__c>();
		events2delete = new List<Event>();
		tasks2delete = new List<Task>();
		attachments2delete = new List<Attachment>();
	}

	// Checks for record type of current record to proceed to check other conditions
	public PageReference redirectOrBlockDeletion(){
		PageReference result;
		if(process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)) { // PROPOSAL
			if(currentUserId == process.OwnerId) { // A USER MUST BE THE OWNER OF THE RECORD
				Boolean saleTermsNotCreated = CommonUtility.allowNextStepSalesProcess(process.RecordTypeId, process.Id, CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT));
				if(saleTermsNotCreated) {
					prepareProposalDeletion();
					disable = false;
					result = null;
				} else {
					/**
					* @sale_terms_created
					**/
	                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, Label.PraliminaryAgreementAlreadyExists);
	                apexpages.addmessage(msg); 					
					result = null;
				}
			} else {
				youAreNotTheOwner(); 								
				result = null;
			}
		} else if(process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)) { // SALE TERMS
		system.debug('currentUserId'+currentUserId); system.debug('process.OwnerId'+process.OwnerId);	if(currentUserId == process.OwnerId) { // A USER MUST BE THE OWNER OF THE RECORD
				Boolean afterSalesServiceNotCreated = CommonUtility.allowNextStepSalesProcess(process.RecordTypeId, process.Id, CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL));
				if(afterSalesServiceNotCreated) {
					if(process.Status__c != CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL) {
						if(process.Date_of_signing__c == null) {
							prepareSaleTermsDeletion();
							disable = false;
							result = null;
						} else {
							/**
							* @developer_agreement_signed
							**/
			                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, Label.SaleTermsAlreadySigned);
			                apexpages.addmessage(msg); 					
							result = null;							
						}
					} else {
						/**
						* @pending_approval_process
						**/
		                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, Label.PendingApproval);
		                apexpages.addmessage(msg); 					
						result = null;						
					}
				} else {
					/**
					* @after-sales_service_created
					**/
	                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, Label.AfterSalesServiceExists);
	                apexpages.addmessage(msg); 					
					result = null;					
				}
			} else {
				youAreNotTheOwner(); 								
				result = null;
			}				
		} else if(process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)) { // AFTER-SALES SERVICE
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, Label.YuoCannotDeleteAfterSales);
            apexpages.addmessage(msg); 					
			result = null;		
		} else if(process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)) { // FINAL AGREEMENT SERVICE
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, Label.YouCannodDeleteFinal);
            apexpages.addmessage(msg); 					
			result = null;		
		} else if(process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)) { // CUSTOMERS
			result = page.DeleteCustomers;
			result.getParameters().put(CommonUtility.URL_PARAM_WRAPMASSACTION, CommonUtility.ONE_PART1);
			result.getParameters().put(CommonUtility.URL_PARAM_SCONTROLCACHING, CommonUtility.ONE_PART1);
			if(ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL) != null && ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL) != ('/' + WebserviceUtilities.getPrefix(CommonUtility.SOBJECT_NAME_SALESPROCESS) + '/o')) {	
				result.getParameters().put(CommonUtility.URL_PARAM_RETURL, ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL));
			} else {
				if(process.After_sales_from_Customer_Group__c != null) {
					result.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/' + process.After_sales_from_Customer_Group__c);
				} else if(process.Developer_Agreement_from_Customer_Group__c != null) {
					result.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/' + process.Developer_Agreement_from_Customer_Group__c);
				} else {
					result.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/' + process.Proposal_from_Customer_Group__c);
				}
			}
			result.getParameters().put(CommonUtility.URL_PARAM_ID, process.Id);
		} else if(process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION) 
			|| process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_DEFINED_PROMOTION)) { // PROMOTIONS
			result = page.DeletePromotions;
			result.getParameters().put(CommonUtility.URL_PARAM_WRAPMASSACTION, CommonUtility.ONE_PART1);
			result.getParameters().put(CommonUtility.URL_PARAM_SCONTROLCACHING, CommonUtility.ONE_PART1);
			if(ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL) != null && ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL) != ('/' + WebserviceUtilities.getPrefix(CommonUtility.SOBJECT_NAME_SALESPROCESS) + '/o'))  {	
				result.getParameters().put(CommonUtility.URL_PARAM_RETURL, ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL));
			} else {
				if(process.After_sales_from_Customer_Group__c != null) {
					result.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/' + process.Sale_Terms_from_Promotions__c);
				} else {
					result.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/' + process.Proposal_from_Promotions__c);
				}
			}
			result.getParameters().put(CommonUtility.URL_PARAM_ID, process.Id);
		} else if(process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)) { // PRODUCT LINES
			result = page.deleteOfferLine;
			result.getParameters().put(CommonUtility.URL_PARAM_WRAPMASSACTION, CommonUtility.ONE_PART1);
			result.getParameters().put(CommonUtility.URL_PARAM_SCONTROLCACHING, CommonUtility.ONE_PART1);
			if(ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL) != null && ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL) != ('/' + WebserviceUtilities.getPrefix(CommonUtility.SOBJECT_NAME_SALESPROCESS) + '/o'))  {	
				result.getParameters().put(CommonUtility.URL_PARAM_RETURL, ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL));
			} else {
				if(process.After_sales_from_Customer_Group__c != null) {
					result.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/' + process.Product_Line_to_Proposal__c);
				} else {
					result.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/' + process.Offer_Line_to_Sale_Term__c);
				}
			}
			result.getParameters().put(CommonUtility.URL_PARAM_ID, process.Id);			
		} else { // OTHER (favourites and sth else?)
			disable = false;
			result = null;
		}
		return result;
		//return null;
	}

	public void prepareProposalDeletion() {
		// get all sales process related lists (customers, lines, promotions)
		List<Sales_Process__c> relatedLists = [SELECT Id FROM Sales_Process__c 
												WHERE (Product_Line_to_Proposal__c = :process.Id 
													AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
												) OR (Proposal_from_Customer_Group__c = :process.Id 
													AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)
												) OR (Proposal_from_Promotions__c = :process.Id 
													AND (
														RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_DEFINED_PROMOTION) 
														OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION)
													)
												)];
		if(!relatedLists.isEmpty()) {
			relatedLists2delete.addAll(relatedLists);
		}
		prepareOtherDeletion();
	}

	public void prepareSaleTermsDeletion() {
		// get all sales process related lists (customers, lines, promotions)
		List<Sales_Process__c> relatedLists = [SELECT Id FROM Sales_Process__c 
												WHERE (Offer_Line_to_Sale_Term__c = :process.Id 
													AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
												) OR (Developer_Agreement_from_Customer_Group__c = :process.Id 
													AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)
												) OR (Sale_Terms_from_Promotions__c = :process.Id 
													AND (
														RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_DEFINED_PROMOTION) 
														OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION)
													)
												)];
		if(!relatedLists.isEmpty()) {
			relatedLists2delete.addAll(relatedLists);
		}		
		List<Payment__c> payments = [SELECT Id FROM Payment__c WHERE Agreements_Installment__c = :process.Id];
		if(!payments.isEmpty()) {
			relatedPayments2delete.addAll(payments);
		}
		prepareOtherDeletion();
	}

	public void prepareOtherDeletion() {
		// get tasks
		List<Task> tasks = [SELECT Id FROM Task WHERE whatId = :process.Id];
		if(!tasks.isEmpty()) {
			tasks2delete.addAll(tasks);
		}
		// get events
		List<Event> events = [SELECT Id FROM Event WHERE whatId = :process.Id];
		if(!events.isEmpty()) {
			events2delete.addAll(events);
		}		
		// get attachments
		List<Attachment> attachments = [SELECT Id FROM Attachment WHERE ParentId = :process.Id];
		if(!attachments.isEmpty()) {
			attachments2delete.addAll(attachments);
		}
	}

	/**
	* @you_are_not_the_record_owner_error_msg
	**/
	public void youAreNotTheOwner() {
        apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, Label.YouAreNotTheOwner);
        apexpages.addmessage(msg);
    }

	// confirm and delete
	public PageReference deleteOffer(){
		Savepoint sp = Database.setSavepoint();
		try{
			if(!relatedLists2delete.isEmpty()) {
				delete relatedLists2delete;
			}
			if(!relatedPayments2delete.isEmpty()) {
				delete relatedPayments2delete;
			}
			if(!events2delete.isEmpty()) {
				delete events2delete;
			}
			if(!tasks2delete.isEmpty()) {
				delete tasks2delete;
			}
			if(!attachments2delete.isEmpty()) {
				delete attachments2delete;
			}												
			delete process;		
		}
		catch(Exception e){
			Database.rollback(sp);
			ErrorLogger.log(e);
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.YouCantEditOrDeleteOfferLine);
			apexpages.addmessage(msg);
			return null;
		}
		return new PageReference('/' + retURL);
	}

	// "back" method
	public PageReference cancel(){
		return new PageReference('/' + process.Id);
	}

}