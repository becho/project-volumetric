/**
* @author      Krystian Bednarek
* @description  Test class for th_payment
*/

@isTest
private class TestClassPaymentTriggerAfterInsert {
    
    @testSetup static void setup() {

        InterestNotesDistribution__c ind = new InterestNotesDistribution__c (
            Interest_Threshold__c = 10,
            Interest_rate__c = 7,
            Name = 'CurrentDistribution'
            );
        insert ind;
    }
   
    /*@isTest static void TypePayment() {
    // Manage incoming payments for after sales service (split between installments, match by bank account number with installments) 
    // Obtain bank account for regular payment that is related to sale terms and is not an installment nor a deposit record
    //  5.2 - distinguish sale terms records between Potential and Current Q. (ManagerPanelManager.cls)
    //  6 - count Potential and Current Quantities  (ManagerPanelManager.cls)
    //  6.2 - Current
        Manager_Panel__c salesTargets = TestHelper.createManagerPanelSalesTarget(
            new Manager_Panel__c(
                Start_Date__c = Date.today()
            ), 
            true
        );

        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987,
                Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED,
                Schedule_Generated__c = true,
                OwnerId = salesTargets.User__c
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        
        // Selected Resource does not match any Product Line attached to the same Sale Terms
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            After_sales_Service__c = afterSales.Id,
            Agreements_Installment__c = saleTerms.Id,
            Payment_For__c = productLine.Offer_Line_Resource__c,
            Paid_Value__c = 250000,
            Due_Date__c = Date.today(),
            Payment_date__c = Date.today(),
            Paid__c = true
        );

        insert newPayment;
        // test    paymentIdsToUpdateSalesTargets.add(newPayment.Id);
        // test    scheduleInstallmentsIns.add(newPayment);
        Test.stopTest();
        }*/

   /* @isTest static void TypePaymentNotPaid() {
    // Manage incoming payments for after sales service (split between installments, match by bank account number with installments) 
    // Obtain bank account for regular payment that is related to sale terms and is not an installment nor a deposit record
    // 5.2 - distinguish sale terms records between Potential and Current Q. (ManagerPanelManager.cls)
    //  6 - count Potential and Current Quantities  (ManagerPanelManager.cls)
    //  6.1 - Potential
        Manager_Panel__c salesTargets = TestHelper.createManagerPanelSalesTarget (
            new Manager_Panel__c (
                Start_Date__c = Date.today() +2
            ), true
        );

        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms (
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987,
                Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED,
                Schedule_Generated__c = true,
                OwnerId = salesTargets.User__c
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        
        // Selected Resource does not match any Product Line attached to the same Sale Terms
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            After_sales_Service__c = afterSales.Id,
            Agreements_Installment__c = saleTerms.Id,
            Payment_For__c = productLine.Offer_Line_Resource__c,
            Paid_Value__c = 25555,
            Paid__c = false,
            Due_Date__c = Date.today()
        );
        insert newPayment;
        // test    paymentIdsToUpdateSalesTargets.add(newPayment.Id);
        // test    scheduleInstallmentsIns.add(newPayment);
        Test.stopTest();
    }

    @isTest static void TypePaymentNotPaidDateSigned() {
    // Manage incoming payments for after sales service (split between installments, match by bank account number with installments) 
    // Obtain bank account for regular payment that is related to sale terms and is not an installment nor a deposit record
    // 5.2 - distinguish sale terms records between Potential and Current Q. (ManagerPanelManager.cls)
    //  6 - count Potential and Current Quantities  (ManagerPanelManager.cls)
    //  6.1 - Potential
        Manager_Panel__c salesTargets = TestHelper.createManagerPanelSalesTarget (
            new Manager_Panel__c (
                Start_Date__c = Date.today()
            ), true
        );

        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms (
            new Sales_Process__c (
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987,
                Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED,
                Schedule_Generated__c = true,
                OwnerId = salesTargets.User__c
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService (
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        
        // Selected Resource does not match any Product Line attached to the same Sale Terms
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            After_sales_Service__c = afterSales.Id,
            Agreements_Installment__c = saleTerms.Id,
            Payment_For__c = productLine.Offer_Line_Resource__c,
            Paid_Value__c = 25555,
            Paid__c = false,
            Due_Date__c = Date.today()

        );
        insert newPayment;
        // test    paymentIdsToUpdateSalesTargets.add(newPayment.Id);
        // test    scheduleInstallmentsIns.add(newPayment);
        Test.stopTest();
    }*/
   
    @isTest static void PaymentSchedulePaidValueZero() {
    // Copy incoming payments's After_sales_from_Incoming_Payment__c to After_sales_from_Incoming_Payment_Curren__c or 
    // After_sales_from_Incoming_Payment_Trust__c to differentiate records between related lists

        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );

        Resource__c res = [
            SELECT Investment_Flat__r.Account__c 
            FROM Resource__c 
            WHERE Id =: productLine.Offer_Line_Resource__c
        ];

        productLine = [
            SELECT Bank_Account_Number__c, Price_With_Discount__c, Offer_Line_Resource__c
            FROM Sales_Process__c
            WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
        ];

        List<Extension__c> constructionStages = new List<Extension__c>();

        List<Payment__c> paymentScheduleInstallments = new List<Payment__c>();
        Integer howManyStages = 2; // make it even
        Decimal cumulatedProgress = 0;
        for(Integer i = 1; i <= howManyStages; i++) {
            cumulatedProgress += (100/howManyStages);
            constructionStages.add(
                TestHelper.createExtensionConstructionStage(
                    new Extension__c(
                        Investment__c = res.Investment_Flat__c,
                        Progress__c = cumulatedProgress
                    ), false
                )
            );
            paymentScheduleInstallments.add(
                TestHelper.createPaymentsInstallment(
                    new Payment__c(
                        Amount_to_pay__c = ((100/howManyStages) * productLine.Price_With_Discount__c / 100),
                        Agreements_Installment__c = saleTerms.Id,
                        Contact__c = saleTerms.Contact__c,
                        Bank_Account_For_Resource__c = productLine.Bank_Account_Number__c,
                        Due_Date__c = Date.today() - 5,
                        Paid_Value__c = 12142142
                    ), false
                )
            );  
        }
        insert constructionStages;
        insert paymentScheduleInstallments;

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService (
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        ); 

        Payment__c newPayment = new Payment__c (
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT),
            After_sales_Service__c = afterSales.Id,
            Agreements_Installment__c = saleTerms.Id,
            Payment_For__c = productLine.Offer_Line_Resource__c,
            Paid_Value__c = 250000,
            Due_Date__c = Date.today(),
            Bank_Account_For_Resource__c = productLine.Bank_Account_Number__c,
            After_sales_from_Incoming_Payment__c  = afterSales.Id
        );
        insert newPayment;  
            // test newIncomingPaymentsForAfterSalesService.add(newPayment);
            // test bankAccountNumbersNewIncomingPaymentsForAfterSalesService.add(newPayment.Bank_Account_For_Resource__c);        
        Test.stopTest();
    }

    @isTest static void PaymentSchedulePaidValueOverZero() {
    // Copy incoming payments's After_sales_from_Incoming_Payment__c to After_sales_from_Incoming_Payment_Curren__c or 
    // After_sales_from_Incoming_Payment_Trust__c to differentiate records between related lists 
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );

        Resource__c res = [
            SELECT Investment_Flat__r.Account__c 
            FROM Resource__c 
            WHERE Id =: productLine.Offer_Line_Resource__c
        ];

        productLine = [
            SELECT Bank_Account_Number__c, Price_With_Discount__c, Offer_Line_Resource__c
            FROM Sales_Process__c
            WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
        ];

        List<Extension__c> constructionStages = new List<Extension__c>();
        
        List<Payment__c> paymentScheduleInstallments = new List<Payment__c>();
        Integer howManyStages = 2; // make it even
        Decimal cumulatedProgress = 0;
        for(Integer i = 1; i <= howManyStages; i++) {
            cumulatedProgress += (100/howManyStages);
            constructionStages.add(
                TestHelper.createExtensionConstructionStage(
                    new Extension__c(
                        Investment__c = res.Investment_Flat__c,
                        Progress__c = cumulatedProgress
                    ), false
                )
            );
            paymentScheduleInstallments.add(
                TestHelper.createPaymentsInstallment(
                    new Payment__c(
                        Amount_to_pay__c = ((100/howManyStages) * productLine.Price_With_Discount__c / 100),
                        Agreements_Installment__c = saleTerms.Id,
                        Contact__c = saleTerms.Contact__c,
                        Bank_Account_For_Resource__c = productLine.Bank_Account_Number__c,
                        Due_Date__c = Date.today() - 5
                    ), false
                )
            );  
        }
        insert constructionStages;
        insert paymentScheduleInstallments;

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        ); 

        Payment__c newPayment = new Payment__c (
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT),
            After_sales_Service__c = afterSales.Id,
            Agreements_Installment__c = saleTerms.Id,
            Payment_For__c = productLine.Offer_Line_Resource__c,
            Paid_Value__c = 250000,
            Due_Date__c = Date.today(),
            Bank_Account_For_Resource__c = productLine.Bank_Account_Number__c,
            After_sales_from_Incoming_Payment__c  = afterSales.Id
            );
        insert newPayment;  
        // test newIncomingPaymentsForAfterSalesService.add(newPayment);
        // test bankAccountNumbersNewIncomingPaymentsForAfterSalesService.add(newPayment.Bank_Account_For_Resource__c);        
        Test.stopTest();
    }

   /* @isTest static void TypeIncomingPaymentAssignmentRelease() {
    // Copy incoming payments's After_sales_from_Incoming_Payment__c to After_sales_from_Incoming_Payment_Curren__c or 
    // After_sales_from_Incoming_Payment_Trust__c to differentiate records between related lists 
            
        Resource__c investment = TestHelper.createInvestment(
            new Resource__c(
                Current_Bank_Account__c = '14249000050000452080437211'
            ), 
            true
        );

        Resource__c flat = TestHelper.createResourceFlatApartment(              
            new Resource__c(
                Investment_Flat__c = investment.Id,
                Price__c = 10000000
            ), 
            true
        );

        Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(
            new Sales_Process__c(
                Product_Line_to_Proposal__c = proposal.Id,
                Contact_from_Offer_Line__c = proposal.Contact__c,
                Offer_Line_Resource__c = flat.Id        
            ), 
            true
        );

        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = proposal.Contact__c,
                Offer__c = proposal.Id,
                Agreement_Value__c = flat.Price__c
            ),
            true
        );

        Test.startTest();
        productLine = [
            SELECT Bank_Account_Number__c, Price_With_Discount__c, Offer_Line_Resource__c
            FROM Sales_Process__c
            WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
        ];

        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService (
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        

        Payment__c newPayment = new Payment__c (
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT),
            After_sales_Service__c = afterSales.Id,
            Agreements_Installment__c = saleTerms.Id,
            Payment_For__c = productLine.Offer_Line_Resource__c,
            Paid_Value__c = 250000,
            Due_Date__c = Date.today(),
            Bank_Account_For_Resource__c = productLine.Bank_Account_Number__c,
            After_sales_from_Incoming_Payment__c  = afterSales.Id,
            Assignment__c = CommonUtility.PAYMENT_ASSIGNMENT_RELEASE
        );
        insert newPayment;

        List<Payment__c> result = new List<Payment__c>();
        for(Integer i = 0; i < 5; i++) {
            Payment__c payment = new Payment__c();
                payment.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT);
                payment.After_sales_Service__c = afterSales.Id;
                payment.Agreements_Installment__c = saleTerms.Id;  
                payment.Payment_For__c = productLine.Offer_Line_Resource__c;
                payment.Paid_Value__c = 250000;
                payment.Due_Date__c = Date.today();
                payment.Bank_Account_For_Resource__c = productLine.Bank_Account_Number__c;
                payment.After_sales_from_Incoming_Payment__c  = afterSales.Id;
                payment.Assignment__c = CommonUtility.PAYMENT_ASSIGNMENT_RELEASE;
            result.add(payment); 
        }
        insert result;
        // test incomingPaymentsWithAccountAllocation.add(newPayment);           
        Test.stopTest();
    }*/

    @isTest static void TypePaymentDeposit() {
    // Copy deposit payment date to sale terms 
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms (
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService (
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        

        Payment__c newPayment = new Payment__c (
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_DEPOSIT),
            Agreements_Installment__c = saleTerms.Id,
            Paid_Value__c = 250000,
            Payment_date__c = Date.today()
        );
        insert newPayment;
        // test   depositsToUpdatePaymentDateOnSaleTerms.add(newPayment)         
        Test.stopTest();
    }

    @isTest static void TypePaymentIncomingPaymentInstallment() {
    // Create interest notes (odsetki) for every incoming payment that has been overdued
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
            ),
            true
        );

        Test.startTest();
        Payment__c somePayment = new Payment__c(Due_Date__c = Date.today());
        insert somePayment;

        Payment__c newPayment = new Payment__c (
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT_INSTALLMENT),
            Agreements_Installment__c = saleTerms.Id,
            Paid_Value__c = 250000,
            Incoming_Payment_junction__c = somePayment.Id
        );
        insert newPayment;
        // test  depositsToUpdatePaymentDateOnSaleTerms.add(newPayment)         
        Test.stopTest();
    }

    @isTest static void TypePaymentIncomingPaymentOverpayment() {
    // Create interest notes (odsetki) for every incoming payment that has been overdued
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(), true);

        Test.startTest();
        Payment__c somePayment = new Payment__c(Due_Date__c = Date.today(), Paid_Value__c = 350000);
        insert somePayment;

        Payment__c newPayment = new Payment__c (
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_OVERPAYMENT),
            Paid_Value__c = 250000,
            Incoming_Payment_from_Overpayment__c  = somePayment.Id
        );
        insert newPayment;
        // test overpaymentValueToSubtractFromIncomingPaymentMap.put(newPayment.Incoming_Payment_from_Overpayment__c, newPayment.Paid_Value__c);
        //  afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(newPayment.After_sales_from_Incoming_Payment__c);      
        Test.stopTest();
    }

    @isTest static void TypePaymentExpense() {
        // Calculate expenses for Marketing campaigns
        Marketing_Campaign__c marketingCampaign = TestHelper.createMarketingCampaign(NULL, true);
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_EXPENSE),
            Marketing_Campaign__c = marketingCampaign.Id,
            Amount_to_pay__c = 2500
        );
        insert newPayment;
        //  marketingCampaignIdsToCountExpenses.add(newPayment.Marketing_Campaign__c);     
        }

    @isTest static void CalculateCustomerBalance() {
        //  Calculate customer balance 
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE)            
        );
        insert newPayment;
    }
}