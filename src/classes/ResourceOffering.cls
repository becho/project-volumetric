/**
* @author       Mateusz Pruszyński
* @description  Class to manage offering resources without reservation
**/

global without sharing class ResourceOffering {

    public Boolean fromResource {get; set;}
    public Sales_Process__c myOffer {get; set;}

    public ResourceOffering() {
        if(Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_PROPOSALID) != null) {
            myOffer = [
                SELECT Id, Contact__c 
                FROM Sales_Process__c 
                WHERE Id = :Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_PROPOSALID)
            ];
            fromResource = false;
        }
        else {
            myOffer = new Sales_Process__c(); 
            fromResource = true;
        }
    }

    /**
    * @WS_METHODS
    **/

    // Details response object
    global class DetailsResponse {
        @TestVisible public List<String> alreadyOfferedList;
        @TestVisible public Sales_Process__c offer;
    }

    // Method to create offer (proposal + product lines, without reservation) - called from js button on Resource
    WebService static String[] massCreateOffer(String[] resourceIds, String contactId) {

        String[] result = new String[] {};

        DetailsResponse resp = massCreateOfferWithDetails(resourceIds, contactId);

        if(resp.offer != null) {
            result.add(CommonUtility.BOOLEAN_TRUE);
            result.add('<a href="' + resp.offer.Id + '">' + resp.offer.Name + '</a>');
        } else {
            result.add(CommonUtility.BOOLEAN_FALSE);
        }

        return result;
    }

    // Method to create offer (proposal + product lines, without reservation) - called from ResSearch 
    WebService static DetailsResponse massCreateOfferWithDetails(String[] resourceIds, String contactId) {
        DetailsResponse resp = new DetailsResponse();

        // First, check if there is Offer/Reservation already created for this contact person
        DetailsResponse alreadyOffers = checkForOffersInDatabase(resourceIds, contactId);

        if(alreadyOffers.alreadyOfferedList != null && alreadyOffers.alreadyOfferedList.size() > 0) {

            return alreadyOffers;

        } else {

            List<Resource__c> resToOffer = [
                SELECT Id, Price__c, Usable_Area_Planned__c, Total_Area_Planned__c, Acceptance_Status__c, Price_After_Measurement__c, 
                       Promotional_Price__c, Promotion_IsActive__c, Defined_Promotion__c
                FROM Resource__c WHERE Id in :resourceIds
            ];

            Sales_Process__c proposalToOffer = new Sales_Process__c(
                RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER),
                Contact__c = contactId,
                Market__c = CommonUtility.SALES_PROCESS_MARKET_PRIMARY,
                Agreement_Value__c = 0
            );

            // sum price from all resources
            for(Resource__c res : resToOffer) {
                if(res.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED) { 
                    proposalToOffer.Agreement_Value__c += res.Price_After_Measurement__c;
                } else {
                    if(!res.Promotion_IsActive__c && (res.Promotional_Price__c == null || res.Promotional_Price__c <= 0)) {
                        proposalToOffer.Agreement_Value__c += res.Price__c;
                    } else {
                        proposalToOffer.Agreement_Value__c += res.Promotional_Price__c;
                    }
                }
            }

            Savepoint sp = Database.setSavepoint();
            try {
                insert proposalToOffer; // insert proposal to obtain its Id

                List<Sales_Process__c> salesLinesToOffer = new List<Sales_Process__c>();
                Sales_Process__c sl2O;
                for(Resource__c res : resToOffer) {
                    sl2O = new Sales_Process__c(
                        RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE),
                        Contact_from_Offer_Line__c = contactId,
                        Offer_Line_Resource__c = res.Id,
                        Product_Line_to_Proposal__c = proposalToOffer.Id,
                        Discount__c = 0,
                        Discount_Amount_Offer__c = 0,
                        Price_With_Discount__c = (res.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED ? res.Price_After_Measurement__c : 
                            ((!res.Promotion_IsActive__c && (res.Promotional_Price__c == null || res.Promotional_Price__c <= 0)) ? res.Price__c : res.Promotional_Price__c)
                        ),
                        Resource_Area__c = res.Total_Area_Planned__c != null ?  res.Usable_Area_Planned__c : res.Total_Area_Planned__c,
                        Queue__c = null,
                        Without_Reservation__c = true
                    );

                    if(res.Price__c != sl2O.Price_With_Discount__c) {
                        Decimal resPrice = res.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED ? res.Price_After_Measurement__c : res.Price__c;
                        sl2O.Discount_Amount_Offer__c = resPrice - sl2O.Price_With_Discount__c;
                        sl2O.Discount__c = (100 / resPrice) * sl2O.Discount_Amount_Offer__c;
                    }

                    if(res.Defined_Promotion__c != null) {
                        sl2O.Promotion_Definition__c = res.Defined_Promotion__c;
                    }

                    salesLinesToOffer.add(sl2O);
                }
                insert salesLinesToOffer;

                resp.offer = [SELECT Id, Name FROM Sales_Process__c WHERE Id = :proposalToOffer.Id];

                return resp;
            } catch(Exception e) {
                Database.rollback(sp);
                ErrorLogger.log(e);
                return null;
            } 
        }
    }

    // Check if there is an Offer already created for selected Resources and Contact person
    public static DetailsResponse checkForOffersInDatabase(String[] resourceIds, String contactId) {
        DetailsResponse resp = new DetailsResponse();

        List<Sales_Process__c> salesLinesWithOffers = [
            SELECT Id, Name, Offer_Line_Resource__r.Name FROM Sales_Process__c
            WHERE Product_Line_to_Proposal__r.Contact__c = :contactId
            AND Product_Line_to_Proposal__r.RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER) 
            AND Offer_Line_Resource__c in :resourceIds
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
        ];

        if(salesLinesWithOffers.size() > 0) {
            resp.alreadyOfferedList = new List<String>();
            for(Sales_Process__c slwo : salesLinesWithOffers) {
                resp.alreadyOfferedList.add(slwo.Offer_Line_Resource__r.Name);
            }
        }

        return resp;
    }
}