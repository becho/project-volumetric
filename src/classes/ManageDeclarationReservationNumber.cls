/**
* @author 		Mateusz Pruszyński
* @description 	Class used to manage declaration reservation number that is always printed on Declaration Reservation documents (enxooDocGen app)
**/
public without sharing class ManageDeclarationReservationNumber {

	private static final String DECLARATION_NUMBER_DEFINITION = 'DeclarationNumberDefinition';

	// DocumentTemplateMethods - return reservation number for next declaration document
	public static String getNextNumber(String spId) {
		// get local reservation number from sale terms
		Sales_Process__c saleTerms = [SELECT Id, Local_Declaration_Number__c, Global_Declaration_Number__c FROM Sales_Process__c WHERE Id = :spId];
		String localNumber = String.valueOf((saleTerms.Local_Declaration_Number__c != null ? saleTerms.Local_Declaration_Number__c : 0) + 1);
		List<String> lnum = formatNumberParts(localNumber);
		List<String> cnum;
		if(saleTerms.Global_Declaration_Number__c != null && saleTerms.Global_Declaration_Number__c > 0) {
			String globalNumber = String.valueOf(saleTerms.Global_Declaration_Number__c);
			cnum = formatNumberParts(globalNumber);
		}
		// get configuration from custom settings
		DeclarationReservationNumber__c declaration_reservation_number = DeclarationReservationNumber__c.getInstance(DECLARATION_NUMBER_DEFINITION);
		if(cnum == null) {
			String currentNumber = String.valueOf((declaration_reservation_number.CurrentDeclarationNumber__c != null ? declaration_reservation_number.CurrentDeclarationNumber__c : 0) + 1);
			cnum = formatNumberParts(currentNumber);
		}
		String initialNumber = String.valueOf((declaration_reservation_number.InitialDeclarationNumber__c != null ? declaration_reservation_number.InitialDeclarationNumber__c : 0) + 1);
		List<String> inum = formatNumberParts(initialNumber);
		return String.valueOf(Decimal.valueOf(cnum[0]) + Decimal.valueOf(inum[0]));
	}

	public static List<String> formatNumberParts(String strNumber) {
		List<String> strNumbers;
		if(strNumber.contains('.')) {
			strNumbers = String.valueOf(strNumber).split('\\.');
		} else if(strNumber.contains(',')) {
			strNumbers = String.valueOf(strNumber).split(',');
		} else {
			strNumbers = new String[] {strNumber};
		}		
		return strNumbers;
	}

	// Th_Attachment - increment CurrentDeclarationNumber__c AND Local_Declaration_Number__c
	public static void renumberFromAttachment(List<Attachment> attachedDeclarations) {
		// get configuration and current Declaration Value from custom settings
		DeclarationReservationNumber__c declaration_reservation_number = DeclarationReservationNumber__c.getInstance(DECLARATION_NUMBER_DEFINITION);
		declaration_reservation_number.CurrentDeclarationNumber__c = declaration_reservation_number.CurrentDeclarationNumber__c == null ? 0 : declaration_reservation_number.CurrentDeclarationNumber__c;
		Set<Id> saleTermsIds = new Set<Id>();
		for(Attachment att : attachedDeclarations) {
			saleTermsIds.add(att.ParentId);
		}
		List<Sales_Process__c> saleTerms = [SELECT Id, Local_Declaration_Number__c, Global_Declaration_Number__c FROM Sales_Process__c WHERE Id in :saleTermsIds];
		for(Attachment att : attachedDeclarations) {
			for(Sales_Process__c st : saleTerms) {
				if(att.ParentId == st.Id) {
					st.Local_Declaration_Number__c = st.Local_Declaration_Number__c == null ? 0 : st.Local_Declaration_Number__c;
					st.Local_Declaration_Number__c++;
					st.Global_Declaration_Number__c = st.Global_Declaration_Number__c == null ? ++declaration_reservation_number.CurrentDeclarationNumber__c : st.Global_Declaration_Number__c;					
					break;
				}
			}
		}
		Savepoint sp = Database.setSavepoint();
		try {
			update saleTerms;
			update declaration_reservation_number;
		} catch(Exception e) {
			Database.rollback(sp);
			ErrorLogger.log(e);
		}
	}
}