@isTest
private class TestClassCreateCrmId {
	
/* Grzegorz Murawiński */
// Test class for creating Crm_Id__c method

// 1.Prepare data: prepare three contractors



Account contractor2 = TestHelper.createAccountIndividualClient(null, false);
//contractor2.Crm_id = 

Account contractor3 = TestHelper.createAccountIndividualClient(null, false);
//contractor3.GroupCrm_id =

Account contractor4 = TestHelper.createAccountIndividualClient(null, false);
//contractor4.Crm_id =
//contractor4.GroupCrm_id =

// 1.1 Check string to hashing for 1, 2 or 3 contractors


// 2.1* Check uniqueness of hash. Individual client, create account, create crm_id from hash Id with md5
// 	    Create second hash from the same Id and check if they are both the same
	@isTest(SeeAllData=true) static void test_method_one() {
/*
		Sales_Process__c saleTerm = TestHelper.createSalesProcessProposal(null, false);

		Account contractor1 = TestHelper.createAccountIndividualClient(null, false);

		insert contractor1;
		saleTerm.Account_from_Customer_Group__c = contractor1.Id;
		saleTerm.Developer_Agreement_from_Customer_Group__c = 'a0K4E000000t9k9UAA';
		
		insert saleTerm;


		Test.startTest();
		saleTerm.Date_of_signing__c = Date.today();
		update saleTerm;
		CreateCrmId testHash = new CreateCrmId();
		Map<Id, Sales_Process__c> hashMap = new Map<Id, Sales_Process__c>([SELECT Developer_Agreement_from_Customer_Group__c 
																		   From Sales_Process__c
																		   WHERE Id = :saleTerm.Id]);
		System.debug(hashMap);
		
		Sales_Process__c sale2assert = [SELECT Crm_id__c From Sales_Process__c WHERE Id =: saleTerm.Id];
		System.debug('sale2assert' + sale2assert);
		

		Test.stopTest();
		
		System.assertEquals('test', sale2assert.Crm_Id__c);*/

		// 1 - Create sales process structure
		// Insert investment
		/*Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flat
		Resource__c flat = TestHelper.createResourceFlatApartment(				
			new Resource__c(
				Investment_Flat__c = investment.Id
			), 
			false
		);

		upsert flat;

		// Insert parking space
		Resource__c parkingSpace = TestHelper.createResourceParkingSpace(				
			new Resource__c(
				Investment_Parking_Space__c = investment.Id
			), 
			true
		);

		Resource__c[] resources = new Resource__c[] {flat, parkingSpace};

		// Insert Account
		Account personAccount = TestHelper.createAccountIndividualClient(null, true);

		// Insert proposal
        Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        proposal.Contact__c = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id LIMIT 1].Id;
   	    insert proposal;

		// Insert product lines
		List<Sales_Process__c> productLines = new List<Sales_Process__c>();
		for(Resource__c res : resources) {
			productLines.add(TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Product_Line_to_Proposal__c = proposal.Id,
					Contact_from_Offer_Line__c = proposal.Contact__c,
					Offer_Line_Resource__c = res.Id
				),  
				true
			));
		}
		//insert productLines;

		// Insert Sale Terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal.Id,
				Contact__c = proposal.Contact__c,
				Expected_Date_of_Signing__c = Date.today()-1,
				Developer_Agreement_from_Customer_Group__c = proposal.Id 
			), 
			false
		);

		Test.startTest();
			insert saleTerms;

			//Change date of signing
			saleTerms.Date_of_signing__c = Date.today();
			upsert saleTerms;

			Sales_Process__c saleTerms2Assert = [
			Select Crm_Id__c
			from Sales_process__c where Developer_Agreement_from_Customer_Group__c = :saleTerms.Id LIMIT 1
			];

			Account crm2Assert = [
			Select Crm_Id__c
			from Account where Id = :proposal.Id LIMIT 1
			];

		Test.stopTest();
		
		System.assertEquals(saleTerms2assert.Crm_Id__c, crm2Assert.Crm_id__c);*/

	}
	
// 2.2* Check uniqueness of hash for two clients. 
	@isTest static void test_method_two() {
		
	}
	
// 2.3* Check uniqueness of hash for three clients. 
	@isTest static void test_method_three() {
		
	}	
}