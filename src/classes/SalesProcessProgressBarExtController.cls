public with sharing class SalesProcessProgressBarExtController {


    /*
        Author: Maicej Jóźwiak, latest changes: Wojciech Słodziak
    */

    public Sales_Process__c currentProcess {get; set;}
    public Id offerId {get;set;}
    public List<Status> statuses {get;set;}
    public Integer countStatuses {get;set;}
    public String tabWidth {get;set;}
    public Map<Id, RecordType> rtTypes {get;set;}
    public boolean noPrelim {get;set;}
    public boolean error {get; set;}

//-------------------------------------------------------------------------------------------------------
//  Constructor
//-------------------------------------------------------------------------------------------------------   
    public SalesProcessProgressBarExtController(ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()){
        stdController.addFields(new String[] {'RecordTypeId', 'RecordType.DeveloperName'});
        }
        currentProcess = (Sales_Process__c)stdController.getRecord();
        this.rtTypes = new Map<Id, RecordType>([SELECT Id, tolabel(Name), DeveloperName FROM RecordType WHERE 
            sObjectType = :CommonUtility.SOBJECT_NAME_SALESPROCESS AND DeveloperName IN (:CommonUtility.SALES_PROCESS_TYPE_OFFER, :CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT, :CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL, :CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)]);
        noPrelim = false;
        error = false;
        refresh();
    } 

    public void refresh() {
        this.statuses = new List<Status>();
        List<Status> tempStatuses = new List<Status>(4);
        Map<String, Integer> rtOrder = new Map<String, Integer>();
        
        List<Sales_Process__c> relId;
        
        //get offerID
        System.debug('currentProcess.RecordTypeId '+currentProcess.RecordTypeId);
        System.debug('CommonUtility.getRecordTypeId '+CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT));
        if(currentProcess.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)) { // <- wzór
            this.offerId = [select Offer__c from Sales_Process__c where id=: currentProcess.Id].Offer__c;
        } else if(currentProcess.RecordTypeId  == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)){   // <-
            Sales_Process__c thisObj = [select Offer__c, Preliminary_Agreement__c, Preliminary_Agreement__r.Offer__c from Sales_Process__c where id=: currentProcess.Id];
            if (thisObj.Preliminary_Agreement__c != null) {
                this.offerId = thisObj.Preliminary_Agreement__r.Offer__c;
            } else {
                 this.offerId = thisObj.Offer__c;
                 noPrelim = true;
            }
        } else if(currentProcess.RecordTypeId  == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)){ // <-
            this.offerId = currentProcess.Id;
            //check if offer is for rent and has final agreement created (without reservation agreement)
            List<Sales_Process__c> agreementFinal = [SELECT Id, Name FROM Sales_Process__c WHERE Offer__c = :offerId AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)];
            List<Sales_Process__c> agreementPrelim = [SELECT Id, Name FROM Sales_Process__c WHERE Offer__c = :offerId AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)];
            if (!agreementFinal.isEmpty() && agreementPrelim.isEmpty()) {
                noPrelim = true;
            }
        } else { //handover
            this.offerId = [select Agreement__r.Offer__c from Sales_Process__c where id=: currentProcess.Id].Agreement__r.Offer__c;
            if (this.offerId == null) {
                this.offerId = [select Agreement__r.Preliminary_Agreement__r.Offer__c from Sales_Process__c where id=: currentProcess.Id].Agreement__r.Preliminary_Agreement__r.Offer__c;
            }
            List<Sales_Process__c> agreementFinal = [SELECT Id, Name FROM Sales_Process__c WHERE Offer__c = :offerId AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)];
            List<Sales_Process__c> agreementPrelim = [SELECT Id, Name FROM Sales_Process__c WHERE Offer__c = :offerId AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)];
            if (!agreementFinal.isEmpty() && agreementPrelim.isEmpty()) {
                noPrelim = true;
            }
        }
        //System.debug('ofr id: ' + offerId);
        if (offerId == null) {
            error = true;
        } else {
            String offerMarket = [select Market__c from Sales_Process__c where id=: this.offerId].Market__c;
            Integer order = 0;
            rtOrder.put(CommonUtility.SALES_PROCESS_TYPE_OFFER, order++);
            if (!noPrelim) {
                rtOrder.put(CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT, order++);
            } 
            if (offerMarket == null || offerMarket == 'Primary') {
                rtOrder.put(CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL, order++);
                rtOrder.put(CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT, order);
            } else {
                rtOrder.put(CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT, order++);
                rtOrder.put(CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL, order);
            }
            //System.debug('rtOrd:' + rtOrder);

            Status sts;
            for(Id rtKey : rtTypes.keySet()){
                RecordType rt = rtTypes.GET(rtKey);
                if (rtOrder.containsKey(rt.DeveloperName)) {
                    sts = new Status(rt.DeveloperName, rt.Name);
                    if(rt.DeveloperName == CommonUtility.SALES_PROCESS_TYPE_OFFER){
                        sts.objectId = this.offerId;
                    } else if(rt.DeveloperName == CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT ){
                        relId = [select id from Sales_Process__c where Offer__c=: offerId and RecordTypeId=: rt.Id];
                        if(!relId.isEmpty()){
                            sts.objectId = relId[0].Id;
                        }
                    } else if (rt.DeveloperName == CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) {
                        relId = [select id from Sales_Process__c where (Preliminary_Agreement__r.Offer__c=: offerId OR Offer__c=: offerId) and RecordTypeId=: rt.Id];
                        if(!relId.isEmpty()){
                            sts.objectId = relId[0].Id;
                        }
                    }else {
                        relId = [select id from Sales_Process__c where (Agreement__r.Offer__c=: offerId or Agreement__r.Preliminary_Agreement__r.Offer__c=: offerId or Agreement__r.Offer__c=: offerId)];
                        if(!relId.isEmpty()){
                            sts.objectId = relId[0].Id;
                        }
                    }
                    tempStatuses.add(rtOrder.get(rt.DeveloperName), sts);
                }
            }
            //System.debug('tmp: ' + tempStatuses);
            for(integer i=0; i< tempStatuses.size(); i++){
                if(tempStatuses.get(i) != null){
                    this.statuses.add(tempStatuses.get(i));
                }
            }
            for(Integer i=0; i< statuses.size(); i++){
                if(statuses.get(i).objectId == null){
                    statuses.get(i-1).isCurrent = true;
                    statuses.get(i).changePossible = true;
                    for (Integer j = 0; j < i-1; j++) {
                        statuses.get(j).isDone = true;
                    }
                    break;
                } else 
                if(statuses.get(i).objectId != null && ((statuses.size() == 3) && i == 2 || i == 3)){
                    statuses.get(i).isCurrent = true;
                     for (Integer j = 0; j < i; j++) {
                        statuses.get(j).isDone = true;
                    }
                }
            }

            //System.debug('stss:' + statuses);
            
            this.countStatuses = statuses.size();
            this.tabWidth = 'width: ' + 100/(Decimal)countStatuses + '%;';  
        }

    }


//-------------------------------------------------------------------------------------------------------
//  Status wrapper class
//-------------------------------------------------------------------------------------------------------

    public class Status {
        public String name {get;set;}
        public String label {get;set;}
        public Boolean isCurrent {get;set;}
        public Boolean isDone {get;set;}
        public Id objectId {get;set;}
        public Boolean changePossible {get;set;}

        public Status(String name, String status) {
            this.name = name;
            this.label = status;
            this.changePossible = false;
            this.isDone = false;
        }

    }
}