/**
* @author       Mateusz Pruszyński
* @description  Controller to prepare and send email from Proposal
*/

public without sharing class OffersEmail {

    public Sales_Process__c proposal {get; set;}
    public Contact fakeContact {get; set;}
    public Contact checkContact {get; set;}
    public Map<String, Contact> contactMap {get; set;}

    
    public List<LinkedAttachment> linkedAttachments {get; set;}
    public List<SelectOption> loEmailTemps {get; set;}
    public String pickTempId {get;set;}
    public String contactId {get; set;}
    public String pdfNames {get; set;}
    public Boolean send {get; set;}

    public Boolean isFakeContact {get; set;}
    public Boolean emailOK {get; set;}
    public String mBody {get;set;} 
    public String mSubject {get;set;}
    public final String RECIPIENT_TO_SUBSTITUTE = 'xxxrepiNamexxx';
    public final String CONTACT_TYPE_CONTACT = 'contact';
    public final String CONTACT_TYPE_PA = 'PA';
	public final String FOLDER_OFERTY = 'Oferty';

    // Wrapper structure for attachments
    public class LinkedAttachment {
        public String name {get; set;}
        public Boolean checked {get; set;}
        public String url {get; set;}
        public Attachment attachment {get; set;}
        public Blob pdf {get; set;}
        public Boolean fromFtp {get; set;}
        LinkedAttachment() {
            checked = false;
        }
    }

    // Constructor
    public OffersEmail(ApexPages.StandardController stdController) {
        send = false;
        isFakeContact = false;
        pdfNames = '';
        String[] fields = getFields();  
        if(!test.isRunningTest()) { 
            stdController.addFields(fields);    
        }        
        proposal = (Sales_Process__c)stdController.getRecord();
        contactMap = getContact();
        emailOK = checkEmail();
        if(emailOK) {
            loEmailTemps = loadEmailTemplates();
            List<Attachment> attachments = [SELECT Id, Name, Description, LastModifiedDate, Body FROM Attachment WHERE ParentId = :proposal.Id AND ContentType = :CommonUtility.FILETYPE_PDF ORDER BY LastModifiedDate]; // direct attachments to the record
            linkedAttachments = new List<LinkedAttachment>();
            if(attachments.size() > 0) {
                for(Attachment att : attachments) {
                    LinkedAttachment la = new LinkedAttachment();
                    la.attachment = att;
                    la.fromFtp = false;
                    la.name = att.Name;
                    linkedAttachments.add(la);
                }
            } 
            List<Sales_Process__c> offerLines = [SELECT Id, Name, Product_Line_to_Proposal__c , Offer_Line_Resource__c, Offer_Line_Resource__r.InvestmentId__c FROM Sales_Process__c WHERE Product_Line_to_Proposal__c =: proposal.Id AND Offer_Line_Resource__c != null];
            if(!offerLines.isEmpty()) {
                /* data sheet from resource */
                // 1 - first get resource ids
                Set<Id> resourcesIDs = new Set<Id>();
                Set<Id> investmentIDs = new Set<Id>();
                for(Sales_Process__c off : offerLines){
                    resourcesIDs.add(off.Offer_Line_Resource__c); 
                    investmentIDs.add(off.Offer_Line_Resource__r.InvestmentId__c);  
                }            
                // 2 - get data sheet from metadata__C
                List<Metadata__c> metaData = [SELECT Metadata__c, Metadata_type__c, ObjectID__c, RecordID__c FROM Metadata__c WHERE ObjectID__c = :CommonUtility.SOBJECT_NAME_RESOURCE AND RecordID__c in: resourcesIDs AND Metadata_type__c IN (:CommonUtility.METADATA_TYPE_DATASHEET, :CommonUtility.METADATA_TYPE_FILE_PDF)];
                for(Metadata__c md : metaData){
                    LinkedAttachment entryTmp = new LinkedAttachment();
                    List<String> matadataElements = md.Metadata__c.split('"');
                    entryTmp.url = matadataElements[7];
                    entryTmp.name = Label.DataSheet + matadataElements[11];
                    entryTmp.fromFtp = true;
                    linkedAttachments.add(entryTmp);
                }    
                /* attachments from investment */   
                List<Attachment> investmentAttachments = [SELECT Id, Name, Description, LastModifiedDate, Body FROM Attachment WHERE ParentId in :investmentIDs];  
                if(!investmentAttachments.isEmpty()) {
                    for(Attachment invAtt : investmentAttachments) {
                        LinkedAttachment la = new LinkedAttachment();
                        la.attachment = invAtt;
                        la.fromFtp = false;
                        la.name = invAtt.Name;
                        linkedAttachments.add(la);
                    }            
                }       
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendOfferEmptyEmail));
        }      
    }

    public String[] getFields() {
        String[] result = new String[] {
            'Contact__c'
        };
        return result;
    }

    public void loadAttachments() {
        transient List<Messaging.Emailfileattachment> atts = new List<Messaging.EmailFileAttachment>(); 
        if(linkedAttachments != null) {    
            if (linkedAttachments.size() > 0) {
                for (Integer i = 0; i < linkedAttachments.size(); i++) {
                    // Add to attachment file list
                    if (linkedAttachments[i].checked == true) {
                        if(linkedAttachments[i].fromFtp == false) {
                            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                            pdfNames += (linkedAttachments[i].attachment.Name);
                            pdfNames += ', ';
                            efa.setFileName(linkedAttachments[i].attachment.Name);
                            efa.setBody(linkedAttachments[i].attachment.Body);
                            atts.add(efa);
                        }
                        else {
                            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                            Http h = new Http();
                            HttpRequest req = new HttpRequest();
                            req.setEndpoint(linkedAttachments[i].url);
                            req.setMethod('GET');
                            HttpResponse res = h.send(req);
                            Blob doc = res.getBodyAsBlob();
                            efa = new Messaging.EmailFileAttachment();
                            efa.setContentType(CommonUtility.FILETYPE_PDF);
                            efa.setFileName(linkedAttachments[i].name + '.pdf');
                            efa.Body = doc;
                            atts.add(efa);
                        }
                    }                   
                } 
            }
        }
        sendOffers(atts);
    }  

    public List<SelectOption> loadEmailTemplates() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', Label.EmailTmpSelectInside));
        for(EmailTemplate emailTemp : [SELECT Id, Name FROM EmailTemplate WHERE Folder.Name = :FOLDER_OFERTY AND IsActive = true]) {
            options.add(new SelectOption(emailTemp.Id, emailTemp.Name));
        }        
        return options;
    }

    public Map<String, Contact> getContact() {
        Map<String, Contact> contactToReturn = new Map<String, Contact>();
        checkContact = [SELECT Id, Email, Name, FirstName, LastName, AccountId, Account.Email__c, Account.IsPersonAccount, RecordTypeId FROM Contact WHERE Id =: proposal.Contact__c];
        if(checkContact.AccountId != null && checkContact.Account.IsPersonAccount) { // Person account - requires walk around
            List<Contact> firstContactFound = [SELECT Id, Name, Email FROM Contact WHERE Email != null LIMIT 1];
            if(!firstContactFound.isEmpty()) { // walk around - get record
                contactId = firstContactFound[0].Id;
                contactToReturn.put(CONTACT_TYPE_PA, checkContact);
                isFakeContact = true; 
            } else {
                // walk around - update record with email
                fakeContact = [SELECT Id, Email FROM Contact WHERE Email = null LIMIT 1];
                fakeContact.Email = 'fake.contact@example.com';
                contactToReturn.put(CONTACT_TYPE_PA, checkContact);
                isFakeContact = true; 
            }
        } else { // Regular contact
            contactToReturn.put(CONTACT_TYPE_CONTACT, checkContact);
            contactId = checkContact.Id;
            isFakeContact = false;
        }
        return contactToReturn;
    }

    public PageReference sendOffers(List<Messaging.Emailfileattachment> atts) {
        Messaging.SingleEmailMessage mail = prepareEmail();
        Messaging.SingleEmailMessage mailToSend = new Messaging.SingleEmailMessage();
        if(!isFakeContact) {
            mailToSend.setTargetObjectId(contactId);
        } else {
            List<String> toAddresses = new List<String>();
            toAddresses.add(checkContact.Account.Email__c);
            mailToSend.setToAddresses(toAddresses); 
            if(fakeContact != null) {
                fakeContact.Email = null;
                update fakeContact;
            }
        }
        mailToSend.setSubject(this.mSubject);
        mailToSend.setHtmlBody(this.mBody);
        Savepoint sp = Database.setSavepoint();
        try {
            String attachments = '';
            if(atts != null && atts.size() > 0) {
                String atchm = '';
                for(Messaging.Emailfileattachment fa : atts) {
                    atchm += fa;
                    attachments += atchm.substringBetween('getFileName=', ';');
                    attachments += '\n';
                    atchm = '';
                }
                
                mailToSend.setFileAttachments(atts);
            }
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToSend});
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, Label.EmailSend));
            
            String taskDescription = Label.emailSentTo + ': ' + (checkContact.AccountId != null && checkContact.Account.IsPersonAccount == true ? checkContact.Account.Email__c : checkContact.Email) + '\n' 
                                    + Label.Attachment + ': \n' 
                                    + attachments + '\n\n'
                                    + Label.EmailSubject + ': ' + mSubject + '\n'
                                    + Label.EmailBody + ': ' + mBody;

            Task activityHistoryTask = new Task(
                WhatId = proposal.Id,
                WhoId = contactMap.containsKey(CONTACT_TYPE_CONTACT) ? contactMap.get(CONTACT_TYPE_CONTACT).Id : contactMap.get(CONTACT_TYPE_PA).Id,
                IsReminderSet = false,
                ActivityDate = Date.today() - 1,
                Description = taskDescription,
                Subject = mSubject,
                Status = 'Completed'
            );
            insert activityHistoryTask;

            send = true;

            if(pdfNames.length() != 0) {
                pdfNames = pdfNames.substring(0, pdfNames.length() - 2);
            }
            linkedAttachments.clear();
            return null;

        } catch (Exception e) {
            ErrorLogger.log(e);                
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, String.valueOf(e));
            apexpages.addmessage(msg); 
            Database.rollback(sp);              
            return null;                                    
        }                   
    }

    private Messaging.SingleEmailMessage prepareEmail() {
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setTargetObjectId(contactId);
        mail.setWhatId(proposal.Id);
        mail.setTemplateId(pickTempId);
        return mail;
    }

    public void loadTemplate() {
        if(fakeContact != null && fakeContact.Id != null) {
            update fakeContact;
            contactId = fakeContact.Id;
        }
        emailOK = checkEmail();
        if(!emailOK) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendOfferEmptyEmail));
        }
        else {
            if(pickTempId != null) {
                Messaging.SingleEmailMessage mail = prepareEmail();
                Savepoint sp = Database.setSavepoint();
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                Database.rollback(sp);      
                if(contactMap.containsKey(CONTACT_TYPE_CONTACT)) {
                    mBody = mail.getHtmlBody().replace(RECIPIENT_TO_SUBSTITUTE, contactMap.get(CONTACT_TYPE_CONTACT).Name);
                    mSubject = mail.getSubject().replace(RECIPIENT_TO_SUBSTITUTE, contactMap.get(CONTACT_TYPE_CONTACT).Name);
                } else {
                    mBody = mail.getHtmlBody().replace(RECIPIENT_TO_SUBSTITUTE, contactMap.get(CONTACT_TYPE_PA).Name);
                    mSubject = mail.getSubject().replace(RECIPIENT_TO_SUBSTITUTE, contactMap.get(CONTACT_TYPE_PA).Name);                    
                }
            }
            else {
                mBody = '';
            }
        }
    }

    public Boolean checkEmail() {
        system.debug('EROR  ');
        system.debug('checkContact.AccountId '+ checkContact.AccountId);
        system.debug('checkContact.Email '+ checkContact.Email);
        system.debug('checkContact.Account.IsPersonAccount '+ checkContact.Account.IsPersonAccount);
        system.debug('checkContact.Account.Email__c '+ checkContact.Account.Email__c);

        if( 
            (checkContact.AccountId == null && checkContact.Email == null) || 
            (checkContact.AccountId != null && checkContact.Account.IsPersonAccount == true && checkContact.Account.Email__c == null) ||
            (checkContact.Email == null && checkContact.Account.Email__c == null)
          ) {
            return false;
        } else {
            return true;
        }        
    }

}