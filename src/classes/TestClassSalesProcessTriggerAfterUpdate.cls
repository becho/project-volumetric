/**
* @author 		Mateusz Pruszyński
* @description 	Test class for SalesProcess Trigger After Update
**/
@isTest
private class TestClassSalesProcessTriggerAfterUpdate {

//// Trigger part calculates Current Amount on SalesTarget associated with User which is owner of triggering Preliminary Agreement
//@isTest static void calculatePotentialAmount() {
//	// Prepare sales targets for a specific user
//	User u = TestHelper.createUser(null, true);
//	Manager_Panel__c salesTarget = TestHelper.createManagerPanelSalesTarget(
//		new Manager_Panel__c(
//			User__c = u.Id,
//			Start_Date__c = Date.today() - 3,
//			End_Date__c = Date.today() + 10,
//			Quantitative_Target__c = 10,
//			Value_Target__c = 3000000
//		), 
//		true
//	);

//	// Prepare resources in order to attach thet to various sales process stages to count potential and current amounts on sales targets
//	Resource__c investment = TestHelper.createInvestment(null, true);

//	// Insert flat
//	Resource__c flat = TestHelper.createResourceFlatApartment(				
//		new Resource__c(
//			Investment_Flat__c = investment.Id,
//			Price__c = 1200000
//		), 
//		true
//	);

//		// Insert parking space
//		Resource__c parkingSpace = TestHelper.createResourceParkingSpace(				
//			new Resource__c(
//				Investment_Parking_Space__c = investment.Id,
//				Price__c = 1800
//			), 
//			true
//		);

//	System.runAs(u) {

//		// Insert proposal
//		Sales_Process__c proposal_flat = TestHelper.createSalesProcessProposal(null, true);	

//		// Insert product line - flat
//		Sales_Process__c productLine_flat = TestHelper.createSalesProcessProductLine(
//			new Sales_Process__c(
//				Product_Line_to_Proposal__c = proposal_flat.Id,
//				Contact_from_Offer_Line__c = proposal_flat.Contact__c,
//				Offer_Line_Resource__c = flat.Id,
//				Price_With_Discount__c = flat.Price__c
//			),  
//			true
//		);

//		// Insert proposal - parkingSpace
//		Sales_Process__c proposal_parkingSpace = TestHelper.createSalesProcessProposal(null, true);	

//		// Insert product line - parkingSpace
//		Sales_Process__c productLine_parkingSpace = TestHelper.createSalesProcessProductLine(
//			new Sales_Process__c(
//				Product_Line_to_Proposal__c = proposal_parkingSpace.Id,
//				Contact_from_Offer_Line__c = proposal_parkingSpace.Contact__c,
//				Offer_Line_Resource__c = parkingSpace.Id,
//				Price_With_Discount__c = parkingSpace.Price__c
//			),  
//			true
//		);

//		// Create sale terms for each proposal
//		List<Sales_Process__c> saleTermsList = new List<Sales_Process__c>();
//		saleTermsList.add(
//			TestHelper.createSalesProcessSaleTerms(
//				new Sales_Process__c(
//					Offer__c = proposal_flat.Id,
//					Contact__c = proposal_flat.Contact__c
//				), 
//				false
//			)
//		);
//		saleTermsList.add(
//			TestHelper.createSalesProcessSaleTerms(
//				new Sales_Process__c(
//					Offer__c = proposal_parkingSpace.Id,
//					Contact__c = proposal_parkingSpace.Contact__c
//				), 
//				false
//			)
//		);

//		Test.startTest();
//			insert saleTermsList;
//		Test.stopTest();
//	}

//}


/*Grzegorz Murawinski*/
// Test of class UpdateCustomerFieldOnSaleTerms - check if change Expected Date of signing fire trigger and 
// transfer first name, last name and nip after contract signature
/*
@isTest(SeeAllData=true) static void TestUpdateCustomerFieldOnSaleTermsForIndividualClient() {
// 1 - Create sales process structure
		// Insert investment
		Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flat
		Resource__c flat = TestHelper.createResourceFlatApartment(				
			new Resource__c(
				Investment_Flat__c = investment.Id
			), 
			true
		);

		// Insert parking space
		Resource__c parkingSpace = TestHelper.createResourceParkingSpace(				
			new Resource__c(
				Investment_Parking_Space__c = investment.Id
			), 
			true
		);

		Resource__c[] resources = new Resource__c[] {flat, parkingSpace};

		// Insert Account
		Account personAccount = TestHelper.createAccountIndividualClient(null, true);

		// Insert proposal
        Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        proposal.Contact__c = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id LIMIT 1].Id;
   	    insert proposal;

		// Insert product lines
		List<Sales_Process__c> productLines = new List<Sales_Process__c>();
		for(Resource__c res : resources) {
			productLines.add(TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Product_Line_to_Proposal__c = proposal.Id,
					Contact_from_Offer_Line__c = proposal.Contact__c,
					Offer_Line_Resource__c = res.Id
				),  
				true
			));
		}
		//insert productLines;

		// Insert Sale Terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal.Id,
				Contact__c = proposal.Contact__c,
				Expected_Date_of_Signing__c = Date.today()-1,
				Developer_Agreement_from_Customer_Group__c = proposal.Id 
			), 
			false
		);

		Test.startTest();
			insert saleTerms;

			//Change date of signing
			saleTerms.Date_of_signing__c = Date.today();
			upsert saleTerms;
			
/*			Sales_Process__c saleTerms2Assert = [
			Select Account_from_Customer_Group__r.FirstName, Account_from_Customer_Group__r.LastName, Account_from_Customer_Group__r.NIP__c, Developer_Agreement_from_Customer_Group__c
			from Sales_process__c where Developer_Agreement_from_Customer_Group__c = :saleTerms.Id LIMIT 1
//			];
*//*
			Sales_Process__c saleTerms2Assert = [
			Select First_names__c, Second_names__c, NIP__c, Account_from_Customer_Group__r.NIP__c, Developer_Agreement_from_Customer_Group__c
			from Sales_process__c where Developer_Agreement_from_Customer_Group__c = :saleTerms.Id LIMIT 1
			];
				
		Test.stopTest();
				
		System.assertEquals(personAccount.LastName, saleTerms2Assert.Second_names__c);
		System.assertEquals(personAccount.FirstName, saleTerms2Assert.First_names__c);
		System.assertEquals(personAccount.NIP__c, saleTerms2Assert.NIP__c);
} */




@isTest static void HelpTestCodeGMurawin(){

Contact contc = TestHelper.createContactPerson(null, true);

Account accnt = TestHelper.createAccountIndividualClient(null,true);

Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
	new Sales_Process__c(
		Contact__c = contc.Id,
		Account_from_Customer_Group__c = accnt.Id
		), 
	false);

Test.startTest();

insert proposal;
System.debug('##Proposal equal: ' + proposal);
System.debug('%%Contact: ' + contc);
System.debug('%%Account:' +accnt);
Sales_Process__c proposal2Assert = [
SELECT Contact__r.LastName, Contact__r.FirstName from Sales_Process__c where Id = :proposal.Id ];
System.assertEquals(contc.LastName, proposal2Assert.Contact__r.LastName);

Test.stopTest();
}

}