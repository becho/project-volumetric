/**
* @author       Mateusz Wolak-Książek
* @description  Test class for DocumentTemplateMethodsSemeko (tokens for documents).
**/ 
@isTest
private class TestClassDocumentTemplateMethodsSemeko {

	static Integer numberOfBuildings = 2;
	static Integer daysToAdd = 10;
	static String buildingDescriptionText = 'Test building description text';
	static String streetName = 'Random Street';
	static String randomCity = 'Random City';
	static String randomPostalCode = '50-270';
	static String randomName = 'Random_name';
	static String bankNumber = '08154010304189103961516143';
	static Date todayPlusDaysToAdd = Date.today().addDays(daysToAdd);
	static Date todayPlusDaysToAddDoubled = Date.today().addDays(daysToAdd * 2);
	static Date todayPlus7Months = Date.today().addMonths(7);
	static String randomDescriptionText = 'This is a random description text';
	static Decimal randomPrice = 12345;
	static Decimal randomArea = 77.42;

	static String getDateToCompare(Date dateToCompare) {
		return String.valueOf(dateToCompare.day()) + '-' 
		+ String.valueOf(dateToCompare.month()) + '-' 
		+ String.valueOf(dateToCompare.year());		
	}

//getting same date format (whenever month is from 1 till 9 there is '0' before)
	static String getReplacedDate(String dateToReplace) {
		return dateToReplace.replace('-0', '-');
	}

	static String convertMetersPerHectare(Decimal area) {
		return String.valueOf(area/10000); 
	}

		

	@testSetup
	static void createData() {

		enxoodocgen__Document_Template__c templ = new enxoodocgen__Document_Template__c();
        templ.enxoodocgen__Source_Id__c = '';
        templ.enxoodocgen__Object_Name__c = 'Sales_Process__c';
        templ.enxoodocgen__Document_Name_Pattern__c = '#NAME#_#YYYY#/#MM#/#DD#';
        templ.enxoodocgen__Active__c = true;
        templ.enxoodocgen__Conversion_to_DOCX__c = true;
        templ.enxoodocgen__Conversion_to_PDF__c = false;
        templ.Name = 'TestTemplate';
        insert templ;

        List<Sales_Process__c> saleProcesses = new List<Sales_Process__c>();
		saleProcesses.add(TestHelper.createSalesProcessProductLine(null, false));
		saleProcesses.add(
			TestHelper.createSalesProcessSaleTerms(
				new Sales_Process__c(
					Contact__c = saleProcesses[0].Contact_from_Offer_Line__c,
					Offer__c = saleProcesses[0].Product_Line_to_Proposal__c
				),
				false
			)
		);
		insert saleProcesses;

	}



	@isTest static void singleBuilding_testMethods() {
		/*
		PREPARE DATA - START
		//SINGLE BUILDING
		*/

		Test.startTest();

			Resource__c building = TestHelper.createResourceBuilding(null, true);
			List<Sales_Process__c> saleProcesses = new List<Sales_Process__c>();

			Sales_Process__c productLine =[
					SELECT Id, Contact_from_Offer_Line__c, Product_Line_to_Proposal__r.Contact__c,
						Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Building__c
					FROM Sales_Process__c
					WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
					LIMIT 1
			];

			Sales_Process__c saleTerm = [
					SELECT Id
					FROM Sales_Process__c
					WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
					LIMIT 1
			];

			Resource__c flat = [
				SELECT Building__c
				FROM Resource__c
				WHERE Id =: productLine.Offer_Line_Resource__c
				AND RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
			];

			building.Investment_Building__c = productLine.Offer_Line_Resource__r.Investment_Flat__c;
			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;

			building.Deadline_Of_Arrangement_Changes__c = todayPlusDaysToAdd;
			building.Deadline_of_Electric_Or_Hydraulics_Chang__c = todayPlusDaysToAdd;
			building.Building_Description_Document__c = buildingDescriptionText;
			building.Street__c = streetName;
			building.House_Number__c = String.valueOf(daysToAdd);
			building.Building_Number__c = daysToAdd;
			building.Description_of_Major_Features__c = randomDescriptionText;
			building.Completion_Date__c = todayPlusDaysToAdd;
			building.Building_Garage_Equipment_Description__c = randomDescriptionText;
			building.Building_Storage_Equipment_Description__c = randomDescriptionText;

			flat.Building__c = building.Id;

			saleProcesses.add(productLine);
			saleProcesses.add(saleTerm);
			update flat;
			update building;
			update saleProcesses;

			Sales_Process__c pl = [
				SELECT Id, Offer_Line_Resource__r.Building__c, Offer_Line_Resource__r.Building__r.Name
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
			];
		
			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			/*
			PREPARE DATA - END
			//SINGLE BUILDING
			*/

			String allBuildings = DocumentTemplateMethodsSemeko.getAllBuildings(template.Id, saleTerm.Id);
			String deadlineArragmentChanges = DocumentTemplateMethodsSemeko.getDeadlineArragmentChanges(template.Id, saleTerm.Id);
			String deadlineHydEleChanges = DocumentTemplateMethodsSemeko.getDeadlineHydEleChanges(template.Id, saleTerm.Id);
			String buildingDescription = DocumentTemplateMethodsSemeko.getBuildingDescription(template.Id, saleTerm.Id);
			String buildingStreet = DocumentTemplateMethodsSemeko.getBuildingStreet(template.Id, saleTerm.Id);
			String buildingNumber = DocumentTemplateMethodsSemeko.getBuildingNumber(template.Id, saleTerm.Id);
			String descriptionOfMajorFeatures = DocumentTemplateMethodsSemeko.getDescriptionOfMajorFeatures(template.Id, saleTerm.Id);
			String inspectInfo = DocumentTemplateMethodsSemeko.getInspectInfo(template.Id, saleTerm.Id);
			String linkedInspectInfo = DocumentTemplateMethodsSemeko.getLinkedInspectInfo(template.Id, saleTerm.Id);
			String buildingCompletionDate = DocumentTemplateMethodsSemeko.getBuildingCompletionDate(template.Id, saleTerm.Id);
			String buildingGarageEquipmentDescription = DocumentTemplateMethodsSemeko.getBuildingGarageEquipmentDescription(template.Id, saleTerm.Id);
			String buildingStorageEquipmentDescription = DocumentTemplateMethodsSemeko.getBuildingStorageEquipmentDescription(template.Id, saleTerm.Id);


		Test.stopTest();
		
		System.assertEquals(allBuildings, building.Name);
		
		System.assertEquals(getReplacedDate(deadlineArragmentChanges), getDateToCompare(todayPlusDaysToAdd));
		System.assertEquals(getReplacedDate(deadlineHydEleChanges), getDateToCompare(todayPlusDaysToAdd));

		System.assertEquals(buildingDescription, building.Building_Description_Document__c);
		System.assertEquals(buildingStreet, building.Street__c + ' ' + String.valueOf(building.House_Number__c));
		System.assertEquals(buildingNumber, String.valueOf(building.Building_Number__c));
		System.assertEquals(descriptionOfMajorFeatures, building.Description_of_Major_Features__c);
		System.assertEquals(buildingCompletionDate, String.valueOf(todayPlusDaysToAdd));
		System.assertEquals(buildingGarageEquipmentDescription, randomDescriptionText);
		System.assertEquals(buildingStorageEquipmentDescription, randomDescriptionText);

	}

	@isTest static void moreBuildings_testMethods() {

		Test.startTest();

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			List<Resource__c> buildings = new List<Resource__c>();
			
			for(Integer i = 0 ; i < numberOfBuildings; i++) {
				buildings.add(
					TestHelper.createResourceBuilding(
						new Resource__c(
							Name = randomName + '_' + i
						), 
						false
					)
				); 
			}
			insert buildings;

			Sales_Process__c secondProductLine = TestHelper.createSalesProcessProductLine(null, true);
			
			List<Sales_Process__c> productLines = [
				SELECT Id, Product_Line_to_Proposal__c, Offer_Line_Resource__c, 
					Offer_Line_Resource__r.Investment_Flat__c, Product_Line_to_Proposal__r.Contact__c,
					Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
			];

			for(Sales_Process__c line : productLines) {
				if(line.Id != secondProductLine.Id) {
					secondProductLine.Product_Line_to_Proposal__c = line.Product_Line_to_Proposal__c;
					secondProductLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
					secondProductLine.Contact_from_Offer_Line__c = secondProductLine.Product_Line_to_Proposal__r.Contact__c;
					break;
				}
			}
			
			for(Integer i = 0; i < buildings.size(); i++ ) {
				buildings[i].Investment_Building__c = productLines[i].Offer_Line_Resource__r.Investment_Flat__c;
			}

			List<Resource__c> flats = [
				SELECT Building__c
				FROM Resource__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)	
			];

			for(Integer i = 0 ; i < flats.size(); i++) {
				if(flats.size() == buildings.size()) { 
					flats[i].Building__c = buildings[i].Id;
				} else {
					flats[i].Building__c = buildings[0].Id;
				}
			}

			buildings[0].Deadline_Of_Arrangement_Changes__c = todayPlusDaysToAdd;
			buildings[0].Deadline_of_Electric_Or_Hydraulics_Chang__c = todayPlusDaysToAdd;
			buildings[1].Deadline_Of_Arrangement_Changes__c = todayPlusDaysToAddDoubled;
			buildings[1].Deadline_of_Electric_Or_Hydraulics_Chang__c = todayPlusDaysToAddDoubled;

			update flats;
			update buildings;
			update secondProductLine;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];


			String allBuildings = DocumentTemplateMethodsSemeko.getAllBuildings(template.Id, saleTerm.Id);
			String deadlineArragmentChanges = DocumentTemplateMethodsSemeko.getDeadlineArragmentChanges(template.Id, saleTerm.Id);
			String deadlineHydEleChanges = DocumentTemplateMethodsSemeko.getDeadlineHydEleChanges(template.Id, saleTerm.Id);


		Test.stopTest();

		System.assertEquals(allBuildings.contains(buildings[0].Name), allBuildings.contains(buildings[0].Name));
		System.assert(deadlineArragmentChanges.contains(buildings[0].Name));
		System.assert(deadlineHydEleChanges.contains(buildings[1].Name));
					
	}


	@isTest static void investor_testMethods() {

		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Investment_Flat__r.Account__c,
					Offer_Line_Resource__r.InvestmentId__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;
			//productLine.Offer_Line_Resource__r.InvestmentId__c = productLine.Offer_Line_Resource__r.Investment_Flat__c; 

			Account investor = [
				SELECT Name, BillingCity, BillingPostalCode, BillingStreet, KRS__c, Regon__c
				FROM Account
				WHERE Type =: CommonUtility.ACCOUNT_TYPE_PICKLIST_DEVELOPER
				AND Id =: productLine.Offer_Line_Resource__r.Investment_Flat__r.Account__c
			];

			investor.Name = randomName;
			investor.BillingCity = randomCity;
			investor.BillingPostalCode = randomPostalCode;
			investor.BillingStreet = streetName;
			investor.KRS__c = '0000012345';	 	//10 digits - string
			investor.Regon__c = 123456789; 	//9 digits - decimal

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			update investor;
			update productLine;

			String investorDestination = DocumentTemplateMethodsSemeko.getInvestorDestination(template.Id, saleTerm.Id);
			String investorName = DocumentTemplateMethodsSemeko.getInvestorName(template.Id, saleTerm.Id);
			String investorCity = DocumentTemplateMethodsSemeko.getInvestorCity(template.Id, saleTerm.Id);
			String investorKRS = DocumentTemplateMethodsSemeko.getInvestorKRS(template.Id, saleTerm.Id);
			String investorInfo = DocumentTemplateMethodsSemeko.getInvestorInfo(template.Id, saleTerm.Id);

		Test.stopTest();

		System.assert(investorDestination.contains(streetName));
		System.assertEquals(investorName, investor.Name);
		System.assertEquals(investorCity, randomCity);
		System.assertEquals(investorKRS.length(), 10);
		System.assertEquals(investorKRS, investor.KRS__c);
		System.assert(investorInfo.contains(String.valueOf(investor.Regon__c)));

	}


	@isTest static void investment_testMethods() {

		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;

			Resource__c investment = TestHelper.createInvestment(
				new Resource__c(
					Name = randomName,
					City__c = randomCity,
					Street__c = streetName,
					House_Number__c = String.valueOf(15),
					Investment_Bank__c = randomName,
					Current_Bank_Account__c = bankNumber,
					Building_Permit_Number__c = String.valueOf(Math.random()),
					Building_Permit_Authorization_Date__c = todayPlusDaysToAdd,
					Planned_Construction_End_Date__c = todayPlusDaysToAddDoubled,
					Date_of_Placing_Investment_into_Service__c = Date.today()			
				), 
				true
			);

			productLine.Offer_Line_Resource__r.Investment_Flat__c = investment.Id;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			update productLine;
			update productLine.Offer_Line_Resource__r;

			String investmentBankName = DocumentTemplateMethodsSemeko.getInvestmentBankName(template.Id, saleTerm.Id);
			String investmentBankNumber = DocumentTemplateMethodsSemeko.getInvestmentBankNumber(template.Id, saleTerm.Id);
			String investmentName = DocumentTemplateMethodsSemeko.getInvestmentName(template.Id, saleTerm.Id);
			String investmentCity = DocumentTemplateMethodsSemeko.getInvestmentCity(template.Id, saleTerm.Id);
			String ivestmentPermitDate = DocumentTemplateMethodsSemeko.getInvestmentPermitDate(template.Id, saleTerm.Id);
			String investmentPermitNumber = DocumentTemplateMethodsSemeko.getInvestmentPermitNumber(template.Id, saleTerm.Id);
			String plannedConstructionEndDate = DocumentTemplateMethodsSemeko.getPlannedConstructionEndDate(template.Id, saleTerm.Id);
			String dateOfPlacingInvestmentIntoService = DocumentTemplateMethodsSemeko.getDateOfPlacingInvestmentIntoServiceAndAdding7Months(template.Id, saleTerm.Id);
			String investmentStreetAddress = DocumentTemplateMethodsSemeko.getInvestmentStreetAddress(template.Id, saleTerm.Id);

			Test.stopTest();

			System.assertEquals(investmentBankName, investment.Investment_Bank__c);
			System.assertEquals(investmentBankNumber, investment.Current_Bank_Account__c);
			System.assertEquals(investmentName, investment.Name);
			System.assertEquals(investmentCity, investment.City__c);
			System.assertEquals(getReplacedDate(ivestmentPermitDate), getDateToCompare(todayPlusDaysToAdd));
			System.assertEquals(investmentPermitNumber, investment.Building_Permit_Number__c);
			System.assertEquals(getReplacedDate(plannedConstructionEndDate), getDateToCompare(todayPlusDaysToAddDoubled));
			System.assertEquals(getReplacedDate(dateOfPlacingInvestmentIntoService), getDateToCompare(todayPlus7Months));
			System.assert(investmentStreetAddress.contains(investment.Street__c));
			System.assert(investmentStreetAddress.contains(investment.House_Number__c));


	}

	@isTest static void stage_testMethods() {

		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Stage__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;

			String textAreaLong = '';
			for(Integer i = 0 ; i < daysToAdd ; i++) {
				textAreaLong += randomDescriptionText;
			}

			Resource__c stage =  
				new Resource__c(
					Name = randomName,
					RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE),
					Plot_Numbers__c =  '1/23; 1/43',
					Land_Register_Court__c =  'Sąd KW w Warszawie',
					Common_Surface__c = 250000,
					Property_Description__c = randomDescriptionText,
					Investment_Description__c = textAreaLong,
					Building_Approval_Description__c = randomDescriptionText,
					Start_Date_of_Construction_Works__c = 'październiku ' + String.valueOf(Date.today().year()),
					Free_of_encumbrances__c = randomDescriptionText

				);
			insert stage;
			productLine.Offer_Line_Resource__r.Stage__c = stage.Id; 

			update productLine;
			update productLine.Offer_Line_Resource__r;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			String stagesInfo = DocumentTemplateMethodsSemeko.getStagesInfo(template.Id, saleTerm.Id);
			String stagesInfos = DocumentTemplateMethodsSemeko.getStagesInfos(template.Id, saleTerm.Id);
			String stageConstructionNumber = DocumentTemplateMethodsSemeko.getStageConstructionNumber(template.Id, saleTerm.Id);
			String propertyDescriptionFromStage = DocumentTemplateMethodsSemeko.getPropertyDescriptionFromStage(template.Id, saleTerm.Id);
			String investmentDescriptionFromStage = DocumentTemplateMethodsSemeko.getInvestmentDescriptionFromStage(template.Id, saleTerm.Id);
			String buildingApprovalDescriptionFromStage = DocumentTemplateMethodsSemeko.getBuildingApprovalDescriptionFromStage(template.Id, saleTerm.Id);
			String startDateOfConstructionWorks = DocumentTemplateMethodsSemeko.getStartDateOfConstructionWorks(template.Id, saleTerm.Id);
			String freeOfEncumbrancesTextFromStage = DocumentTemplateMethodsSemeko.getFreeOfEncumbrancesTextFromStage(template.Id, saleTerm.Id);
			String plotNumbersFromStage = DocumentTemplateMethodsSemeko.getPlotNumbersFromStage(template.Id, saleTerm.Id);
			String plotInfo = DocumentTemplateMethodsSemeko.getPlotInfo(template.Id, saleTerm.Id);
			String stagesLandRegisterCourt = DocumentTemplateMethodsSemeko.getStagesLandRegisterCourt(template.Id, saleTerm.Id);


		Test.stopTest();

		String[] splittedPlotNumber = stage.Plot_Numbers__c.split(';');

		System.assert(stagesInfo.contains(convertMetersPerHectare(stage.Common_Surface__c)));
		System.assert(stagesInfo.contains(randomName));
		System.assertNotEquals(stagesInfo, stagesInfos);
		for(String plotNumber : splittedPlotNumber) {
			System.assert(stagesInfos.contains(plotNumber));
		}
		System.assertEquals(stageConstructionNumber, stage.Name);
		System.assertEquals(propertyDescriptionFromStage, stage.Property_Description__c);
		System.assertEquals(investmentDescriptionFromStage, textAreaLong);
		System.assertEquals(buildingApprovalDescriptionFromStage, randomDescriptionText);
		System.assert(startDateOfConstructionWorks.contains(String.valueOf(Date.today().year())));
		System.assert(freeOfEncumbrancesTextFromStage.contains(randomDescriptionText));
		System.assert(plotNumbersFromStage.contains(','));
		System.assertNotEquals(plotNumbersFromStage, stage.Plot_Numbers__c);
		System.assert(!(plotInfo.contains(stage.Plot_Numbers__c)));
		System.assertEquals(stagesLandRegisterCourt, stage.Land_Register_Court__c);


	}

	@isTest static void customer_testMethods() {

		Test.startTest();

			Sales_Process__c productLine = [
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c, Product_Line_to_Proposal__r.Contact__r.PESEL__c,
					Offer_Line_Resource__r.Investment_Flat__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;
			

			Sales_Process__c process = TestHelper.createSalesProcessCustomerGroup(
				new Sales_Process__c(
					Proposal_from_Customer_Group__c = productLine.Product_Line_to_Proposal__c
				)
				, true
			);

			process = [ 
				SELECT Id, Developer_Agreement_from_Customer_Group__c, Share_Type__c, Account_from_Customer_Group__r.BillingCity,
					Account_from_Customer_Group__r.BillingStreet 
				FROM Sales_Process__c 
				WHERE Id =: process.Id
			];
			process.Developer_Agreement_from_Customer_Group__c = saleTerm.Id;
			String nipToCheck = String.valueOf(TestHelper.createNIP());
			process.Account_from_Customer_Group__r.Name = randomName;
			process.Account_from_Customer_Group__r.NIP__c = nipToCheck;
			process.Account_from_Customer_Group__r.BillingCity = randomCity;
			process.Account_from_Customer_Group__r.BillingStreet = streetName;
			process.After_sales_from_Customer_Group__c = saleTerm.Id;
			update process;

			update productLine;
			update process.Account_from_Customer_Group__r;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			String allCustomersDataModified = DocumentTemplateMethodsSemeko.getAllCustomersDataModified(template.Id, saleTerm.Id);
			String allCustomersFromAfterSales = DocumentTemplateMethodsSemeko.getAllCustomersFromAfterSales(template.Id, saleTerm.Id);

		Test.stopTest();

		System.assert(allCustomersDataModified.contains(Label.NoValue));
		System.assert(!String.isEmpty(allCustomersFromAfterSales));
		System.assertEquals(allCustomersFromAfterSales, randomName);

	}


	@isTest static void parkingSpace_testMethods() {


		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Stage__c, Price_With_Discount__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;



			Resource__c parkingSpace = new Resource__c(
				Status__c = 'Active',
				Storey__c = String.valueOf(daysToAdd),
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
			);
			insert parkingSpace;

			productLine.Offer_Line_Resource__c = parkingSpace.Id;
			productLine.Price_With_Discount__c = randomPrice;
			productLine.Bank_Account_Number__c = bankNumber;

			update productLine;
			//update productLine.Offer_Line_Resource__r;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];


			String parkingSpaceDiscountPriceInWords = DocumentTemplateMethodsSemeko.getParkingSpaceDiscountPriceInWords(template.Id, saleTerm.Id);
			String parkingSpaceArea = DocumentTemplateMethodsSemeko.getParkingSpaceArea(template.Id, saleTerm.Id);
			String parkingSpaceStorey = DocumentTemplateMethodsSemeko.getParkingSpaceStorey(template.Id, saleTerm.Id);

			parkingSpace.Parking_Space_Type__c = CommonUtility.RESOURCE_PARKING_SPACE_TYPE_MOTO;
			update parkingSpace;

			String parkingSpaceMotoDiscountPriceInWords = DocumentTemplateMethodsSemeko.getParkingSpaceMotoDiscountPriceInWords(template.Id, saleTerm.Id);

			parkingSpace.Parking_Space_Type__c = CommonUtility.RESOURCE_PARKING_SPACE_TYPE_BICECLES;
			update parkingSpace;

			String parkingSpaceBicycleDiscountPriceInWords = DocumentTemplateMethodsSemeko.getParkingSpaceBicycleDiscountPriceInWords(template.Id, saleTerm.Id);

		Test.stopTest();


		System.assertEquals(parkingSpaceDiscountPriceInWords, NumberToWords.convert(randomPrice, 'Polish', 'PL'));
		System.assertEquals(parkingSpaceMotoDiscountPriceInWords, NumberToWords.convert(randomPrice, 'Polish', 'PL'));
		System.assertEquals(parkingSpaceBicycleDiscountPriceInWords, NumberToWords.convert(randomPrice, 'Polish', 'PL'));
		System.assert(parkingSpaceArea.contains('0.00'));
		System.assertEquals(parkingSpaceStorey, String.valueOf(daysToAdd));


	}



	@isTest static void storage_testMethods() {


		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Stage__c, Price_With_Discount__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;


			Resource__c storage = new Resource__c(
				Name = randomName,
				Status__c = 'active',
				RecordTypeId = CommonUtility.getRecordTypeId('Resource__c', CommonUtility.RESOURCE_TYPE_STORAGE) 
			);
			insert storage;

			productLine.Offer_Line_Resource__c = storage.Id;
			productLine.Price_With_Discount__c = randomPrice;

			update productLine;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];


			String allStorages = DocumentTemplateMethodsSemeko.getAllStorages(template.Id, saleTerm.Id);
			String storageArea = DocumentTemplateMethodsSemeko.getStorageArea(template.Id, saleTerm.Id);
			String storageDiscountPrice = DocumentTemplateMethodsSemeko.getStorageDiscountPrice(template.Id, saleTerm.Id);
			String storageDiscountPriceInWords = DocumentTemplateMethodsSemeko.getStorageDiscountPriceInWords(template.Id, saleTerm.Id);

		Test.stopTest();

		System.assertEquals(allStorages, storage.Name);
		System.assert(storageArea.contains('0.00'));
		System.assert(storageDiscountPrice.contains(String.valueOf(randomPrice)));
		System.assertEquals(storageDiscountPriceInWords, NumberToWords.convert(randomPrice, 'Polish', 'PL'));

	}


	@isTest static void commercialProperty_testMethods() {


		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Stage__c, Price_With_Discount__c, Offer_Line_to_After_sales_Service__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;

			Resource__c commercialProperty = new Resource__c(
				Name = randomName,
				Status__c = 'active',
				City__c = randomCity,
				ZIP_Code__C = randomPostalCode,
				Street__c = streetName,
				House_Number__c = String.valueOf(15),
				RecordTypeId = CommonUtility.getRecordTypeId('Resource__c', CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY)
			);

			insert commercialProperty;

			productLine.Offer_Line_Resource__c = commercialProperty.Id;
			productLine.Offer_Line_to_After_sales_Service__c = saleTerm.Id;
			productLine.Price_With_Discount__c = randomPrice;

			update productLine;


			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];


			String allCommercialProperties = DocumentTemplateMethodsSemeko.getAllCommercialProperties(template.Id, saleTerm.Id);
			String commercialPropertyArea = DocumentTemplateMethodsSemeko.getCommercialPropertyArea(template.Id, saleTerm.Id);
			String commercialPropertyDiscountPrice = DocumentTemplateMethodsSemeko.getCommercialPropertyDiscountPrice(template.Id, saleTerm.Id);
			String commercialPropertyDiscountPriceInWords = DocumentTemplateMethodsSemeko.getCommercialPropertyDiscountPriceInWords(template.Id, saleTerm.Id);
			String premisesType = DocumentTemplateMethodsSemeko.getPremisesType(template.Id, saleTerm.Id);
			String premisesAddress = DocumentTemplateMethodsSemeko.getPremisesAddress(template.Id, saleTerm.Id);

		Test.stopTest();

		System.assertEquals(allCommercialProperties, commercialProperty.Name);
		System.assert(commercialPropertyArea.contains('0.00'));
		System.assert(commercialPropertyDiscountPrice.contains(String.valueOf(randomPrice)));
		System.assertEquals(commercialPropertyDiscountPriceInWords, NumberToWords.convert(randomPrice, 'Polish', 'PL'));
		System.assertEquals(premisesType, 'lokalu użytkowego');
		System.assert(premisesAddress.contains(randomPostalCode));
		System.assert(premisesAddress.contains(Label.NoValue));

	}

	@isTest static void appartment_testMethods() {

		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Stage__c, Price_With_Discount__c, Offer_Line_to_After_sales_Service__c, Offer_Line_Resource__r.Flat_number__c, Offer_Line_Resource__r.Area__c, Offer_Line_Resource__r.Bank_Name__c, Offer_Line_Resource__r.Bank_Account_Number__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			];

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;


			update productLine;

			productLine.Offer_Line_Resource__r.Flat_number__c = String.valueOf(daysToAdd);
			productLine.Offer_Line_Resource__r.Bank_Name__c = randomName;
			productLine.Offer_Line_Resource__r.Bank_Account_Number__c = bankNumber;
			update productLine.Offer_Line_Resource__r;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];

			String flatNumber = DocumentTemplateMethodsSemeko.getFlatNumber(template.Id, saleTerm.Id);
			String flatConstructionNumber = DocumentTemplateMethodsSemeko.getFlatConstructionNumber(template.Id, saleTerm.Id);
			String flatPriceDiscountModified = DocumentTemplateMethodsSemeko.getFlatPriceDiscountModified(template.Id, saleTerm.Id);
			String flatAreaModified = DocumentTemplateMethodsSemeko.getFlatAreaModified(template.Id, saleTerm.Id);
			String resourcesBankAccountName = DocumentTemplateMethodsSemeko.getResourcesBankAccountName(template.Id, saleTerm.Id);
			String resourcesBankAccountNumber = DocumentTemplateMethodsSemeko.getResourcesBankAccountNumber(template.Id, saleTerm.Id);


		Test.stopTest();

		System.assertEquals(flatNumber, String.valueOf(daysToAdd));
		System.assert(!String.isEmpty(flatConstructionNumber));
		System.assertEquals(flatAreaModified, String.valueOf(productLine.Offer_Line_Resource__r.Area__c));
		System.assertEquals(resourcesBankAccountName, randomName);
		System.assertEquals(resourcesBankAccountNumber, bankNumber);
	}


	@isTest static void changes_testMethods() {

		Test.startTest();

			Sales_Process__c productLine =[
				SELECT Id, Contact_from_Offer_Line__c, Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__r.Contact__c,
					Offer_Line_Resource__r.Investment_Flat__c, Offer_Line_Resource__r.Stage__c, Price_With_Discount__c, Offer_Line_to_After_sales_Service__c
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				LIMIT 1
			]; 

			Sales_Process__c saleTerm = [
				SELECT Id
				FROM Sales_Process__c
				WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1
			];

			productLine.Offer_Line_to_Sale_Term__c = saleTerm.Id;
			productLine.Contact_from_Offer_Line__c = productLine.Product_Line_to_Proposal__r.Contact__c;

			update productLine;

			Sales_Process__c afterSale =  TestHelper.createSalesProcessAfterSalesService(
				new Sales_Process__c(
					Agreement__c = saleTerm.Id
				),
				true
			);


			Extension__c change = TestHelper.createExtensionChange(
				new Extension__c(
					Resource__c = productLine.Offer_Line_Resource__c,
					After_sales_Service__c = afterSale.Id
				),
				true
			);

			afterSale.Hidden_Change_Id__c = change.Id;
			afterSale.Agreement_Number__c = String.valueOf(daysToAdd);
			update afterSale;

			change.Quotation__c = randomPrice;
			update change;

			enxoodocgen__Document_Template__c template = [
				SELECT Id
				FROM enxoodocgen__Document_Template__c
				WHERE enxoodocgen__Object_Name__c =: 'Sales_Process__c'
			];


			String todaysDate = DocumentTemplateMethodsSemeko.getTodaysDate(template.Id, afterSale.Id);
			String changesNumber = DocumentTemplateMethodsSemeko.getChangesNumber(template.Id, afterSale.Id);
			String agreementNumberFromAfterSales = DocumentTemplateMethodsSemeko.getAgreementNumberFromAfterSales(template.Id, afterSale.Id);
			List<List<String>> elementChangesNegative = DocumentTemplateMethodsSemeko.getElementChangesNegative(afterSale.Id);
			List<List<String>> elementChangesPositive = DocumentTemplateMethodsSemeko.getElementChangesPositive(afterSale.Id);
			String totalValueNegativeChanges = DocumentTemplateMethodsSemeko.getTotalValueNegativeChanges(template.Id, afterSale.Id);
			String totalValuePositiveChanges = DocumentTemplateMethodsSemeko.getTotalValuePositiveChanges(template.Id, afterSale.Id);
			List<List<String>> elementChangesPositiveToCompare = DocumentTemplateMethodsSemeko.executeMethod(template.Id, afterSale.Id, 'getElementChangesPositive');
			String changesQuotation = DocumentTemplateMethodsSemeko.getChangesQuotation(template.Id, afterSale.Id);
			String premisesTotalValue = DocumentTemplateMethodsSemeko.getPremisesTotalValue(template.Id, afterSale.Id);


		Test.stopTest();

		System.assertEquals(getReplacedDate(todaysDate), getDateToCompare(Date.today()));
		System.assertEquals(changesNumber, change.Name);
		System.assertEquals(agreementNumberFromAfterSales, String.valueOf(afterSale.Agreement_Number__c));
		System.assertEquals(elementChangesPositiveToCompare, elementChangesPositive);
		System.assertEquals(changesQuotation.substring(0, changesQuotation.length() - 3), String.valueOf(change.Quotation__c));
		System.assert(!String.isEmpty(premisesTotalValue));


	}

}