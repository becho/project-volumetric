/**
* @author       Mateusz Wolak-Książek
* @description  Test class for ResourceOffering
**/ 
@isTest
private class TestClassResourceOffering {

	private static Decimal randomPrice = 12345.6;
	private static Decimal randomArea = 55.5;


	@isTest static void testCreatingOfferWithoutReservation() {

		Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);

		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c(
				Price__c = randomPrice,
				Usable_Area_Planned__c = randomArea,
				Total_Area_Planned__c = randomArea,
				Promotion_IsActive__c = false
			)
			, true
		);


		Test.startTest();

			ApexPages.currentPage().getParameters().put(CommonUtility.URL_PARAM_PROPOSALID, productLine.Product_Line_to_Proposal__c);

			ResourceOffering ro = new ResourceOffering();

			List<String> offers = ResourceOffering.massCreateOffer( new String[]{flat.Id}, productLine.Product_Line_to_Proposal__r.Contact__c);


		Test.stopTest();

		Sales_Process__c newProductLine = [
			SELECT Product_Line_to_Proposal__r.Name, Product_Line_to_Proposal__c
			FROM Sales_Process__c
			WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
			AND Id =: productLine.Id
		];

			System.assertEquals(offers[0], CommonUtility.BOOLEAN_TRUE);
			System.assert(offers[1].contains('<a href="'));
		
		} 


		@isTest static void testCreatingOfferWithAlreadyReserved() {

			Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);


		Test.startTest();

			ApexPages.currentPage().getParameters().put(CommonUtility.URL_PARAM_PROPOSALID, productLine.Product_Line_to_Proposal__c);

			ResourceOffering ro = new ResourceOffering();

			List<String> offers = ResourceOffering.massCreateOffer( new String[]{productLine.Offer_Line_Resource__c}, productLine.Contact_from_Offer_Line__c);


		Test.stopTest();

		System.assertEquals(offers[0], CommonUtility.BOOLEAN_FALSE);


		}
	
 
	
}