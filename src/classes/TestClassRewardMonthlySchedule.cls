/**
* @author       Mariia Dobzhanska
* @description  test class for RewardMonthlySchedule Scheduler, which executes the RewardMonthlyBatch
**/
@isTest
private class TestClassRewardMonthlySchedule {

   // CRON expression: midnight on March 15.
   // Because this is a test, job executes
   // immediately after Test.stopTest().
   public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
 //  static testmethod void test() {
 //  		//Creating a user (sales representative) for testing
 //       User testusertempl = new User (FirstName = 'Test', LastName = 'User');
 //     	User testuser = TestHelper.createUser(testusertempl, true);
        
 //       //Creating the resource for testing, the resource must have the value for the Resource_Commission_Rate__c field
 //       Resource__c restempl = new Resource__c(Resource_Commission_Rate__c = 1);
 //       Resource__c testresource = TestHelper.createResourceFlatApartment(restempl, true);
	//	List<Sales_Process__c> testsps = new List<Sales_Process__c>();
        
 //       //Creating the Product line for testing (and Proposal)
 //       Sales_Process__c prlinetemp = new Sales_Process__c(Offer_Line_Resource__c = testresource.Id);
 //       Sales_Process__c testprline = TestHelper.createSalesProcessProductLine(prlinetemp, false);
 //       testprline.Price_With_Discount__c = 250000;
        
 //       //Creating the Sales Terms for testing with the Proposal created while Product line creating
 //       Sales_Process__c stermtemp = new Sales_Process__c(Offer__c = testprline.Product_Line_to_Proposal__c);
 //       Sales_Process__c testsalesterms = TestHelper.createSalesProcessSaleTerms(stermtemp, false);

 //       //testsalesterms.Date_of_signing__c = Date.newInstance(Date.today().year(), Date.today().month() - 1 , Date.today().day()-3);
 //       testsalesterms.Reward_Taken__c = false; 
 //       testsalesterms.Agreement_Value__c = testprline.Price_With_Discount__c;
 //       testsalesterms.OwnerId = testuser.Id;

 //       testprline.Offer_Line_to_Sale_Term__c = testsalesterms.Id;

	//	List<Sales_Process__c> sps = new List<Sales_Process__c>{testsalesterms, testprline};
 //       insert sps;

 //    	Test.startTest();
	//		testsalesterms.Status__c = CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER;
	//		testsalesterms.Date_of_signing_Reservation__c = Date.newInstance(Date.today().year(), Date.today().month() - 1 , Date.today().day()-4);
	//		update testsalesterms;
			
	//		testsalesterms.Date_of_signing__c = Date.newInstance(Date.today().year(), Date.today().month() - 1 , Date.today().day()-3);
	//		update testsalesterms;
	//		//Creating the Payment for testing
	//		Payment__c paymtemp = new Payment__c(Agreements_Installment__c = testsalesterms.Id);
	//		Payment__c testpaym = TestHelper.createPaymentsInstallment(paymtemp, false);
	//		testpaym.Payment_For__c = testresource.Id;
	//		testpaym.Paid_Value__c = testprline.Price_With_Discount__c;
	//		testpaym.Payment_date__c = Date.newInstance(Date.today().year(), Date.today().month() - 1 , Date.today().day()+1);
	//		insert testpaym;

	//		//Creating Manager Panel Reward Threshold for test
	//		Manager_Panel__c testpanrewth = TestHelper.createManagerPanelRewardThreshold(null, false);
	//		testpanrewth.Threshold_From__c = 150000;
	//		testpanrewth.Threshold_To__c = 400000;
	//		testpanrewth.Commision_Value__c = 1;
	//		testpanrewth.Active__c = true;
	//		insert testpanrewth;
	//		//Creating the Manager panel Sales Representative for the test user
	//		Manager_Panel__c templsppanel = new Manager_Panel__c(User__c = testuser.Id, Name = 'TestPanelSalesRep');
	//		Manager_Panel__c testsalesreppanel = TestHelper.createManagerPanelSalesRepresentative(templsppanel, false);
	//		testsalesreppanel.User__c = testuser.Id;
	//		insert  testsalesreppanel;
	//		//Creating the Manager Panel Reps Reward for the test user
	//		Manager_Panel__c testreprewpanel = TestHelper.createManagerPanelRepsReward(null, false);
	//		testreprewpanel.Sales_Representative__c = testsalesreppanel.Id;
	//		testreprewpanel.Threshold__c = testpanrewth.Id;
	//		insert testreprewpanel;
	//		// Schedule the test job
	//		String jobId = System.schedule('ScheduleApexClassTest',
	//		        CRON_EXP, 
	//		        new RewardMonthlySchedule());

	//		// Get the information from the CronTrigger API object
	//		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
	//		NextFireTime
	//		FROM CronTrigger WHERE id = :jobId];

	//		// Verify the expressions are the same
	//		System.assertEquals(CRON_EXP, 
	//		ct.CronExpression);

	//		// Verify the job has not run
	//		System.assertEquals(0, ct.TimesTriggered);

	//		// Verify the next time the job will run
	//		System.assertEquals('2022-03-15 00:00:00', 
	//		String.valueOf(ct.NextFireTime));
	//		// Verify the scheduled job hasn't run yet - because after running the Reward Payment exists
	//		Id rewId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_REWARD); 
	//		Payment__c[] rew = [SELECT Id, RecordTypeId FROM Payment__c 
	//		             WHERE RecordTypeId = :rewId];
	//		System.assertEquals(rew.size(),0);
	//	Test.stopTest();

	//	//Checking if the expected Reward Payment object was created and if the Sales Terms has been updated
	//	//Checking if the reward has been created
	//	/*List<Payment__c> receivedrew = [
	//		SELECT Id 
	//		FROM Payment__c 
	//		//WHERE RecordTypeId = :rewId
	//	];
	//	//System.assertEquals(2, receivedrew.size());

	//	//Checking if the created sales terms field has been updated
	//	List<Sales_Process__c> updsalesterms = [
	//		SELECT Id, Reward_Taken__c
	//		FROM Sales_Process__c
	//		WHERE Id = :testsalesterms.Id
 //           AND Reward_Taken__c = true
	//	];
 //      	//System.debug(updsalesterms);
 //      	//System.assertequals(updsalesterms[0].Reward_Taken__c, true);
	//	System.assertEquals(1, updsalesterms.size());*/

	//}
}