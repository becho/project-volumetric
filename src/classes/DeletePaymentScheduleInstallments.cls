/**
* @author 		Mateusz Pruszyński
* @description 	class used to delete payment schedule installments
**/
public without sharing class DeletePaymentScheduleInstallments {
	
	public List<Payment__c> selectedInstallments {get; set;}
	public String retUrl {get; set;}
	public Boolean renderTable {get; set;}

	public DeletePaymentScheduleInstallments(ApexPages.StandardSetController stdSetController) {
        if (!Test.isRunningTest()) {
			stdSetController.addFields(new String[] {'Agreements_Installment__c', 'After_Sales_Service__c', 'Name', 'of_contract_value__c', 'Amount_to_pay__c', 'Paid_Value__c'});	
        	selectedInstallments = (List<Payment__c>)stdSetController.getSelected();
        } else {
        	selectedInstallments = (List<Payment__c>)stdSetController.getSelected();
        	Set<Id> installmentsIds = new Set<Id>();
        	for(Payment__c se : selectedInstallments) {
        		installmentsIds.add(se.Id);
        	}
        	selectedInstallments = [SELECT Id, Agreements_Installment__c, After_Sales_Service__c, 
        								   Name, of_contract_value__c, Amount_to_pay__c, Paid_Value__c
        							FROM Payment__c
        							WHERE Id in :installmentsIds];
        }
		retUrl = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL);
		if(!selectedInstallments.isEmpty()) {	
			checkConditions();
		} else {
         	renderTable = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.NoRecordsSelected + ' ' + Label.ChooseAtLeastOne));			
		}
	}

	/* CANNOT DELETE INSTALLMENTS IF:
		- sale terms has been accepted by manager / developer agreement has been signed
		- after-sales service has been created
		- any of the installment has been at least partially paid
	*/
	public PageReference checkConditions() {
		if(selectedInstallments[0].After_Sales_Service__c != null) {
            renderTable = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CannotDeleteInstallmentsAfterSales)); 
		} else {
			Sales_Process__c saleTerms = [SELECT Id, Status__c, Date_of_signing__c FROM Sales_Process__c WHERE Id = :selectedInstallments[0].Agreements_Installment__c];
			if((saleTerms.Date_of_signing__c != null && saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED)
				|| saleTerms.Status__c == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
            	renderTable = false;
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CannotDeleteInstallments + ' (' + Label.Status + ' = ' + returnPicklistLabel(saleTerms.Status__c) + ')'));
			} else {
				renderTable = true;
				for(Payment__c installment : selectedInstallments) {
					if(installment.Paid_Value__c > 0) {
						renderTable = false;
						break;
					}
				}
				if(!renderTable) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.InstallmentPaidAtLeastPartially));
				}
			}
		}
		return null;
	}

	public String returnPicklistLabel(String value) {
		String result;
		Schema.DescribeFieldResult fieldResult =
		Sales_Process__c.Status__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	    for(Schema.PicklistEntry f : ple) {
	    	if(f.getValue() == value) {
	    		result = f.getLabel();
	    		break;
	    	}
	    }   		
		return result;
	}

	public PageReference yes() {
		try {
			delete selectedInstallments;
		} catch(Exception e) {
			ErrorLogger.log(e);
			renderTable = false;
			return null;
		}
		return new PageReference(retUrl);
	}
}