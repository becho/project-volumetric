@isTest
private class TestClassSalesTarget {
	
	/*
		Author: Wojciech Słodziak
		Desc: Test class for SalesTarget - tested classes: ExtensionManager.cls, TH_ExtensionTrigger.cls, TH_SalesProcessTrigger.cls
	*/


	//private static Map<Integer, Id> prepareData1(boolean withSalesTarget) {
	//	Map<Integer, Id> idList = new Map<Integer, Id>();

	//	Profile p = TestHelper.getProfile(CommonUtility.PROFILE_PROPERTO_BACKOFFICE_NAME);
	//	idList.put(1, TestHelper.createUser(p, 'user1', 'ln1', 'username1@test.pl').Id);
	//	idList.put(2, TestHelper.createUser(p, 'user2', 'ln2', 'username2@test.pl').Id);

	//	List<Sales_Process__c> spToInsert =  new List<Sales_Process__c>();
	//	List<Sales_Process__c> spToInsert2 =  new List<Sales_Process__c>();

	//	spToInsert.add(TestHelper.createOffer(new Sales_Process__c(OwnerId = idList.get(1), Offer_Price__c = 3000), false));
	//	spToInsert.add(TestHelper.createOffer(new Sales_Process__c(OwnerId = idList.get(2), Offer_Price__c = 5000), false));
	//	insert spToInsert;
	//	spToInsert2.add(TestHelper.createOfferLine(new Sales_Process__c(Product_Line_to_Proposal__c = spToInsert.get(0).Id, Price_With_Discount__c = 1000), false));
	//	spToInsert2.add(TestHelper.createOfferLine(new Sales_Process__c(Product_Line_to_Proposal__c = spToInsert.get(0).Id, Price_With_Discount__c = 2000), false));
	//	spToInsert2.add(TestHelper.createOfferLine(new Sales_Process__c(Product_Line_to_Proposal__c = spToInsert.get(1).Id, Price_With_Discount__c = 5000), false));
	//	insert spToInsert2;

	//	idList.put(3, spToInsert.get(0).Id);
	//	idList.put(4, spToInsert.get(1).Id);
	//	idList.put(5, spToInsert2.get(0).Id);
	//	idList.put(6, spToInsert2.get(1).Id);
	//	idList.put(7, spToInsert2.get(2).Id);

	//	if (withSalesTarget) {
	//		idList.put(8, TestHelper.createSalesTarget(new Extension__c(User__c = idList.get(1)), true).Id);
	//		idList.put(9, TestHelper.createSalesTarget(new Extension__c(User__c = idList.get(2)), true).Id);
	//	}

	// 	return idList;

	//}

	//private static Map<Integer, Id> prepareData2(boolean withSalesTarget) {
	//	Map<Integer, Id> idList = new Map<Integer, Id>();

	//	Profile p = TestHelper.getProfile(CommonUtility.PROFILE_PROPERTO_BACKOFFICE_NAME);
	//	idList.put(1, TestHelper.createUser(p, 'user1', 'ln1', 'username1@test.pl').Id);
	//	idList.put(2, TestHelper.createUser(p, 'user2', 'ln2', 'username2@test.pl').Id);

	//	List<Sales_Process__c> spToInsert =  new List<Sales_Process__c>();
	//	List<Sales_Process__c> spToInsert2 =  new List<Sales_Process__c>();
	//	spToInsert.add(TestHelper.createOffer(new Sales_Process__c(OwnerId = idList.get(1), Offer_Price__c = 1000), false));
	//	spToInsert.add(TestHelper.createOffer(new Sales_Process__c(OwnerId = idList.get(2), Offer_Price__c = 2000), false));
	//	spToInsert.add(TestHelper.createOffer(new Sales_Process__c(OwnerId = idList.get(2), Offer_Price__c = 5000), false));
	//	insert spToInsert;
	//	spToInsert2.add(TestHelper.createOfferLine(new Sales_Process__c(Product_Line_to_Proposal__c = spToInsert.get(0).Id, Price_With_Discount__c = 1000), false));
	//	spToInsert2.add(TestHelper.createOfferLine(new Sales_Process__c(Product_Line_to_Proposal__c = spToInsert.get(1).Id, Price_With_Discount__c = 2000), false));
	//	spToInsert2.add(TestHelper.createOfferLine(new Sales_Process__c(Product_Line_to_Proposal__c = spToInsert.get(2).Id, Price_With_Discount__c = 5000), false));
	//	insert spToInsert2;

		
	//	idList.put(3, spToInsert.get(0).Id);
	//	idList.put(4, spToInsert.get(1).Id);
	//	idList.put(5, spToInsert.get(2).Id);
	//	idList.put(6, spToInsert2.get(0).Id);
	//	idList.put(7, spToInsert2.get(1).Id);
	//	idList.put(8, spToInsert2.get(2).Id);


	//	if (withSalesTarget) {
	//		idList.put(9, TestHelper.createSalesTarget(new Extension__c(User__c = idList.get(1)), true).Id);
	//		idList.put(10, TestHelper.createSalesTarget(new Extension__c(User__c = idList.get(2)), true).Id);
	//	}

	// 	return idList;

	//}

	//private static Map<Integer, Id> prepareData3(boolean withSalesTarget) {
	//		Map<Integer, Id> idList = new Map<Integer, Id>();

	//	Profile p = TestHelper.getProfile(CommonUtility.PROFILE_PROPERTO_BACKOFFICE_NAME);
	//	idList.put(1, TestHelper.createUser(p, 'user1', 'ln1', 'username1@test.pl').Id);
	//	idList.put(2, TestHelper.createUser(p, 'user2', 'ln2', 'username2@test.pl').Id);

	//	List<Sales_Process__c> spToInsert =  new List<Sales_Process__c>();
	//	List<Sales_Process__c> spToInsert2 =  new List<Sales_Process__c>();
	//	spToInsert.add(TestHelper.createOffer(new Sales_Process__c(OwnerId = idList.get(1), Offer_Price__c = 1000), false));
	//	spToInsert.add(TestHelper.createOffer(new Sales_Process__c(OwnerId = idList.get(2), Offer_Price__c = 2000), false));
	//	spToInsert.add(TestHelper.createOffer(new Sales_Process__c(OwnerId = idList.get(2), Offer_Price__c = 5000), false));
	//	insert spToInsert;
	//	spToInsert2.add(TestHelper.createPreliminaryAgreement(new Sales_Process__c(OwnerId = idList.get(1),  Offer__c = spToInsert.get(1).Id, Agreement_Value__c = 2000), false));
	//	spToInsert2.add(TestHelper.createPreliminaryAgreement(new Sales_Process__c(OwnerId = idList.get(2),  Offer__c = spToInsert.get(2).Id, Agreement_Value__c = 5000), false));
	//	insert spToInsert2;

		
	//	idList.put(3, spToInsert.get(0).Id);
	//	idList.put(4, spToInsert.get(1).Id);
	//	idList.put(5, spToInsert.get(2).Id);
	//	idList.put(6, spToInsert2.get(0).Id);
	//	idList.put(7, spToInsert2.get(1).Id);


	//	if (withSalesTarget) {
	//		idList.put(8, TestHelper.createSalesTarget(new Extension__c(User__c = idList.get(1)), true).Id);
	//		idList.put(9, TestHelper.createSalesTarget(new Extension__c(User__c = idList.get(2)), true).Id);
	//	}

	// 	return idList;
	//}


	//public @isTest static void testChangeUser() {
	//	Map<Integer, Id> idList  = prepareData1(true);

	//	System.debug([SELECT Potential_Amount__c FROM Extension__c]);
	//	Extension__c stBefore1 = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(1)][0];
	//	System.assertEquals(3000, stBefore1.Potential_Amount__c);
	//	Extension__c stBefore2 = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(2)][0];
	//	System.assertEquals(5000, stBefore2.Potential_Amount__c);

	//	Sales_Process__c offer = [SELECT Offer_Price__c FROM Sales_Process__c WHERE Id = :idList.get(3)];
	//	System.assertEquals(3000, offer.Offer_Price__c);
	//	Sales_Process__c offer2 = [SELECT Offer_Price__c FROM Sales_Process__c WHERE Id = :idList.get(4)];
	//	System.assertEquals(5000, offer2.Offer_Price__c);

	//	Sales_Process__c ofr1 = new Sales_Process__c();
	//	ofr1.Id = idList.get(4);
	//	ofr1.OwnerId = idList.get(1);
	//	Test.startTest();
	//	update ofr1;
	//	Test.stopTest();

	//	Extension__c stAfter1 = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(1)][0];
	//	System.assertEquals(8000, stAfter1.Potential_Amount__c);
	//	Extension__c stAfter2 = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(2)][0];
	//	System.assertEquals(0, stAfter2.Potential_Amount__c);

	//}
	
	//public @isTest static void testDeleteOfferLine() {
	//	Map<Integer, Id> idList  = prepareData1(true);

	//	Sales_Process__c ofrl = new Sales_Process__c();
	//	ofrl.Id = idList.get(5);
	//	Test.startTest();
	//	delete ofrl;
	//	Test.stopTest();

	//	Extension__c stAfter = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(1)][0];
	//	System.assertEquals(2000, stAfter.Potential_Amount__c);
	//	Extension__c st2After = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(2)][0];
	//	System.assertEquals(5000, st2After.Potential_Amount__c);

	//}

	//public @isTest static void testDeleteOffer() {
	//	Map<Integer, Id> idList  = prepareData1(true);


	//	Sales_Process__c ofr = new Sales_Process__c();
	//	ofr.Id = idList.get(3);
	//	Test.startTest();
	//	delete ofr;
	//	Test.stopTest();

	//	Extension__c stAfter = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(1)][0];
	//	System.assertEquals(0, stAfter.Potential_Amount__c);
	//	Extension__c st2After = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(2)][0];
	//	System.assertEquals(5000, st2After.Potential_Amount__c);

	//}

	//public @isTest static void testOfferStatus() {
	//	Map<Integer, Id> idList  = prepareData2(true);

	//	Sales_Process__c ofr = new Sales_Process__c();
	//	ofr.Id = idList.get(4);
	//	ofr.Status__c = CommonUtility.SALES_PROCESS_STATUS_SOLD;
	//	Test.startTest();
	//	update ofr;
	//	Test.stopTest();

	//	Extension__c stAfter = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(1)][0];
	//	System.assertEquals(1000, stAfter.Potential_Amount__c);
	//	Extension__c st2After = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(2)][0];
	//	System.assertEquals(5000, st2After.Potential_Amount__c);

	//}

	//public @isTest static void testChangeUser2() {
	//	Map<Integer, Id> idList  = prepareData2(true);

	//	Sales_Process__c ofr = new Sales_Process__c();
	//	ofr.Id = idList.get(4);
	//	ofr.OwnerId = idList.get(1);
	//	Test.startTest();
	//	update ofr;
	//	Test.stopTest();

	//	Extension__c stAfter = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(1)][0];
	//	System.assertEquals(3000, stAfter.Potential_Amount__c);
	//	Extension__c st2After = [SELECT Potential_Amount__c FROM Extension__c WHERE User__c = :idList.get(2)][0];
	//	System.assertEquals(5000, st2After.Potential_Amount__c);

	//}

	//public @isTest static void testPrelimStatus() {
	//	Map<Integer, Id> idList  = prepareData3(true);

	//	Extension__c stBefore = [SELECT Current_Amount__c FROM Extension__c WHERE User__c = :idList.get(1)][0];
	//	//System.assertEquals(2000, stBefore.Current_Amount__c);
	//	System.assertEquals(0, stBefore.Current_Amount__c);


	//	Sales_Process__c prelim = new Sales_Process__c();
	//	prelim.Id = idList.get(6);
	//	prelim.Status__c = CommonUtility.SALES_PROCESS_STATUS_REJECTED;
	//	Test.startTest();
	//	update prelim;
	//	Test.stopTest();

	//	Extension__c stAfter = [SELECT Current_Amount__c FROM Extension__c WHERE User__c = :idList.get(1)][0];
	//	System.assertEquals(0, stAfter.Current_Amount__c);
	//	Extension__c st2After = [SELECT Current_Amount__c FROM Extension__c WHERE User__c = :idList.get(2)][0];
	//	//System.assertEquals(5000, st2After.Current_Amount__c);
	//	System.assertEquals(0, st2After.Current_Amount__c);

	//}

	//public @isTest static void testPrelimChangeUser() {
	//	Map<Integer, Id> idList  = prepareData3(true);

	//	Extension__c stBefore = [SELECT Current_Amount__c FROM Extension__c WHERE User__c = :idList.get(1)][0];
	//	//System.assertEquals(2000, stBefore.Current_Amount__c);
	//	System.assertEquals(0, stBefore.Current_Amount__c);

	//	Sales_Process__c prelim = new Sales_Process__c();
	//	prelim.Id = idList.get(6);
	//	prelim.OwnerId = idList.get(2);
	//	Test.startTest();
	//	update prelim;
	//	Test.stopTest();

	//	Extension__c stAfter = [SELECT Current_Amount__c FROM Extension__c WHERE User__c = :idList.get(1)][0];
	//	System.assertEquals(0, stAfter.Current_Amount__c);
	//	Extension__c st2After = [SELECT Current_Amount__c FROM Extension__c WHERE User__c = :idList.get(2)][0];
	//	//System.assertEquals(7000, st2After.Current_Amount__c);
	//	System.assertEquals(0, st2After.Current_Amount__c);

	//}
	
}