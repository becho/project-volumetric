public with sharing class SalesProcessErrorsController {
    /*
        Author: Wojciech Słodziak
        Desc: Class that is used for displaying errors found by javascript while creating new records for one of following: 
        Final Agreement, Preliminary Agreement, Handover Protocol
    */
    Id retId { get; set; }
    String errorType { get; set; }


    public SalesProcessErrorsController() {
        retId = ApexPages.currentPage().getParameters().get('retId');
        errorType = ApexPages.currentPage().getParameters().get('errorType');
        setupErrorMessage();
    }

    private void setupErrorMessage() {
        if (errorType == 'AgreementNotSigned') {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.AgreementNotSigned));
        } else
        if (errorType == 'noCustomers') {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.NoCustomersOnOffer));
        } else
        if (errorType == 'HandoverProtocolPreliminaryError') {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.HandoverProtocolPreliminaryError));
        } else
        if (errorType == 'HandoverProtocolFinalError') {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.HandoverProtocolFinalError));
        } else
        if (errorType == 'FinalAgreementAllDefectsNotFixed') {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.FinalAgreementAllDefectsNotFixed));
        } else
        if (errorType == 'FinalAgreementFromHandoverError') {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.FinalAgreementFromHandoverError));
        } else
        if (errorType == 'FinalAgreementFromPreliminaryError') {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.FinalAgreementFromPreliminaryError));
        } else
        if (errorType == 'HandOverProtocolAlreadyExists'){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.HandOverProtocolAlreadyExists));
        } else
        if (errorType == 'HandoverNotSigned'){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.HandoverNotSigned));
        } else
        if (errorType == 'OfferNotAcceptedError'){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.ErrorAcceptedByCustomer));
        } else
        if (errorType == 'NotFirstInQueue'){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.SelectedOfferLinesMustBeFirst));
        } else
        if (errorType == 'ReservationAgreementNotSigned'){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.ReservationAgreementNotSigned));
        } else
        if (errorType == 'FinalAgreementAlreadyExists'){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.FinalAgreementAlreadyExists));
        } else
        if (errorType == 'PraliminaryAgreementAlreadyExists'){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.PraliminaryAgreementAlreadyExists));
        } else
        if (errorType == 'MissingSharesAmongCustomers'){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.MissingSharesAmongCustomers));
        } else
        if (errorType == 'ReservationAgreementAlreadyExists'){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.ReservationAgreementAlreadyExists));
        }       
    }

    public PageReference back() {
        return new PageReference('/' + retId);
    }


}