/**
* @author		Michał Kłobucki
* @description	The class is called from "new commission" list button (Sales Process -> Preliminary/Final Agreement) or EditPayment.cls when a record is edited
*/

public with sharing class NewCommision {

	public Payment__c myCommision {get; set;}
	public String selectedType {get; set;}
	public String selectedContact {get; set;}
	public Sales_Process__c tr {get; set;}
	public Id editMyCommisionId {get;set;}
	public String referenceId {get;set;}

	public static String OUT  = 'Outgoing';
	public static String INCOMING = 'Incoming';

	public static String COMMISSION_SELECTED_CONTACT_CONTACT = 'contact';
	public static String COMMISSION_SELECTED_CONTACT_ACCOUNT = 'account';
	public static String COMMISSION_SELECTED_CONTACT_USER = 'user';

	Id profileId = userinfo.getProfileId();
    String profileName = [Select Id,Name from Profile where Id=:profileId].Name;

	public NewCommision(){

		String pageReferrer = getReferer();

		myCommision = new Payment__c();
		myCommision.Due_Date__c = Date.today() +31;
		if(Apexpages.currentPage().getParameters().get('Id') != null){ // "new commission" list button (Sales Process -> Preliminary/Final Agreement)
			referenceId = Apexpages.currentPage().getParameters().get('Id');
			myCommision.Agreements_Comission__c = Apexpages.currentPage().getParameters().get('Id');
		}
		else if(Apexpages.currentPage().getParameters().get('commision') != null){ // "edit" action (goes from EditPayment.cls)
			referenceId = Apexpages.currentPage().getParameters().get('commision');
			this.editMyCommisionId = Apexpages.currentPage().getParameters().get('commision');
		}
		else{
			System.debug('no url parameter provided');
		}
		if(this.editMyCommisionId != null){
			myCommision = [SELECT Id, RecordType.DeveloperName, Name, Account__c, Contact__c, Paid_Formula__c, Due_Date__c, Payment_Date__c, Agreements_Comission__c,
							Type__c, User__c, Value__c, Value_Percent__c, Commission_Participant__c
							FROM Payment__c
							WHERE Id =: this.editMyCommisionId
							];

			selectedType = myCommision.Type__c;
			selectedContact = myCommision.Commission_Participant__c; 
			if(selectedType == INCOMING){
				if(myCommision.Contact__c != null){
					selectedContact = COMMISSION_SELECTED_CONTACT_CONTACT;
				}
				else{
					selectedContact = COMMISSION_SELECTED_CONTACT_ACCOUNT;
				}

			}
			if(selectedType == OUT){
				if(myCommision.User__c != null){
					selectedContact = COMMISSION_SELECTED_CONTACT_USER;
				}
				else{
					selectedContact = COMMISSION_SELECTED_CONTACT_ACCOUNT;
				}
			}
			Sales_Process__c spRt = [SELECT RecordTypeId FROM Sales_Process__c WHERE Id = : myCommision.Agreements_Comission__c];
			Id recTyp = CommonUtility.getRecordTypeId('Sales_Process__c', CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT);
				
			if(spRt.RecordTypeId == recTyp){
				tr = [SELECT Id, Name, Agreement_Value__c FROM Sales_Process__c WHERE Id =: myCommision.Agreements_Comission__c];
			}
			else{
				Sales_Process__c finalAgr = [SELECT Preliminary_Agreement__c, Id, Name FROM Sales_Process__c WHERE Id = : myCommision.Agreements_Comission__c];
				tr = [SELECT Id, Name, Agreement_Value__c FROM Sales_Process__c WHERE Id = :finalAgr.Preliminary_Agreement__c];
				System.debug(tr);
			}
		}
		else{
			Sales_Process__c spRt = [SELECT RecordTypeId FROM Sales_Process__c WHERE Id = :myCommision.Agreements_Comission__c];
			Id recTyp = CommonUtility.getRecordTypeId('Sales_Process__c', CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT);

			if(spRt.RecordTypeId == recTyp){
				tr = [SELECT Id, Name, Agreement_Value__c FROM Sales_Process__c WHERE Id =: myCommision.Agreements_Comission__c];
			} else if(spRt.RecordTypeId == CommonUtility.getRecordTypeId('Sales_Process__c', CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)) {
				tr = [SELECT Id, Name, Agreement__r.Agreement_Value__c FROM Sales_Process__c WHERE Id = :myCommision.Agreements_Comission__c];
				tr.Agreement_Value__c = tr.Agreement__r.Agreement_Value__c;
			} else{
				Sales_Process__c finalAgr = [SELECT Preliminary_Agreement__c, Id, Name FROM Sales_Process__c WHERE Id = :myCommision.Agreements_Comission__c];
				System.debug(finalAgr);
				tr = [SELECT Id, Name, Agreement_Value__c FROM Sales_Process__c WHERE Id = :finalAgr.Preliminary_Agreement__c];	
			}
			myCommision.Value_Percent__c = 0;
			myCommision.Value__c = 0;
		}
	}

	public List<SelectOption> getType(){
		system.debug('profileName' + profileName);
		List<SelectOption> options = new List<SelectOption>();
		if(profileName != CommonUtility.PROFILE_PROPERTO_BACKOFFICE_NAME && profileName != CommonUtility.PROFILE_PROPERTO_FINANCIAL_NAME && profileName != CommonUtility.PROFILE_PROPERTO_SALES_NAME){

		    options.add(new SelectOption('', Label.ResSearchSelect));
		    Schema.DescribeFieldResult fieldResult =Payment__c.Type__c.getDescribe();
		    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		    
			for( Schema.PicklistEntry f : ple){
			         options.add(new SelectOption(f.getValue(),f.getLabel()));
			}
		}
		else{
			options.add(new SelectOption('', Label.ResSearchSelect));
		    Schema.DescribeFieldResult fieldResult =Payment__c.Type__c.getDescribe();
		    List<Schema.PicklistEntry> pV = fieldResult.getPicklistValues();
		    Schema.PicklistEntry getVL = pV[1];
			options.add(new SelectOption(getVL.getValue(),getVL.getLabel()));
		}
	    getContact();       
	    return options;
	 }

	 public List<SelectOption> getContact(){

	   List<SelectOption> options = new List<SelectOption>();
	    options.add(new SelectOption('', Label.ResSearchSelect));
	    if(selectedType == INCOMING){
			options.add(new SelectOption(COMMISSION_SELECTED_CONTACT_CONTACT, Schema.Payment__c.fields.Contact__c.getDescribe().getLabel())); 
			options.add(new SelectOption(COMMISSION_SELECTED_CONTACT_ACCOUNT, Schema.Payment__c.fields.Account__c.getDescribe().getLabel()));
	    	
	    }
	    if(selectedType == OUT){
	    	options.add(new SelectOption(COMMISSION_SELECTED_CONTACT_USER, Schema.Payment__c.fields.User__c.getDescribe().getLabel()));
			options.add(new SelectOption(COMMISSION_SELECTED_CONTACT_ACCOUNT, Schema.Payment__c.fields.Account__c.getDescribe().getLabel()));
	    }
	    return options;
	 }

	 public PageReference updateAmount(){
 		
	 	try{

	 			if(myCommision.Value__c == null){
				   			myCommision.Value__c = 0;
				}

				if(myCommision.Value_Percent__c == null){
				   			myCommision.Value_Percent__c = 0;
				}

			 	if(myCommision.Value_Percent__c > 100)
			 		myCommision.Value_Percent__c = 100;
			 	myCommision.Value__c = myCommision.Value_Percent__c * tr.Agreement_Value__c/100;
			 	System.debug('myCommision.Value__c: ' + myCommision.Value__c);
			 	return null;
	 	}
	 	catch(Exception e){
	 		system.debug(e.getMessage());
	 		myCommision.Value__c = 0.00;
            ErrorLogger.log(e);
	 		return null;
	 	}
	 	return null;

	}

	public PageReference Cancel(){
		return new PageReference('/'+referenceId);
	}

	public PageReference save(){
		System.debug('myCommision.Value__c before: ' + myCommision.Value__c);
		updateAmount();
		System.debug('myCommision.Value__c after: ' + myCommision.Value__c);
		if(selectedType == null || selectedContact == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.ErrorMaximumCommission));
			return null;
		}
		if(selectedType == OUT){
			if(selectedContact == COMMISSION_SELECTED_CONTACT_USER){
				if(myCommision.User__c != null){
					User user = [SELECT Id, Name, Maximum_Commission__c FROM User WHERE Id =: myCommision.User__c];
					if(user.Maximum_Commission__c != null){
						Decimal max = (user.Maximum_Commission__c/100) * tr.Agreement_Value__c;

						if(myCommision.Value__c > max){
							ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.ErrorMaximumCommission));
    						return null;
    					}
					}
					else{
						if(user.Maximum_Commission__c == null){
							user.Maximum_Commission__c = 0;
						}
						else{
							ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.ErrorMaximumCommission));
							return null;
						}
					}
				}
			}
			if(selectedContact == COMMISSION_SELECTED_CONTACT_ACCOUNT){
				if(myCommision.Account__c != null){
					Account account = [SELECT Id, Name, MaxCommission__c FROM Account WHERE Id =: myCommision.Account__c];
					if(account.MaxCommission__c != null){
						Decimal max = (account.MaxCommission__c/100) * tr.Agreement_Value__c;
						if( myCommision.Value__c > max){
							ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.ErrorMaximumCommission));
    						return null;
    					}
					}
					else{
						if(account.MaxCommission__c == null){
							account.MaxCommission__c = 0;
						}
						if(myCommision.Value__c > 0){
							ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.ErrorMaximumCommission));
    						return null;
						}
					}
				}
			}

		}

		Id rt = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_COMMISION);
		if(rt != null)
		myCommision.RecordTypeId = rt;

		Schema.DescribeFieldResult fieldResult =Payment__c.Type__c.getDescribe();
	    	List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry f : ple){
	   	    	if(f.getValue() == selectedType)
	   	    		myCommision.Type__c = f.getValue();
   	    }

		if(selectedType == OUT){
			if(selectedContact == COMMISSION_SELECTED_CONTACT_USER){
				myCommision.Account__c = null;
				myCommision.Contact__c = null;
			}
			if(selectedContact == COMMISSION_SELECTED_CONTACT_ACCOUNT){
				myCommision.User__c = null;
				myCommision.Contact__c = null;
			}
		}
		if(selectedType == INCOMING){
			System.debug(myCommision);
			if(myCommision.Contact__c == null && myCommision.Account__c == null){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.ErrorFillContactOrAccount));
				return null;
			}
			if(selectedContact == COMMISSION_SELECTED_CONTACT_CONTACT){
				myCommision.User__c = null;
				myCommision.Account__c = null;
			}
			if(selectedContact == COMMISSION_SELECTED_CONTACT_ACCOUNT){
				myCommision.User__c = null;
				myCommision.Contact__c = null;
			}
		}

		myCommision.Commission_Participant__c = selectedContact;


		try{
			System.debug('myCommision.Value__c in try: ' + myCommision.Value__c);
			upsert myCommision;
			System.debug('myCommision.Value__c after upsert: ' + myCommision.Value__c);
			return new PageReference('/'+referenceId);
		}
		catch(DmlException e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            ErrorLogger.log(e);
			return null;
		}
		return null;
	}

	public String getReferer()
		{
		    return ApexPages.currentPage().getHeaders().get('referer');
		}

	
}