/**
* @author       Michał Kłobucki
* @editor       Mateusz Pruszynski
* @description  Class to override custom edit button on Sales Process
*/

public with sharing class EditSalesProcess {

    public Sales_Process__c process {get; set;}
    public String processId {get; set;}

    public EditSalesProcess(ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) {
            stdController.addFields(new String[] {'RecordTypeId', 'Status__c'});
        }
        process = (Sales_Process__c)stdController.getRecord();
        processId = process.Id;
    }
    
    public PageReference ret() {
        if(process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)) {
            Set<Id> offerId = new Set<Id>();
            offerId.add(process.Id);    
            Boolean editFlag = SalesProcessManager.checkAgrForOffer(offerId);
            if(!editFlag) {
                PageReference edit = Page.NewProposal;
                edit.getParameters().put(CommonUtility.URL_PARAM_SPID,  process.Id);
                return edit;
            } else {
                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.YouCantEditOrDeleteOffer);
                apexpages.addmessage(msg);
                return null;
            }
        }
        if(process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)) {
            processId = process.Product_Line_to_Proposal__c;
            Set<Id> offerId = new Set<Id>();
            offerId.add(process.Product_Line_to_Proposal__c);
            Boolean editFlag = SalesProcessManager.checkAgrForOffer(offerId);
            Sales_Process__c rtOfProcessMain = [SELECT Product_Line_to_Proposal__c, Offer_Line_to_Sale_Term__c, Offer_Line_to_Sale_Term__r.RecordTypeId, Offer_Line_to_Sale_Term__r.Status__c FROM Sales_Process__c WHERE Id =: process.Id];
            if(!editFlag || (editFlag && rtOfProcessMain.Offer_Line_to_Sale_Term__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT))) {
                if(rtOfProcessMain.Offer_Line_to_Sale_Term__r.Status__c == CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL) {
                    apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.RecordIsWaitingForManager);
                    apexpages.addmessage(msg);                  
                    return null;
                } else if(rtOfProcessMain.Offer_Line_to_Sale_Term__r.Status__c == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER) {
                    apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.termsAccepted);
                    apexpages.addmessage(msg);                  
                    return null;    
                } else if(rtOfProcessMain.Offer_Line_to_Sale_Term__r.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED) {
                    apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.SaleTermsAlreadySigned);
                    apexpages.addmessage(msg);                  
                    return null;
                } else {
                    String spId = ApexPages.currentPage().getParameters().get(CommonUtility.URL_PARAM_RETURL).replace('/', '');

                    spId = spId.left(15);   
                                     
                    if(String.isBlank(rtOfProcessMain.Offer_Line_to_Sale_Term__c) 
                        || (String.isNotBlank(rtOfProcessMain.Offer_Line_to_Sale_Term__c)
                            && spId == rtOfProcessMain.Offer_Line_to_Sale_Term__c
                        )
                    ) {
                        PageReference edit = Page.editLine;
                        if(spId != process.Id) {
                            edit.getParameters().put(CommonUtility.URL_PARAM_SPID,  spId);
                        } else {
                            edit.getParameters().put(CommonUtility.URL_PARAM_SPID,  rtOfProcessMain.Offer_Line_to_Sale_Term__c != null ? rtOfProcessMain.Offer_Line_to_Sale_Term__c : rtOfProcessMain.Product_Line_to_Proposal__c);
                        }
                        edit.getParameters().put(CommonUtility.URL_PARAM_LINE2,  process.Id);
                        edit.setRedirect(true);
                        return edit;
                    } else {
                        apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.YouCantEditOrDeleteOfferLine);
                        apexpages.addmessage(msg);                  
                        return null;
                    }
                }
            } else {
                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.YouCantEditOrDeleteOfferLine);
                apexpages.addmessage(msg);
                return null;
            }
        }
        if(process.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)) {            
            Profile profile = [SELECT Id FROM Profile WHERE Name = :Label.SystemAdministrator];
            Set<Id> offerId = new Set<Id>();
            offerId.add(process.Id);
            Boolean editFlag = SalesProcessManager.checkAgrForOffer(offerId);
            if(!editFlag) {
                if((process.Status__c == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER || 
                process.Status__c == CommonUtility.SALES_PROCESS_STATUS_RESERVATION_SIGNED ||
                process.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED
                ) 
                && UserInfo.getProfileId() != profile.Id) {
                    PageReference edit = Page.EditSalesTerms;
                    edit.getParameters().put(CommonUtility.URL_PARAM_ID, process.Id);
                    edit.setRedirect(true);
                    return edit;
                }
            } else {
                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, Label.YouCantEditOrDeleteSaleTerms);
                apexpages.addmessage(msg);
                return null;
            }
        }

        ApexPages.StandardController processCTRL = new ApexPages.StandardController(process);
        PageReference standardRef = processCTRL.edit();
        standardRef.getParameters().put(CommonUtility.URL_PARAM_NOOVERRIDE, CommonUtility.ONE_PART1);
        standardRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, process.Id);
        standardRef.getParameters().put(CommonUtility.URL_PARAM_SAVEURL, process.Id);
        return standardRef;
    }
    public PageReference back() {
        return new PageReference('/' + processId);
    }

}