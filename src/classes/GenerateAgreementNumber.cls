/**
* @author 		Mateusz Pruszynski
* @description 	Class to generate agreement number
**/

global without sharing class GenerateAgreementNumber {

	public static final String GARAGE = 'Garage';
	public static final String LACK_OF_DATA = Label.LackOfBuildingOrFlat;

	public static final Map<String, String> typeOfPropertyMap = new Map<String, String> {
		CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT			=> 'M',
		CommonUtility.RESOURCE_TYPE_STORAGE					=> 'K',
		GARAGE 												=> 'G',
		CommonUtility.RESOURCE_TYPE_PARKING_SPACE 			=> 'P',
		CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY 	=> 'L'
	};

	 ////////////////
	/* WEBSERVICE */
   ////////////////

	/*	______________________________________________________

		Agreement number has the following pattern:
		XXXXX/Building_number/Type_of_property/Property_number
		______________________________________________________

		1) XXXXX - current agreement number + 1 (starting from 
		initial value stored in custom settings)
		2) Building_number - Construction number taken from 
		parent building of the resource
		3) Type_of_property: *****
			M - flat
			K - storage
			G - garage (parking space with "Collective Space" == true)
			P - parking space (parking space with "Collective Space" != true)
			L - commercial property
		4) Property_number - Flat_Number__c
		______________________________________________________	

		The method is called either from js buttons "Print 
		declaration reservation" or "Generate developer 
		Agreement"
		______________________________________________________

		Once the Agreement number is created, we never 
		override it.
		______________________________________________________

		*****It is assumed that the main Type_of_property is 
		Flat/Appartment. If a process does not relate to Flat
		we take the first property type found related to the 
		process.
		______________________________________________________
	*/
	WebService static String generateNumberForAgreement(String saleTermsId) {

		String result = '';

		// 1) get configuration from custom settings
		AgreementNumberConfig__c currentAgreementConfig = AgreementNumberConfig__c.getInstance('currentConfig');

		// Method does not execute its logic when AgreementNumberConfig i`s turned off
		if(currentAgreementConfig.Switch_On__c) {

			// check if agreement number is already filled for selected sale terms
			Sales_Process__c saleTerms = [
				SELECT Id, Agreement_Number__c FROM Sales_Process__c WHERE Id = :saleTermsId LIMIT 1
			];

			if(String.isBlank(saleTerms.Agreement_Number__c)) {

				// increment current agreement number
				currentAgreementConfig.Current_Number__c = String.valueOf(Decimal.valueOf(currentAgreementConfig.Current_Number__c ) + 1);

				// create number
				String[] returnedResult = createAgreementNumber(
					currentAgreementConfig.Current_Number__c,
					currentAgreementConfig.Initial_Value__c,
					saleTermsId
				);
				String numberAgr = returnedResult[0];


				if(returnedResult.size() > 1) {
					String propertyType = returnedResult[1];
					saleTerms.Resource_Type_for_Event__c  = propertyType;
				}
				
				if(numberAgr != LACK_OF_DATA) {
					saleTerms.Agreement_Number__c = numberAgr;
				} else {
					return LACK_OF_DATA;
				}

				// update all records that take part in agreement number generation
				Savepoint sp = Database.setSavepoint();
				try {
					update saleTerms;
					update currentAgreementConfig;
				} catch(Exception e) {
					Database.rollback(sp);
					ErrorLogger.log(e);
				}

			}

		}

		return result;
	}

	WebService static String generateNumberForListOfAgreements(List<Sales_Process__c> saleTerms) {
		
		String result = '';

		// 1) get configuration from custom settings
		AgreementNumberConfig__c currentAgreementConfig = AgreementNumberConfig__c.getInstance('currentConfig');
		// Method does not execute its logic when AgreementNumberConfig i`s turned off
		if(currentAgreementConfig.Switch_On__c) {
			
			List<Sales_Process__c> saleTermsToUpdate = new List<Sales_Process__c>();

			for(Sales_Process__c st : saleTerms) {
				
				// check if agreement number is already filled for selected sale terms
				if(String.isBlank(st.Agreement_Number__c)) {	
					Sales_Process__c newSt = new Sales_Process__c(
						Id = st.Id
					);
					// increment current agreement number
					currentAgreementConfig.Current_Number__c = String.valueOf(Decimal.valueOf(currentAgreementConfig.Current_Number__c ) + 1);
					// create number
					String[] returnedResult = createAgreementNumber(
						currentAgreementConfig.Current_Number__c,
						currentAgreementConfig.Initial_Value__c != null ? currentAgreementConfig.Initial_Value__c : '0',
						newSt.Id
					);

					String numberAgr = returnedResult[0];
					if(returnedResult.size() > 1) {
						String propertyType = returnedResult[1];
						newSt.Resource_Type_for_Event__c  = propertyType;
					}
					
					if(numberAgr != LACK_OF_DATA) {
						newSt.Agreement_Number__c = numberAgr;
					} else {
						return LACK_OF_DATA;
					}

					saleTermsToUpdate.add(newSt);
				}
			}
			// update all records that take part in agreement number generation
			Savepoint sp = Database.setSavepoint();
			try {
				update saleTermsToUpdate;
				update currentAgreementConfig;
			} catch(Exception e) {
				Database.rollback(sp);
				ErrorLogger.log(e);
			}

		}
		return result;
	}

	public static List<String> createAgreementNumber(String currentNo, String initialValue, String saleTermsId) {

		// get data from resource and its building from product lines
		List<Sales_Process__c> productLines = getProductLines(saleTermsId);

		if(!productLines.isEmpty()) {

			Sales_Process__c mainProduct;
			for(Sales_Process__c pl : productLines) {
				if(pl.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)) {
					mainProduct = pl;
					break;
				}	
			}

			if(mainProduct == null) {
				mainProduct = productLines[0];
			}

			List<String> result = new List<String>();
			result.add (
						String.valueOf(Decimal.valueOf(initialValue) + Decimal.valueOf(currentNo)) + '/' 
						+ mainProduct.Offer_Line_Resource__r.Building__r.Name + '/' 
						+ typeOfPropertyMap.get(createMapKey(mainProduct)) + '/'
						+ mainProduct.Offer_Line_Resource__r.Flat_Number__c
						);

			result.add ( 
						createMapKey(mainProduct)
						);	

			return result;	
		} else {
			List<String> result = new List<String>();
			result.add(LACK_OF_DATA);
			return result;
		}

	}

	public static List<Sales_Process__c> getProductLines(String saleTermsId) {
		return [
			SELECT Id, Offer_Line_Resource__r.RecordType.DeveloperName, Offer_Line_Resource__r.RecordTypeId, Offer_Line_Resource__r.Collective_Space_parking__c,
				   Offer_Line_Resource__r.Building__r.Name, Offer_Line_Resource__r.Name, Offer_Line_Resource__r.Flat_Number__c
			FROM Sales_Process__c
			WHERE Offer_Line_to_Sale_Term__c = :saleTermsId
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
			AND Offer_Line_Resource__c <> null
			AND Offer_Line_Resource__r.Building__c <> null
			AND Offer_Line_Resource__r.Flat_Number__c <> null
		];
	}

	public static String createMapKey(Sales_Process__c prodLine) {
	
		String result = '';

		if(prodLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
			&& prodLine.Offer_Line_Resource__r.Collective_Space_parking__c != null) {
			return GARAGE;
		} else {
			return prodLine.Offer_Line_Resource__r.RecordType.DeveloperName;
		}
	
	}

	 //////////////////
	/* TRIGGER PART */
   //////////////////

	/*
		______________________________________________________	

		Agreement number is generated on Sale Terms stage, and 
		it's moved to following stages (After-sales as well as 
		Final Agreement).
		______________________________________________________

	*/

	public static void copyAgreementNumberToAfterSales(List<Sales_Process__c> afterSalesServices) {

		Map<String, Sales_Process__c> afterSalesMapWithKeyAsSaleTermsId = new Map<String, Sales_Process__c>();
		for(Sales_Process__c afterSales : afterSalesServices) {
			afterSalesMapWithKeyAsSaleTermsId.put(String.valueOf(afterSales.Agreement__c).left(15), afterSales);
		}

		List<Sales_Process__c> saleTerms = [
			SELECT Id, Agreement_Number__c, Resource_Type_for_Event__c FROM Sales_Process__c 
			WHERE Id in :afterSalesMapWithKeyAsSaleTermsId.keySet()
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
		];

		for(String saleTermsId : afterSalesMapWithKeyAsSaleTermsId.keySet()) {
			
			for(Sales_Process__c st : saleTerms) {

				if(saleTermsId == String.valueOf(st.Id).left(15)) {
					afterSalesMapWithKeyAsSaleTermsId.get(saleTermsId).Agreement_Number__c = st.Agreement_Number__c;
					afterSalesMapWithKeyAsSaleTermsId.get(saleTermsId).Resource_Type_for_Event__c = st.Resource_Type_for_Event__c;
				}

			}

		}

	}

	public static void copyAgreementNumberToFinalAgreement(List<Sales_Process__c> finalAgreements) {

		Map<String, Sales_Process__c> finalAgreementsMapWithKeyAsSaleTermsId = new Map<String, Sales_Process__c>();
		for(Sales_Process__c finalAgreement : finalAgreements) {
			finalAgreementsMapWithKeyAsSaleTermsId.put(String.valueOf(finalAgreement.Handover_Protocol__c).left(15), finalAgreement);
		}

		List<Sales_Process__c> afterSales = [
			SELECT Id, Agreement_Number__c, Resource_Type_for_Event__c FROM Sales_Process__c 
			WHERE Id in :finalAgreementsMapWithKeyAsSaleTermsId.keySet()
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)
		];

		for(String afterSalesId : finalAgreementsMapWithKeyAsSaleTermsId.keySet()) {
			
			for(Sales_Process__c ass : afterSales) {

				if(afterSalesId == String.valueOf(ass.Id).left(15)) {
					finalAgreementsMapWithKeyAsSaleTermsId.get(afterSalesId).Agreement_Number__c = ass.Agreement_Number__c;
					finalAgreementsMapWithKeyAsSaleTermsId.get(afterSalesId).Resource_Type_for_Event__c = ass.Resource_Type_for_Event__c;
				}

			}

		}

	}

}