/**
* @author   Mariia Dobzhanska
* @description  Test class for resourcePDF.cls
*/
@isTest
private class TestClassResourcePDF {
    public final static String testflname = 'TestFlatName';

    @testsetup
    static void setuptestdata() {
        //Creating the Resource
        Resource__c testflat = TestHelper.createResourceFlatApartment(null, false);
        testflat.Name = testflname;
        insert testflat;
        //Creating the metadata attachment
        Metadata__c md = new Metadata__c();
        md.Metadata__c = '{"Category":"listing","originalFilename":"kitchen-673733_1280.jpg","URL":"http://static-mennica.properto-portal.enxoo.com/C.LU.I.01/fzzQyrnzaHSNSu4X_30Jun2016090238GMT_1467277358026.jpg","Main":"false","FileType":"IMG"}';
        md.Metadata_type__c = 'Data sheet';
        md.ObjectID__c = CommonUtility.SOBJECT_NAME_RESOURCE;
        md.RecordID__c = testflat.Id;
        insert md;  
    }
        
  /**
  **Testing if the appropriate pagereference and button show value are received if for the controller record
  **exists an attachment
  **/
    
    static testmethod void reloadAttOkTest() {
        //Loading the resource and attachment
        Resource__c resource = [
                                SELECT Id, Name
                                FROM Resource__c
                                WHERE Name = :testflname
                                LIMIT 1
        ];
        Metadata__c att = [
                            SELECT Id, Metadata__c
                            FROM Metadata__c
                            WHERE RecordID__c = :resource.Id
                            LIMIT 1
        ];
        //Creating a test class instance
        ApexPages.StandardController stdController = new ApexPages.StandardController(resource);
        String expdataurl = CommonUtility.getMetadataValue(att.Metadata__c, CommonUtility.METADATA_METADATA_URL_URL);
        Pagereference exppageref = new Pagereference(expdataurl);
        Test.startTest();
          ResourcePDF testobj = new ResourcePDF(stdController); 
        PageReference actpageref = testobj.reload();
        Test.stopTest();
        //Checking if the Pagereference url and button show option are as expected
        System.assertEquals(exppageref.getUrl(), actpageref.getUrl());
        System.assertEquals(false, testobj.btnShow);
    }
      
  /**
  **Testing if the appropriate error and button show property are received while the resource doesn't have an attachment
  **/
      
   static testmethod void reloadAttEmpTest() {
      //Loading the resource and attachment
      Resource__c resource = [
                             SELECT Id, Name
                             FROM Resource__c
                             WHERE Name = :testflname
                             LIMIT 1
      ];
      Metadata__c att = [
                         SELECT Id, Metadata__c
                         FROM Metadata__c
                         WHERE RecordID__c = :resource.Id
                         LIMIT 1
      ];
      //Removing the attachment
      delete att;
      ApexPages.StandardController stdController = new ApexPages.StandardController(resource);
      Test.startTest();
        ResourcePDF testobj = new ResourcePDF(stdController); 
        PageReference actpageref = testobj.reload();
      Test.stopTest();
      List<Apexpages.Message> msgs = ApexPages.getMessages();
      //Searching for the expected error is receives while the attachments are empty
      Boolean errorfound = false;

      for(ApexPages.Message message : msgs) {
       if (message.getDetail().contains(Label.NoDataSheetHasBeenUploaded))
           errorfound = true;      
      }
      //Checking if the expected error was received, and if the Pagereference is null, checking button show option
      System.assert(errorfound);
      System.assertEquals(null, actpageref);
      System.assertEquals(true, testobj.btnShow);
  }
}