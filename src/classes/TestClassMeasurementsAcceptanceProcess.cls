@isTest
private class TestClassMeasurementsAcceptanceProcess {

    @testSetup
    private static void setupData() {
        TestHelper.CreateAreaAndVatPercentageDistribution_CurrentDistributionFlat();
        Resource__c resource1 = TestHelper.createResourceFlatApartment(new Resource__c(Price__c = 100, Total_Area_Measured__c = 100, Usable_Area_Planned__c = 100), true);
        Resource__c resource2 = TestHelper.createResourceFlatApartment(new Resource__c(Price__c = 100, Total_Area_Measured__c = 100, Usable_Area_Planned__c = 100, Acceptance_Status__c = CommonUtility.RESOURCE_ACCEPTANCE_STATUS_DIRECTED_FOR_APPROVAL), true);
        
        Sales_Process__c salesProcess1 = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Offer_Line_Resource__c = resource1.Id, Discount_Amount_Offer__c = 1, RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)), true);
        Sales_Process__c salesProcess2 = TestHelper.createSalesProcessAfterSalesService(null, true);
    }

	private static testMethod void rejectMeasurementsTest() {
        List<Resource__c> resourceList = [select Id from Resource__c];
        List<String> idList = new List<String>();
        
        for(Resource__c resource : resourceList) {
            idList.add(resource.Id);
        }
        
        Test.startTest();
            List<String> strList = MeasurementsAcceptanceProcess.rejectMeasurements(idList, true);
            strList = MeasurementsAcceptanceProcess.rejectMeasurements(idList, false);
        Test.stopTest();
	}
	
	private static testMethod void acceptMeasurementsTest() {
        List<Resource__c> resourceList = [select Id from Resource__c];
        List<String> idList = new List<String>();
        
        for(Resource__c resource : resourceList) {
            idList.add(resource.Id);
        }
        
        Test.startTest();
            List<String> strList = MeasurementsAcceptanceProcess.acceptMeasurements(idList, 'true');
            strList = MeasurementsAcceptanceProcess.acceptMeasurements(idList, 'false');
        Test.stopTest();
	}

    private static testMethod void acceptOdRejectResourcesTest() {
        List<Resource__c> resourceList = [select Id from Resource__c];
        
        Test.startTest();
            String str = MeasurementsAcceptanceProcess.acceptOdRejectResources(resourceList, true);
            str = MeasurementsAcceptanceProcess.acceptOdRejectResources(resourceList, false);
        Test.stopTest();
	}
	
	private static testMethod void manageSalesProcessAndPaymentScheduleTest() {
        Test.startTest();
        List<Resource__c> resourceList = [select Id from Resource__c];
        List<Sales_Process__c> salesProcessList = [select Id, Offer_Line_to_Sale_Term__c, Offer_Line_Resource__c, 
                    Offer_Line_Resource__r.Price__c, Offer_Line_Resource__r.Price_After_Measurement__c, 
                    Offer_Line_Resource__r.The_Difference_After_Measurement__c,
                    Offer_Line_Resource__r.Usable_Area_Planned__c,
                    Discount_Amount_Offer__c
                    from Sales_Process__c where Offer_Line_Resource__c <> null];
        
        System.debug(salesProcessList.get(0).Offer_Line_Resource__r.Price__c);
        System.debug(salesProcessList.get(0).Discount_Amount_Offer__c);
        
            MeasurementsAcceptanceProcess.manageSalesProcessAndPaymentSchedule(salesProcessList);
        Test.stopTest();
	}
	
	private static testMethod void updateAfterSalesServiceTest() {
        Test.startTest();
        List<Sales_Process__c> salesProcessList = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
        
        
            salesProcessList.get(0).Price_Changed_After_Measurements__c = true;
            update salesProcessList;
        Test.stopTest();
	}
}