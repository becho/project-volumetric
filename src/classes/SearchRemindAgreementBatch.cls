/**
* @author       Mariia Dobzhanska
* @description  A batch which searches for Sales processes, which Developer/Prelimininary agreement is not signed, to create the Task
* which stores an information about the contact and sales process to be reminded about the Developer/Prelimininary agreement signing
**/
global class SearchRemindAgreementBatch implements Database.Batchable<Sales_Process__c>, Database.Stateful {

	global Iterable<Sales_Process__c> start(database.batchablecontext BC) {

		//Getting the Id of the recordtype Sales Process
		String SPrecType = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT);

		return [
			SELECT Id, Name, Date_of_signing__c, Status__c, RecordTypeId, Date_of_signing_Reservation__c, 
			Expected_date_of_signing__c, Contact__c, Contact__r.Name
			FROM Sales_process__c 
			WHERE Status__c = :CommonUtility.SALES_PROCESS_STATUS_RESERVATION_SIGNED
			AND RecordTypeId = :SPrecType
			AND Date_of_signing_Reservation__c != null 
			AND Expected_date_of_signing__c != null 
			AND Date_of_signing__c = null
		];
	}

	global void execute(Database.BatchableContext BC, List<Sales_Process__c> scope) {
		List<Sales_Process__c> sps = scope;
		if (!sps.isEmpty()) {

			for (Sales_Process__c sp : sps) {

				Date expsigndate = sp.Expected_date_of_signing__c.date();
				//Take the remind before day count from the custom settings
				RemindTaskDayCount__c beforeDaysObj = RemindTaskDayCount__c.getInstance('REMIND_DAYS_COUNT_DEV');
				Integer custRemday = beforeDaysObj.Count__c.intValue();
				//Setting the date for reminding
				Date daterem = expsigndate - custRemday;
				Date today = Date.today();
				//Checking if the remind date is not today or earlier
				if(today <= daterem) {
					//Creating the remind task
                    RemindAgreementTask.maketask(sp, daterem);
				}

				// If the expected sign date later than today and remind date has expired 
				// or the expected sign date is today
				//we create the task with remind for today
				else if (today > daterem && today <= expsigndate) {
					//Create task with activitydate now
                    RemindAgreementTask.maketask(sp, today);
				}

			}

		}
	}

	global void finish(Database.BatchableContext info) {}

}