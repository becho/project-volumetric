/**
* @author 		Krystian Bednarek
* @description 	Test class for GenerateAgreementNumber class
**/


@isTest
private class TestClassGenerateAgreementNumber {

	@testsetup
	static void setuptestData(){
		// Saleterms config set up
		ConditionalSaleTermsAcceptance__c config2 = new ConditionalSaleTermsAcceptance__c();
		config2.Switch_On__c = false;
		config2.Name = 'currentConfig';
		insert config2;
		AgreementNumberConfig__c saleTermsConfig = new AgreementNumberConfig__c();
		saleTermsConfig.Name = 'currentConfig';
		saleTermsConfig.Current_Number__c = '1';
		saleTermsConfig.Switch_On__c = true;
		saleTermsConfig.Initial_Value__c = '0';
		insert saleTermsConfig;
	}

	@isTest 
	static void TestDateOfSigningUpdate() {
		//Creating investment and resource for the product line
		Resource__c investment = TestHelper.createInvestment(null, true);

		Resource__c building = TestHelper.createResourceBuilding(new Resource__c(Investment_Building__c = investment.Id), true);
	
		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c(
				Investment_Flat__c = investment.Id, 
				Building__c = building.Id, 
				Flat_Number__c = '123'
			), 
			true
		);

		Account personAccount = TestHelper.createAccountIndividualClient(
			new Account(
				NIP__c = '1234563218'
			), 
			true
		);
		
		Contact contactPerson = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = contactPerson.Id
			), 
			true
		);

		Sales_Process__c prline = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Contact_from_Offer_Line__c = contactPerson.Id,
				Product_Line_to_Proposal__c = proposal.Id,
				Offer_Line_Resource__c = flat.Id
			), true
		);

		Test.startTest();

			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
				new Sales_Process__c(
					Offer__c = proposal.Id,
					Contact__c = proposal.Contact__c,
					Expected_Date_of_Signing_Reservation__c = Date.today()-1,
					Expected_Date_of_Signing__c = Date.today()-1,
					Reservation_Quque_First_Place__c = true, 
					Share_Ok_Among_Customers__c = true
				),
				true
			);

			prline.Offer_Line_to_Sale_Term__c = saleTerms.Id;
			update prline;

			Payment__c paymentInstallment = TestHelper.createPaymentsInstallment(
				new Payment__c(
					Agreements_Installment__c = saleTerms.Id,
					Contact__c = proposal.Contact__c
				),
				true
			);

			saleTerms.Date_of_Signing_Reservation__c = Date.today();
			saleTerms.Date_of_signing__c = Date.today();
			update saleTerms;
		
			// check if Agreement number has been generated e.q different then null
			Sales_Process__c saleTermsAfterUpdate = [SELECT Id, Agreement_Number__c FROM Sales_Process__c WHERE id=:saleTerms.Id];
			/*System.assert(saleTermsAfterUpdate.Agreement_Number__c != null);*/

		Test.stopTest();		
 
	}

	@isTest 
	static void TestGenerateDeveloperAgreementButton() {
		//Creating investment and resource for the product line
		Resource__c investment = TestHelper.createInvestment(null, true);

		Resource__c building = TestHelper.createResourceBuilding(new Resource__c(Investment_Building__c = investment.Id), true);
	
		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c(
				Investment_Flat__c = investment.Id, 
				Building__c = building.Id, 
				Flat_Number__c = '123'
			), 
			true
		);

		Account personAccount = TestHelper.createAccountIndividualClient(
			new Account(
				NIP__c = '1234563218'
			), 
			true
		);
		
		Contact contactPerson = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = contactPerson.Id
			), 
			true
		);

		Sales_Process__c prline = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Contact_from_Offer_Line__c = contactPerson.Id,
				Product_Line_to_Proposal__c = proposal.Id,
				Offer_Line_Resource__c = flat.Id
			), true
		);

		Test.startTest();

			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
				new Sales_Process__c(
					Offer__c = proposal.Id,
					Contact__c = proposal.Contact__c,
					Expected_Date_of_Signing_Reservation__c = Date.today()-1,
					Expected_Date_of_Signing__c = Date.today()-1,
					Reservation_Quque_First_Place__c = true, 
					Share_Ok_Among_Customers__c = true,
					Status__c = CommonUtility.SALES_PROCESS_STATUS_REJECTED
				),
				true
			);

			prline.Offer_Line_to_Sale_Term__c = saleTerms.Id;
			update prline;

			Payment__c paymentInstallment = TestHelper.createPaymentsInstallment(
				new Payment__c(
					Agreements_Installment__c = saleTerms.Id,
					Contact__c = proposal.Contact__c
				),
				true
			);	
		GenerateAgreementNumber.generateNumberForAgreement(saleTerms.Id);

		// Check if agreement number has been generated
		Sales_Process__c saleTermsAfterUpdate = [SELECT Id, Agreement_Number__c FROM Sales_Process__c WHERE Id=:SaleTerms.Id];
		System.assert(saleTermsAfterUpdate.Agreement_Number__c != NULL);
		Test.stopTest();
	}

	@isTest 
	static void TestTransferAgreementNbrToAfterSales() {
		//Creating investment and resource for the product line
		Resource__c investment = TestHelper.createInvestment(null, true);

		Resource__c building = TestHelper.createResourceBuilding(new Resource__c(Investment_Building__c = Investment.Id), true);
	
		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c(
				Investment_Flat__c = investment.Id, 
				Building__c = building.Id, 
				Flat_Number__c = '123'
			), 
			true
		);

		Account personAccount = TestHelper.createAccountIndividualClient(
			new Account(
				NIP__c = '1234563218'
			), 
			true
		);
		
		Contact contactPerson = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = contactPerson.Id
			), 
			true
		);

		Sales_Process__c prline = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Contact_from_Offer_Line__c = contactPerson.Id,
				Product_Line_to_Proposal__c = proposal.Id,
				Offer_Line_Resource__c = flat.Id
			), true
		);

		

		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal.Id,
				Contact__c = proposal.Contact__c,
				Expected_Date_of_Signing_Reservation__c = Date.today()-1,
				Expected_Date_of_Signing__c = Date.today()-1,
				Reservation_Quque_First_Place__c = true, 
				Share_Ok_Among_Customers__c = true,
				Status__c = CommonUtility.SALES_PROCESS_STATUS_REJECTED
			),
			true
		);

		prline.Offer_Line_to_Sale_Term__c = saleTerms.Id;
		update prline;

		Payment__c paymentInstallment = TestHelper.createPaymentsInstallment(
			new Payment__c(
				Agreements_Installment__c = saleTerms.Id,
				Contact__c = proposal.Contact__c
			),
			true
		);	

		Test.startTest();
			saleTerms.Date_of_Signing_Reservation__c = Date.today();
			saleTerms.Date_of_signing__c = Date.today();
			update saleTerms;
			
			// Check if saleterms is ready for transfer to after sales service 
			String allowHandoverProtocol = WebserviceUtilities.allowHandoverProtocol(saleTerms.Id);

			// if saleTerms is ready create afterSales with a connection between them 
			if(allowHandoverProtocol == 'true') {
				Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
					new Sales_Process__c(
						Agreement__c = saleTerms.Id,
						Contact__c 	 = contactPerson.Id,
						Status__c = CommonUtility.SALES_PROCESS_STATUS_HANDOVER_SIGNED
					),
					true
				);
				// check if Agreement_Number__c has been transfered to afterSales
				Sales_Process__c afterSalesQuerry = [SELECT Id, Agreement_Number__c FROM Sales_Process__c WHERE Id=:afterSales.Id];
				/*System.assert(afterSalesQuerry.Agreement_Number__c != NULL);*/

				// Check if afterSales is ready for transfer to Final Agreement 
				String allowFinalAgreement = WebserviceUtilities.allowFinalAgreement(afterSalesQuerry.Id);
				System.debug('allowFinalAgreement '+allowFinalAgreement);

				if(allowFinalAgreement == 'true') {
					Sales_Process__c finalAgreement = TestHelper.createSalesProcessFinalAgreement(
						new Sales_Process__c(
							Handover_Protocol__c = afterSales.Id,
							Preliminary_Agreement__c = saleTerms.Id,
							Contact__c = contactPerson.Id
						),
						true
					);

					// check if Agreement_Number__c has been transfered to finalAgreement
					Sales_Process__c finalAgreementQuerry = [SELECT Id, Agreement_Number__c FROM Sales_Process__c WHERE Id=:finalAgreement.Id];
					/*System.assert(finalAgreementQuerry.Agreement_Number__c != NULL);*/
				}
			}
		Test.stopTest();
	}
}