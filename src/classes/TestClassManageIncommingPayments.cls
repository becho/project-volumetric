/**
*   @author         Beniamin Cholewa
*   @description    Test class for ManageIncommingPayments
**/
@isTest
private class TestClassManageIncommingPayments {

	private static testMethod void allowIncomingPaymentAfterSalesTest() {
	   // Resource__c resource = TestHelper.createResourceFlatApartment(new Resource__c(), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessAfterSalesService(new Sales_Process__c(), true);
	    Test.startTest();
	   // Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(After_sales_Service__c = sp.Id, Bank_Account_For_Resource__c = '00000000000000000000000000', Payment_For__c = resource.Id), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(After_sales_Service__c = sp.Id, Bank_Account_For_Resource__c = '00000000000000000000000000'), true);
	    
        ManageIncommingPayments.allowIncomingPaymentAfterSales(sp.Id);
        Test.stopTest();
	}

}