/**
* @author       Przemysław Tustanowski
* @description  Class for managing resource fields 
*/

public without sharing class ResourceManager {
    public ResourceManager() {
        
    }
    // method for setting investment lookup
    public static void setInvestmentLookup(Resource__c res){
        res.Investment__c = null;
        CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT);
        String recTypeName;
        for(String rtDevName : CommonUtility.mRecordTypeName2Id.keyset()){
            if(CommonUtility.mRecordTypeName2Id.get(rtDevName) == res.RecordTypeId){
                recTypeName = rtDevName;
                break;
            }
        }

        if (recTypeName.contains(CommonUtility.RESOURCE_TYPE_STORAGE.toUpperCase())) {
            res.Investment__c = res.Investment_Storage__c;
        } else
        if (recTypeName.contains(CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT.toUpperCase())) {
            res.Investment__c = res.Investment_Flat__c;
        } else
        if (recTypeName.contains(CommonUtility.RESOURCE_TYPE_HOUSE.toUpperCase())) {
            res.Investment__c = res.Investment_House__c;
        } else
        if (recTypeName.contains(CommonUtility.RESOURCE_TYPE_INTERIOR_DESIGN.toUpperCase())) {
            res.Investment__c = res.Investment_Interior_Design_Package__c;
        } else
        if (recTypeName.contains(CommonUtility.RESOURCE_TYPE_PARKING_SPACE.toUpperCase())) {
            res.Investment__c = res.Investment_Parking_Space__c;
        } else
        if (recTypeName.contains(CommonUtility.RESOURCE_TYPE_COMPLETION.toUpperCase())) {
            res.Investment__c = res.Investment_Standard_of_Completion__c;
        } else
        if (recTypeName.contains(CommonUtility.RESOURCE_TYPE_BUILDING.toUpperCase())) {
            res.Investment__c = res.Investment_Building__c;
        } else
        if (recTypeName.contains(CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY.toUpperCase())) {
            res.Investment__c = res.Investment_Commercial_Property__c;
        }
    }
    
    /* Mateusz Pruszyński */
    // Trigger part to update low and high vat rates   
    public static void updateVatRates(List<Resource__c> resources) {
    	// get vat distribution from custom settings
    	AreaAndVatPercentageDistribution__c distribution = AreaAndVatPercentageDistribution__c.getInstance('CurrentDistributionFlat');
    	for(Resource__c res : resources) {
            if(res.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)) {
    		    updateRatesFlat((res.Total_Area_Planned__c != null ? res.Total_Area_Planned__c : res.Usable_Area_Planned__c), res, distribution);
            } else {
                res.Area_Percentage_Share_LowRate__c = 100;
                res.Area_Percentage_Share_HighRate__c = 0;
            }
    	}
        //update resources; // -- uncomment if called from execute anonymous. Otherwise leave commented
    }  
    public static void updateRatesFlat(Decimal area, Resource__c res, AreaAndVatPercentageDistribution__c distribution) {
        res.Area_Percentage_Share_LowRate__c = (area > distribution.Distribution_Threshold_Area__c) ? (distribution.Distribution_Threshold_Area__c * 100 / area) : 100;
        res.Area_Percentage_Share_HighRate__c = (area > distribution.Distribution_Threshold_Area__c) ? ((area - distribution.Distribution_Threshold_Area__c) * 100 / area) : 0;
    } 

    public static void checkResourcesUniqueness(Map<Id,Resource__c> resourcesToRestrictUniqueness) {
        List<Resource__c> allInvestments = [SELECT Id FROM Resource__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)];
        if(!allInvestments.isEmpty()) {
            List<Id> allInvestmentIds = new List<Id>();
            for(Resource__c allInv : allInvestments) {
                allInvestmentIds.add(allInv.Id);
            }
            List<Resource__c> allResourcesWithInvestment = [SELECT Id, Name, InvestmentId__c FROM Resource__c WHERE Investment__c in :allInvestmentIds];
            for(Id invId : resourcesToRestrictUniqueness.keySet()) {
                Resource__c res = resourcesToRestrictUniqueness.get(invId);
                for(Resource__c allRes : allResourcesWithInvestment) {
                    if(invId == allRes.InvestmentId__c && res.Name == allRes.Name) {
                        res.Name.addError(Label.DuplicateNameRes);
                    }
                }
            }
        }
    }
    public static void checkResourcesAreaOnAfter(Map<Id, List<Resource__c>> resourcesAreaOnAfter) {            
        Map<Id, Resource__c> stages = new Map<Id, Resource__c>([
            SELECT Id, Common_Surface__c
            FROM Resource__c
            WHERE Id in: resourcesAreaOnAfter.keySet()
        ]);

        List<Resource__c> allResources = [
            SELECT Usable_Area_Planned__c, Stage__c, Collective_Space_parking__c, Collective_Space_storage__c
            FROM Resource__c
            WHERE Stage__c in: stages.keySet()
        ];
        List<Resource__c> stagesToUpdate = new List<Resource__c>();
        for(Id key : stages.keySet()) {
            Decimal areaToAdd = 0;
            for(Resource__c r : allResources) {
                if(r.Stage__c == key && r.Usable_Area_Planned__c != null && (r.Collective_Space_parking__c == null && r.Collective_Space_storage__c == null)) {
                    areaToAdd += r.Usable_Area_Planned__c;
                }
            }
            stages.get(key).Common_Surface__c = areaToAdd;
            stagesToUpdate.add(stages.get(key));
        }
        if(!stagesToUpdate.isEmpty()) {
            update stagesToUpdate;
        }
    }

    public static void checkResourceWithConnectedCollectiveSpace(Map<Id, List<Resource__c>> resourceWithConnectedCollectiveSpace) {
        Map<Id, Resource__c> collectiveSpace = new Map<Id, Resource__c>([
                SELECT Id, Usable_Area_Planned__c
                FROM Resource__c
                WHERE Id in: resourceWithConnectedCollectiveSpace.keySet()
            ]);
            List<Resource__c> allResource = new List<Resource__c>();
            for(Id key : collectiveSpace.keySet()) {
                List<Resource__c> resource2assing = resourceWithConnectedCollectiveSpace.get(key);
                for(Resource__c r : resource2assing) {
                    Resource__c res = new Resource__c (
                        Id = r.Id,
                        Common_Surface__c = collectiveSpace.get(key).Usable_Area_Planned__c
                    );
                    allResource.add(res);
                } 
            }
            if(!allResource.isEmpty()) {
                update allResource;
            }
    }

    public static void checkResourceAfterUpdatingCollectiveSpace(Map<Id, Resource__c> resourceAfterUpdatingCollectiveSpace) {                 
        List<Resource__c> allResources = [
            SELECT Common_Surface__c, Collective_Space_parking__c, Collective_Space_storage__c
            FROM Resource__c
            WHERE Collective_Space_parking__c in: resourceAfterUpdatingCollectiveSpace.keySet()
            OR Collective_Space_storage__c in: resourceAfterUpdatingCollectiveSpace.keySet()
        ];
        List<Resource__c> resourcesToUpdate = new List<Resource__c>();
        for(Id key : resourceAfterUpdatingCollectiveSpace.keySet()) {
            for(Resource__c r : allResources) {
                if(r.Collective_Space_parking__c == resourceAfterUpdatingCollectiveSpace.get(key).Id 
                    || r.Collective_Space_storage__c == resourceAfterUpdatingCollectiveSpace.get(key).Id) {
                    r.Common_Surface__c = resourceAfterUpdatingCollectiveSpace.get(key).Usable_Area_Planned__c;
                    resourcesToUpdate.add(r);
                }
            }
        }

        if(!resourcesToUpdate.isEmpty()) {
            update resourcesToUpdate;
        }
    }

    public static void checkResourceLandSurfaceAfterUpdatingStageField(Map<Id, List<Resource__c>> resourceLandSurfaceAfterUpdatingStageField) {
        Map<Id, Resource__c> stages = new Map<Id, Resource__c>([
            SELECT Id, Common_Surface__c
            FROM Resource__c
            WHERE Id in: resourceLandSurfaceAfterUpdatingStageField.keySet()
        ]);
        List<Resource__c> allResource = new List<Resource__c>();
        for(Id key : stages.keySet()) {
             List<Resource__c> resource2assignLandSurface = resourceLandSurfaceAfterUpdatingStageField.get(key);
             for(Resource__c r : resource2assignLandSurface) {
                if(r.Collective_Space_parking__c == null && r.Collective_Space_storage__c == null) {
                    Resource__c res = new Resource__c(
                        Id = r.Id,
                        Common_Surface__c = stages.get(key).Common_Surface__c
                    );
                    allResource.add(res);
                }
             }
        }
        if(!allResource.isEmpty()) {
            update allResource;
        }
    }
    public static void checkResourceLandSurface(Map<String, List<Resource__c>> resourceLandSurface) {
        List<Resource__c> allResource =  [
            SELECT InvestmentId__c, Common_Surface__c, Collective_Space_parking__c, Collective_Space_storage__c 
            FROM Resource__c 
            WHERE InvestmentId__c in: resourceLandSurface.keySet()
        ];
        for(Resource__c res : allResource){
            if(res.Collective_Space_parking__c == null && res.Collective_Space_storage__c == null) {
                res.Common_Surface__c = resourceLandSurface.get(res.InvestmentId__c)[0].Common_Surface__c;
            }
        }
        if(!allResource.isEmpty()){
            update allResource;
        }
    }

    public static void checkResourceLandSurfaceBeforeInsert(Map<Id, List<Resource__c>> resourceLandSurfaceBeforeInsert) {
        Map<Id, Resource__c> stages = new Map<Id, Resource__c>([
            SELECT Id, Common_Surface__c
            FROM Resource__c
            WHERE Id in: resourceLandSurfaceBeforeInsert.keySet()
        ]);

        for(Id key : stages.keySet()) {
             List<Resource__c> resource2assignLandSurface = resourceLandSurfaceBeforeInsert.get(key);
                 for(Resource__c r : resource2assignLandSurface) {
                    r.Common_Surface__c = stages.get(key).Common_Surface__c;
                 }
        }   
    }

    public static void checkResourceWithConnectedCollectiveSpaceBeforeInsert(Map<Id, List<Resource__c>> resourceWithConnectedCollectiveSpaceBeforeInsert) {
        Map<Id, Resource__c> collectiveSpace = new Map<Id, Resource__c>([
            SELECT Id, Usable_Area_Planned__c
            FROM Resource__c
            WHERE Id in: resourceWithConnectedCollectiveSpaceBeforeInsert.keySet()
        ]);

        for(Id key : collectiveSpace.keySet()) {
             List<Resource__c> resource2assign = resourceWithConnectedCollectiveSpaceBeforeInsert.get(key);
                 for(Resource__c r : resource2assign) {
                    r.Common_Surface__c = collectiveSpace.get(key).Usable_Area_Planned__c;
                 }
        }  
     }
     public static void checkResourceLandSurfaceWithNullStage(Map<Id, List<Resource__c>> resourceLandSurfaceWithNullStage) {
        List<Resource__c> resources = new List<Resource__c>();
        for(Id key : resourceLandSurfaceWithNullStage.keySet() ) {
            for(Resource__c r : resourceLandSurfaceWithNullStage.get(key)){
                Resource__c res = new Resource__c(
                    Id = r.Id,
                    Common_Surface__c = 0
                );
                resources.add(res);
            }
        }
        if(!resources.isEmpty()) {
            update resources;
        }
     }


}