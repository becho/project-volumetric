@isTest
private class TestClassManageDefinedPromotions {

    @testSetup
	private static void setupData() {
	    Manager_Panel__c managerPanel = TestHelper.createManagerPanelMonthlySalesTarget(new Manager_Panel__c(Discount_Value__c = 0,Year__c = String.valueOf(Date.today().year()), Month__c = String.valueOf(Date.today().month())), true);
        Resource__c resource = TestHelper.createResourceStorage(new Resource__c(Price__c = 10, Defined_Promotion__c = managerPanel.Id), true);
        User testUser = TestHelper.createUser(new User(), true);
	}
	
	private static testMethod void updatePromotionPriceOnResourceTest() {
	    Map<String, List<Resource__c>> resourceMap = new Map<String, List<Resource__c>>();
	    resourceMap.put('test', [select Id from Resource__c]);
	    Test.startTest();
	        ManageDefinedPromotions.updatePromotionPriceOnResource(resourceMap);
	    Test.stopTest();
	}
	
	private static testMethod void calculatePromotionalPriceTest() {
	    Resource__c resource = [select Id, Price__c from Resource__c where Price__c <> null limit 1];
        Manager_Panel__c managerPanel = [select Id, Discount_Kind__c, Discount_Value__c, Active__c from Manager_Panel__c];
        
	    Test.startTest();
	        ManageDefinedPromotions.calculatePromotionalPrice(resource, managerPanel);
	    Test.stopTest();
	}

    private static testMethod void updateIsActivePromotionOnResourceTest() {
        List<Manager_Panel__c> managerPanel = [select Id, Discount_Kind__c, Discount_Value__c, Active__c from Manager_Panel__c];
        
	    Test.startTest();
	        ManageDefinedPromotions.updateIsActivePromotionOnResource(managerPanel);
	    Test.stopTest();
	}
	
	private static testMethod void sendEmailWithPromoResultTest() {
        List<Manager_Panel__c> managerPanel = [select Id, Discount_Kind__c, Discount_Value__c, Active__c from Manager_Panel__c];
        List<User> testUser = [select Id from User];
        
	    Test.startTest();
	        ManageDefinedPromotions.sendEmailWithPromoResult('test', 'test', 'test', testUser.get(0).Id, managerPanel.get(0).Id);
	    Test.stopTest();
	}
}