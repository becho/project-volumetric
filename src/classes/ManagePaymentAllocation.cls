/**
*   @author         Mateusz Pruszyński
*   @description    Manage payment allocation (incoming payments with Assignment__c = CommonUtility.PAYMENT_ASSIGNMENT_RELEASE)
**/
public without sharing class ManagePaymentAllocation {

	/* Manage incoming payments for for account allocation */
	public static void managePaymentAllocationForIncomingPayments(List<Payment__c> incomingPayments) {
		// initializa wrapper structure
		Map<Id, PaymentAllocationWrapper> paymentAllocationMap = new Map<Id, PaymentAllocationWrapper>();
		// 1 - get after-sales service Ids, to match each incoming payment with particulat investment, later on
		Set<Id> afterSalesIds = new Set<Id>();
		for(Payment__c ip : incomingPayments) {
			afterSalesIds.add(ip.After_sales_from_Incoming_Payment__c);
		}
		// 2 - pull any product attached to after-sales service to match with investment (products can be attached only from within same investment for each process!)
		List<Sales_Process__c> productLines = getProductLines(afterSalesIds);
		if(!productLines.isEmpty()) {
			Map<Id, Set<Payment__c>> prodLinePaymentsMap = new Map<Id, Set<Payment__c>>();
			for(Sales_Process__c pl : productLines) {
				for(Payment__c ip : incomingPayments) {
					if(ip.After_sales_from_Incoming_Payment__c == pl.Offer_Line_to_After_sales_Service__c) {
						if(prodLinePaymentsMap.containsKey(pl.Offer_Line_Resource__r.Investment__c)) {
							prodLinePaymentsMap.get(pl.Offer_Line_Resource__r.Investment__c).add(ip);
						} else {
							prodLinePaymentsMap.put(pl.Offer_Line_Resource__r.Investment__c, new Set<Payment__c> {ip});
						}
					}
				}
			}
			// 3 - finally, get investments
			List<Resource__c> investments = getInvestments(prodLinePaymentsMap.keySet());
			if(!investments.isEmpty()) {
				PaymentAllocationWrapper pam;
				for(Resource__c inv : investments) {
					pam = new PaymentAllocationWrapper(inv);
					for(Id key : prodLinePaymentsMap.keySet()) {
						if(key == inv.Id) {
							pam.incomingPayments.addAll(prodLinePaymentsMap.get(key));
						}
					}
					paymentAllocationMap.put(inv.Id, pam);
				}
			}
			List<Payment__c> payments2update = new List<Payment__c>();
			for(Id key : paymentAllocationMap.keySet()) {
				for(Payment__c ip : paymentAllocationMap.get(key).incomingPayments) {
					payments2update.add(
						new Payment__c (
							Id = ip.Id,
							After_sales_from_Incoming_Payment_Trust__c = null,
							After_sales_from_Incoming_Payment_Curren__c = ip.After_sales_from_Incoming_Payment__c,
							Bank_Account_For_Resource__c = paymentAllocationMap.get(key).investment.Current_Bank_Account__c
						)
					);
				}
			}
			try {
				update payments2update;
			} catch(Exception e) {
				ErrorLogger.log(e);
			}
		}
	}

	public static List<Sales_Process__c> getProductLines(Set<Id> afterSalesIds) {
		return [SELECT Offer_Line_Resource__r.Investment__c, Offer_Line_to_After_sales_Service__c
				 FROM Sales_Process__c 
				 WHERE Offer_Line_to_After_sales_Service__c in :afterSalesIds
				 AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
				 AND Offer_Line_Resource__c <> null];
	}

	public static List<Resource__c> getInvestments(Set<Id> investmentIds) {
		return [SELECT Current_Bank_Account__c
				 FROM Resource__c 
				 WHERE Id in :investmentIds
				 AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
				 AND Current_Bank_Account__c <> null];		
	}

	/* Wrapper structure to match incoming payments with bank accounts from investments */
	private class PaymentAllocationWrapper {
		private List<Payment__c> incomingPayments {get; set;}
		private Resource__c investment {get; set;}

		public PaymentAllocationWrapper(Resource__c investment) {
			incomingPayments = new List<Payment__c>();
			this.investment = investment;
		}
	}

}