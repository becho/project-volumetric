/**
* @author 		Mateusz Wolak-Książek
* @description 	Test class for deleteOfferLineController.cls
*/

@isTest
private class TestClassDeleteOfferLineController {
	private static final String CURRENT_WAY = 'CurrentWay';

	@testSetup static void createData() {
		BankAccountGenWay__c currentWay = new BankAccountGenWay__c();
        currentWay.Name = ManageBankAccountForSalesProcess.CURRENT_WAY;
        currentWay.Choose_Way__c = 2;
        insert currentWay;
	}

	@isTest static void testPassingResourceInUrl() {

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        List<Sales_Process__c> sps = new List<Sales_Process__c>();
        sps.add(proposal);

        insert sps;

        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(
        	new Sales_Process__c(
        		Product_Line_to_Proposal__c = proposal.Id,
        		Offer_Line_Resource__c = TestHelper.createResourceFlatApartment(new Resource__c(Bank_Account_Number__c = '11122211122211122211122209') ,true).Id
        	),
        	true
        );

        Test.startTest();

	        PageReference pageRef = Page.DeleteOfferLine;
	        Test.setCurrentPageReference(pageRef);
	        
	        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ offerLine.Offer_Line_Resource__c );
	        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offerLine.Offer_Line_Resource__c );
	        
	        List<Sales_Process__c> selectedOfferLines = new List<Sales_Process__c>();
	        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedOfferLines); 
	        stdSetController.setSelected(selectedOfferLines);
	        DeleteOfferLineController controller = new DeleteOfferLineController(stdSetController);    

	    Test.stopTest();

	    for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(Label.cannotDeleteSPFromResource,  msg.getSummary()); 
            System.assertEquals(ApexPages.Severity.Info, msg.getSeverity());
	    }
	    System.assert(!controller.renderTable);


	}

	 @isTest static void testingPageReferenceFunctions() {

	 	Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        List<Sales_Process__c> sps = new List<Sales_Process__c>();
        sps.add(proposal);
        insert sps;

        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(
        	new Sales_Process__c(
        		Product_Line_to_Proposal__c = proposal.Id,
        		Offer_Line_Resource__c = TestHelper.createResourceFlatApartment(new Resource__c(Bank_Account_Number__c = '11122211122211122211122209') ,true).Id
        	),
        	true
        );
        
        PageReference pageRef = Page.DeleteOfferLine;
        Test.setCurrentPageReference(pageRef);
        String retUrl = '/'+proposal.Id;
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, retUrl);
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offerLine.Id );

  		List<Sales_Process__c> selectedOfferLines = new List<Sales_Process__c>();
        selectedOfferLines.add(offerLine);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedOfferLines); 
        stdSetController.setSelected(selectedOfferLines);
        DeleteOfferLineController controller = new DeleteOfferLineController(stdSetController);
        
        Test.startTest();
			String pageToTestNo = controller.no().getUrl();

	        String pageToTestYes = controller.yes().getUrl();
        
        Test.stopTest();

        List<Sales_Process__c> retrivedOfferLine = [SELECT Id FROM Sales_Process__c WHERE Id =: offerLine.Id];
	        
        System.assertEquals(retUrl, pageToTestNo);
        System.assert(retrivedOfferLine.isEmpty());
        System.assertEquals(retUrl, pageToTestYes);
    }

	@isTest static void testPassingNullObject() {
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        List<Sales_Process__c> sps = new List<Sales_Process__c>();
        sps.add(proposal);
        insert sps;

        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(
        	new Sales_Process__c(
        		Product_Line_to_Proposal__c = proposal.Id,
        		Offer_Line_Resource__c = TestHelper.createResourceFlatApartment(new Resource__c(Bank_Account_Number__c = '11122211122211122211122209') ,true).Id
        	),
        	true
        );

        PageReference pageRef = Page.DeleteOfferLine;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/null');
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offerLine.Id );
        
        List<Sales_Process__c> selectedOfferLines = new List<Sales_Process__c>();
        selectedOfferLines.add(offerLine);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedOfferLines); 
        stdSetController.setSelected(selectedOfferLines);
        DeleteOfferLineController controller = new DeleteOfferLineController(stdSetController);
        
       	System.assertEquals(controller.renderTable, true);

	}

	@isTest static void testDeletionFromAfterSalesService() {

		 Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        Sales_Process__c saleTerms = new Sales_Process__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT));
        Sales_Process__c afterSales = new Sales_Process__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL));
        List<Sales_Process__c> sps = new List<Sales_Process__c>();
        sps.add(proposal);
        sps.add(saleTerms);
        sps.add(afterSales);
        insert sps;

        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(
        	new Sales_Process__c(
        		Product_Line_to_Proposal__c = proposal.Id,
        		Offer_Line_to_Sale_Term__c = saleTerms.Id,
        		Offer_Line_to_After_sales_service__c = afterSales.Id,
        		Offer_Line_Resource__c = TestHelper.createResourceFlatApartment(new Resource__c(Bank_Account_Number__c = '11122211122211122211122209') ,true).Id
        	),
        	true
        );

        PageReference pageRef = Page.DeleteOfferLine;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ afterSales.Id );
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offerLine.Id );
        
        List<Sales_Process__c> selectedOfferLines = new List<Sales_Process__c>();
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedOfferLines); 
        stdSetController.setSelected(selectedOfferLines);
        DeleteOfferLineController controller = new DeleteOfferLineController(stdSetController);
        
        
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(Label.YouCannotUseButton + ' ' + Label.AfterSalesServiceExists,  msg.getSummary()); 
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }

	}

	@isTest static void testProposalWithAlreadyCreatedSaleTerms() {

		 Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        Sales_Process__c saleTerms = new Sales_Process__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT));
        List<Sales_Process__c> sps = new List<Sales_Process__c>();
        sps.add(proposal);
        sps.add(saleTerms);
        insert sps;

        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(
        	new Sales_Process__c(
        		Product_Line_to_Proposal__c = proposal.Id,
        		Offer_Line_to_Sale_Term__c = saleTerms.Id,
        		Offer_Line_Resource__c = TestHelper.createResourceFlatApartment(new Resource__c(Bank_Account_Number__c = '11122211122211122211122209') ,true).Id
        	),
        	true
        );

        PageReference pageRef = Page.DeleteOfferLine;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ proposal.Id );
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offerLine.Id );
        
        List<Sales_Process__c> selectedOfferLines = new List<Sales_Process__c>();
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedOfferLines); 
        stdSetController.setSelected(selectedOfferLines);
        DeleteOfferLineController controller = new DeleteOfferLineController(stdSetController);
        
        
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(Label.YouCannotUseButton + ' ' + Label.PraliminaryAgreementAlreadyExists,  msg.getSummary()); 
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }

	}



	@isTest static void testSaleTermsWithAfterSalesConnected() {

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        Sales_Process__c saleTerms = new Sales_Process__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT));
        Sales_Process__c afterSales = new Sales_Process__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL));
        List<Sales_Process__c> sps = new List<Sales_Process__c>();
        sps.add(proposal);
        sps.add(saleTerms);
        sps.add(afterSales);
        insert sps;

        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(
        	new Sales_Process__c(
        		Product_Line_to_Proposal__c = proposal.Id,
        		Offer_Line_to_Sale_Term__c = saleTerms.Id,
        		Offer_Line_to_After_sales_service__c = afterSales.Id,
        		Offer_Line_Resource__c = TestHelper.createResourceFlatApartment(new Resource__c(Bank_Account_Number__c = '11122211122211122211122209') ,true).Id
        	),
        	true
        );

        PageReference pageRef = Page.DeleteOfferLine;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ saleTerms.Id );
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offerLine.Id );
        
        List<Sales_Process__c> selectedOfferLines = new List<Sales_Process__c>();
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedOfferLines); 
        stdSetController.setSelected(selectedOfferLines);
        DeleteOfferLineController controller = new DeleteOfferLineController(stdSetController);
        
        
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(Label.YouCannotUseButton + ' ' + Label.AfterSalesServiceExists,  msg.getSummary()); 
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }



	}

	@isTest static void testEmptyDeletion() {
		List<Sales_Process__c> selectedOfferLines        = new List<Sales_Process__c>();
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedOfferLines); 

        stdSetController.setSelected(selectedOfferLines);

        PageReference pageRef = Page.DeleteOfferLine;
        Test.setCurrentPageReference(pageRef);
        Sales_Process__c processProposal = TestHelper.createSalesProcessProposal(null, true);

        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(
        	new Sales_Process__c(
        		Product_Line_to_Proposal__c = processProposal.Id
        	),
        	true
        );
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ processProposal.Id );
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offerLine.Id );
 
        Test.startTest();
 	       DeleteOfferLineController controller = new DeleteOfferLineController(stdSetController);
    	Test.stopTest();

        System.assertEquals(controller.selectedOfferLines[0].Id, offerLine.Id);
        System.assertEquals(controller.renderTable, true);
	}


	@isTest static void testSaleTermsApexMessageSaleTermsAlreadySigned() {

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        Sales_Process__c saleTerms = new Sales_Process__c(
        	Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED,
        	Date_of_signing__c = Date.today(),
            Schedule_Generated__c = true,
            Reservation_Quque_First_Place__c = true,
            Share_Ok_Among_Customers__c = true,
        	RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
        );
        List<Sales_Process__c> sps = new List<Sales_Process__c>();
        sps.add(proposal);
        sps.add(saleTerms);
        insert sps;

        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(
        	new Sales_Process__c(
        		Product_Line_to_Proposal__c = proposal.Id,
        		Offer_Line_to_Sale_Term__c = saleTerms.Id,
        		Offer_Line_Resource__c = TestHelper.createResourceFlatApartment(new Resource__c(Bank_Account_Number__c = '11122211122211122211122209') ,true).Id
        	),
        	true
        );

        Test.startTest();

	        PageReference pageRef = Page.DeleteOfferLine;
	        Test.setCurrentPageReference(pageRef);
	        
	        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ saleTerms.Id );
	        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offerLine.Id );
	        
	        List<Sales_Process__c> selectedOfferLines = new List<Sales_Process__c>();
	        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedOfferLines); 
	        stdSetController.setSelected(selectedOfferLines);
	        DeleteOfferLineController controller = new DeleteOfferLineController(stdSetController);    

	    Test.stopTest();

	    for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(Label.YouCannotUseButton + ' ' + Label.SaleTermsAlreadySigned,  msg.getSummary()); 
            System.assertEquals(ApexPages.Severity.Info, msg.getSeverity());
	    }

	}


	@isTest static void testSaleTermsApexMessageWaitingForManagerAproval() {

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        Sales_Process__c saleTerms = new Sales_Process__c(
        	Status__c = CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL,
        	RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
        );
        List<Sales_Process__c> sps = new List<Sales_Process__c>();
        sps.add(proposal);
        sps.add(saleTerms);
        insert sps;

        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(
        	new Sales_Process__c(
        		Product_Line_to_Proposal__c = proposal.Id,
        		Offer_Line_to_Sale_Term__c = saleTerms.Id,
        		Offer_Line_Resource__c = TestHelper.createResourceFlatApartment(new Resource__c(Bank_Account_Number__c = '11122211122211122211122209') ,true).Id
        	),
        	true
        );

        Test.startTest();

	        PageReference pageRef = Page.DeleteOfferLine;
	        Test.setCurrentPageReference(pageRef);
	        
	        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ saleTerms.Id );
	        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offerLine.Id );
	        
	        List<Sales_Process__c> selectedOfferLines = new List<Sales_Process__c>();
	        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedOfferLines); 
	        stdSetController.setSelected(selectedOfferLines);
	        DeleteOfferLineController controller = new DeleteOfferLineController(stdSetController);    

	    Test.stopTest();

	    for(ApexPages.Message msg :  ApexPages.getMessages()) {
	            System.assertEquals(Label.YouCannotUseButton + ' ' + Label.RecordIsWaitingForManager,  msg.getSummary()); 
	            System.assertEquals(ApexPages.Severity.Info, msg.getSeverity());
	    }

	}


	@isTest static void testSaleTermsApexMessage() {

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
        Sales_Process__c saleTerms = new Sales_Process__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT));
        List<Sales_Process__c> sps = new List<Sales_Process__c>();
        sps.add(proposal);
        sps.add(saleTerms);
        insert sps;

        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(
        	new Sales_Process__c(
        		Product_Line_to_Proposal__c = proposal.Id,
        		Offer_Line_to_Sale_Term__c = saleTerms.Id,
        		Offer_Line_Resource__c = TestHelper.createResourceFlatApartment(new Resource__c(Bank_Account_Number__c = '11122211122211122211122209') ,true).Id
        	),
        	true
        );

        Test.startTest();

	        PageReference pageRef = Page.DeleteOfferLine;
	        Test.setCurrentPageReference(pageRef);
	        
	        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ saleTerms.Id );
	        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offerLine.Id );
	        
	        List<Sales_Process__c> selectedOfferLines = new List<Sales_Process__c>();
	        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedOfferLines); 
	        stdSetController.setSelected(selectedOfferLines);
	        DeleteOfferLineController controller = new DeleteOfferLineController(stdSetController);
	        

	        saleTerms.Status__c = CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER;
	        update saleTerms;
	        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ saleTerms.Id);
	        DeleteOfferLineController controllerMsg = new DeleteOfferLineController(stdSetController);
	        
	        for(ApexPages.Message msg :  ApexPages.getMessages()) {
	            System.assertEquals(Label.YouCannotUseButton + ' ' + Label.SaleTermsAccepted,  msg.getSummary()); 
	            System.assertEquals(ApexPages.Severity.Info, msg.getSeverity());
	        }

	    Test.stopTest();

        System.assert(controller.renderTable);


	}


	
}