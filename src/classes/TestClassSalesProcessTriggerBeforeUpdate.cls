/* Grzegorz Murawiński */
// Test class trigger sales process before update

@isTest
private class TestClassSalesProcessTriggerBeforeUpdate {


// Test Update Customer Field on Sale Terms 

// Prepare test currentConfig
@testsetup
static void setuptestData(){
	 ConditionalSaleTermsAcceptance__c acceptanceConfig = new ConditionalSaleTermsAcceptance__c();
	 acceptanceConfig.Name = 'currentConfig';
	 acceptanceConfig.Switch_On__c = false;
	 insert acceptanceConfig;
}

// Test for company: check assign name and NIP number
@isTest static void TestUpdateCustomerFieldOnSaleTermsForCompany() {

//prepare data
// New investment
	Resource__c investment = TestHelper.createInvestment(null, true);

//New flat
	Resource__c flat = TestHelper.createResourceFlatApartment(
		new Resource__c(
			Investment_Flat__c = investment.Id
			), 
		true
		);

// New company account
	Account companyAccount = TestHelper.createAccountStandard(
		new Account(
			NIP__c = '1234563218'
			), 
		true
		);
	
System.debug('companyAccount: ' + companyAccount);
// Contact person for company
	Contact contactPerson = TestHelper.createContactPerson(
		new Contact(
			AccountId = companyAccount.Id
			), 
		true
		);
System.debug('contactPerson: ' + contactPerson);

// Proposal for company
	Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
		new Sales_Process__c(
			Contact__c = contactPerson.Id
			), 
		true
	);
System.debug('proposal TEST: ' + proposal);


// Sale terms for company	
	Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
		new Sales_Process__c(
			Offer__c = proposal.Id,
			Offer_Line_to_Sale_Term__c = proposal.Id,
			Contact__c = proposal.Contact__c,
			Contact_from_Offer_Line__c = proposal.Contact__c,
			Expected_Date_of_Signing_Reservation__c = Date.today()-1,
			Expected_Date_of_Signing__c = Date.today()-1,
			Account_from_Customer_Group__c = companyAccount.Id,
			Reservation_Quque_First_Place__c = true, 
			Share_Ok_Among_Customers__c = true,
			Developer_Agreement_from_Customer_Group__c = proposal.Id 
			),
		true
		);

// Payment for sale terms by company
	Payment__c paymentInstallment = TestHelper.createPaymentsInstallment(
		new Payment__c(
			Agreements_Installment__c = saleTerms.Id,
			Contact__c = proposal.Contact__c
			),
		true
		);

	TestHelper.CreatePaymentScheduleConfig_CurrentConfig(true);
	System.debug('saleTerms TEST: ' + saleTerms);



	Test.startTest();
// update date of signing reservation: run trigger UpdateCustomerFieldsOnSaleTerms	
		saleTerms.Date_of_Signing_Reservation__c = Date.today();
		update saleTerms;

	Test.stopTest();

	Sales_Process__c saleTerms2Assert = [
				Select First_names__c, Second_names__c, NIP__c, Account_from_Customer_Group__r.NIP__c, Developer_Agreement_from_Customer_Group__c
				from Sales_process__c 
				where Developer_Agreement_from_Customer_Group__c = :saleTerms.Developer_Agreement_from_Customer_Group__c 
				LIMIT 1
				];

	System.assertEquals(companyAccount.Name,saleTerms2Assert.First_names__c);
	System.assertEquals(companyAccount.NIP__c,saleTerms2Assert.NIP__c);
	System.debug('#########Sale Terms: ' +saleTerms2Assert);

}


// Test for customer group: two companies; check names and NIP numbers
@isTest static void TestUpdateCustomerFieldOnSaleTermsForCustomerGroup() {

// prepare data
// New investment
	Resource__c investment = TestHelper.createInvestment(null, true);

// New flat
	Resource__c flat = TestHelper.createResourceFlatApartment(
		new Resource__c(
			Investment_Flat__c = investment.Id
			), 
		true
		);

// First company
	Account companyAccount = TestHelper.createAccountStandard(
		new Account(
			NIP__c = '1234563218'
			), 
		true
		);

// Second company
	Account companyAccount2 = TestHelper.createAccountStandard(
		new Account(
			NIP__c = '9832613976'
			), 
		true
		);
	
// Contact for company 1	
System.debug('companyAccount: ' + companyAccount);

	Contact contactPerson = TestHelper.createContactPerson(
		new Contact(
			AccountId = companyAccount.Id
			), 
		true
		);
System.debug('contactPerson: ' + contactPerson);

// Proposal for customer group
	Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
		new Sales_Process__c(
			Contact__c = contactPerson.Id
			), 
		true
	);

// Add second company to proposal
	Sales_Process__c customGroup = TestHelper.createSalesProcessCustomerGroup(
		new Sales_Process__c(
			Proposal_from_Customer_Group__c = proposal.Id,
			Account_from_Customer_Group__c = companyAccount2.Id
			), 
		true
		);
System.debug('proposal TEST: ' + proposal);


//Sale terms for proposal	
	Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
		new Sales_Process__c(
			Offer__c = proposal.Id,
			Offer_Line_to_Sale_Term__c = proposal.Id,
			Contact__c = proposal.Contact__c,
			Contact_from_Offer_Line__c = proposal.Contact__c,
			Expected_Date_of_Signing_Reservation__c = Date.today()-1,
			Expected_Date_of_Signing__c = Date.today()-1,
			Account_from_Customer_Group__c = companyAccount.Id,
			Reservation_Quque_First_Place__c = true, 
			Share_Ok_Among_Customers__c = true,
			Developer_Agreement_from_Customer_Group__c = proposal.Id 
			),
		true
		);

	Payment__c paymentInstallment = TestHelper.createPaymentsInstallment(
		new Payment__c(
			Agreements_Installment__c = saleTerms.Id,
			Contact__c = proposal.Contact__c
			),
		true
		);

	TestHelper.CreatePaymentScheduleConfig_CurrentConfig(true);
	System.debug('saleTerms TEST: ' + saleTerms);



	Test.startTest();
// update date of signing reservation: run trigger UpdateCustomerFieldsOnSaleTerms	
		saleTerms.Date_of_Signing_Reservation__c = Date.today();
		update saleTerms;

	Test.stopTest();

	Sales_Process__c saleTerms2Assert = [
				Select First_names__c, Second_names__c, NIP__c, Account_from_Customer_Group__r.NIP__c, Developer_Agreement_from_Customer_Group__c
				from Sales_process__c 
				where Developer_Agreement_from_Customer_Group__c = :saleTerms.Developer_Agreement_from_Customer_Group__c 
				LIMIT 1
				];

	System.assertEquals(companyAccount.Name+', '+companyAccount2.Name,saleTerms2Assert.First_names__c);
	System.assertEquals(companyAccount.NIP__c+', '+companyAccount2.NIP__C,saleTerms2Assert.NIP__c);
	System.debug('#########Sale Terms: ' +saleTerms2Assert);

}


// Test for individual client; check: first name, second name, PESEL, NIP and address
@isTest static void TestUpdateCustomerFieldOnSaleTermsForPersonAccount() {

// New Investment
	Resource__c investment = TestHelper.createInvestment(
		new Resource__c(
			Bank_Account_Number__c = '12312312312312312312312312'
			), 
		true
		);

// New flat
	Resource__c flat = TestHelper.createResourceFlatApartment(
		new Resource__c(
			Investment_Flat__c = investment.Id
			), 
			true
		);

// New person account
	Account personAccount = TestHelper.createAccountIndividualClient(
		new Account(
			BillingStreet = 'TestStreet',
			BillingPostalCode = '00-000',
			BillingCity = 'TestCity'
			), 
		true
		);
	
	
System.debug('personAccount ?: ' + personAccount +'is PA ## : '+personAccount.IsPersonAccount + personAccount.RecordTypeId );
System.debug('personAccount check' + [Select Id, isPersonAccount from Account where id = :personAccount.Id] );

// Get contact for person account
	Contact contactPerson = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id];

System.debug('contactPerson: ' + contactPerson);

// New proposal for individual client
	Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
		new Sales_Process__c(
			Contact__c = contactPerson.Id
			), 
		true
	);
System.debug('proposal TEST: ' + proposal);


// New sale terms for individual client	
	Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
		new Sales_Process__c(
			Offer__c = proposal.Id,
			Offer_Line_to_Sale_Term__c = proposal.Id,
			Contact__c = proposal.Contact__c,
			Contact_from_Offer_Line__c = proposal.Contact__c,
			Expected_Date_of_Signing_Reservation__c = Date.today()-1,
			Expected_Date_of_Signing__c = Date.today()-1,
			Account_from_Customer_Group__c = personAccount.Id,
			Reservation_Quque_First_Place__c = true, 
			Share_Ok_Among_Customers__c = true,
			Developer_Agreement_from_Customer_Group__c = proposal.Id
			),
			true
		);

	Payment__c paymentInstallment = TestHelper.createPaymentsInstallment(
		new Payment__c(
			Agreements_Installment__c = saleTerms.Id,
			Contact__c = proposal.Contact__c
			),
			true
		);

	TestHelper.CreatePaymentScheduleConfig_CurrentConfig(true);
	System.debug('saleTerms TEST: ' + saleTerms);



	Test.startTest();
// update date of signing reservation: run trigger UpdateCustomerFieldsOnSaleTerms	
		saleTerms.Date_of_Signing_Reservation__c = Date.today();
		update saleTerms;
System.debug('saleTerms after UPDATE: ' + saleTerms.First_names__c);

	Test.stopTest();

	Sales_Process__c saleTerms2Assert = [
				Select First_names__c, Second_names__c, PESEL__c, NIP__c, Client_Address__c
				from Sales_process__c 
				where Developer_Agreement_from_Customer_Group__c = :saleTerms.Developer_Agreement_from_Customer_Group__c 
				LIMIT 1
				];

	System.assertEquals(personAccount.FirstName,saleTerms2Assert.First_names__c);
	System.assertEquals(personAccount.LastName,saleTerms2Assert.Second_names__c);
	System.assertEquals(personAccount.PESEL__c,saleTerms2Assert.PESEL__c);
	System.assertEquals(personAccount.NIP__c,saleTerms2Assert.NIP__c);
	System.assertEquals(personAccount.BillingStreet+', '+personAccount.BillingPostalCode+' '+personAccount.BillingCity,saleTerms2Assert.Client_Address__c);
	System.debug('#########Sale Terms: ' +saleTerms2Assert);

}

// Test for customer group: two infividual Clients: check first names, second names, PESEL numbers and addresses
@isTest static void TestUpdateCustomerFieldOnSaleTermsForGroupPersonAccount() {

// New investment
	Resource__c investment = TestHelper.createInvestment(null, true);

// New flat
	Resource__c flat = TestHelper.createResourceFlatApartment(
		new Resource__c(
			Investment_Flat__c = investment.Id
			), 
			true
		);

// First individual client
	Account personAccount = TestHelper.createAccountIndividualClient(
		new Account(
			BillingStreet = 'TestStreet',
			BillingPostalCode = '00-000',
			BillingCity = 'TestCity'
			), 
		true
		);
	
	
System.debug('personAccount ?: ' + personAccount +'is PA ## : '+personAccount.IsPersonAccount + personAccount.RecordTypeId );
System.debug('personAccount check' + [Select Id, isPersonAccount from Account where id = :personAccount.Id] );

// Second individual client
	Account personAccount2 = TestHelper.createAccountIndividualClient(
		new Account(
			BillingStreet = '2TestStreet',
			BillingPostalCode = '20-000',
			BillingCity = '2TestCity'
			), 
		true
		);
// Get contact to first individual client

	Contact contactPerson = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id];

System.debug('contactPerson: ' + contactPerson);

// New proposal for customer group
	Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
		new Sales_Process__c(
			Contact__c = contactPerson.Id
			), 
		true
	);
System.debug('proposal TEST: ' + proposal);

// Add second individual client to proposal
	Sales_Process__c customGroup = TestHelper.createSalesProcessCustomerGroup(
		new Sales_Process__c(
			Proposal_from_Customer_Group__c = proposal.Id,
			Account_from_Customer_Group__c = personAccount2.Id
			), 
		true
	);

// Sale terms for customer group	
	Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
		new Sales_Process__c(
			Offer__c = proposal.Id,
			Offer_Line_to_Sale_Term__c = proposal.Id,
			Contact__c = proposal.Contact__c,
			Contact_from_Offer_Line__c = proposal.Contact__c,
			Expected_Date_of_Signing_Reservation__c = Date.today()-1,
			Expected_Date_of_Signing__c = Date.today()-1,
			Account_from_Customer_Group__c = personAccount.Id,
			Reservation_Quque_First_Place__c = true, 
			Share_Ok_Among_Customers__c = true,
			Developer_Agreement_from_Customer_Group__c = proposal.Id
			),
			true
		);

// Payment for sale terms
	Payment__c paymentInstallment = TestHelper.createPaymentsInstallment(
		new Payment__c(
			Agreements_Installment__c = saleTerms.Id,
			Contact__c = proposal.Contact__c
			),
			true
		);

	TestHelper.CreatePaymentScheduleConfig_CurrentConfig(true);
	System.debug('saleTerms TEST: ' + saleTerms);



	Test.startTest();
// update date of signing reservation: run trigger UpdateCustomerFieldsOnSaleTerms	
		saleTerms.Date_of_Signing_Reservation__c = Date.today();
		update saleTerms;
	System.debug('saleTerms after UPDATE: ' + saleTerms);

	Test.stopTest();

	Sales_Process__c saleTerms2Assert = [
				Select First_names__c, Second_names__c, PESEL__c, NIP__c, Client_Address__c
				from Sales_process__c 
				where Developer_Agreement_from_Customer_Group__c = :saleTerms.Developer_Agreement_from_Customer_Group__c 
				LIMIT 1
				];

	System.assertEquals(personAccount.FirstName+', '+personAccount2.FirstName,saleTerms2Assert.First_names__c);
	System.assertEquals(personAccount.LastName+', '+personAccount2.LastName,saleTerms2Assert.Second_names__c);
	System.assertEquals(personAccount.PESEL__c+', '+personAccount2.PESEL__c,saleTerms2Assert.PESEL__c);
	System.assertEquals(personAccount.BillingStreet+', '+personAccount.BillingPostalCode+' '+personAccount.BillingCity+';  '+personAccount2.BillingStreet+', '+personAccount2.BillingPostalCode+' '+personAccount2.BillingCity,saleTerms2Assert.Client_Address__c);
	System.debug('#########Sale Terms: ' +saleTerms2Assert);

}

}