/**
* @author       Mariia Dobzhanska
* @description  A batch which searches for unpaшed Payments which have Sales Processes with the RecordType of Preliminary_Agreement and
* Status equals  Developer/ Preliminary Agreement signed, and Payments has Recordypes of 
**/

global class SearchRemindPaymentBatch implements Database.Batchable<Payment__c>, Database.Stateful {

    //Email templates names
    public static String emailTemplateNameBefore = 'Przypomnienie o zbliżającym się terminie zapłaty';
    public static String emailTemplateNameAfter = 'Wezwanie do zapłaty';
	public List<Date> beforeafter;
    
    /*
    *Getting all the Sales Processes which has expected values of appropriate parameters
    */

    global Iterable<Payment__c> start(database.batchablecontext BC) { 

        beforeafter = RemindDates.getremdates();
        
        Set<Id> paymRecTypeId = new Set<Id>{
            CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_DEPOSIT),
           	CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT_OF_CHANGE)
        };

        //Get the Preliminary agreemant recordtype name
        String SPrecType = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT);
        
        /*return [
            SELECT Id, Name, Due_Date__c, Paid_Formula__c, Contact__c, Contact__r.Email, Agreements_Installment__c, Agreements_Installment__r.RecordTypeId, Agreements_Installment__r.Status__c
            FROM Payment__c
            WHERE 
            Paid_Formula__c = false
            AND RecordTypeId IN :paymRecTypeId
            AND Agreements_Installment__r.RecordTypeId = :SPrecType
            AND Agreements_Installment__r.Status__c = :CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED
            AND (Due_Date__c = :beforeafter[1] OR Due_Date__c = :beforeafter[0])
        ];*/

        /* Grzegorz Murawiński */
        return [
            SELECT Id, Today__c, Amount_to_pay__c, Name, Due_Date__c, Type__c, Payment_For__c, Payment_For__r.Bank_Name__c, Agreement_Number__c, Date_of_signing_agreement__c, Paid_Formula__c, Bank_Account_For_Resource__c, Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, Contact__r.AccountId, Agreements_Installment__c, Agreements_Installment__r.RecordTypeId, Agreements_Installment__r.Status__c
            FROM Payment__c
            WHERE 
            Paid_Formula__c = false
            AND RecordTypeId IN :paymRecTypeId
            AND Agreements_Installment__r.RecordTypeId = :SPrecType
            AND Agreements_Installment__r.Status__c = :CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED
            AND (Due_Date__c = :beforeafter[1] OR Due_Date__c = :beforeafter[0])
        ];



        
    }

	/*
	*Sending the remind emails to the Payment's contacts
    */

    global void execute(Database.BatchableContext BC, List<Payment__c> scope) {

        
		List<Payment__c> payments = scope;
       
        Map<Id,Id> accPay = new Map<Id,Id>();
        for (Payment__c payment : payments) {
            accPay.put(payment.Id, payment.Contact__r.AccountId);
        }
        Map<Id, Account> accMap = new Map<Id, Account>([SELECT Id, ShippingStreet, ShippingPostalCode, ShippingCity
                                     FROM Account
                                     WHERE Id in :accPay.values()]);
        Map<Id, Account> mapPayAcc= new Map<Id, Account>();
        for(Id key : accPay.keySet()){
            Id accId = accPay.get(key);
            Account acc = accMap.get(accId);
            mapPayAcc.put(key, acc);
        }


  
        List<Date> beforeafter = RemindDates.getremdates();
        //If the searched payments are not empty
        //remind Emails before and after Due Date in payment expires
        
        if (!payments.isEmpty()) {


            for (Payment__c payment : payments) {

                Account accForPayment = mapPayAcc.get(payment.Id);
				
                //If for the current payment the Due date equals Today - some day count from the current settings,
                //then we have to send the Email that the payment date has passed
                
                if (payment.Due_Date__c == beforeafter[1]) {

                      RemindEmail.sendRemind(payment, accForPayment, emailTemplateNameAfter);
                }
                
                 //If for the current payment the Due date equals Today + some day count from the current settings,
                //then we have to send the remind Email that the payment date is coming soon

                else if(payment.Due_Date__c == beforeafter[0]) {

                    RemindEmail.sendRemind(payment, accForPayment, emailTemplateNameBefore);
                }


            }
        }
        
    }
    
    global void finish(Database.BatchableContext info) {}


}