public without sharing class TH_ListingTrigger extends TriggerHandler.DelegateBase {

	//public boolean authData;
	//public List<List<String>> parsedCSV;
	//public List<Listing__c> externalLinesToInsert;
	//public List<Id> listingsToUpdate;
	//public List<Id> listingsToDelete;
 //   public String profileN;


	//public override void prepareAfter(){
	//	parsedCSV = new List<List<String>>();
	//    externalLinesToInsert = new List<Listing__c>();
	//    List<StaticResource> srAuthData = [ SELECT id, Body From StaticResource WHERE Name = 'ExternalPortalAuthData'];
	//    if (srAuthData.size() == 1) {
	//        string srBody = srAuthData[0].Body.toString();
	//        parsedCSV = CommonUtility.parseCSV(srBody, false);
	//        authData = true;
	//    } else {
	//    	authData = false;
	//    }

	//   listingsToUpdate = new List<Id>(); 
 //      listingsToDelete = new List<Id>(); 
	//}

 //   public override void prepareBefore(){
 //       Id profId = userinfo.getProfileId();
 //       profileN = [Select Name from Profile where Id=:profId].Name;
 //   }


 //   public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o){
 //       Map<Id, Listing__c> oList = (Map<Id, Listing__c>)old;
 //       Map<Id, Listing__c> nList = (Map<Id, Listing__c>)o;

 //        for(Id key : nList.keySet()){
 //           Listing__c lstN = nList.get(key);
 //           Listing__c lstO = oList.get(key);
 //           // Author: Wojciech Słodziak, previous author: Przemysław Tustatnowski
 //           if(!CommonUtility.skipValidation && lstO.Status__c == CommonUtility.STATUS_INACTIVE_SOLD && profileN != Label.SystemAdministrator){
 //               lstN.addError(Label.CannotEditSoldListing);
 //           }
 //       }
 //   }
    


	//public override void afterInsert(Map<Id, sObject> o){
 //       Map<Id, Listing__c> lst = (Map<Id, Listing__c>)o;
 //       List<Id> resources = new List<Id>();
 //       for(Id key : lst.keySet()){
 //           Listing__c lstN = lst.get(key);

 //           Id listingSaleId = CommonUtility.getRecordTypeId('Listing__c', CommonUtility.LISTING_TYPE_SALE);
 //           Id listingRentId = CommonUtility.getRecordTypeId('Listing__c', CommonUtility.LISTING_TYPE_RENT);
 //           Id externalLineId = CommonUtility.getRecordTypeId('Listing__c', CommonUtility.LISTING_TYPE_EXTERNAL_LINE);
 //           //create External Lines (with data from static resource) 
 //           if ((lstN.recordTypeId == listingSaleId || lstN.recordTypeId == listingRentId) && authData) {
 //               for(List<string> line:parsedCSV) {
 //                   Listing__c tempListing = new Listing__c();
 //                   tempListing.Name__c = line[0];
 //                   tempListing.Listing_EL__c = lstN.Id;
 //                   tempListing.RecordTypeId = externalLineId;
 //                   externalLinesToInsert.add(tempListing);
 //               }
 //           }

 //           if(lstN.recordTypeId == listingRentId || lstN.recordTypeId == listingSaleId)
 //           {
 //               resources.add(lstN.Resource__c);
 //           }
            
            
 //       }
 //       List<Listing__c> listingsWithResources = [SELECT Id, Name, Resource__c, RecordTypeId, Status__c, RecordType.Name FROM Listing__c WHERE Resource__c in: resources];
 //       for(Id key : lst.keySet()){
 //           Listing__c lstN = lst.get(key);
 //           for(Listing__c lstRes : listingsWithResources){
 //               if(lstN.RecordTypeId == lstRes.RecordTypeId && lstN.Resource__c == lstRes.Resource__c && lstN.Id != lstRes.Id && (lstN.Status__c == CommonUtility.STATUS_ACTIVE || lstN.Status__c == CommonUtility.STATUS_RESERVATION) && (lstRes.Status__c == CommonUtility.STATUS_ACTIVE || lstRes.Status__c == CommonUtility.STATUS_RESERVATION )){
 //                   lstN.addError(Label.ErrorListingExist + '  ' + lstRes.NAme + '. ' + Label.ErrorListingExist2);
 //               }
 //           }
 //       }
	//}

	//public override void afterUpdate(Map<Id, sObject>old, Map<Id, sObject> o){
 //        Map<Id, Listing__c> lstOld = ( Map<Id, Listing__c>)old;
 //        Map<Id, Listing__c> lst = ( Map<Id, Listing__c>)o;

	//	Set<string> inactiveStatuses = new Set<string>{CommonUtility.STATUS_INACTIVE, CommonUtility.STATUS_INACTIVE_SOLD};
 //  		Set<string> activeStatuses = new Set<string>{CommonUtility.STATUS_ACTIVE, CommonUtility.STATUS_RESERVATION};
 //  		Id listingSaleId = CommonUtility.getRecordTypeId('Listing__c', CommonUtility.LISTING_TYPE_SALE);
	//	Id listingRentId = CommonUtility.getRecordTypeId('Listing__c', CommonUtility.LISTING_TYPE_RENT);

 //       List<Id> resources = new List<Id>();
 //       for(Id key : lst.keySet()){
 //           Listing__c lstN = lst.get(key);
 //           if(lstN.recordTypeId == listingRentId || lstN.recordTypeId == listingSaleId)
 //           {
 //               resources.add(lstN.Resource__c);
 //           }
            
 //       }
 //       List<Listing__c> listingsWithResources = [SELECT Id, Name, Resource__c, RecordTypeId, Status__c, RecordType.Name FROM Listing__c WHERE Resource__c in: resources];

 //      for(Id key : lst.keySet()){
 //           Listing__c lstN = lst.get(key);
 //           Listing__c lstO = lstOld.get(key);


 //           if (lstN.recordTypeId == listingSaleId && lstN.Publish_Date__c != null && 
 //               (lstN.Price__c != lstO.Price__c || lstN.Description__c != lstO.Description__c ||
 //               (lstN.Status__c != lstO.Status__c && activeStatuses.contains(lstO.Status__c) && activeStatuses.contains(lstN.Status__c))))
 //           { 
 //               //system.debug('Price or Desc has changed');
 //               listingsToUpdate.add(lstN.Id);
 //           }
 //           if (lstN.recordTypeId == listingSaleId && lstN.Publish_Date__c != null && 
 //               !inactiveStatuses.contains(lstO.Status__c) && inactiveStatuses.contains(lstN.Status__c)) { 
 //               //system.debug('Status has changed - remove from Developer Portal');
 //               listingsToDelete.add(lstN.Id);
 //           }



 //           /*
 //           / Code part made by Michał Kłobucki
 //           / Trigger part check listings and validate status
 //           */
 //   		if(lstN.Status__c != lstO.Status__c){
 //               system.debug(CommonUtility.skipValidation);
 //               if((profileN == 'Properto Backoffice' || profileN == 'Properto Sales') && CommonUtility.skipValidation == false ){
 //                   try{
 //                       if(CommonUtility.checkIfAllowedStatusChangeListingSales(lstO, lstN) == false){
 //                           system.debug('AddError');
 //                           lstN.addError( Label.ErrorCantChangeStatus + ' ' + Label.From + ' ' + lstO.Status__c + ' ' + Label.To + ' ' + lstN.Status__c);
 //                       }
 //                   }
 //                   catch(Exception e){
 //                       system.debug('valid status');
 //                   }
 //               }
 //           }

 //       	CommonUtility.skipValidation = false;

 //           for(Listing__c lstRes : listingsWithResources){
 //               if(lstN.RecordTypeId == lstRes.RecordTypeId && lstN.Resource__c == lstRes.Resource__c && lstN.Id != lstRes.Id && (lstN.Status__c == 'Active' || lstN.Status__c == 'Reservation') && (lstRes.Status__c == 'Active' || lstRes.Status__c == 'Reservation')){
 //                   lstN.addError(Label.ErrorListingExist + '  ' + lstRes.NAme + '. ' + Label.ErrorListingExist2);
 //               }
 //           }
 //       }
	//}

	//public override void beforeInsert(List<sObject> o){
 //       List<Listing__c> lst = (List<Listing__c>)o;
 //       for(Listing__c lstT : lst){

 //           /*
 //           / Code part made by Mateusz Pruszyński
 //           / Trigger part that fills "Market" field with "Primary" value every time a new record is created BY FOLLOWING PROFILES:
 //           / Properto Sales, Properto Backoffice, Properto Financial (dla RecordType.DeveloperName = 'Listing_Sale')
 //           */
 //           if (lstT.recordTypeId == CommonUtility.getRecordTypeId('Listing__c', CommonUtility.LISTING_TYPE_SALE) && ( profileN == CommonUtility.PROFILE_PROPERTO_SALES_NAME || profileN == CommonUtility.PROFILE_PROPERTO_BACKOFFICE_NAME || profileN == CommonUtility.PROFILE_PROPERTO_FINANCIAL_NAME)){
 //               lstT.Market__c = CommonUtility.LISTING_MARKET_PRIMARY;
 //           }
        
 //       }
 //   }

	//public override void finish(){
	//	if(externalLinesToInsert != null && externalLinesToInsert.size()>0){
 //           try {
	//	      insert externalLinesToInsert;
 //           } catch(Exception e) {
 //               ErrorLogger.log(e);                
 //           }
	//	}

	//	if (listingsToUpdate != null && listingsToUpdate.size()>0) {
 //           system.debug('listing trigger - starting updateAppartmentsAtFuture');
 //           IntegrationService.updateAppartmentsAtFuture(listingsToUpdate);                 
 //       }
 //       if (listingsToDelete != null && listingsToDelete.size()>0) {
 //           system.debug('listing trigger - starting deleteAppartmentsAtFuture');
 //           IntegrationService.removeAppartmentsAtFuture(listingsToDelete);                 
 //       }
	//}
}