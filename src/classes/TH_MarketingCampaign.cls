public without sharing class TH_MarketingCampaign extends TriggerHandler.DelegateBase {
	//Author Michał Kłobucki
	//Calculate Number_of_Leads__c on campain, and ensures no contact duplicate
	List<Id> ids = new List<Id>();
	List<Marketing_Campaign__c> mc = new List<Marketing_Campaign__c>();
	List<Marketing_Campaign__c> mcc = new List<Marketing_Campaign__c>();
	Id rtContact;
    public override void prepareBefore(){

    }

    public override void prepareAfter(){
    	rtContact  = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN, CommonUtility.MARKETING_CAMPAIGN_TYPE_MARKETINGCAMPAIGNCONTACT);
    }

    public override void beforeInsert(List<sObject> o ){
    }

    public override void afterInsert(Map<Id, sObject> o){
        Map<Id, Marketing_Campaign__c> resNew = (Map<Id, Marketing_Campaign__c>)o;
        for(Id key : resNew.keySet()){
            Marketing_Campaign__c resN = resNew.get(key);

        	if (resN.RecordTypeId == rtContact) {
				ids.add(resN.Marketing_Campaign__c);
			}
      
        }   
        system.debug(ids);
        if(ids.size() > 0){
			mc = [SELECT Id, Name, Marketing_Campaign__c, Number_of_Leads__c FROM Marketing_Campaign__c WHERE id in: ids];
			ids.clear();
			for(Marketing_Campaign__c tmp : mc){
				ids.add(tmp.Id);
			}

			mcc = [SELECT Id, Name, Marketing_Campaign__c, Contact__c FROM Marketing_Campaign__c WHERE Marketing_Campaign__c in: ids];

			system.debug(mc);
			system.debug(mcc);

			for(Id key : resNew.keySet()){
	            Marketing_Campaign__c resN = resNew.get(key);
	            if (resN.RecordTypeId == rtContact) {
					for(Marketing_Campaign__c mcQuery : mcc){
						if(resN.Contact__c == mcQuery.Contact__c && resN.Marketing_Campaign__c == mcQuery.Marketing_Campaign__c && resN.Id != mcQuery.Id){
							resN.AddError(Label.CampainValidationContact);
						}
					}
				}
	        }
		}

      
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o){
        
    }

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o){
        Map<Id, Marketing_Campaign__c> resOld = (Map<Id, Marketing_Campaign__c>)old;
        Map<Id, Marketing_Campaign__c> resNew = (Map<Id, Marketing_Campaign__c>)o;
        for(Id key : resNew.keySet()){
            Marketing_Campaign__c resN = resNew.get(key);
            Marketing_Campaign__c resO = resOld.get(key);

        	if (resN.RecordTypeId == rtContact) {
				ids.add(resN.Marketing_Campaign__c);
			}
        }  
        if(ids.size() > 0){
			mc = [SELECT Id, Name, Marketing_Campaign__c, Number_of_Leads__c FROM Marketing_Campaign__c WHERE id in: ids];
			ids.clear();
			for(Marketing_Campaign__c tmp : mc){
				ids.add(tmp.Id);
			}

			mcc = [SELECT Id, Name, Marketing_Campaign__c, Contact__c FROM Marketing_Campaign__c WHERE Marketing_Campaign__c in: ids];


			for(Id key : resNew.keySet()){
	            Marketing_Campaign__c resN = resNew.get(key);
	            if (resN.RecordTypeId == rtContact) {
					for(Marketing_Campaign__c mcQuery : mcc){
						if(resN.Contact__c == mcQuery.Contact__c && resN.Marketing_Campaign__c == mcQuery.Marketing_Campaign__c && resN.Id != mcQuery.Id){
							resN.AddError(Label.CampainValidationContact);
						}
					}
				}
	        }
		}
    }

    public override void afterDelete(Map<Id, sObject> old){
		Map<Id, Marketing_Campaign__c> resOld = (Map<Id, Marketing_Campaign__c>)old;
        for(Id key : resOld.keySet()){
            Marketing_Campaign__c resO = resOld.get(key);

        	if (resO.RecordTypeId == rtContact) {
				ids.add(resO.Marketing_Campaign__c);
			}	
        	   
        }

        if(ids.size() > 0){

			mc = [SELECT Id, Name, Marketing_Campaign__c, Number_of_Leads__c FROM Marketing_Campaign__c WHERE id in: ids];
			ids.clear();
			for(Marketing_Campaign__c tmp : mc){
				ids.add(tmp.Id);
			}

			mcc = [SELECT Id, Name, Marketing_Campaign__c, Contact__c FROM Marketing_Campaign__c WHERE Marketing_Campaign__c in: ids];

		}

	}

    public override void finish(){
    	if(mc.size() > 0){
	    	for(Marketing_Campaign__c campain : mc){
				Decimal count = 0;
				if(mcc.size() > 0){
					for(Marketing_Campaign__c contact : mcc){
						if(campain.Id == contact.Marketing_Campaign__c){
							count++;
						}
					}
				}
				campain.Number_of_Leads__c = count;
			}
			try {
				update mc;
			} catch(Exception e) {
				ErrorLogger.log(e);
			}
    		
    	}
    }

}