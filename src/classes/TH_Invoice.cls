public without sharing class TH_Invoice extends TriggerHandler.DelegateBase  {


	Set<String> invalidStatusesForEdit = new Set<String>{CommonUtility.INVOICE_STATUS_SENT, CommonUtility.INVOICE_STATUS_DELIVERED_SIGNED, CommonUtility.INVOICE_STATUS_DECLINED};

	public override void prepareBefore(){

	}

	public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o){
        Map<Id, Invoice__c> resOld = (Map<Id, Invoice__c>)old;
        Map<Id, Invoice__c> resNew = (Map<Id, Invoice__c>)o;
        for(Id key : resNew.keySet()){
            Invoice__c resN = resNew.get(key);
            Invoice__c resO = resOld.get(key);

           	if (resO.Status__c != resN.Status__c && (resN.Status__c == CommonUtility.INVOICE_STATUS_SENT || resN.Status__c == CommonUtility.INVOICE_STATUS_DELIVERED_SIGNED || resN.Status__c == CommonUtility.INVOICE_STATUS_DECLINED)) {
			          resN.addError(Label.ErrorEditStatusOnInvoice);
           	}
              
        }
    }
}