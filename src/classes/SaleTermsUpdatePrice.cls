/**
* @author 		Mateusz Pruszynski
* @description 	calculate Proposal/Sale Terms price in case of insert/update/delete offer line
*/
global class SaleTermsUpdatePrice {

	public static void saleTermsUpdatePrices(Set<Id> saleTermsOrProposalToUpdatePrice) {
		// 0 check if after-sales service exists. If so, we cannot modify historical data on sale terms
		Set<Id> forbiddenSaleTermsIds = new Set<Id>();
		for(Sales_Process__c afterSales : [
			SELECT Id, Agreement__c FROM Sales_Process__c 
			WHERE Agreement__c in :saleTermsOrProposalToUpdatePrice
			AND Agreement__r.RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)
		]) {
			forbiddenSaleTermsIds.add(afterSales.Agreement__c);
		}

	 	// 1 search for proposals/sale terms records by ids
	    List<Sales_Process__c> saleTermsOrProposals = [
	    	SELECT Id, RecordTypeId, Agreement_Value__c, Discount__c 
	    	FROM Sales_Process__c 
	    	WHERE Id in :saleTermsOrProposalToUpdatePrice 
	    	AND (
	    		RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER) 
	    		OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
	    	)
	    	AND Id not in: forbiddenSaleTermsIds
	    ];

	    // 2 get all sales lines related
	    List<Sales_Process__c> offerLines = [
	    	SELECT Id, Price_With_Discount__c, Offer_Line_Resource__r.Price_After_Measurement__c, Product_Line_to_Proposal__c, Offer_Line_Resource__r.Price__c,
	    		   Offer_Line_to_Sale_Term__c, Standard_of_Completion_Price__c, Offer_Line_Resource__r.Acceptance_Status__c, VAT_Amount__c, Net_Price_With_Discount__c 
	    	FROM Sales_Process__c 
	    	WHERE (
	    		Offer_Line_to_Sale_Term__c in :saleTermsOrProposalToUpdatePrice 
	    		OR Product_Line_to_Proposal__c in :saleTermsOrProposalToUpdatePrice
	    	) 
	    	AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
	    ];

	    // 3 split saleTermsOrProposals into two lists 
	    List<Sales_Process__c> proposals = new List<Sales_Process__c>();
	    List<Sales_Process__c> saleTerms = new List<Sales_Process__c>();

	    for(Sales_Process__c stop : saleTermsOrProposals) {
	        if(stop.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)) {
	            proposals.add(stop);
	        } else {
	            saleTerms.add(stop);
	        }
	    }
	    
	    // 4 now, iterate over main sales process and promotions as well as offer lines to sum prices after discount and promotion values
	    List<Sales_Process__c> salesProcess2update = new List<Sales_Process__c>();
	    Decimal nakedPrice; // price for resource, promotion, any discount just like resource Price but with std of completion
	    Decimal offerLineNetTotalPrice;
	    Decimal offerLineNetTotalVatValue;
	    Decimal offerLineTotalPrice;
	    if(!proposals.isEmpty()) { // iterate over proposals

	        for(Sales_Process__c proposal : proposals) {

	            nakedPrice = 0;
	            offerLineTotalPrice = 0;
	            offerLineNetTotalPrice = 0;
				offerLineNetTotalVatValue = 0;
	            for(Sales_Process__c offerLine : offerLines) {
	                if(offerLine.Product_Line_to_Proposal__c == proposal.Id) {
	                    offerLineTotalPrice += offerLine.Price_With_Discount__c != null ? offerLine.Price_With_Discount__c : 0;
	                    offerLineNetTotalPrice += offerLine.Net_Price_With_Discount__c != null ? offerLine.Net_Price_With_Discount__c : 0 ;
						offerLineNetTotalVatValue += offerLine.VAT_Amount__c != null ? offerLine.VAT_Amount__c : 0;
	                    
	                    if(offerLine.Offer_Line_Resource__r.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED) {
	                    	nakedPrice += offerLine.Offer_Line_Resource__r.Price_After_Measurement__c != null ? offerLine.Offer_Line_Resource__r.Price_After_Measurement__c : 0;
	                    } else {
	                    	nakedPrice += offerLine.Offer_Line_Resource__r.Price__c != null ? offerLine.Offer_Line_Resource__r.Price__c : 0;
	                    }
	                    
	                    if(offerLine.Standard_of_Completion_Price__c != null){
	                         nakedPrice += offerLine.Standard_of_Completion_Price__c != null ? offerLine.Standard_of_Completion_Price__c : 0;
	                    }
	                }
	            }

	            if(nakedPrice != 0){
	                if(proposal.Agreement_Value__c != offerLineTotalPrice || proposal.Discount__c != ((1 - (offerLineTotalPrice / nakedPrice)) * 100)) {
	                    proposal.Agreement_Value__c = offerLineTotalPrice;
	                    proposal.Discount_Amount_Offer__c = nakedPrice - proposal.Agreement_Value__c;
	                    proposal.Discount__c = (1 - (proposal.Agreement_Value__c / nakedPrice)) * 100;
	                    proposal.Net_Agreement_Price__c = offerLineNetTotalPrice;
	                    proposal.VAT_Amount__c =  offerLineNetTotalVatValue;                          
	                    salesProcess2update.add(proposal);
	                }
	            }

	        }
	    }
	    if(!saleTerms.isEmpty()) { // iterate over sale terms
	        for(Sales_Process__c st : saleTerms) {

	            nakedPrice = 0;
	            offerLineTotalPrice = 0;
	            offerLineNetTotalPrice = 0;
				offerLineNetTotalVatValue = 0;
	            for(Sales_Process__c offerLine : offerLines) {
	                if(offerLine.Offer_Line_to_Sale_Term__c == st.Id) {
	                    offerLineTotalPrice += offerLine.Price_With_Discount__c != null ? offerLine.Price_With_Discount__c : 0;
	                    offerLineNetTotalPrice += offerLine.Net_Price_With_Discount__c != null ? offerLine.Net_Price_With_Discount__c : 0;
						offerLineNetTotalVatValue += offerLine.VAT_Amount__c != null ? offerLine.VAT_Amount__c : 0;

	                    if(offerLine.Offer_Line_Resource__r.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED) {
	                    	nakedPrice += offerLine.Offer_Line_Resource__r.Price_After_Measurement__c != null ? offerLine.Offer_Line_Resource__r.Price_After_Measurement__c : 0;
	                    } else {
	                    	nakedPrice += offerLine.Offer_Line_Resource__r.Price__c != null ? offerLine.Offer_Line_Resource__r.Price__c : 0;
	                    }

	                    if(offerLine.Standard_of_Completion_Price__c != null){
	                         nakedPrice += offerLine.Standard_of_Completion_Price__c;
	                    }
	                }
	            }

	            if(nakedPrice != 0){
	                if(st.Agreement_Value__c != offerLineTotalPrice || st.Discount__c != ((1 - (offerLineTotalPrice / nakedPrice)) * 100)) {
	                    st.Agreement_Value__c = offerLineTotalPrice;
	                    st.Discount_Amount_Offer__c = nakedPrice - st.Agreement_Value__c;
	                    st.Discount__c = (1 - (st.Agreement_Value__c / nakedPrice)) * 100;
	                    st.Net_Agreement_Price__c = offerLineNetTotalPrice;
	                    st.VAT_Amount__c =  offerLineNetTotalVatValue; 

	                    st.Final_Agreement_Value_Gross__c = st.Agreement_Value__c;
	                    st.Final_Agreement_Value_Net__c = st.Net_Agreement_Price__c;
	                    st.Final_Vat_Amount__c = st.VAT_Amount__c;
	                    salesProcess2update.add(st);
	                }
	            }

	        }
	    }
	    if(!salesProcess2update.isEmpty()) { // update
	        try {
	            update salesProcess2update;
	        } catch(Exception e) {
	            ErrorLogger.log(e);
	        }
	    }
	}

	/* Matuesz Pruszyński */
	// Copy agreement value from Sale Terms on Final Agreement creation
	// edit: Mateusz Wolak-Ksiazek
	// added: Copy net agreement price and vat amount
	public static void copyAgreementValuesToSaleTerms(List<Sales_Process__c> saleTermsToCopyAgreementValue) {
        Set<Id> afterSalesIds = new Set<Id>();
        for(Sales_Process__c saleTerms : saleTermsToCopyAgreementValue) {
            afterSalesIds.add(saleTerms.Handover_Protocol__c);
        }
        for(Sales_Process__c afterSalesService : [SELECT Id, Agreement__r.Agreement_Value__c, Agreement__r.Net_Agreement_Price__c, Agreement__r.VAT_Amount__c FROM Sales_Process__c 
                                                 WHERE Id in :afterSalesIds AND Agreement__c != null AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)]) {
            for(Sales_Process__c saleTerms : saleTermsToCopyAgreementValue) {
                if(afterSalesService.Id == saleTerms.Handover_Protocol__c) {
                    saleTerms.Agreement_Value__c = afterSalesService.Agreement__r.Agreement_Value__c;
                    saleTerms.Net_Agreement_Price__c = afterSalesService.Agreement__r.Net_Agreement_Price__c;
                    saleTerms.VAT_Amount__c = afterSalesService.Agreement__r.VAT_Amount__c;

                    saleTerms.Final_Agreement_Value_Gross__c = saleTerms.Agreement_Value__c;
                    saleTerms.Final_Agreement_Value_Net__c = saleTerms.Net_Agreement_Price__c;
                    saleTerms.Final_Vat_Amount__c = saleTerms.VAT_Amount__c;                    
                    break;
                }
            }
        }		
	}

}