/**
* @author 		Mateusz Pruszyński , taken from other project
* @editor		Mateusz Wolak-Książek
* @description 	Override for standard "new" action on Manager Panel object
**/
public with sharing class ManagerPanelNewActionOverride {
	
	public Manager_Panel__c createdRecord {get; set;}
	public String prefix {get; set;}
	public String retURL {get; set;}
	public String saveURL {get; set;}

	public ManagerPanelNewActionOverride(ApexPages.StandardController stdController) {
		stdController.addFields(getFields());
		createdRecord = (Manager_Panel__c)stdController.getRecord();
		prefix = CommonUtility.getManagerPanelPrefix();
		retURL = ApexPages.currentPage().getParameters().get('retURL') != null ? '&retURL=' + ApexPages.currentPage().getParameters().get('retURL') : prefix + '/o';
		saveURL = ApexPages.currentPage().getParameters().get('saveURL') != null ? '&saveURL=' + ApexPages.currentPage().getParameters().get('saveURL') : '';
	}

	public String[] getFields() {
		return new String[] {'RecordTypeId'};
	}

	public PageReference redirect() {
		PageReference p;
		String url = '/' + prefix + '/e?nooverride=1&RecordType=' + createdRecord.RecordTypeId;
		
		if(createdRecord.RecordTypeId == CommonUtility.getRecordTypeId('Manager_Panel__c', CommonUtility.MANAGER_PANEL_TYPE_MONTHLY_SALES_TARGET) ||
			createdRecord.RecordTypeId == CommonUtility.getRecordTypeId('Manager_Panel__c', CommonUtility.MANAGER_PANEL_TYPE_SALES_TARGET)) {
	        String[] fieldLabels = WebserviceUtilities.getLabelForField('Manager_Panel__c', new String[] {'Name'});
	        String[] fieldIds = WebserviceUtilities.getFieldIds(fieldLabels, url);				
			url += '&' + fieldIds[0] + '=' + Label.PopulatedAutomatically;
		}
		url += retURL;
		url += saveURL;
		p = new PageReference(url);
		
		return p;
	}
}