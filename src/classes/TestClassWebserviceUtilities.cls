@isTest
private class TestClassWebserviceUtilities {
    
    @testSetup
    private static void setupData() {
        // Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(), true);
        // List<Sales_Process__c> salesProcessList = new List<Sales_Process__c>();
        
        // Sales_Process__c sp = TestHelper.createSalesProcessProposal(new Sales_Process__c(), false);
        // Sales_Process__c sp2 = TestHelper.createSalesProcessAfterSalesService(new Sales_Process__c(), false);
   			Sales_Process__c sp = TestHelper.createSalesProcessProposal(new Sales_Process__c(), true);
    	Test.startTest();
	        Sales_Process__c sp3 = TestHelper.createSalesProcessFinalAgreement(new Sales_Process__c(), true);
	        Sales_Process__c sp4 = TestHelper.createSalesProcessCustomerGroup(new Sales_Process__c(), true);
    	Test.stopTest();
        // salesProcessList.add(sp);
        // salesProcessList.add(sp2);
        // salesProcessList.add(sp3);
        // Test.startTest();
        // insert salesProcessList;
        // Test.stopTest();
    }

	private static testMethod void generateUrlTest() {
 		Test.startTest();
		    String spId = [select Id from Sales_Process__c limit 1].Id;
	        String testString = WebserviceUtilities.generateUrl(JSON.serialize(new JSONWrapper(spId)), true);
 		Test.stopTest();
	}
	
	private static testMethod void conditionalConversionNullTest() {
	    System.assertEquals('', WebserviceUtilities.conditionalConversion(null));
	}
	
	private static testMethod void conditionalConversionBooleanTest() {
	    System.assertEquals('1', WebserviceUtilities.conditionalConversion('true'));
	    System.assertEquals('0', WebserviceUtilities.conditionalConversion('false'));
	}
	
	private static testMethod void conditionalConversionNumberTest() {
	    System.assertEquals('0,0', WebserviceUtilities.conditionalConversion('0.0'));
	    System.assertEquals('1,0', WebserviceUtilities.conditionalConversion('1.0'));
	    System.assertEquals('-1,0', WebserviceUtilities.conditionalConversion('-1.0'));
	}
	
	private static testMethod void newFlatTest() {
	    Test.startTest();
	    Resource__c res = TestHelper.createResourceBuilding(new Resource__c(), true);
	   // Resource__c res = [select Id, Investment_Building__c from Resource__c where Investment_Building__c <> null limit 1];
	    WebserviceUtilities.newFlat(res.Investment_Building__c, res.Id, true);
	    Test.stopTest();
	}
	
	private static testMethod void allowDeveloperAgreementTest() {
	    Test.startTest();
	    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER) limit 1];
	    WebserviceUtilities.allowDeveloperAgreement(sp.Id);
	    Test.stopTest();
	}
	
	private static testMethod void checkCustomersRelatedListTest() {

	    List<Sales_Process__c> sp = [select Id, Share_Type__c from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)];
	    WebserviceUtilities.checkCustomersRelatedList(sp);
	}
	
	private static testMethod void allowHandoverProtocolTest() {
	 	Test.startTest();
		    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL) limit 1];
		    WebserviceUtilities.allowHandoverProtocol(sp.Id);
	    Test.stopTest();
	}
	
	private static testMethod void allowFinalAgreementTest() {
		Test.startTest();
		    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) limit 1];
		    WebserviceUtilities.allowFinalAgreement(sp.Id);
	     Test.stopTest();
	}
	
	private static testMethod void recallSaleTermsApprovalProcessTest() {
	    Test.startTest();
		    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) limit 1];
		    WebserviceUtilities.recallSaleTermsApprovalProcess(sp.Id);
	    Test.stopTest();
	}
	
	private static testMethod void getOfferLinesTest() {
		 Test.startTest();
	    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) limit 1];
	    WebserviceUtilities.getOfferLines(sp.Id);
	    
	   
	    Resource__c res = TestHelper.createResourceBuilding(new Resource__c(), true);
	    Sales_Process__c handoverProtocol = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
	    
	    Sales_Process__c preliminaryAgreement = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)];
	    Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Offer_Line_to_After_sales_Service__c = sp.Id, Offer_Line_to_Sale_Term__c = preliminaryAgreement.Id, Offer_Line_Resource__c = res.Id, Bank_Account_Number__c = '1109010140000071219812874', Agreement_Value__c = 10), true);
	    
	    WebserviceUtilities.getOfferLines(sp.Id);
	    Test.stopTest();
	}
	
	private static testMethod void allowProposalButtonsTest() {
		Test.startTest();
		    List<Sales_Process__c> sp = [select Id, Share_Type__c from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)];
		    WebserviceUtilities.allowProposalButtons(sp.get(0).Id);
	    Test.stopTest();
	}
	
	private static testMethod void allowGenerateSaleTermsCustomerCompanyTest() {
	    Test.startTest();
	    Sales_Process__c sp = [select Developer_Agreement_from_Customer_Group__c, Developer_Agreement_from_Customer_Group__r.Contact__c, Account_from_Customer_Group__c from Sales_Process__c where Developer_Agreement_from_Customer_Group__c <> null limit 1];
	   // Contact con = [select Id from Contact where Id = :sp.Developer_Agreement_from_Customer_Group__r.Contact__c];
	    Sales_Process__c developerAgreement = [select Id from Sales_Process__c where Id = :sp.Developer_Agreement_from_Customer_Group__c];
	    Account personAccount = TestHelper.createAccountIndividualClient(new Account(), true);
	    WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    
	    Account acc = [select Id from Account where Id = :sp.Account_from_Customer_Group__c];
	    
	    acc.Foreign_Company__c = true;
	    
	    update acc;
	    
	    WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    
	    acc.NIP__c = '4213149834';
	    acc.BillingStreet = '1';
	    acc.BillingCity = '1';
	    acc.BillingPostalCode = '1';
	    acc.ShippingStreet = '1';
	    acc.ShippingCity = '1';
	    acc.ShippingPostalCode = '1';
	    acc.Email__c = 'test@test.test';
	    acc.KRS__c = '1';
	    acc.Regon__c = 1;
	    acc.Phone = '1';
	    
	    update acc;
	    
	    WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    
	    acc.Foreign_Company__c = false;
	    
	    update acc;
	    
	    WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    
	    Test.stopTest();
	}
	
	private static testMethod void allowGenerateSaleTermsSelfEmploymentTest() {
	    Test.startTest();
	    Sales_Process__c sp = [select Developer_Agreement_from_Customer_Group__c, Developer_Agreement_from_Customer_Group__r.Contact__c, Account_from_Customer_Group__c from Sales_Process__c where Developer_Agreement_from_Customer_Group__c <> null limit 1];
	   // Contact con = [select Id from Contact where Id = :sp.Developer_Agreement_from_Customer_Group__r.Contact__c];
	    Sales_Process__c developerAgreement = [select Id from Sales_Process__c where Id = :sp.Developer_Agreement_from_Customer_Group__c];
	    Account personAccount = TestHelper.createAccountIndividualClient(new Account(), true);
	    
	    personAccount.NIP__c = '4213149834';
	    personAccount.BillingStreet = '1';
	    personAccount.BillingCity = '1';
	    personAccount.BillingPostalCode = '1';
	    personAccount.ShippingStreet = '1';
	    personAccount.ShippingCity = '1';
	    personAccount.ShippingPostalCode = '1';
	    personAccount.Email__c = 'test@test.test';
	    personAccount.KRS__c = '1';
	    personAccount.Regon__c = 1;
	    personAccount.Phone = '1';
	    
	    personAccount.FirstName = 'test';
	    personAccount.LastName = 'test';
	    personAccount.PESEL__c = '32042018677';
	    personAccount.Mother_s_name__pc = 'test';
	    personAccount.Father_s_name__pc = 'test';
	    personAccount.Marital_Status__pc = 'test';
	    personAccount.No_identity_card__pc = 'test';
	    personAccount.Mobile_Phone__c = 'test';
	    developerAgreement.Contact__c = null;
	    
	    
	    
	    sp.Account_from_Customer_Group__c = personAccount.Id;
	    sp.Self_Employment__c = true;
	    
	    update sp;
	    update developerAgreement;
	    
	    WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    
	    update personAccount;
	    
	    WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    
	    
	    
	   // acc.Foreign_Company__c = false;
	    
	   // update acc;
	    
	   // WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    
	   //// con.RecordTypeId = CommonUtility.getRecordTypeId('Contact', 'Partner_Company_Contact');
	    
	   // sp.Self_Employment__c = true;
	    
	   //// update con;
	   // developerAgreement.Contact__c = null;
	   // update sp;
	   // update developerAgreement;
	    
	   // WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    
	   // sp.Self_Employment__c = false;
	    
	   // WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    Test.stopTest();
	}
	
	private static testMethod void allowGenerateSaleTermsOtherTest() {
	    Test.startTest();
	    Sales_Process__c sp = [select Developer_Agreement_from_Customer_Group__c, Developer_Agreement_from_Customer_Group__r.Contact__c, Account_from_Customer_Group__c from Sales_Process__c where Developer_Agreement_from_Customer_Group__c <> null limit 1];
	   // Contact con = [select Id from Contact where Id = :sp.Developer_Agreement_from_Customer_Group__r.Contact__c];
	    Sales_Process__c developerAgreement = [select Id from Sales_Process__c where Id = :sp.Developer_Agreement_from_Customer_Group__c];
	    Account personAccount = TestHelper.createAccountIndividualClient(new Account(
    	        NIP__c = '4213149834',
                BillingStreet = '1',
                BillingCity = '1',
                BillingPostalCode = '1',
                ShippingStreet = '1',
                ShippingCity = '1',
                ShippingPostalCode = '1',
                Email__c = 'test@test.test',
                KRS__c = '1',
                Regon__c = 1,
                Phone = '1',
                FirstName = 'test',
                LastName = 'test',
                PESEL__c = '32042018677',
                Mother_s_name__pc = 'test',
                Father_s_name__pc = 'test',
                Marital_Status__pc = 'test',
                No_identity_card__pc = 'test',
                Mobile_Phone__c = 'test'
	        ), true);
	    
	   // personAccount.NIP__c = '4213149834';
	   // personAccount.BillingStreet = '1';
	   // personAccount.BillingCity = '1';
	   // personAccount.BillingPostalCode = '1';
	   // personAccount.ShippingStreet = '1';
	   // personAccount.ShippingCity = '1';
	   // personAccount.ShippingPostalCode = '1';
	   // personAccount.Email__c = 'test@test.test';
	   // personAccount.KRS__c = '1';
	   // personAccount.Regon__c = 1;
	   // personAccount.Phone = '1';
	    
	   // personAccount.FirstName = 'test';
	   // personAccount.LastName = 'test';
	   // personAccount.PESEL__c = '32042018677';
	   // personAccount.Mother_s_name__pc = 'test';
	   // personAccount.Father_s_name__pc = 'test';
	   // personAccount.Marital_Status__pc = 'test';
	   // personAccount.No_identity_card__pc = 'test';
	   // personAccount.Mobile_Phone__c = 'test';
	    developerAgreement.Contact__c = null;
	    
	    
	    
	    sp.Account_from_Customer_Group__c = personAccount.Id;
	    sp.Self_Employment__c = false;
	    
	    update sp;
	    update developerAgreement;
	    
	    WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    
	    personAccount.Mother_s_name__pc = '';
	    
	    update personAccount;
	    
	    WebserviceUtilities.allowGenerateSaleTerms(sp.Developer_Agreement_from_Customer_Group__c);
	    
	    Test.stopTest();
	}
	
	private static testMethod void updateAccountFieldsTest() {
	    Test.startTest();
	    Sales_Process__c sp = [select Developer_Agreement_from_Customer_Group__c, Developer_Agreement_from_Customer_Group__r.Contact__c, Account_from_Customer_Group__c from Sales_Process__c where Developer_Agreement_from_Customer_Group__c <> null limit 1];
	   // Contact con = [select Id from Contact where Id = :sp.Developer_Agreement_from_Customer_Group__r.Contact__c];
	    Sales_Process__c developerAgreement = [select Id from Sales_Process__c where Id = :sp.Developer_Agreement_from_Customer_Group__c];
	    
	    Account acc = TestHelper.createAccountIndividualClient(new Account(
	        NIP__c = '4213149834',
            BillingStreet = '1',
            BillingCity = '1',
            BillingPostalCode = '1',
            ShippingStreet = '1',
            ShippingCity = '1',
            ShippingPostalCode = '1',
            Email__c = 'test@test.test',
            KRS__c = '1',
            Regon__c = 1,
            Phone = '1',
            FirstName = 'test',
            LastName = 'test',
            PESEL__c = '32042018677',
            Mother_s_name__pc = 'test',
            Father_s_name__pc = 'test',
            Marital_Status__pc = 'test',
            No_identity_card__pc = 'test',
            Mobile_Phone__c = 'test'
        ), true);
        developerAgreement.Contact__c = null;
	    
	    
	    
	    sp.Account_from_Customer_Group__c = acc.Id;
	    sp.Self_Employment__c = true;
	    
	    update sp;
	    update developerAgreement;
	    
	    String[] fields = new String[17];
	    
	    fields[0] = 'individual';
	    fields[1] = acc.Id;
        fields[2] = acc.FirstName;
        fields[3] = acc.LastName;
        fields[4] = acc.PESEL__c;
        fields[5] = acc.Marital_Status__pc;
        fields[6] = acc.Mother_s_name__pc;
        fields[7] = acc.Father_s_name__pc;
        fields[8] = acc.No_identity_card__pc;
        fields[9] = acc.Mobile_Phone__c;
        fields[10] = acc.Email__c ;
        fields[11] = acc.BillingStreet ;
        fields[12] = acc.BillingCity ;
        fields[13] = acc.BillingPostalCode ;
        fields[14] = acc.ShippingStreet ;
        fields[15] = acc.ShippingCity ;
        fields[16] = acc.ShippingPostalCode;
	    
	    WebserviceUtilities.updateAccountFields(fields, developerAgreement.Id); 
	    
	    fields[10] = 'invalidEmail' ;
	    
	    WebserviceUtilities.updateAccountFields(fields, developerAgreement.Id); 
	    
	    fields[0] = 'test';
	    fields[1] = acc.Id;
        fields[2] = acc.NIP__c;
        fields[3] = acc.BillingStreet;
        fields[4] = acc.BillingCity;
        fields[5] = acc.BillingPostalCode;
        fields[6] = acc.ShippingStreet;
        fields[7] = acc.ShippingCity;
        fields[8] = acc.ShippingPostalCode;
        fields[9] = acc.KRS__c;
        fields[10] = String.valueOf(acc.Regon__c);
        fields[11] = acc.Phone;
        fields[12] = acc.Email__c;
        
	    WebserviceUtilities.updateAccountFields(fields, developerAgreement.Id); 
	    
	    fields[12] = 'invalidEmail';
	    
	    WebserviceUtilities.updateAccountFields(fields, developerAgreement.Id); 
	    
	    fields[9] = 'invalidKRS';
	    
	    WebserviceUtilities.updateAccountFields(fields, developerAgreement.Id); 
	    Test.stopTest();
	}
	
	private static testMethod void checkCompanyRepresentativeTest() {
	    Test.startTest();
	    Contact con = TestHelper.createContactPerson(new Contact(), true);
	    WebserviceUtilities.checkCompanyRepresentative(con.Id);
	    Test.stopTest();
	}
	
	private static testMethod void updateContactPersonFieldsTest() {
	    Test.startTest();
	    
	    Contact con = TestHelper.createContactPerson(new Contact(), true);
	    
	    String[] fields = new String[12];
	    
	    fields[0] = con.Id;
	    fields[1] = 'TestFirstName';
        fields[2] = 'TestLastName';
        fields[3] = 'TestMothersName';
        fields[4] = 'TestFathersName';
        fields[5] = '32042018677';
        fields[6] = '1';
        fields[7] = 'TestMailingStreet';
        fields[8] = 'TestMailingCity';
        fields[9] = 'TestMailingPostalCode';
        fields[10] = '889786976';
        fields[11] = 'test@test.test';
	    
	    WebserviceUtilities.updateContactPersonFields(fields);
	    Test.stopTest();
	}
	
	private static testMethod void salesProcessResignationTest() {
	    Test.startTest();
	    Sales_Process__c sp = [select Id, Share_Type__c from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER) limit 1];
	    
	    WebserviceUtilities.salesProcessResignation(sp.Id, CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT), '', CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL);
	    Test.stopTest();
	}
	
	private static testMethod void submitForApprovalManagerTest() {
	    Test.startTest();
	    Sales_Process__c sp = [select Id, Share_Type__c from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER) limit 1];
	    
	    WebserviceUtilities.submitForApprovalManager(sp.Id);
	    Test.stopTest();
	}
	
	private static testMethod void changeAccountOwnerTest() {
	    Test.startTest();
	   // Sales_Process__c sp = [select Id, Share_Type__c from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER) limit 1];
	    List<Account> accounts = TestHelper.createAccountsIndividualClient(null, 5, true);
	    List<Id> accountIds = new List<Id>();
	    
	    for(Account acc: accounts) {
	        accountIds.add(acc.Id);
	    }
	    
	    
	    WebserviceUtilities.changeAccountOwner(accountIds, [select Id from User limit 1].Id);
	    Test.stopTest();
	}
	
	private static testMethod void prepareAdvanceInvoiceNewTest() {
	    Test.startTest();
	    Payment__c payment = TestHelper.createPaymentIncomingPayment(new Payment__c(), true);
	    
	    String[] paymentIds = new String[1];
	    paymentIds[0] = payment.Id;
	    
	    WebserviceUtilities.PrepareAdvanceInvoiceNew(paymentIds);
	    WebserviceUtilities.findDepositId(payment.Id);
	    Test.stopTest();
	}
	
	private static testMethod void getResourcesForInvoiceTest() {
	    Test.startTest();
	    Sales_Process__c handoverProtocol = TestHelper.createSalesProcessAfterSalesService(new Sales_Process__c(), true);
	    Sales_Process__c preliminaryAgreement = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(), true);
	    
	    WebserviceUtilities.getResourcesForInvoice(handoverProtocol.Id, CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL));
	    WebserviceUtilities.getResourcesForInvoice(preliminaryAgreement.Id, CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT));
	    Test.stopTest();
	}
	
	private static testMethod void chackLastInstallmentForInvoiceTest() {
	    Test.startTest();
	    Resource__c res = TestHelper.createResourceBuilding(new Resource__c(), true);
	    Sales_Process__c handoverProtocol = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
	   // Sales_Process__c offerLine = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)];
	   
	    
	    Sales_Process__c preliminaryAgreement = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)];
	    Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Offer_Line_to_Sale_Term__c = preliminaryAgreement.Id, Offer_Line_Resource__c = res.Id, Bank_Account_Number__c = '1109010140000071219812874', Agreement_Value__c = 10), true);
	    
	   // handoverProtocol.Agreement__c = offerLine.Id;
	   // update handoverProtocol;
	    
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(After_sales_Service__c = handoverProtocol.Id, Payment_For__c = res.Id, Agreements_Installment__c = preliminaryAgreement.Id, Bank_Account_For_Resource__c = offerLine.Bank_Account_Number__c, Amount_to_pay__c = 10, Paid_Value__c = 10), true);
	   // Payment__c interestNote = TestHelper.createPaymentsInstallment(new Payment__c(Incoming_Payment_from_Interest_Note__c = payment.Id, Interest_Note_Status__c = CommonUtility.PAYMENT_INTEREST_NOTE_STATUS_ACCEPTED, RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE), After_sales_Service__c = handoverProtocol.Id, Payment_For__c = res.Id, Agreements_Installment__c = preliminaryAgreement.Id, Bank_Account_For_Resource__c = offerLine.Bank_Account_Number__c, Amount_to_pay__c = 10, Paid_Value__c = 10), true);
	    
	    WebserviceUtilities.chackLastInstallmentForInvoice(res.Id, handoverProtocol.Id, CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL));
	    WebserviceUtilities.interestsNotPaid(res.Id, handoverProtocol.Id, new Set<Id>());
	    WebserviceUtilities.getResourcesForDeclaration(preliminaryAgreement.Id);
	    Test.stopTest();
	}
	
    private static testMethod void getOtherDocumentTemplatesTest() {
	    Test.startTest();
	    WebserviceUtilities.getOtherDocumentTemplates();
	    Test.stopTest();
	}
	
	private static testMethod void getDeveloperAgreementDocumentTemplatesTest() {
	    Test.startTest();
	    Resource__c res = TestHelper.createResourceBuilding(new Resource__c(), true);
	    Sales_Process__c handoverProtocol = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
	   // Sales_Process__c offerLine = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)];
	   
	    
	    Sales_Process__c preliminaryAgreement = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)];
	    Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Offer_Line_to_Sale_Term__c = preliminaryAgreement.Id, Offer_Line_Resource__c = res.Id, Bank_Account_Number__c = '1109010140000071219812874', Agreement_Value__c = 10), true);
	    
	    res.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT);
	    
	    update res;
	    
	    WebserviceUtilities.getDeveloperAgreementDocumentTemplates(preliminaryAgreement.Id);
	    
	    res.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE);
	    
	    update res;
	    
	    WebserviceUtilities.getDeveloperAgreementDocumentTemplates(preliminaryAgreement.Id);
	    
	    res.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY);
	    
	    update res;
	    
	    WebserviceUtilities.getDeveloperAgreementDocumentTemplates(preliminaryAgreement.Id);
	    
	    res.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE);
	    
	    update res;
	    
	    WebserviceUtilities.getDeveloperAgreementDocumentTemplates(preliminaryAgreement.Id);
	    
	    Test.stopTest();
	}
	
	private static testMethod void getWarantyDefectsTest() {
	    Test.startTest();
	    
	    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) limit 1];
	    
	    Extension__c extension = TestHelper.createExtensionDefect(new Extension__c(Final_Agreement__c = sp.Id), true);
	    
	    WebserviceUtilities.getWarantyDefects(sp.Id);
	    Test.stopTest();
	}
	
	private static testMethod void updateHiddenDefectIdTest() {
	    Test.startTest();
	    
	    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) limit 1];
	    
	    Extension__c extension = TestHelper.createExtensionDefect(new Extension__c(Final_Agreement__c = sp.Id), true);
	    
	    WebserviceUtilities.updateHiddenDefectId(sp.Id, extension.Id);
	    WebserviceUtilities.updateHiddenDefectId(sp.Id, sp.Id);
	    Test.stopTest();
	}
	
	private static testMethod void getChangesTest() {
	    Test.startTest();
	    
	    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) limit 1];
	    Sales_Process__c handoverProtocol = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
	    
	    Extension__c extension = TestHelper.createExtensionDefect(new Extension__c(Final_Agreement__c = sp.Id, After_sales_Service__c = handoverProtocol.Id), true);
	    
	    WebserviceUtilities.getChanges(handoverProtocol.Id);
	    WebserviceUtilities.getChanges(extension.Id);
	    Test.stopTest();
	}
	
	private static testMethod void getChangesForReimbursementsTest() {
	    Test.startTest();
	    
	    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) limit 1];
	    Sales_Process__c handoverProtocol = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
	    
	    Extension__c extension = TestHelper.createExtensionDefect(new Extension__c(Final_Agreement__c = sp.Id, After_sales_Service__c = handoverProtocol.Id), true);
	    
	    WebserviceUtilities.getChangesForReimbursements(handoverProtocol.Id);
	    WebserviceUtilities.getChangesForReimbursements(extension.Id);
	    Test.stopTest();
	}
	
	private static testMethod void updateQuotationTest() {
	    Test.startTest();
	    
	    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) limit 1];
	    Sales_Process__c handoverProtocol = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
	    
	    Extension__c extension = TestHelper.createExtensionDefect(new Extension__c(Final_Agreement__c = sp.Id, After_sales_Service__c = handoverProtocol.Id), true);
	    
	    WebserviceUtilities.updateQuotation(handoverProtocol.Id, '10');
	    Test.stopTest();
	}
	
	private static testMethod void updateHiddenChangeIdTest() {
	    Test.startTest();
	    
	    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) limit 1];
	    Sales_Process__c handoverProtocol = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
	    
	    Extension__c extension = TestHelper.createExtensionDefect(new Extension__c(Final_Agreement__c = sp.Id, After_sales_Service__c = handoverProtocol.Id), true);
	    
	    WebserviceUtilities.updateHiddenChangeId(handoverProtocol.Id, 'test');
	    WebserviceUtilities.updateHiddenChangeId(handoverProtocol.Id, extension.Id);
	    Test.stopTest();
	}
	
	private static testMethod void getResourcesAfterSalesServiceTest() {
	    Test.startTest();
	    
	    Sales_Process__c sp = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) limit 1];
	    Sales_Process__c handoverProtocol = [select Id from Sales_Process__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)];
	    
	    Extension__c extension = TestHelper.createExtensionDefect(new Extension__c(Final_Agreement__c = sp.Id, After_sales_Service__c = handoverProtocol.Id), true);
	    
	    WebserviceUtilities.getResourcesAfterSalesService(handoverProtocol.Id);
	    Test.stopTest();
	}
	
	private class JSONWrapper {
	    String sObjectFromId;
	    String sObjectFromName = 'Sales_Process__c';
	    String sObjectToName = 'Sales_Process__c';
	    String recordTypeDevName = 'Preliminary_Agreement';
	    List<String> fieldFromApiNames = new List<String> {'Name', 'Contact__r.Name', 'Financing__c', 'Marketing_Campaign__r.Name', 'Customer_Company__c'};
	    List<String> fieldToApiNames = new List<String> {'Offer__c', 'Contact__c', 'Financing__c', 'Marketing_Campaign__c', 'Customer_Company__c'};
	    
	    public JSONWrapper(String sObjectFromId) {
	        this.sObjectFromId = sObjectFromId;
	    }
	}

}