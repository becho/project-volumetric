/**
* @author 		Krystian Bednarek
* @description 	Test class for CannotEditLookupsSalesProcess class
**/

@isTest
private class TestClassCannotEditLookupsSalesProcess {
	
	@isTest 
	static void TestCannotEditLookupSaleTerms() {

		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null ,true);
		// Differnt proposal id needed for Offer__c update
		Sales_Process__c proposalForUpdate = TestHelper.createSalesProcessProposal(null, true);
		
		saleTerms.Offer__c = proposalForUpdate.Id;
		
		// Try to update Offer__c to trigger LookupsSalesProcess class and check if the error message is returned as expected
		Test.startTest();
		    try {
		       update saleTerms;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_SALESPROCESS).getDescribe().fields.getMap().get('Offer__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
	    Test.stopTest();
	}
	@isTest 
	static void TestCannotEditLookupAfterSales() {
		Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(null, true);
		// Differnt saleTerms id needed for Agreement__c update
		Sales_Process__c saleTerms  = TestHelper.createSalesProcessSaleTerms(null, true);

		afterSales.Agreement__c = saleTerms.Id;

		Test.startTest();
		    try {
		       update afterSales;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_SALESPROCESS).getDescribe().fields.getMap().get('Agreement__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
	    Test.stopTest();
	}

	@isTest 
	static void TestCannotEditSaleTermsLookupOnFinalAgreement() {
		Sales_Process__c finalAgreement = TestHelper.createSalesProcessFinalAgreement(null, true);
		// Differnt saleTerms id needed for Agreement__c update

		Test.startTest();
			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null, true);
			finalAgreement.Preliminary_Agreement__c = saleTerms.Id;
		    try {
		       update finalAgreement;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_SALESPROCESS).getDescribe().fields.getMap().get('Preliminary_Agreement__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }

	    Test.stopTest();
	}
	@isTest 
	static void TestCannotEditAfterSalesLookupOnFinalAgreement() {
		Sales_Process__c finalAgreement = TestHelper.createSalesProcessFinalAgreement(null, true);
		
		Test.startTest();
			Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(null, true);
			finalAgreement.Handover_Protocol__c = afterSales.Id;
			try {
		       update finalAgreement;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_SALESPROCESS).getDescribe().fields.getMap().get('Handover_Protocol__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
	    Test.stopTest();
	}

	@isTest 
	static void TestCannotEditSaleTermsLookupOnPayment() {
		Sales_Process__c  saleTerms =  TestHelper.createSalesProcessSaleTerms(null, true);

		Payment__c payment = TestHelper.createPaymentsInstallment(
			new Payment__c(
				Agreements_Installment__c = saleTerms.Id
			),
			true
		);

		Test.startTest();
			Sales_Process__c  saleTermsToUpdate =  TestHelper.createSalesProcessSaleTerms(null, true);
			payment.Agreements_Installment__c = saleTermsToUpdate.Id;
			try {
		       update payment;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('Agreements_Installment__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
		Test.stopTest();
		 
	}

	@isTest 
	static void TestCannotAfterSalesTermsLookupOnPayment() {
		Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(null, true);

		Payment__c payment = TestHelper.createPaymentsInstallment(
			new Payment__c(
				After_sales_Service__c  = afterSales.Id
			),
			true
		);

		Test.startTest();
			Sales_Process__c afterSalesToUpdate = TestHelper.createSalesProcessAfterSalesService(null, true);
			payment.After_sales_Service__c = afterSalesToUpdate.Id;
			try {
		       update payment;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('After_sales_Service__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
		Test.stopTest(); 
	}


	@isTest 
	static void TestCannotAfterSalesLookupOnPayment() {
		Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(null, true);

		Payment__c payment = TestHelper.createPaymentsInstallment(
			new Payment__c(
				After_sales_Service__c  = afterSales.Id
			),
			true
		);

		Test.startTest();
			Sales_Process__c afterSalesToUpdate = TestHelper.createSalesProcessAfterSalesService(null, true);
			payment.After_sales_Service__c = afterSalesToUpdate.Id;
			try {
		       update payment;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('After_sales_Service__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
		Test.stopTest(); 
	}


	@isTest 
	static void TestCannotEditContactLookupOnPayment() {
		Account personAccount = TestHelper.createAccountIndividualClient(null, true);
		Contact contactPerson = [SELECT Id FROM Contact WHERE AccountId = :personAccount.Id LIMIT 1];

		Payment__c payment = TestHelper.createPaymentsInstallment(
			new Payment__c(
				Contact__c  = contactPerson.Id
			),
			true
		);

		Test.startTest();
			Account personAccountToUpdate = TestHelper.createAccountIndividualClient(null, true);
			Contact contactPersonToUpdate = [SELECT Id FROM Contact WHERE AccountId = :personAccountToUpdate.Id LIMIT 1];
			payment.Contact__c = contactPersonToUpdate.Id;
			try {
		       update payment;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('Contact__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
		Test.stopTest(); 
	}

/*	@isTest 
	static void TestCannotEditResourceLookupOnPayment() {
	// needs rework Payment_For__c = flat.Id is not working as expected - produtct line has to be made first 


		Resource__c flat = TestHelper.createResourceFlatApartment(null, true);

		Payment__c payment = TestHelper.createPaymentsInstallment(
			new Payment__c(
				Payment_For__c = flat.Id
			),
			true
		);

		Test.startTest();
			Resource__c flatToUpdate = TestHelper.createResourceFlatApartment(null, true);
			payment.Payment_For__c = flatToUpdate.Id;
			try {
		       update payment;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('Payment_For__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
		Test.stopTest(); w
	}*/

	@isTest 
	static void TestCannotEditAfterSalesLookupOnExtention() {
		Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(null, true);

		Extension__c extention = TestHelper.createExtensionChange(
			new Extension__c(
				After_sales_Service__c = afterSales.Id
			), 
			true
		);


		Test.startTest();
			Sales_Process__c afterSalesToupdate = TestHelper.createSalesProcessAfterSalesService(null, true);
			extention.After_sales_Service__c = afterSalesToupdate.Id;
			try {
		       update extention;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_EXTENSION).getDescribe().fields.getMap().get('After_sales_Service__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
		Test.stopTest(); 
	}

	@isTest 
	static void TestCannotEditAfterSalesLookupOnExtentionDefect() {
		Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(null, true);
		
		Test.startTest();
			Extension__c extention = TestHelper.createExtensionDefect(
				new Extension__c(
					Handover_Protocol__c  = afterSales.Id
				), 
				true
			);

			Sales_Process__c afterSalesToupdate = TestHelper.createSalesProcessAfterSalesService(null, true);
			extention.Handover_Protocol__c  = afterSalesToupdate.Id;
			try {
		       update extention;
		       System.assert(false, 'DmlException expected!');
		    } catch(System.DmlException e) {
	  				String message = e.getMessage();
	  				String errorMessage = Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_EXTENSION).getDescribe().fields.getMap().get('Handover_Protocol__c').getDescribe().getLabel();
	  				System.assert(message.contains(errorMessage), 'message=' + message);
		    }
		Test.stopTest(); 
	}
}