/**
* @author 		Mateusz Pruszyński
* @description 	This class is used to override standard "New" button from Extension__c home page. The button is overriden for Sales Target RT record creation purposes.
* 				Since the "Name" field has to be in a particular format, we have to provide a temporary placeholder with text saying that the field will be populated automatically.
*/
public with sharing class ExtensionNewButtonOverride {

	public Id userRecordTypeId {get; set;} //Record type, choosed by user from standard page: setup/ui/recordtypeselect.jsp
	//public Id salesTargetRecordTypeId {get; set;}
	public Extension__c extensionRecord {get; set;}
	public String nameId {get; set;}
	public String preFix {get; set;}

	public ExtensionNewButtonOverride(ApexPages.StandardController stdController) {
		extensionRecord = (Extension__c)stdController.getRecord();
		userRecordTypeId  = ApexPages.currentPage().getParameters().get('RecordType');
		//salesTargetRecordTypeId = CommonUtility.getRecordTypeId('Extension__c', CommonUtility.EXTENSION_TYPE_SALES_TARGET);
		preFix = CommonUtility.getExtensionPrefix();
	}
	public PageReference reload(){
		nameId = getFieldId(TranslationLabel.getLabelForField('Extension__c', new List<String>{'Name'})[0]);
		System.debug(nameId);
		//if(userRecordTypeId == salesTargetRecordTypeId){
		//	PageReference overridePage = new PageReference('/' + preFix + '/e?nooverride=1&RecordType=' + userRecordTypeId + '&' + nameId + '=' + Label.SalesTarbetNameFieldPlaceholder + '&retURL=/' + preFix + '/o');
		//	return overridePage;
		//}
		//else{
			PageReference overridePage = new PageReference('/' + preFix + '/e?nooverride=1&RecordType=' + userRecordTypeId + '&retURL=/' + preFix + '/o');
			return overridePage;
		//}
	}

	public String getFieldId(String field_label){
        // Obtain the magic ids
        System.debug(field_label);
        PageReference p = new PageReference('/' + preFix + '/e?nooverride=1&RecordType=' + userRecordTypeId);
        String html = '';
        if (Test.IsRunningTest()){
        	html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">'
        	+ '<div class="pbBody"><div class="pbError" id="errorDiv_ep" style="display: none">Error: Invalid Data. <br/>Review all error messages below to correct your data.</div><div class="pbSubheader brandTertiaryBgr first tertiaryPalette" id="head_1_ep"><span class="pbSubExtra"><span class="requiredLegend brandTertiaryFgr"><span class="requiredExampleOuter"><span class="requiredExample">&nbsp;</span></span><span class="requiredMark">*</span><span  class="requiredText"> = Required Information</span></span></span><h3>Information<span  class="titleSeparatingColon">:</span></h3></div><div class="pbSubsection"><table  class="detailList" border="0" cellpadding="0" cellspacing="0"><tr><td class="labelCol requiredInput"><label for="Name"><span class="assistiveText">*</span>Name</label></td><td class="dataCol col02"><div class="requiredInput"><div class="requiredBlock"></div><input  id="Name" maxlength="80" name="Name" size="20" tabindex="1" type="text" /></div></td><td class="labelCol"><label>Owner</label></td><td class="dataCol">Mateusz Pruszyński</td></tr>';
        }
        
        else{
        	html = p.getContent().toString();
        }
        
        Map<String, String> labelToId = new Map<String, String>();
        Matcher m = Pattern.compile('<label for="(.*?)">(<span class="assistiveText">\\*</span>)?(.*?)</label>').matcher(html);
        while (m.find()) {
            String label = m.group(3);
            String id = m.group(1);
            if(label.equalsIgnoreCase(field_label))
                return id; // return field Id.
        }
        return '';
    }
}