/**
* @author 		Mateusz Pruszyński
* @description 	Test class for SalesProcess Trigger Before Insert
**/
@isTest
private class TestClassSalesProcessTriggerBeforeInsert {

	// Populates Resource_Type_c for all product lines
	//@isTest static void populateResourceType() {
	//	// 1 - create 20 differend product lines for same sales process (different resources)
	//	Integer howManyProductLines = 20;

	//	// Insert investment
	//	Resource__c investment = TestHelper.createInvestment(null, true);

	//	// Insert flats
	//	List<Resource__c> resources = new List<Resource__c>();
	//	for(Integer i = 0; i < howManyProductLines; i++) {
	//		resources.add(TestHelper.createResourceFlatApartment(
	//			new Resource__c(
	//				Investment_Flat__c = investment.Id
	//			),
	//			false
	//		));
	//	}
	//	insert resources;

	//	// Insert proposal
	//	Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);

	//	List<Sales_Process__c> productLines = new List<Sales_Process__c>();
	//	for(Resource__c res : resources) {
	//		productLines.add(TestHelper.createSalesProcessProductLine(
	//			new Sales_Process__c(
	//				Product_Line_to_Proposal__c = proposal.Id,
	//				Contact_from_Offer_Line__c = proposal.Contact__c,
	//				Offer_Line_Resource__c = res.Id
	//			),  
	//			false
	//		));
	//	}

	//	Test.startTest();
	//		// Insert product lines
	//		insert productLines;
	//		// 2.1 - check if all inserted product lines have Resource_Type__c <> null (Query)
	//		List<Sales_Process__c> productLines2assert = [
	//			SELECT Id, Resource_Type__c, Offer_Line_Resource__r.RecordType.Name
	//			FROM Sales_Process__c 
	//			WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
	//			AND Resource_Type__c <> null
	//		];
	//	Test.stopTest();
	//	// 2.1 - check if all inserted product lines have Resource_Type__c <> null (Assert)
	//	System.assertEquals(howManyProductLines, productLines2assert.size());
	//	// 3.1 - check if Resource Types are flats
	//	Boolean result = true;
	//	for(Sales_Process__c pl : productLines2assert) {
	//		if(!pl.Resource_Type__c.contains(pl.Offer_Line_Resource__r.RecordType.Name)) {
	//			result = false;
	//			break;
	//		}
	//	}
	//	System.assert(result);
	//}

	// Copy "Resources_on_Related_List__c" from sale terms on after sales service creation
	// Copy "Resources_on_Related_List__c" from after sales service on final agreement creation	
	/*@isTest static void resourcesOnRelatedLists() {
		// 1 - Create Sales Process With two different product lines (two different resources attached)
		// Insert investment
		Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flat
		Resource__c flat = TestHelper.createResourceFlatApartment(				
			new Resource__c(
				Investment_Flat__c = investment.Id
			), 
			true
		);

		// Insert parking space
		Resource__c parkingSpace = TestHelper.createResourceParkingSpace(				
			new Resource__c(
				Investment_Parking_Space__c = investment.Id
			), 
			true
		);

		Resource__c[] resources = new Resource__c[] {flat, parkingSpace};

		// Insert proposal
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);	

		// Insert product lines
		List<Sales_Process__c> productLines = new List<Sales_Process__c>();
		for(Resource__c res : resources) {
			productLines.add(TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Product_Line_to_Proposal__c = proposal.Id,
					Contact_from_Offer_Line__c = proposal.Contact__c,
					Offer_Line_Resource__c = res.Id
				),  
				false
			));
		}
		insert productLines;

		// Insert Sale Terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal.Id,
				Contact__c = proposal.Contact__c
			), 
			true
		);

		Test.startTest();
			// Insert After-sales Service
			Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
				new Sales_Process__c(
					Agreement__c = saleTerms.Id,
					Contact__c = proposal.Contact__c
				), 
				true
			);	
			
			// 2.1 - check if resources have been copied from sale terms to after-sales (Query)
			Sales_Process__c afterSales2Assert = [
				SELECT Id, Resources_on_Related_List__c, Agreement__r.Resources_on_Related_List__c
				FROM Sales_Process__c 
				WHERE Id = :afterSales.Id
				LIMIT 1
			];

			// Insert Final Agreement
			Sales_Process__c finalAgreement = TestHelper.createSalesProcessFinalAgreement(
				new Sales_Process__c(
					Preliminary_Agreement__c = saleTerms.Id,
					Handover_Protocol__c = afterSales.Id,
					Contact__c = proposal.Contact__c
				),
				true
			);

			// 4.1 - check if resources have been copied from after-sales to final agreement (Query)
			Sales_Process__c finalAgreement2Assert = [
				SELECT Id, Resources_on_Related_List__c, Handover_Protocol__r.Resources_on_Related_List__c
				FROM Sales_Process__c 
				WHERE Id = :finalAgreement.Id
				LIMIT 1
			];
		Test.stopTest();
		// 2.1 - check if resources have been copied from sale terms to after-sales (Assert)
		System.assertNotEquals(null, afterSales2Assert.Resources_on_Related_List__c);
		System.assertEquals(afterSales2Assert.Resources_on_Related_List__c, afterSales2Assert.Agreement__r.Resources_on_Related_List__c);			
		// 3 - Check if the field is assembled correctly
		System.assert(afterSales2Assert.Resources_on_Related_List__c.contains(flat.Name));
		System.assert(afterSales2Assert.Resources_on_Related_List__c.contains(parkingSpace.Name));
		// 4.1 - check if resources have been copied from after-sales to final agreement (Assert)
		System.assertEquals(finalAgreement2Assert.Resources_on_Related_List__c, finalAgreement2Assert.Handover_Protocol__r.Resources_on_Related_List__c);
	}*/

	// Copy agreement value from Sale Terms on Final Agreement creation
	@isTest static void copyAgreementValueToFinalAgreement() {
		// 1 - Create sales process structure

		// Insert investment
		Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flat
		Resource__c flat = TestHelper.createResourceFlatApartment(				
			new Resource__c(
				Investment_Flat__c = investment.Id
			), 
			true
		);

		// Insert proposal
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);	

		// Insert product lines
		Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Product_Line_to_Proposal__c = proposal.Id,
				Contact_from_Offer_Line__c = proposal.Contact__c,
				Offer_Line_Resource__c = flat.Id
			),  
			true
		);

		// Insert Sale Terms
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Offer__c = proposal.Id,
				Contact__c = proposal.Contact__c
			), 
			true
		);

		// Insert After-sales Service
		Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
			new Sales_Process__c(
				Agreement__c = saleTerms.Id,
				Contact__c = proposal.Contact__c
			), 
			false
		);		

		Test.startTest();
			insert afterSales;
			// Insert Final Agreement
			Sales_Process__c finalAgreement = TestHelper.createSalesProcessFinalAgreement(
				new Sales_Process__c(
					Preliminary_Agreement__c = saleTerms.Id,
					Handover_Protocol__c = afterSales.Id,
					Contact__c = proposal.Contact__c
				),
				true
			);

			// 2.1 - Check if agreement value has been updated (Query)
			Sales_Process__c finalAgreement2Assert = [
				SELECT Id, Agreement_Value__c, Preliminary_Agreement__r.Agreement_Value__c
				FROM Sales_Process__c 
				WHERE Id = :finalAgreement.Id
				LIMIT 1
			];
		Test.stopTest();
		// 2.1 - Check if agreement value has been updated (Assert)
		System.assertEquals(finalAgreement2Assert.Agreement_Value__c, finalAgreement2Assert.Preliminary_Agreement__r.Agreement_Value__c);
	}

	// trigger part that updates Proposal with lookup to marketing campaign if a contact person comes from any campaign
	@isTest static void proposalWithMarketingCampaign() {
		// 1 - Create customer and assign him/her to a marketing campaign

		// Insert PA
		Account personAccount = TestHelper.createAccountIndividualClient(null, true);
		// Get Contact related to PA
		Contact paContact = [
			SELECT Id FROM Contact WHERE AccountId = :personAccount.Id
		];

		// Insert Marketing Campaign
		Marketing_Campaign__c marketingCampaign = TestHelper.createMarketingCampaign(null, true);

		// Connect PA with MC
		Marketing_Campaign__c marketingCampaignContact = TestHelper.createMarketingCampaignContact(
			new Marketing_Campaign__c(
				Contact__c = paContact.Id,
				Marketing_Campaign__c = marketingCampaign.Id
			), 
			true
		);

		Test.startTest();
			// 2 - Create proposal for the customer
			Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
				new Sales_Process__c(
					Contact__c = paContact.Id
				), true
			);

			// 3 - Check if marketing campaign has been attached
			Sales_Process__c proposal2Assert = [
				SELECT Id, Marketing_Campaign__c
				FROM Sales_Process__c
				WHERE Id = :proposal.Id
				LIMIT 1
			];
		Test.stopTest();
		System.assertEquals(marketingCampaign.Id, proposal2Assert.Marketing_Campaign__c);
	}

	// checks if share for customers from one proposal exceeds 100%. IF so, adds error
	// checks if share for customers from one sale terms exceeds 100%. IF so, adds error
	@isTest static void checkCustomerShare() {
		// 1 - set up proposal with one customer, tha add another one with summed up share > 100%

		// Insert flat
		Resource__c flat = TestHelper.createResourceFlatApartment(null, true);

		// Insert proposal
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);	

		// Insert product line
		Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(
			new Sales_Process__c(
				Product_Line_to_Proposal__c = proposal.Id,
				Contact_from_Offer_Line__c = proposal.Contact__c,
				Offer_Line_Resource__c = flat.Id
			),  
			true
		);		

		String errorMessageProposal = '';
		String errorMessageSaleTerms = '';
		Test.startTest();
 			// change share for existing customer
 			Sales_Process__c mainCustomer = [
 				SELECT Id
 				FROM Sales_Process__c
 				WHERE Proposal_from_Customer_Group__c = :proposal.Id
 				AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)
 			];
 			mainCustomer.Share_Type__c = CommonUtility.SALES_PROCESS_SHARE_TYPE_SHARED_OWNERSHIP;
 			mainCustomer.Share__c = 70;
 			update mainCustomer;

	 			// try to add new one to compare share to 100%
	 			Sales_Process__c newCustomer = TestHelper.createSalesProcessCustomerGroup(
	 				new Sales_Process__c(
	 					Proposal_from_Customer_Group__c = proposal.Id,
	 					Share_Type__c = CommonUtility.SALES_PROCESS_SHARE_TYPE_SHARED_OWNERSHIP,
	 					Share__c = 50
	 				), 
	 				false
	 			);

	 			try {
	 				insert newCustomer;
	 			} catch(Exception e) {
	 				errorMessageProposal = String.valueOf(e);
	 			}

			// 2 - set up sale terms with one customer, tha add another one with summed up share > 100%
			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
				new Sales_Process__c(
					Offer__c = proposal.Id,
					Contact__c = proposal.Contact__c
				),
				true
			);

	 			// try to add new one to compare share to 100%
	 			newCustomer.Developer_Agreement_from_Customer_Group__c = saleTerms.Id;

	 			try {
	 				insert newCustomer;
	 			} catch(Exception e) {
	 				errorMessageSaleTerms = String.valueOf(e);
	 			}			
		Test.stopTest();
		// Assert for proposal
		System.assert(errorMessageProposal.contains(Label.CurrentShareGreaterThan100));
		System.assert(errorMessageSaleTerms.contains(Label.CurrentShareGreaterThan100));
	}

	// Automatically assings newly created record in reservation line
	@isTest static void assignProductLine() {
		// 1 - create 5 differend product lines for same sales process (different resources)
		Integer howManyProductLines = 5;

		// Insert investment
		Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flats
		List<Resource__c> resources = new List<Resource__c>();
		for(Integer i = 0; i < howManyProductLines; i++) {
			resources.add(TestHelper.createResourceFlatApartment(
				new Resource__c(
					Investment_Flat__c = investment.Id
				),
				false
			));
		}
		insert resources;

		Sales_Process__c proposal_one = TestHelper.createSalesProcessProposal(null, true);

		List<Sales_Process__c> productLines_one = new List<Sales_Process__c>();
		for(Resource__c res : resources) {
			productLines_one.add(TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Product_Line_to_Proposal__c = proposal_one.Id,
					Contact_from_Offer_Line__c = proposal_one.Contact__c,
					Offer_Line_Resource__c = res.Id
				),  
				false
			));
		}
		insert productLines_one;
		
		// 2 - Do the same for another sales process (same resources)
		Sales_Process__c proposal_two = TestHelper.createSalesProcessProposal(null, true);

		List<Sales_Process__c> productLines_two = new List<Sales_Process__c>();
		for(Resource__c res : resources) {
			productLines_two.add(TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Product_Line_to_Proposal__c = proposal_two.Id,
					Contact_from_Offer_Line__c = proposal_two.Contact__c,
					Offer_Line_Resource__c = res.Id
				),  
				false
			));
		}		

		Test.startTest();
			insert productLines_two;
		Test.stopTest();

		Set<Id> productLines_twoIds = new Set<Id>();
		for(Sales_Process__c pl2a : productLines_two) {
			productLines_twoIds.add(pl2a.Id);
		}

		List<Sales_Process__c> productLines_two_assert = [
			SELECT Id, Queue__c
			FROM Sales_Process__c
			WHERE Id in :productLines_twoIds
		];

		Boolean result = true;
		for(Sales_Process__c pl2a : productLines_two_assert) {
			if(pl2a.Queue__c <= 1) {
				result = false;
				break;
			}
		}

		System.assert(result);
	}

	// Every newly inserted line has default "Developer = 0zł" standard of completion assigned as soon as a user does not assign other standard of completion.
	@isTest static void assignDeveloperStdOfComp() {
		// 1 - create 20 differend product lines for same sales process (different resources)
		Integer howManyProductLines = 20;

		// Insert investment
		Resource__c investment = TestHelper.createInvestment(null, true);

		// Insert flats
		List<Resource__c> resources = new List<Resource__c>();
		for(Integer i = 0; i < howManyProductLines; i++) {
			resources.add(TestHelper.createResourceFlatApartment(
				new Resource__c(
					Investment_Flat__c = investment.Id
				),
				false
			));
		}
		insert resources;

		// Insert proposal
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);

		List<Sales_Process__c> productLines = new List<Sales_Process__c>();
		for(Resource__c res : resources) {
			productLines.add(TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Product_Line_to_Proposal__c = proposal.Id,
					Contact_from_Offer_Line__c = proposal.Contact__c,
					Offer_Line_Resource__c = res.Id
				),  
				false
			));
		}

		Test.startTest();
			// Insert product lines
			insert productLines;
		Test.stopTest();		
	
		Set<Id> productLinesIds = new Set<Id>();
		for(Sales_Process__c pl2a : productLines) {
			productLinesIds.add(pl2a.Id);
		}

		List<Sales_Process__c> productLines_assert = [
			SELECT Id, Standard_of_Completion__c
			FROM Sales_Process__c
			WHERE Id in :productLinesIds
		];

		Boolean result = true;
		for(Sales_Process__c pl2a : productLines_assert) {
			if(pl2a.Standard_of_Completion__c == null) {
				result = false;
				break;
			}
		}

		System.assert(result);	
	}

	// Trigger part to copy marital status from Account to Customer Group record (on creation)
	@isTest static void copyMaritalStatusFroAccount() {
		// create individual customer with marital status set
		Account personAccount = TestHelper.createAccountIndividualClient(
			new Account(
				Marital_Status__pc = CommonUtility.CONTACT_MARITAL_STATUS_MARRIED
			),
			true
		);

		// create proposal for this customer
		Contact contactFromPA = [
			SELECT Id FROM Contact WHERE AccountId = :personAccount.Id LIMIT 1
		];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = contactFromPA.Id
			), 
			true
		);

		Test.startTest();
			// main customer gets created automatically
			Sales_Process__c mainCustomer = [
				SELECT Id, Marital_Status__c FROM Sales_Process__c 
				WHERE Proposal_from_Customer_Group__c = :proposal.Id
				AND Account_from_Customer_Group__c = :personAccount.Id
			];
		Test.stopTest();	
		System.assertEquals(personAccount.Marital_Status__pc, mainCustomer.Marital_Status__c);
	}

	// calculates customer balance for Proposals
	@isTest static void customerBalanceProposal() {
		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, false);
		proposal.Agreement_Value__c = 1009;

		Test.startTest();
			insert proposal;
			// query proposal
			Sales_Process__c proposal_assert = [
				SELECT Id, Agreement_Value__c, Balance__c
				FROM Sales_Process__c
				WHERE Id = :proposal.Id
			];
		Test.stopTest();			
		System.assertEquals((-1*proposal_assert.Agreement_Value__c), proposal_assert.Balance__c);
	}

	// calculates customer balance for Sale Terms
	@isTest static void customerBalancesaleTerms() {
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Agreement_Value__c = 987
			),
			false
		);

		Test.startTest();
			insert saleTerms;
			// query sale terms
			Sales_Process__c saleTermsAssert = [
				SELECT Id, Agreement_Value__c, Balance__c
				FROM Sales_Process__c
				WHERE Id = :saleTerms.Id
			];
		Test.stopTest();			
		System.assertEquals((-1*saleTermsAssert.Agreement_Value__c), saleTermsAssert.Balance__c);
	}

	// calculates customer balance for After-sales Service
	@isTest static void customerBalanceForAfterSales() {
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
			new Sales_Process__c(
				Agreement_Value__c = 987
			),
			true
		);

		Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
			new Sales_Process__c(
				Agreement__c = saleTerms.Id, 
				Contact__c = saleTerms.Contact__c
			), 
			false
		);

		Test.startTest();
			insert afterSales;
			// query sale terms
			Sales_Process__c afterSalesAssert = [
				SELECT Id, Agreement_Value__c, Balance__c
				FROM Sales_Process__c
				WHERE Id = :afterSales.Id
			];
		Test.stopTest();			
		System.assertEquals((-1*saleTerms.Agreement_Value__c), afterSalesAssert.Balance__c);		
	}

}