/**
* @author 		Mateusz Pruszyński
* @editor 		Maciej Jóźwiak
* @description 	Class used to dynamically get some user info and his/her locale settings for "Offer_template". It works with Offer_Template_Component.component
*/

public without sharing class OfferTemplateController {

	public Contact emailReceipient{get; set;}
	public Sales_Process__c offer{get; set;}
	public String userName {get; set;}
	public String userLanguage {get; set;}
	public Boolean isRendered {get; set;}
	
	public OfferTemplateController() {

		//if(ApexPages.currentPage().getParameters().get('Id') != null){
			isRendered = true;
			String userFirstName = userinfo.getFirstName();
			String userLastName = userinfo.getLastName();
			userName = userFirstName + ' ' + userLastName;

			//String offerId = OffersEmail.offerId;
			//System.debug(offerId);

			//offer = [SELECT Id, Name, Contact__c FROM Sales_Process__c WHERE Id = :offerId];
			//emailReceipient = [SELECT Id, Name, First_and_second_name__c FROM Contact WHERE Id = :offer.Contact__c];

			System.debug(offer);
			System.debug(emailReceipient);

		    if(userLanguage == null) {
		        userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
		    }
		//}
		//else{
		//	isRendered = false;
		//}
	}

}