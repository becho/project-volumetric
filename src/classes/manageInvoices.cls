/**
* @author 		Mateusz Pruszyński
* @description 	Class to store invoice methods
**/

public with sharing class manageInvoices {

    public final static Decimal VAT_OTHER_RESOURCES = 23;

    public final static String UNIT_OF_MEASURE_SZT = 'szt.';

    public final static String SD_ORDER_ZDZA = 'ZDZA'; // ZDZA - advance order 
    public final static String SD_ORDER_ZDSA = 'ZDSA'; // ZDSA - settlement order   

    public final static String SAP_SERVICE_INDEX_DZAL0108 = 'DZAL0108';
    public final static String SAP_SERVICE_INDEX_DZAL0123 = 'DZAL0123';
    public final static String SAP_SERVICE_INDEX_DZAL0223 = 'DZAL0223';
    public final static String SAP_SERVICE_INDEX_DZAL0308 = 'DZAL0308';
    public final static String SAP_SERVICE_INDEX_DZAL0323 = 'DZAL0323';

	// get payment lines
	public static List<Payment__c> getPaymentLines(String incomingPaymentId) {
		return [SELECT Id, Installment_junction__c, Installment_junction__r.Payment_For__c, Paid_Value__c, Installment_junction__r.Payment_For__r.Name,
						Incoming_Payment_junction__c, Installment_junction__r.Payment_For__r.Area_Percentage_Share_HighRate__c,
						Installment_junction__r.Payment_For__r.Area_Percentage_Share_LowRate__c, Installment_junction__r.Payment_For__r.RecordTypeId,
						Incoming_Payment_junction__r.Payment_Date__c, Installment_junction__r.After_sales_Service__c, Installment_junction__r.Payment_For__r.Area__c
				FROM Payment__c
				WHERE Incoming_Payment_junction__c = :incomingPaymentId
				AND RecordTypeId = :CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT_INSTALLMENT)];
	}


	// make invoice line values (vat, net, gross values)
	public static Decimal[] getInvoiceLineValues(Decimal priceWithDiscount, Decimal area, AreaAndVatPercentageDistribution__c dist) {
		AreaAndVatPercentageDistribution__c distribution;
		if(dist == null) {
			distribution = AreaAndVatPercentageDistribution__c.getInstance('CurrentDistributionFlat');
		} else {
			distribution = dist;
		}
		if(distribution.Distribution_Threshold_Area__c != null && area > distribution.Distribution_Threshold_Area__c) {
			Decimal lowDenominator = (1 + (distribution.Low_Vat_Rate__c / 100)) * distribution.Distribution_Threshold_Area__c;
			Decimal highDenominator = (1 + (distribution.High_Vat_Rate__c / 100)) * (area - distribution.Distribution_Threshold_Area__c);
			Decimal netValueFlatSqrMeter = priceWithDiscount / (lowDenominator + highDenominator);

			Decimal lowNetValue = netValueFlatSqrMeter * distribution.Distribution_Threshold_Area__c;
			Decimal lowVatAmount = lowNetValue * (distribution.Low_Vat_Rate__c / 100);
			Decimal highNetValue = netValueFlatSqrMeter * (area - distribution.Distribution_Threshold_Area__c);
			Decimal highVatAmount = highNetValue * (distribution.High_Vat_Rate__c / 100);

			Decimal lowGrossValue = lowNetValue + lowVatAmount;
			Decimal highGrossValue = highNetValue + highVatAmount;
			return new Decimal[] {lowGrossValue.setScale(2), highGrossValue.setScale(2), lowVatAmount.setScale(2), highVatAmount.setScale(2), lowNetValue.setScale(2), highNetValue.setScale(2)};
		} else {
			Decimal lowGrossValue = priceWithDiscount;	
			Decimal lowNetValue = lowGrossValue / (1 + (distribution.Low_Vat_Rate__c / 100));
			Decimal lowVatAmount = lowGrossValue - lowNetValue;	
			return new Decimal[] {lowGrossValue.setScale(2), 0, lowVatAmount.setScale(2), 0, lowNetValue.setScale(2), 0};		
		}
	}	

	// make invoice number
	public static String makeInvoiceNumber(Date dateOfTransaction) {
		String year = dateOfTransaction.year() < 10 ? 
					  String.valueOf('0' + dateOfTransaction.year()) : 
					  String.valueOf(dateOfTransaction.year());
		String month = dateOfTransaction.month() < 10 ? 
					   String.valueOf('0' + dateOfTransaction.month()) : 
					   String.valueOf(dateOfTransaction.month());
		String day = dateOfTransaction.day() < 10 ? 
					 String.valueOf('0' + dateOfTransaction.day()) : 
					 String.valueOf(dateOfTransaction.day());
		return 'FV/' + year.right(2) + '/' + month + '/' + day;
	}

}