/**
* @author 		Joanna Stępińska
* @description 	test class for EditSalesProcess.cls
**/
@isTest
private class TestClassEditSalesProcess {
    
    //public method for initializing controller and its selected Stages
    public static EditSalesProcess getController(Sales_Process__c process) {
        ApexPages.StandardController pageController = new ApexPages.StandardController(process);
        return new EditSalesProcess(pageController);
    }
    
    //public method for intitializing page reference and its parameter retUrl
    public static PageReference getPageReference(String processId) {
        PageReference pageRef = Page.EditSalesProcess;
        //PageReference otherPageRef = Page.EditSalesProcess;
        Test.setCurrentPageReference(pageRef);
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, processId);
        //pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, otherPageRef.getUrl());
        return pageRef;
    }
    
    //static testMethod for checking SalesProcess of a type SALES_PROCESS_TYPE_OFFER
    static testMethod void testProcessProposal() {
        Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);
        PageReference pageRef = getPageReference(String.valueOf(proposal.Id));
        EditSalesProcess editSalesProcess = getController(proposal);
        
        Test.startTest();
        	String nextPage = editSalesProcess.ret().getUrl();
            System.assertEquals(('/apex/newproposal?spId=' + String.valueOf(proposal.Id)), nextPage);
        Test.stopTest();
    }
    
    //static testMethod for checking SalesProcess of a type SALES_PROCESS_TYPE_OFFER_LINE 
    static testMethod void testProductLine() {
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTermsTemplate = new Sales_Process__c(
            Offer__c = productLine.Product_Line_to_Proposal__c,
            Contact__c = productLine.Contact_from_Offer_Line__c
        );
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(saleTermsTemplate, true);
        productLine.Offer_Line_to_Sale_Term__c = saleTerms.Id;
        saleTerms.Status__c = CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL;
        update saleTerms;
        //update productLine;
        Sales_Process__c rtOfProcessMain = [SELECT Offer_Line_to_Sale_Term__c, Offer_Line_to_Sale_Term__r.RecordTypeId, Offer_Line_to_Sale_Term__r.Status__c FROM Sales_Process__c WHERE Id =: productLine.Id];
        //productLine.Offer_Line_to_Sale_Term__c = saleTerms.Id;
        
        PageReference pageRef = getPageReference(String.valueOf(productLine.Id));
        EditSalesProcess editSalesProcess = getController(productLine);
        
        Test.startTest();
        	List<Apexpages.Message> msgs = new List<Apexpages.Message>();
        	boolean b = false;
        	editSalesProcess.ret();
        	//System.assert(editSalesProcess.ret().getUrl().contains(CommonUtility.URL_PARAM_SPID));
        	msgs = ApexPages.getMessages();
			for(Apexpages.Message msg:msgs) {
                if (msg.getDetail().contains(Label.RecordIsWaitingForManager)) {
                    b = true;
                }
            }
			//System.assert(b);
        
   //     	offer.Status__c = CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER;
   //     	update offer;
   //     	process.Offer_Line_to_Sale_Term__c = offer.Id;
   //     	System.assert(editSalesProcess.ret().getUrl().contains(CommonUtility.URL_PARAM_SPID));
   //     	msgs = ApexPages.getMessages();
			//for(Apexpages.Message msg:msgs) {
   //             if (msg.getDetail().contains(Label.termsAccepted)) {
   //                 b = true;
   //             }
   //         }
			////System.assert(b);
        	
   //     	offer.Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED;
   //     	update offer;
   //         process.Offer_Line_to_Sale_Term__c = offer.Id;
   //     	System.assert(editSalesProcess.ret().getUrl().contains(CommonUtility.URL_PARAM_SPID));
   //     	msgs = ApexPages.getMessages();
			//for(Apexpages.Message msg:msgs) {
   //             if (msg.getDetail().contains(Label.SaleTermsAlreadySigned)) {
   //                 b = true;
   //             }
   //         }
			////System.assert(b);
        
   //     Test.stopTest();
    }
    /*
    //static testMethod for checking SalesProcess of a type SALES_PROCESS_TYPE_OFFER_LINE with status waiting for manager approval 
    static testMethod void testProductLineStatusWaiting(){
        process = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c offer= new Sales_Process__c();
        offer.Status__c = CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL;
        process.Offer_Line_to_Sale_Term__r=offer;
        PageReference pageRef = getPageReference(String.valueOf(process.Id));
        EditSalesProcess editSalesProcess = getController();
        
        Test.startTest();
        	List<Apexpages.Message> msgs = ApexPages.getMessages();
        	boolean b = false;
			for(Apexpages.Message msg:msgs) {
                if (msg.getDetail().contains(Label.RecordIsWaitingForManager)) {
                    b = true;
                }
            }
			System.assert(b);
        Test.stopTest();
    }
    
    //static testMethod for checking SalesProcess of a type SALES_PROCESS_TYPE_OFFER_LINE with status accepted manager 
    static testMethod void testProductLineStatusAccepted(){
        process = TestHelper.createSalesProcessProductLine(null, true);
        process.Offer__c = process.Id;
        process.Offer_Line_to_Sale_Term__r.Status__c = CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER;
        PageReference pageRef = getPageReference(String.valueOf(process.Id));
        EditSalesProcess editSalesProcess = getController();
        
        Test.startTest();
        	List<Apexpages.Message> msgs = ApexPages.getMessages();
        	boolean b = false;
			for(Apexpages.Message msg:msgs) {
                if (msg.getDetail().contains(Label.termsAccepted)) {
                    b = true;
                }
            }
			System.assert(b);
        Test.stopTest();
    }
    
     //static testMethod for checking SalesProcess of a type SALES_PROCESS_TYPE_OFFER_LINE with status developer signed 
    static testMethod void testProductLineStatusSigned(){
        process = TestHelper.createSalesProcessProductLine(null, true);
        process.Offer__c = process.Id;
        process.Offer_Line_to_Sale_Term__r.Status__c = CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED;
        PageReference pageRef = getPageReference(String.valueOf(process.Id));
        EditSalesProcess editSalesProcess = getController();
        
        Test.startTest();
        	List<Apexpages.Message> msgs = ApexPages.getMessages();
        	boolean b = false;
			for(Apexpages.Message msg:msgs) {
                if (msg.getDetail().contains(Label.SaleTermsAlreadySigned)) {
                    b = true;
                }
            }
			System.assert(b);
        Test.stopTest();
    }
    */
    
    //static testMethod for checking SalesProcess of a type SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT with Empty Status
    static testMethod void testProcessSaleTermsEmptyStatus(){
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null, true);
        PageReference pageRef = getPageReference(String.valueOf(saleTerms.Id));
        EditSalesProcess editSalesProcess = getController(saleTerms);
        User u = TestHelper.createUser(null, true);
        
        Test.startTest();
            System.runAs(u){
       			boolean b = false;
                if (editSalesProcess.ret().getUrl().contains(String.valueOf(saleTerms.Id))) {
                   b = true;
                }
                System.assert(b);
            }
        Test.stopTest();
    }
    
    ////static testMethod for checking SalesProcess of a type SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT with status accepted manager
    //static testMethod void testProcessSaleTerms(){
    //    process = TestHelper.createSalesProcessSaleTerms(null, true);
    //    process.Status__c = CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER;
    //    PageReference pageRef = getPageReference(String.valueOf(process.Id));
    //    EditSalesProcess editSalesProcess = getController();
    //    User u= TestHelper.createUser(null, true);
        
    //    Test.startTest();
    //        System.runAs(u){
    //            String nextPage = editSalesProcess.ret().getUrl();
    //            System.assertEquals(('/apex/editsalesterms?Id='+String.valueOf(process.Id)), nextPage);
    //        }
    //    Test.stopTest();
    //}
    
    //static testMethod for checking SalesProcess of a type SALES_PROCESS_TYPE_HANDOVER_PROTOCOL
    static testMethod void testProcessAfterSalesService(){
        Sales_Process__c afterSalesService = TestHelper.createSalesProcessSaleTerms(null, true);
        afterSalesService.Status__c = CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL;
        PageReference pageRef = getPageReference(String.valueOf(afterSalesService.Id));
        EditSalesProcess editSalesProcess = getController(afterSalesService);
        
        Test.startTest();
        	String nextPage = editSalesProcess.ret().getUrl();
        	System.assert(nextPage.contains(CommonUtility.URL_PARAM_NOOVERRIDE));
        Test.stopTest();
    }
    
    //static testMethod for checking function back()
    static testMethod void testBack(){
        Sales_Process__c process = TestHelper.createSalesProcessSaleTerms(null, true);
        PageReference pageRef = getPageReference(String.valueOf(process.Id));
        EditSalesProcess editSalesProcess = getController(process);
        
        Test.startTest();
        	PageReference pageRefer = new PageReference('/'+process.Id);
        	String nextPage = editSalesProcess.back().getUrl();
        	System.assertEquals(pageRefer.getUrl(), nextPage);
        Test.stopTest();
    }
    
    ////static testMethod for checking error message
    //static testMethod void testError(){
    //    process = new Sales_Process__c();
    //    process.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER);
    //    PageReference pageRef = getPageReference(String.valueOf(process.Id));
    //    EditSalesProcess editSalesProcess = getController();
        
    //    Test.startTest();
    //    	boolean b = false;
    //    	List<Apexpages.Message> msgs = ApexPages.getMessages();
    //    	String nextPage = editSalesProcess.back().getUrl();
    //       	for(Apexpages.Message msg:msgs) {
    //            if (msg.getDetail().contains(Label.YouCantEditOrDeleteOffer)) {
    //                b = true;
    //            }
    //        }
    //        //System.assert(b);
    //    Test.stopTest();
    //}
   
}