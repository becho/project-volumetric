@isTest
private class TestClassSalesProcessManager {

	private static testMethod void updateQueueTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessProposal(new Sales_Process__c(Reservation_Status__c = SalesProcessManager.RESERVATION_STATUS_RESERVED), true);
	    Sales_Process__c sp2 = TestHelper.createSalesProcessProposal(new Sales_Process__c(Reservation_Status__c = SalesProcessManager.RESERVATION_STATUS_CANCELLED), true);
	    
	    Resource__c res = TestHelper.createResourceFlatApartment(null, true);
        
        Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Product_Line_to_Proposal__c = sp.Id, Offer_Line_Resource__c = res.Id), true);
        
        SalesProcessManager.updateQueue(new Set<Id> {sp.Id}, null);
        SalesProcessManager.updateQueue(null, new Set<Id>{res.Id});
	}

    private static testMethod void updateResLstOfferOnAgreementStatusSignedTest() {
        Sales_Process__c sp = TestHelper.createSalesProcessFinalAgreement(null, true);
        SalesProcessManager.updateResLstOfferOnAgreementStatusSigned(new Set<Id>{sp.Id}, true);
        SalesProcessManager.updateResLstOfferOnAgreementStatusSigned(new Set<Id>{sp.Id}, false);
	}
	
	private static testMethod void updateResStatusOnHandoverStatusSignedTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessAfterSalesService(null, true);
        SalesProcessManager.updateResStatusOnHandoverStatusSigned(new Set<Id>{sp.Id});
	}
	
	private static testMethod void cancelledReservationTaskTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessAfterSalesService(new Sales_Process__c(Reservation_Status__c = SalesProcessManager.RESERVATION_STATUS_CANCELLED), true);
        SalesProcessManager.cancelledReservationTask(new List<Id>{sp.Id});
	}
	
	private static testMethod void copyPrice2resTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(), true);
	    Sales_Process__c offerLine = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Offer_Line_to_Sale_Term__c = sp.Id), true);
        SalesProcessManager.copyPrice2res(new List<Sales_Process__c>{sp});
	}
	
	private static testMethod void promotionQuantitiesUpdateTest() {
	    Resource__c res = TestHelper.createResourceStorage(new Resource__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PROMOTION)), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Promotion__c = res.Id), true);
	    SalesProcessManager.promotionQuantitiesUpdate(new Set<Sales_Process__c> {sp});
	}
	
	private static testMethod void manageResignationStatusTest() {
	    
	    Resource__c res = TestHelper.createResourceStorage(new Resource__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PROMOTION)), true);
	    Sales_Process__c finalAgreement = TestHelper.createSalesProcessFinalAgreement(new Sales_Process__c(Promotion__c = res.Id), true);
	    Sales_Process__c afterSalesService = [select Id, Rejection_Reason__c, Agreement__c from Sales_Process__c where Id = :finalAgreement.Handover_Protocol__c];
	    Sales_Process__c saleTerms = [select Id, Offer__c, Rejection_Reason__c from Sales_Process__c where Id = :finalAgreement.Preliminary_Agreement__c];
	    Test.startTest();
	    SalesProcessManager.manageResignationStatus(new List<Sales_Process__c> {saleTerms}, new List<Sales_Process__c> {afterSalesService}, new List<Sales_Process__c> {finalAgreement});
	    Test.stopTest();
	}
	
	private static testMethod void updateInvestmentAfterProductLineDeletionTest() {
	    Resource__c res = TestHelper.createResourceStorage(new Resource__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PROMOTION)), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessProductLine(new Sales_Process__c(Promotion__c = res.Id), true);
	    SalesProcessManager.updateInvestmentAfterProductLineDeletion(new Set<Id> {sp.Product_Line_to_Proposal__c});
	}
	
	private static testMethod void createFinalAgreementTest() {
	    Resource__c res = TestHelper.createResourceStorage(new Resource__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PROMOTION)), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessAfterSalesService(new Sales_Process__c(Promotion__c = res.Id), true);
	    SalesProcessManager.createFinalAgreement(new Map<Id, Sales_Process__c> {sp.Id => sp});
	}
}