/**
* @author 		Dariusz Paszel
* @description 	Test for EditPayment
*/

@isTest
private class TestClassEditPayment {
	
	static testMethod void editPayment() {

	Payment__c installment = TestHelper.createPaymentsInstallment(null, true);

	PageReference pageRef = Page.EditPayment;
	PageRef.getParameters().put('referer', '.com' + '/' + installment.Id);
	Test.setCurrentPage(pageRef);

	ApexPages.StandardController stdController = new ApexPages.standardController(installment);
	EditPayment ePage = new EditPayment(stdController);

	test.startTest();
	String referer = ePage.getReferer();
	PageReference ret = ePage.ret(); 
	String getRetUrlFromUrl = apexpages.currentpage().getparameters().get('retURL');
	test.stopTest();
	System.debug ('ret ---> ' + ret);
	System.assertEquals(getRetUrlFromUrl, null);
	System.assertEquals(referer, null);
	}
	
}