global with sharing class AddFavourite {
	/**
	* @author       Mateusz Pruszyński
	* @description  Opopup window to create favourite
	*/
	public Sales_Process__c myOffer {get; set;}
    public Boolean fromContact {get; set;}

	public AddFavourite(){
        myOffer = new Sales_Process__c(); 
        if(Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_CONTACTID) != null) {
            // from contact
            myOffer.Customer_Favorite__c = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_CONTACTID);
        }
        else if(Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ACCOUNTID) != null) {
            // from person account 
            List<Contact> c = [SELECT Id FROM Contact WHERE accountId = :Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ACCOUNTID) LIMIT 1];
            if(!c.isEmpty()) {
                myOffer.Customer_Favorite__c = c[0].Id;
            }
        }
        if(myOffer.Customer_Favorite__c != null) {
            fromContact = true;
        }
        else {
            fromContact = false;
        }
	}   

    /** Webservice methods
    * --------------------
    */

    webservice static Boolean massAddToFavorite(String[] resourceIds, String contactId) {
        List<Sales_Process__c> favouritesToInsert = new List<Sales_Process__c>();
        Sales_Process__c fav;
        for(String resId : resourceIds) {
            fav = new Sales_Process__c();
            fav.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FAVOURITE);
            fav.Resource_Favourite__c = resId;
            fav.Customer_Favorite__c = contactId;
            favouritesToInsert.add(fav);
        }
        try {
            insert favouritesToInsert;
            return true;
        } catch (Exception e) {
            ErrorLogger.log(e);
            return false;
        }
    }
}