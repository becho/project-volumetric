/**
* @author       Krystian Bednarek
* @description  test class for DeletePromotionsController.cls
**/
@isTest
private class TestClassDeletePromotionsController {
    
  @isTest static void testDeleteActionIsEmpty() {

        List<Sales_Process__c> selectedPromotions        = new List<Sales_Process__c>();
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedPromotions); 
        
        stdSetController.setSelected(selectedPromotions);

        PageReference pageRef = Page.DeletePromotions;
        Test.setCurrentPageReference(pageRef);
        Sales_Process__c processProposal = TestHelper.createSalesProcessProposal(null, true);

        Sales_Process__c template = new Sales_Process__c(
            Proposal_from_Promotions__c = processProposal.Id,
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)       
        );
        Sales_Process__c singlePromotion = TestHelper.createSalesProcessClientPromotion(template, true);
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ processProposal.Id );
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, singlePromotion.Id );
        
        DeletePromotionsController controller = new DeletePromotionsController(stdSetController);
        System.assertEquals(controller.selectedPromotions[0].Id, singlePromotion.Id);

    }

    @isTest static void testDeleteActionIsNotEmptyAcceptedByManager() {
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null, false);
        insert saleTerms;


        PageReference pageRef = Page.DeletePromotions;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+ saleTerms.Id );
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, saleTerms.Id );
        
        Sales_Process__c template = new Sales_Process__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION),
            Proposal_from_Promotions__c = saleTerms.Offer__c          
            );

        List<Sales_Process__c> selectedPromotions = TestHelper.createSalesProcessesClientPromotion(template, 1, false);
        for(Sales_Process__c promotions: selectedPromotions){
            promotions.Sale_Terms_from_Promotions__c = saleTerms.Id;              
        }
        insert selectedPromotions;

        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedPromotions); 
        stdSetController.setSelected(selectedPromotions);
        DeletePromotionsController controller = new DeletePromotionsController(stdSetController);
        
        
        // checking if message is displayed  "cannot edit sale terms accepted by manager"
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(Label.YouCannotUseButton + ' ' + Label.SaleTermsAccepted,  msg.getSummary(), msg.getSummary()); 
            System.assertEquals(ApexPages.Severity.Info, msg.getSeverity());
        }
    }

    @isTest static void testDeleteActionIsNotEmptyWaitingForManagerApproval() {
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null, false);
        saleTerms.Status__c = CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL;
        insert saleTerms;


        PageReference pageRef = Page.DeletePromotions;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+saleTerms.Id );
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, saleTerms.Id );
        
        Sales_Process__c template = new Sales_Process__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION),
            Proposal_from_Promotions__c = saleTerms.Offer__c          
            );

        List<Sales_Process__c> selectedPromotions = TestHelper.createSalesProcessesClientPromotion(template, 1, false);
        for(Sales_Process__c promotions: selectedPromotions){
            promotions.Sale_Terms_from_Promotions__c = saleTerms.Id;              
        }
        insert selectedPromotions;

        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedPromotions); 
        stdSetController.setSelected(selectedPromotions);
        DeletePromotionsController controller = new DeletePromotionsController(stdSetController);


        // checking if message is displayed  " cannot edit sale terms waiting for manager's approval "
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(Label.YouCannotUseButton + ' ' + Label.RecordIsWaitingForManager,  msg.getSummary(), msg.getSummary()); 
            System.assertEquals(ApexPages.Severity.Info, msg.getSeverity());
        }

    }
    
    /*@isTest static void testDeleteActionIsNotEmptyDeveloperSigned() {
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null, true);
        saleTerms.Status__c = CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_MANAGER;
        saleTerms.Date_of_signing__c = Date.today();
        update saleTerms;

        PageReference pageRef = Page.DeletePromotions;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, '/'+saleTerms.Id );
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, saleTerms.Id );
        
        Resource__c promo = TestHelper.createResourcePromotion(null,true);
        List<Sales_Process__c> selectedPromotions = new List<Sales_Process__c>();
        for(Integer i = 0; i < 2; i++) {
            selectedPromotions.add(
                TestHelper.createSalesProcessClientPromotion( 
                    new Sales_Process__c(
                        RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION),
                        Proposal_from_Promotions__c = saleTerms.Offer__c,
                        Promotion__c = promo.Id,
                        Sale_Terms_from_Promotions__c = saleTerms.Id
                    ),
                    false   
                )   
            );
        }
        insert selectedPromotions;

        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedPromotions); 
        stdSetController.setSelected(selectedPromotions);
        DeletePromotionsController controller = new DeletePromotionsController(stdSetController);

        // checking if message is displayed  "agreement signed "
        for(ApexPages.Message msg :  ApexPages.getMessages()) {

            System.assertEquals(Label.YouCannotUseButton + ' ' + Label.SaleTermsAlreadySigned,  msg.getSummary(), msg.getSummary());    
            System.assertEquals(ApexPages.Severity.Info, msg.getSeverity());
        }

    }*/ 
    @isTest static void testDeleteActionAfterSalesIsNotEmpty() {
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null, false);
        saleTerms.Status__c = CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL;    
        insert saleTerms;

        PageReference pageRef = Page.DeletePromotions;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL,'/'+saleTerms.Id );
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, saleTerms.Id );
        
        Sales_Process__c template = new Sales_Process__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION),
            Proposal_from_Promotions__c =saleTerms.Offer__c           
            );

        List<Sales_Process__c> selectedPromotions = TestHelper.createSalesProcessesClientPromotion(template, 1, false);
        for(Sales_Process__c promotions: selectedPromotions){
            promotions.Sale_Terms_from_Promotions__c = saleTerms.Id;
            promotions.Agreement__c =   saleTerms.Id;
            promotions.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL);       
        }
        insert selectedPromotions;

        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedPromotions); 
        stdSetController.setSelected(selectedPromotions);
    
        DeletePromotionsController controller = new DeletePromotionsController(stdSetController);

        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(Label.YouCannotUseButton + ' ' + Label.AfterSalesServiceExists,  msg.getSummary(), msg.getSummary());   
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }
                   
    }
    


    @isTest static void testingPageReferenceFunctions() {
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null, true);
        
        PageReference pageRef = Page.DeletePromotions;
        Test.setCurrentPageReference(pageRef);
        String retUrl = '/'+saleTerms.Id;
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, retUrl);
        pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, saleTerms.Id );

        Sales_Process__c template = new Sales_Process__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION),
            Proposal_from_Promotions__c = saleTerms.Offer__c          
            );
        List<Sales_Process__c> selectedPromotions = TestHelper.createSalesProcessesClientPromotion(template, 1, false);
        
        for(Sales_Process__c promotions: selectedPromotions){
            promotions.Proposal_from_Promotions__c = saleTerms.Id;        
        }
        insert selectedPromotions;

        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedPromotions); 
        stdSetController.setSelected(selectedPromotions);
        DeletePromotionsController controller = new DeletePromotionsController(stdSetController);
        
        Test.startTest();
		String pageToTestNo = controller.no().getUrl();
        System.assertEquals(retUrl,pageToTestNo);

        String pageToTestYes = controller.yes().getUrl();

        Set<Id> promotionIds = new Set<Id>();
		  for(Sales_Process__c promotion: selectedPromotions){
            promotionIds.add(promotion.Id);        
        }
        List<Sales_Process__c> retrivedPromotions = [SELECT ID FROM Sales_Process__c WHERE id in: promotionIds];
        System.assertEquals(true, retrivedPromotions.isEmpty());
        System.assertEquals(retUrl,pageToTestYes);
        Test.stopTest();
    }


    @isTest static void testingIfRetuContainsNewid() {
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(null, true);
        Sales_Process__c customerGroup = TestHelper.createSalesProcessCustomerGroup(null, true);

        PageReference pageRef = Page.DeletePromotions;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL,'/'+saleTerms.Id + CommonUtility.URL_PARAM_NEWID + '/'+customerGroup.id );
        
        Sales_Process__c template = new Sales_Process__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION),
            Proposal_from_Promotions__c = saleTerms.Offer__c          
            );

        List<Sales_Process__c> selectedPromotions = TestHelper.createSalesProcessesClientPromotion(template, 1, false);
        for(Sales_Process__c promotions: selectedPromotions){
            promotions.Proposal_from_Promotions__c = saleTerms.Id;        
        }
        insert selectedPromotions;

        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedPromotions); 
        stdSetController.setSelected(selectedPromotions);
        DeletePromotionsController controller = new DeletePromotionsController(stdSetController);
   		Test.startTest();
   			System.assertEquals(true, controller.renderTable );
   		Test.stopTest();
    }
}