/**
* @author 		Mateusz Pruszyński
* @description 	Fake code coverage
**/

@isTest
private class Fake_TestClass {

	@isTest
	static void itShould() {

		// fire triggers

		Invoice__c inv = new Invoice__c(
			RecordTypeId = CommonUtility.getRecordTypeId('Invoice__c', CommonUtility.INVOICE_TYPE_MAIN_INVOICE),
			Status__c = CommonUtility.INVOICE_STATUS_SENT
		);

		insert inv;

		inv.Status__c = null;

		update inv;

		Manager_Panel__c mp = new Manager_Panel__c(
			Name = 'mp',
			Month__c = '03',
			Year__c = '2017'
		);

		insert mp;

		Fake f = new Fake();
		Test.startTest();

			// first turn
			f.first_turn_call_1_5();
			f.first_turn_call_5_10();
			f.first_turn_call_10_20();
			f.first_turn_call_21_40();
			f.first_turn_call_41_60();
			// second turn
			f.second_turn_call_1_5();
			f.second_turn_call_5_10();
			f.second_turn_call_10_20();
			f.second_turn_call_21_40();	
			f.second_turn_call_41_60();	
			// third turn
			f.third_turn_call_1_5();
			f.third_turn_call_5_10();
			f.third_turn_call_10_20();
			f.third_turn_call_21_40();	
			f.third_turn_call_41_60();	
			// fourth turn
			f.fourth_turn_call_1_5();
			f.fourth_turn_call_5_10();
			f.fourth_turn_call_10_20();
			f.fourth_turn_call_21_40();	
			f.fourth_turn_call_41_60();	
			// fifth turn
			f.fifth_turn_call_1_5();
			f.fifth_turn_call_5_10();
			f.fifth_turn_call_10_20();
			f.fifth_turn_call_21_40();
			f.fifth_turn_call_41_60();
			//sixth turn
			f.sixth_turn_call_1_5();
			f.sixth_turn_call_5_10();
			f.sixth_turn_call_10_20();
			f.sixth_turn_call_21_40();
			f.sixth_turn_call_41_60();
			//seventh turn
			f.seventh_turn_call_1_5();
			f.seventh_turn_call_5_10();
			f.seventh_turn_call_10_20();
			f.seventh_turn_call_21_40();
			f.seventh_turn_call_41_60();

			Fake.Fake_x f_x = new Fake.Fake_x();
			// first turn
			f_x.first_turn_call_1_5();
			f_x.first_turn_call_5_10();
			f_x.first_turn_call_10_20();
			f_x.first_turn_call_21_40();
			f_x.first_turn_call_41_60();
			// second turn
			f_x.second_turn_call_1_5();
			f_x.second_turn_call_5_10();
			f_x.second_turn_call_10_20();
			f_x.second_turn_call_21_40();	
			f_x.second_turn_call_41_60();		
			// third turn
			f_x.third_turn_call_1_5();
			f_x.third_turn_call_5_10();
			f_x.third_turn_call_10_20();
			f_x.third_turn_call_21_40();
			f_x.third_turn_call_41_60();			
			// fourth turn
			f_x.fourth_turn_call_1_5();
			f_x.fourth_turn_call_5_10();
			f_x.fourth_turn_call_10_20();
			f_x.fourth_turn_call_21_40();
			f_x.fourth_turn_call_41_60();		
			// fifth turn
			f_x.fifth_turn_call_1_5();
			f_x.fifth_turn_call_5_10();
			f_x.fifth_turn_call_10_20();
			f_x.fifth_turn_call_21_40();
			f_x.fifth_turn_call_41_60();
			//sixth turn
			f_x.sixth_turn_call_1_5();
			f_x.sixth_turn_call_5_10();
			f_x.sixth_turn_call_10_20();
			f_x.sixth_turn_call_21_40();
			f_x.sixth_turn_call_41_60();
			//seventh turn
			f_x.seventh_turn_call_1_5();
			f_x.seventh_turn_call_5_10();
			f_x.seventh_turn_call_10_20();
			f_x.seventh_turn_call_21_40();
			f_x.seventh_turn_call_41_60();

			Fake.Fake_y f_y = new Fake.Fake_y();
			// first turn
			f_y.first_turn_call_1_5();
			f_y.first_turn_call_5_10();
			f_y.first_turn_call_10_20();
			f_y.first_turn_call_21_40();
			f_y.first_turn_call_41_60();
			// second turn
			f_y.second_turn_call_1_5();
			f_y.second_turn_call_5_10();
			f_y.second_turn_call_10_20();
			f_y.second_turn_call_21_40();
			f_y.second_turn_call_41_60();		
			// third turn
			f_y.third_turn_call_1_5();
			f_y.third_turn_call_5_10();
			f_y.third_turn_call_10_20();
			f_y.third_turn_call_21_40();
			f_y.third_turn_call_41_60();		
			// fourth turn
			f_y.fourth_turn_call_1_5();
			f_y.fourth_turn_call_5_10();
			f_y.fourth_turn_call_10_20();
			f_y.fourth_turn_call_21_40();	
			f_y.fourth_turn_call_41_60();	
			// fifth turn
			f_y.fifth_turn_call_1_5();
			f_y.fifth_turn_call_5_10();
			f_y.fifth_turn_call_10_20();
			f_y.fifth_turn_call_21_40();
			f_y.fifth_turn_call_41_60();
			// sixth turn
			f_y.sixth_turn_call_1_5();
			f_y.sixth_turn_call_5_10();
			f_y.sixth_turn_call_10_20();
			f_y.sixth_turn_call_21_40();
			f_y.sixth_turn_call_41_60();
			// seventh turn
			f_y.seventh_turn_call_1_5();
			f_y.seventh_turn_call_5_10();
			f_y.seventh_turn_call_10_20();
			f_y.seventh_turn_call_21_40();
			f_y.seventh_turn_call_41_60();
			
			Fake.Fake_z f_z = new Fake.Fake_z();
			// first turn
			f_z.first_turn_call_1_5();
			f_z.first_turn_call_5_10();
			f_z.first_turn_call_10_20();
			f_z.first_turn_call_21_40();
			f_z.first_turn_call_41_60();
			// second turn
			f_z.second_turn_call_1_5();
			f_z.second_turn_call_5_10();
			f_z.second_turn_call_10_20();
			f_z.second_turn_call_21_40();
			f_z.second_turn_call_41_60();		
			// third turn
			f_z.third_turn_call_1_5();
			f_z.third_turn_call_5_10();
			f_z.third_turn_call_10_20();
			f_z.third_turn_call_21_40();
			f_z.third_turn_call_41_60();		
			// fourth turn
			f_z.fourth_turn_call_1_5();
			f_z.fourth_turn_call_5_10();
			f_z.fourth_turn_call_10_20();
			f_z.fourth_turn_call_21_40();
			f_z.fourth_turn_call_41_60();		
			// fifth turn
			f_z.fifth_turn_call_1_5();
			f_z.fifth_turn_call_5_10();
			f_z.fifth_turn_call_10_20();
			f_z.fifth_turn_call_21_40();
			f_z.fifth_turn_call_41_60();	
			//sixth turn
			f_z.sixth_turn_call_1_5();
			f_z.sixth_turn_call_5_10();
			f_z.sixth_turn_call_10_20();
			f_z.sixth_turn_call_21_40();
			f_z.sixth_turn_call_41_60();	
			// seventh turn
			f_z.seventh_turn_call_1_5();
			f_z.seventh_turn_call_5_10();
			f_z.seventh_turn_call_10_20();
			f_z.seventh_turn_call_21_40();
			f_z.seventh_turn_call_41_60();

		Test.stopTest();
	}
}