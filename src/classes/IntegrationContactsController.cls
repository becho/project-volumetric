/**
* @author       Maciej Jóźwiak, Wojtek Slodziak
* @description  used for displaying contacts from portal (leads) on the SF home page
*/

public with sharing class IntegrationContactsController{

	private final static String NEXT_CLASS_INACTIVE = 'inactive';
	private final static String PREVIOUS_CLASS_INACTIVE = 'inactive';

    private ApexPages.StandardController sController;  
    public List<Contact> integrationContacts {get; set;}
    public Id integrationUser {get; set;}

    //this is used for choosing groups of five records to show on related list
    public Integer groupNumber = 1;
    public String previousClass { get; set; }
    public String nextClass { get; set; }
    public Integer minIndex { get; set; }
    public Integer maxIndex { get; set; }

    public final Integer groupSize = 5;

    public IntegrationContactsController(ApexPages.StandardController controller){
        sController = controller; 
    }


    public List<Contact> getContacts() {
        integrationUser = [SELECT ID FROM User where Name = 'integration'].Id;
        integrationContacts = [SELECT Id, Name, Source__c,Email,Phone, MobilePhone, CreatedDate FROM Contact where OwnerId =: integrationUser ORDER BY CreatedDate DESC];

        return sublist(this.integrationContacts, groupSize * (groupNumber - 1), groupSize * groupNumber);
    }
    public void nextGroup() {
        if (groupNumber*groupSize < integrationContacts.size()) {
            groupNumber++;
        }
    }
    public void previousGroup() {
        if (groupNumber > 1) {
            groupNumber--;
        }
    }
    public void lastGroup() {
        groupNumber = Integer.valueOf(Decimal.valueOf(Double.valueOf(integrationContacts.size())/groupSize).round(System.RoundingMode.CEILING));
    }
    public void firstGroup() {
        groupNumber = 1;
    }

    private void verifyClassesAndIndexes() {
        if (groupNumber*groupSize >= integrationContacts.size()) {
            nextClass = NEXT_CLASS_INACTIVE;
        } else {
            nextClass = '';
        }
        if (groupNumber <= 1) {

            previousClass = PREVIOUS_CLASS_INACTIVE;
        } else {
            previousClass = '';
        }
        minIndex = integrationContacts.size() > 0? (groupNumber - 1)*groupSize + 1 : 0;
        maxIndex = (groupNumber*groupSize) > integrationContacts.size()? integrationContacts.size() : (groupNumber*groupSize);
    }

    private List<sObject> sublist(List<sObject> recordList, Integer fromIndex, Integer toIndex) {
        List<sObject> resultList = new List<sObject>();
        for (Integer i = fromIndex; i < toIndex; i++) {
            if (i < recordList.size()){
                resultList.add(recordList[i]);
            }
        }
        verifyClassesAndIndexes();
        return resultList;
    }

}