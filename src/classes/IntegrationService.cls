/**
* @author       Maciej Jóźwiak
* @description  used for the buttons on Listing, Resource for publishing/unpublishing offers, listings...
*/

global class IntegrationService {
    
	private final static String RESOURCE_INVESTMENT = 'INVESTMENT';

    // used in Resource custom button - publish Investment on Developer Portal
    webService static String publishInvestmentOnDevloperPortal(String resourceId) {
        DeveloperPortalIntegration devPortal = new DeveloperPortalIntegration();
        string result = devPortal.updateInvestment(resourceId);
        system.debug(result);
        return result;
    }
    
    // used in Resource custom button - delete Investment from Developer Portal
    webService static String removeInvestmentFromDevloperPortal(String resourceId) {
        List<Id> listings = new List<Id>();
        listings.add(resourceId);       
        DeveloperPortalIntegration devPortal = new DeveloperPortalIntegration();
        string result = devPortal.deleteResources(RESOURCE_INVESTMENT, listings);
        system.debug(result);
        return result;
    }
    
    // used in Resource trigger to update prices in Developer Portal
    @future (callout=true)
    public static void updateAppartmentsAtFuture(List<Id> resourcesIds) {
        DeveloperPortalIntegration devPortal = new DeveloperPortalIntegration();
        String result = devPortal.updateResourcesStatuse(resourcesIds, DeveloperPortalIntegration.APPARTMENT);
    }
    
    // used in Resource trigger to delete listings from Developer Portal
    @future (callout=true)
    public static void removeAppartmentsAtFuture(List<Id> resourcesIds) {
        DeveloperPortalIntegration devPortal = new DeveloperPortalIntegration();
        string result = devPortal.deleteResources(DeveloperPortalIntegration.APPARTMENT, resourcesIds);
    }
        
}