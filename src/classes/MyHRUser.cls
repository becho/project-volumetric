public class MyHRUser {

    private final String username;

    public MyHRUser() {
        username = [SELECT username FROM User
                   WHERE username like 'hruser%'][0].username;
    }

    public String getUsername() {
        return username;
    }
}