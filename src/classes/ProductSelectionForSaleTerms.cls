/**
* @AUTHOR: 			Mateusz Pruszyński
* @DESCRIPTION:  	This global class is used to store webservice method for "Create Sale Terms" related list button (Proposal layout, sObject: Sales_Process__c)
**/

global class ProductSelectionForSaleTerms {
	
	// Checks if creation of Sale Terms for the Proposal is allowed
	webservice static String allowSaleTermsForSelectedLines(String spId, String[] productLinesIds) {
		String result = 'false';
		List<Sales_Process__c> customers = [
			SELECT Id, Share_Type__c, Share__c, Main_Customer__c, Account_from_Customer_Group__c, Account_from_Customer_Group__r.SAP_ID__c 
			FROM Sales_Process__c 
			WHERE Proposal_from_Customer_Group__c = :spId 
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)
		];

        if(!customers.isEmpty()) {

            Sales_Process__c offer = [
            	SELECT Id, Status__c, RecordTypeId 
            	FROM Sales_Process__c 
            	WHERE Id = :spId LIMIT 1
            ];

            Boolean allow = CommonUtility.allowNextStepSalesProcess(offer.RecordTypeId, spId, CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT));
            
            if(allow) {

                List<Sales_Process__c> offerLines = [SELECT Id, Queue__c FROM Sales_Process__c WHERE Id in :productLinesIds];
                
                if(!offerLines.isEmpty()) {
                    Integer count = 0;

                    for(Sales_Process__c ol : offerLines) {
                        if(ol.Queue__c == 1) {
                            count++;
                        }
                    }

                    if(count == offerLines.size()) {
                        result = WebserviceUtilities.checkCustomersRelatedList(customers);                     
                    } else {
                        result = WebserviceUtilities.WRONG_QUEUE;
                    }

                }

            } else {
                result = WebserviceUtilities.PRELIMINARY_EXISTS;
            } 

        } else {
            result = WebserviceUtilities.NO_CUSTOMERS;
        }     
        return result;		
	}

	// Method to create new Proposal and change related references from old Proposal to the new one (executed in background, while loading)
	webservice static String createSalesProcessStructure(String[] productLinesIds, String oldProposalId) {
		Id newProposalId = cloneProposal(oldProposalId); // At this stage, new Proposal is inserted to the database
		Savepoint sp = Database.setSavepoint();
		if(String.isNotBlank(newProposalId)) {
			List<Sales_Process__c> relatedListsToUpdate = new List<Sales_Process__c>();
			List<Sales_Process__c> reassignedProductLines = reassignProductLines(productLinesIds, newProposalId); // Product Lines
			if(!reassignedProductLines.isEmpty()) {
				relatedListsToUpdate.addAll(reassignedProductLines);
			}
			List<Sales_Process__c> reassignedCustomers = copyCustomersToNewSP(oldProposalId, newProposalId); // Customers
			if(!reassignedCustomers.isEmpty()) {
				relatedListsToUpdate.addAll(reassignedCustomers);
			}	
			List<Sales_Process__c> reassignedPromotions = reassignPromotions(oldProposalId, newProposalId); // Promotions
			if(!reassignedPromotions.isEmpty()) {
				relatedListsToUpdate.addAll(reassignedPromotions);
			}	
			// Update bulk of Sales Process records
			Boolean salesProcessUpdated = updateSalesProcessBulk(relatedListsToUpdate);
			if(salesProcessUpdated) {
				Boolean eventsANDTasksReassigned = reassignEventsAndTasks(oldProposalId, newProposalId); // Update Events
				if(eventsANDTasksReassigned) {
					Boolean notesAndAttachmentsReassigned = reassignNotesAndAttachments(oldProposalId, newProposalId); // Update Notes & Attachments
					if(notesAndAttachmentsReassigned) {
						return String.valueOf(newProposalId);
					} else {
						Database.rollback(sp); 
						return 'false';
					}
				} else {
					Database.rollback(sp); 
					return 'false';
				}
			} else {
				Database.rollback(sp); 
				return 'false';
			}				
		} else {
			Database.rollback(sp); 
			return 'false';
		}	
	}

	// Flag goes back to false - validation rule skip
	webservice static Boolean reassignLookupToFalse(String newProposalId, String oldProposalId) {
		Set<Id> oldProposals = new Set<Id>();
		oldProposals.add(oldProposalId);
		SaleTermsUpdatePrice.saleTermsUpdatePrices(oldProposals);
		List<Sales_Process__c> relatedLists = [SELECT Id FROM Sales_Process__c WHERE (Proposal_from_Promotions__c = :newProposalId AND (RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION) OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_DEFINED_PROMOTION))) OR (Proposal_from_Promotions__c = :newProposalId AND (RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION) OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_DEFINED_PROMOTION))) OR (RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) AND Product_Line_to_Proposal__c = :newProposalId)];
		Sales_Process__c newOffer = new Sales_Process__c(
			Id = newProposalId,
			Reassign_Lookup__c = false
		);
		for(Sales_Process__c rl : relatedLists) {
			rl.Reassign_Lookup__c = false;
		}
		relatedLists.add(newOffer);
		try {
			update relatedLists;
			return true;
		} catch(Exception e) {
			ErrorLogger.log(e);
			return false;
		}
	}

	// Method to change Product_Line_to_Proposal__c lookup from old to new Proposal
	public static List<Sales_Process__c> reassignProductLines(String[] productLinesIds, String newProposalId) {
		List<Sales_Process__c> productLines = new List<Sales_Process__c>();
		Sales_Process__c productLine;
		for(String productLineId : productLinesIds) {
			productLine = new Sales_Process__c(
				Id = productLineId,
				Product_Line_to_Proposal__c = newProposalId,
				Reassign_Lookup__c = true
			);
			productLines.add(productLine);
		}
		return productLines;
	}

	// Method to clone Proposal record
	public static Id cloneProposal(String oldProposalId) {
		Sales_Process__c oldProposal = [
			SELECT Id, Name, Status__c, Contact__c, Marketing_Campaign__c, Financing_Offer__c, RecordTypeId, Financial_Verification__c, Reservation_Status__c, Reservation_Date__c, 
				   Reservation_Expiration_Date__c, Comments__c, Rejection_Reason__c, Date_of_Flat_transmission__c, Analyst__c, Approval_Status__c, Approve_Reason__c, 
				   Buying_For_Personal_Assets__c, Comment__c, Comments_short__c, Credit_Request_Status__c, Balance__c, Customer_Transaction__c, Customer_Offer__c, 
				   Date_of_signing__c, Discount__c, Discount_Amount_Offer__c, Expected_date_of_signing__c, Expiration_Date__c, Investment_for_Report__c, Investment_for_Event__c, InvestmentId__c, Investor__c,
				   Resource_Type__c, Resource_Type_for_Event__c, Net_Agreement_Price__c, Net_Price_With_Discount__c
			FROM Sales_Process__c WHERE Id = :oldProposalId LIMIT 1
		];
		Sales_Process__c newProposal = oldProposal.clone(false, true, true, false);
		newProposal.Reassign_Lookup__c = true;
		try {
			insert newProposal;
			return newProposal.Id;
		} catch(Exception e) {
			ErrorLogger.log(e);
			return null;
		}
	}

	// Reassign Customers
	public static List<Sales_Process__c> copyCustomersToNewSP(String oldProposalId, String newProposalId) {

		List<Sales_Process__c> customers = [
			SELECT Id, Proposal_from_Customer_Group__c, RecordTypeId, Account_from_Customer_Group__c, Main_Customer__c, 
			       Marital_Status__c, Self_Employment__c, Share_Type__c, Share__c, Buying_For_Personal_Assets__c  
			FROM Sales_Process__c 
			WHERE Proposal_from_Customer_Group__c = :oldProposalId 
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)
		];

		List<Sales_Process__c> customersToInsert = new List<Sales_Process__c>();

		for(Sales_Process__c customer : customers) {	
			Sales_Process__c newCustomer = customer.clone(false, true, true, false);
			newCustomer.Proposal_from_Customer_Group__c = newProposalId;
			newCustomer.Reassign_Lookup__c = true;
			customersToInsert.add(newCustomer);
		}

		return customersToInsert;
	}

	// Reassign Promotions (either Defined or Customer Promotions)
	public static List<Sales_Process__c> reassignPromotions(String oldProposalId, String newProposalId) {

		List<Sales_Process__c> promotions = [
			SELECT Id 
			FROM Sales_Process__c 
			WHERE Proposal_from_Promotions__c = :oldProposalId 
			AND (
				RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_PROMOTION) 
				OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_DEFINED_PROMOTION)
			)
		];

		for(Sales_Process__c promotion : promotions) {
			promotion.Proposal_from_Promotions__c = newProposalId;
			promotion.Reassign_Lookup__c = true;
		}

		return promotions;
	}

	// Update bulk of Sales Processes (related lists)
	public static Boolean updateSalesProcessBulk(List<Sales_Process__c> relatedListsToUpdate) {
		try {
			upsert relatedListsToUpdate;
			return true;
		} catch(Exception e) {
			ErrorLogger.log(e);
			return false;
		}
	}

	// Reassign and update Events and Tasks
	public static Boolean reassignEventsAndTasks(String oldProposalId, String newProposalId) {

		List<Event> events = [
			SELECT Id, IsAllDayEvent, ActivityDate, Description, DurationInMinutes, EndDateTime, Location, 
				   IsPrivate, ShowAs, StartDateTime, Subject, ActivityDateTime, Type, WhatId, WhoId 
			FROM Event 
			WHERE WhatId = :oldProposalId
		];

		List<Task> tasks = [
			SELECT Id, ActivityDate, Description, Subject, Type, CallDurationInSeconds, 
				   CallDisposition, CallType, RecordTypeId, Priority, WhatId, WhoId 
			FROM Task WHERE WhatId = :oldProposalId
		];

		List<Event> eventsToInsert = new List<Event>();
		List<Task> tasksToInsert = new List<Task>();

		for(Event event : events) {
			Event eventClone = event.clone(false, true, true, false);
			eventClone.WhatId = newProposalId;
			eventsToInsert.add(eventClone);
		}

		for(Task task : tasks) {
			Task taskClone = task.clone(false, true, true, false);
			taskClone.WhatId = newProposalId;
			tasksToInsert.add(taskClone);
		}

		try {
			if(!eventsToInsert.isEmpty()) {	
				insert eventsToInsert;
			}
			if(!tasksToInsert.isEmpty()) {
				insert tasksToInsert;
			}
			return true;
		} catch(Exception e) {   
			ErrorLogger.log(e);
			return false;
		}
	}

	// Reassign Notes & Attachments
	public static Boolean reassignNotesAndAttachments(String oldProposalId, String newProposalId) {

		List<Attachment> attachments = [
			SELECT Id, Body, ContentType, Description, OwnerId, IsPrivate, Name 
			FROM Attachment WHERE ParentId = :oldProposalId
		];
		List<Attachment> attachments2insert = new List<Attachment>();

		for(Attachment attachment : attachments) {

			Attachment newAtt = attachment.clone(false, true, true, false);
			newAtt.ParentId = newProposalId;
			attachments2insert.add(newAtt);
			
		}

		List<Note> notes = [
			SELECT Id, Body, OwnerId, Title FROM Note WHERE ParentId = :oldProposalId
		];
		List<Note> notes2insert = new List<Note>();

		for(Note note : notes) {

			Note newNote = note.clone(false, true, true, false);
			newNote.ParentId = newProposalId;
			notes2insert.add(newNote);

		}

		try {
			if(!attachments2insert.isEmpty()) {	
				insert attachments2insert;
			}
			if(!notes2insert.isEmpty()) {
				Update notes2insert;
			}
			return true;
		} catch(Exception e) {   
			ErrorLogger.log(e);
			return false;
		}
	}

}