/**
* @author 		Mateusz Pruszynski
* @description 	Class with methods to manage acceptance process of measured resource area 
**/

global without sharing class MeasurementsAcceptanceProcess {
	
	public static final String MSG_ERROR = 'error';
	public static final String MSG_SUCCESS = 'success';
	public static final String MSG_PARTIAL = 'partial';

	// -------------------------------- CALLED FROM TRIGGER -----------------------

	// Rejects selected record's measurements
	Webservice static String[] rejectMeasurements(String[] resourceIds, Boolean userCanReject) {

		// only users with After_Sales_Service_Team__c checked can execute the acceptance process
		if(userCanReject) {

			// get resources from database
			List<Resource__c> resources2reject = getResources(resourceIds);

			// sort list of resources - we can proceed only with records with Acceptance_status__c = "Directed For Approval"
			// rest of them are excluded and user is informed
			List<Resource__c> resources2rejectFiltered = new List<Resource__c>(); // list of right resources
			String[] res2return = new String[] {}; // list with info about rejected resources

			for(Resource__c r2r : resources2reject) {

				if(r2r.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_DIRECTED_FOR_APPROVAL) {
					resources2rejectFiltered.add(r2r);
				} else {
					res2return.add(r2r.Id);
					res2return.add(r2r.Name + ' | ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_RESOURCE).getDescribe().fields.getMap().get('Acceptance_Status__c').getDescribe().getLabel() + ': ' + r2r.Acceptance_Status__c);
				}

			}

			if(resources2rejectFiltered.isEmpty()) { // no records meet criteria
				return new String[] {MSG_ERROR, Label.noRecordsMeetCriteria};
			} else if(!resources2rejectFiltered.isEmpty() && !res2return.isEmpty()) { // partial success

				String reject = acceptOdRejectResources(resources2rejectFiltered, false);

				if(reject.equals(MSG_SUCCESS)) {
					String[] result = new String[] {MSG_PARTIAL};
					result.addAll(res2return);
					return result;
				} else {
					return new String[] {MSG_ERROR, reject};
				}

			} else { // all records meet criteria

				String reject = acceptOdRejectResources(resources2rejectFiltered, false);

				if(reject.equals(MSG_SUCCESS)) {
					return new String[] {MSG_SUCCESS};
				} else {
					return new String[] {MSG_ERROR, reject};
				}
			}

		} else {
			return new String[] {MSG_ERROR, Label.rejectionOnlyByAfterSalesTeam};
		}

	}

	// Accepts selected record's measurements
	Webservice static String[] acceptMeasurements(String[] resourceIds, String userCanAccept) {
		
		// only users with After_Sales_Service_Team__c checked can execute the acceptance process
		if(Boolean.valueOf(userCanAccept)) {

			// get resources from database
			List<Resource__c> resources2accept = getResources(resourceIds);

			// sort list of resources - we can proceed only with records with Acceptance_status__c = "Directed For Approval"
			// rest of them are excluded and user is informed
			List<Resource__c> resources2acceptFiltered = new List<Resource__c>(); // list of right resources
			String[] res2return = new String[] {}; // list with info about rejected resources

			for(Resource__c r2a : resources2accept) {

				if(r2a.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_DIRECTED_FOR_APPROVAL) {
					resources2acceptFiltered.add(r2a);
				} else {
					res2return.add(r2a.Id);
					res2return.add(r2a.Name + ' | ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_RESOURCE).getDescribe().fields.getMap().get('Acceptance_Status__c').getDescribe().getLabel() + ': ' + r2a.Acceptance_Status__c);
				}

			}

			if(resources2acceptFiltered.isEmpty()) { // no records meet criteria
				return new String[] {MSG_ERROR, Label.noRecordsMeetCriteria};
			} else if(!resources2acceptFiltered.isEmpty() && !res2return.isEmpty()) { // partial success

				String accept = acceptOdRejectResources(resources2acceptFiltered, true);

				if(accept.equals(MSG_SUCCESS)) {
					String[] result = new String[] {MSG_PARTIAL};
					result.addAll(res2return);
					return result;
				} else {
					return new String[] {MSG_ERROR, accept};
				}

			} else { // all records meet criteria

				String accept = acceptOdRejectResources(resources2acceptFiltered, true);

				if(accept.equals(MSG_SUCCESS)) {
					return new String[] {MSG_SUCCESS};
				} else {
					return new String[] {MSG_ERROR, accept};
				}
			}

		} else {
			return new String[] {MSG_ERROR, Label.acceptanceOnlyByAfterSalesTeam};
		}
	}

	// accepts records given in the parameter
	public static String acceptOdRejectResources(List<Resource__c> resources2acceptFiltered, Boolean accept) {
		if(accept) {
			for(Resource__c res : resources2acceptFiltered) {
				res.Acceptance_Status__c = CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED;
			}
		} else {
			for(Resource__c res : resources2acceptFiltered) {
				res.Acceptance_Status__c = CommonUtility.RESOURCE_ACCEPTANCE_STATUS_DISAPPROVED;
			}
		}

		try {
			update resources2acceptFiltered;
			return MSG_SUCCESS;
		} catch(Exception e) {
			ErrorLogger.log(e);
			return String.valueOf(e.getMessage());
		}
	}

	// query resources from database
	public static List<Resource__c> getResources(String[] resourceIds) {
		return [
			SELECT Id, Name, Acceptance_Status__c
			FROM Resource__c
			WHERE Id in :resourceIds
			ORDER BY Acceptance_Status__c asc
		];
	}	

	// -------------------------------- CALLED FROM BATCH -----------------------

	public static void manageSalesProcessAndPaymentSchedule(List<Sales_Process__c> productLines) {

		List<Sales_Process__c> salesProcess2update = new List<Sales_Process__c>();
		List<Payment__c> payments2upsert = new List<Payment__c>();
		Map<Id, Resource__c> resources2update = new Map<Id, Resource__c>();

		List<ProductLineWrapper> productLinesWrappers = new List<ProductLineWrapper>(); // wrapper structure for all data to be processed
		Set<Id> saleTermsIds = new Set<Id>();

		for(Sales_Process__c productLine : productLines) {

			productLinesWrappers.add(
				new ProductLineWrapper(productLine) // add product lines to wrapper
			);

			if(productLine.Offer_Line_to_Sale_Term__c != null) {
				saleTermsIds.add(productLine.Offer_Line_to_Sale_Term__c);
			}

		}
			
		List<Payment__c> paymentScheduleInstallments = getInstallments(saleTermsIds);

		Map<Id, Sales_Process__c> afterSales2update = new Map<Id, Sales_Process__c>();

		for(ProductLineWrapper pw : productLinesWrappers) {
	
			// add installments to wrapper
			for(Payment__c installment : paymentScheduleInstallments) {
				if(pw.productLine.Offer_Line_to_Sale_Term__c != null 
					&& pw.productLine.Offer_Line_to_Sale_Term__c == installment.Agreements_Installment__c 
					&& pw.productLine.Offer_Line_Resource__c == installment.Payment_For__c) {
					pw.paymentScheduleInstallments.add(installment);
				}
			}

			// execute grouping methods inside wrapper
			pw.groupInstallments();
	
			salesProcess2update.add(pw.productLine);

			// update prices on resource
			if(!resources2update.containsKey(pw.productLine.Offer_Line_Resource__c)) {
				if(pw.productLine.Offer_Line_to_Sale_Term__c != null && pw.productLine.Offer_Line_to_Sale_Term__r.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED) {
					resources2update.put(
						pw.productLine.Offer_Line_Resource__c,
						new Resource__c(
							Id = pw.productLine.Offer_Line_Resource__c,
							Final_Price__c = pw.productLine.Price_With_Discount__c
						)
					);
				} else {
					resources2update.put(
						pw.productLine.Offer_Line_Resource__c,
						new Resource__c(
							Id = pw.productLine.Offer_Line_Resource__c,
							Final_Price__c = pw.productLine.Offer_Line_Resource__r.Price_After_Measurement__c
						)
					);					
				}
			} else {
				if(pw.productLine.Offer_Line_to_Sale_Term__c != null && pw.productLine.Offer_Line_to_Sale_Term__r.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED) {
					resources2update.put(
						pw.productLine.Offer_Line_Resource__c,
						new Resource__c(
							Id = pw.productLine.Offer_Line_Resource__c,
							Final_Price__c = pw.productLine.Price_With_Discount__c
						)
					);
				}				
			}

			// update after-sales service
			if(pw.afterSalesService != null) {
				
				if(!afterSales2update.containsKey(pw.afterSalesService.Id)) {						

					afterSales2update.put(pw.afterSalesService.Id, pw.afterSalesService);
				
				} else {
					
					Sales_Process__c afterSales = afterSales2update.get(pw.afterSalesService.Id);
					
					if(!afterSales.Measurement_Threshold_Exceeded__c && pw.afterSalesService.Measurement_Threshold_Exceeded__c) {
						afterSales.Measurement_Threshold_Exceeded__c = true;
					}				

				}

			}

			if(!pw.payments2upsert.isEmpty()) {
				payments2upsert.addAll(pw.payments2upsert);

			}

		}

		// update after sales service
		if(afterSales2update.size() > 0) {

            for(Id key : afterSales2update.keySet()) {

                salesProcess2update.add(afterSales2update.get(key));

            }			
		}

		Savepoint sp = Database.setSavepoint();
		try {
			upsert payments2upsert;
			update salesProcess2update;
			update resources2update.values();
		} catch(Exception e) {
			Database.rollback(sp);
			ErrorLogger.log(e);
		}

	}

	/* QUERY */

	public static List<Payment__c> getInstallments(Set<Id> saleTermsIds) {
		return [
			SELECT Id, Due_Date__c, Agreements_Installment__c, Paid_Value__c, Amount_to_Pay__c, Payment_For__c, Paid_Formula__c, After_Sales_Service__c
			FROM Payment__c 
			WHERE Agreements_Installment__c in :saleTermsIds
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)
			ORDER BY Due_Date__c DESC
		];
	}

	
	/* WRAPPERS */

	private class ProductLineWrapper {

		public Sales_Process__c productLine {get; set;}
		public List<Payment__c> paymentScheduleInstallments {get; set;}

		public Payment__c returnPayment {get; set;}

		public List<Payment__c> payments2upsert {get; set;}

		public Sales_Process__c afterSalesService {get; set;}

		public ProductLineWrapper(Sales_Process__c productLine) {

			this.productLine = productLine;
			paymentScheduleInstallments = new List<Payment__c>();
			payments2upsert = new List<Payment__c>();

		}

		public void groupInstallments() {

			Decimal oldPrice = productLine.Offer_Line_Resource__r.Price__c - productLine.Discount_Amount_Offer__c;
			Decimal newPrice = productLine.Offer_Line_Resource__r.Price_After_Measurement__c - (productLine.Discount_Amount_Offer__c != null ? productLine.Discount_Amount_Offer__c : 0);

			// update schedule with new values
			if(!paymentScheduleInstallments.isEmpty()) {
				updateSchedule(oldPrice, newPrice);
			}

			// count prices after measurements for product line
			countProductLinePrices(newPrice);
			Decimal changeValuePercent = Math.abs(productLine.Offer_Line_Resource__r.The_Difference_After_Measurement__c * ( 100 /  productLine.Offer_Line_Resource__r.Usable_Area_Planned__c));
			// checbox on After-sales-service
			if(productLine.Offer_Line_to_After_sales_Service__c != null) {
				
				afterSalesService = new Sales_Process__c(
					Id = productLine.Offer_Line_to_After_sales_Service__c,
					Price_Changed_After_Measurements__c = true
				);

				MeasurementAcceptanceConfig__c conf = MeasurementAcceptanceConfig__c.getInstance('currentConfig');

                if(conf.Threshold__c < changeValuePercent) {
                	afterSalesService.Measurement_Threshold_Exceeded__c = true;
                }
			}

		}

		public void updateSchedule(Decimal oldPrice, Decimal newPrice) {

			Decimal priceDifference = oldPrice - newPrice;

			Integer i = 0;

			do {

				if(i < paymentScheduleInstallments.size()) {

					// null pointer protecion
					if(paymentScheduleInstallments[i].Paid_Value__c == null) {
						paymentScheduleInstallments[i].Paid_Value__c = 0;
					}

					// update installment
					if(!paymentScheduleInstallments[i].Paid_Formula__c) { // we try to find installments that are not paid in whole yet

						if(priceDifference < (paymentScheduleInstallments[i].Amount_to_Pay__c - paymentScheduleInstallments[i].Paid_Value__c)) { /* price difference is less than amount 
						left to be paid -> amount to pay gets lowered by the difference */

							paymentScheduleInstallments[i].Amount_to_Pay__c = paymentScheduleInstallments[i].Amount_to_Pay__c - priceDifference;
							paymentScheduleInstallments[i].Amount_Change_After_Measurements__c = true;
							paymentScheduleInstallments[i].Amount_Difference_After_Measurements__c = priceDifference;
							payments2upsert.add(paymentScheduleInstallments[i]);

							priceDifference = 0;
							
							break;

						} else { // price difference exceeds amount left

							paymentScheduleInstallments[i].Amount_Difference_After_Measurements__c = 
								paymentScheduleInstallments[i].Amount_to_Pay__c - paymentScheduleInstallments[i].Paid_Value__c;

							paymentScheduleInstallments[i].Amount_to_Pay__c = paymentScheduleInstallments[i].Paid_Value__c;
							paymentScheduleInstallments[i].Amount_Change_After_Measurements__c = true;
							payments2upsert.add(paymentScheduleInstallments[i]);

							priceDifference = priceDifference - paymentScheduleInstallments[i].Amount_Difference_After_Measurements__c;

						}

					}

				}

				i++;

			} while(i < paymentScheduleInstallments.size());

			// check if priceDifference is greater than 0 to create return record
			if(priceDifference > 0) {
				payments2upsert.add(
					new Payment__c(
						RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_REIMBURSEMENT),
						Amount_to_Pay__c = priceDifference,
						Due_Date__c = Date.today() + 30,
						After_Sales_Service_reimbusement__c = paymentScheduleInstallments[0].After_Sales_Service__c,
						Reimbursement_Status__c = 'Waiting for acceptance'
					)
				);
			}

		}

		//edit: Mateusz Wolak-Ksiazek
		//added: updating net and vat value after measurments
		public void countProductLinePrices(Decimal newPrice) {

			productLine.Price_With_Discount__c = newPrice; 
			productLine.Discount__c = productLine.Discount_Amount_Offer__c * (100 / productLine.Price_With_Discount__c);
			productLine.Price_Changed_After_Measurements__c = true;
		}

	}

	// called from th_SalesProcessTrigger
	// updates balance and agreement value on after sales service
	// edit: 
	public static void updateAfterSalesService(Map<Id, Sales_Process__c> afterSalesServices2updateAgreementValue) {
        List<Sales_Process__c> ass2update = new List<Sales_Process__c>();
        // select prodlines prices
        List<Sales_Process__c> prodLines = [
            SELECT Id, Price_With_Discount__c, Offer_Line_to_After_sales_Service__c, Net_Price_With_Discount__c, VAT_Amount__c FROM Sales_Process__c 
            WHERE Offer_Line_to_After_sales_Service__c in :afterSalesServices2updateAgreementValue.keySet()
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Sales_Process__c', CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
            ORDER BY Offer_Line_to_After_sales_Service__c ASC
        ];

        // select installments
        List<Payment__c> installments = [
            SELECT Id, Amount_Difference_After_Measurements__c, After_sales_Service__c FROM Payment__c
            WHERE After_sales_Service__c in :afterSalesServices2updateAgreementValue.keySet()
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_TYPE_PAYMENT)
            AND Amount_Change_After_Measurements__c = true
            ORDER BY After_sales_Service__c ASC
        ];

        for(Id key : afterSalesServices2updateAgreementValue.keySet()) {
           
            Sales_Process__c ass = new Sales_Process__c(
                Id = afterSalesServices2updateAgreementValue.get(key).Id
            );
           
            ass.Agreement_Value__c = 0;
            ass.Net_Agreement_Price__c = 0;
			ass.VAT_Amount__c = 0;

            for(Sales_Process__c pl : prodlines) {
                if(pl.Offer_Line_to_After_sales_Service__c == ass.Id) {
                    ass.Agreement_Value__c += pl.Price_With_Discount__c;
                    ass.Net_Agreement_Price__c += pl.Net_Price_With_Discount__c;
                    ass.VAT_Amount__c += pl.VAT_Amount__c;
                }
            }

            for(Payment__c inst : installments) {
                if(inst.After_sales_Service__c == ass.Id) {
                    if(ass.Balance__c == null) {    
                        ass.Balance__c = (-1) * ass.Agreement_Value__c;
                    } else {                                                        
                        ass.Balance__c += inst.Amount_Difference_After_Measurements__c;
                    }
                }
            }
            
            ass2update.add(ass);
        }

        try {
            update ass2update;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
	}

}