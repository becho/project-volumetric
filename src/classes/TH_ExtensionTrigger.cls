public without sharing class TH_ExtensionTrigger extends TriggerHandler.DelegateBase {

	public class MyException extends Exception{}  

	public Set<Id> changeIdsForQuotationSumUp; //Marta Malinowska - add roll up for Change Elements

	public Map<Extension__c, Extension__c> cannotEditLookups;
	public Map<Id, List<Extension__c>> ownerOfDefectMap;

    public Map<String, List<Extension__c>> changesWithResourceIdMap2updateExtraFields;
    public Map<String, List<Extension__c>> handoverDefectsWithResourceIdMap2updateExtraFields;
    public Map<String, List<Extension__c>> defectsWithResourceIdMap2updateExtraFields;
    public Set<Id> changeToUpdateValueOnAfterSales;

    //xxx - semeko
    public Set<Id> changedFieldOnChanges2updateDate;

	public override void prepareBefore() {
    	cannotEditLookups = new Map<Extension__c, Extension__c>();
        changesWithResourceIdMap2updateExtraFields = new Map<String, List<Extension__c>>();
        handoverDefectsWithResourceIdMap2updateExtraFields = new Map<String, List<Extension__c>>();
        defectsWithResourceIdMap2updateExtraFields = new Map<String, List<Extension__c>>();
	}

	public override void prepareAfter() {
    	//ownerOfDefectMap = new Map<Id, List<Extension__c>>();
    	changeIdsForQuotationSumUp = new Set<Id>(); //Marta Malinowska - add roll up for Change Elements
        changedFieldOnChanges2updateDate = new Set<Id>();
        changeToUpdateValueOnAfterSales = new Set<Id>();
	}


    public override void beforeInsert(List<sObject> o){
        List<Extension__c> newExtensions = (List<Extension__c>)o; 
        for(Extension__c newExt : newExtensions) {     
            /* Mateusz Pruszyński */
            // Trigger part that fills Construction Manager's email, investment name on changes records
            if(newExt.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE)
                && newExt.Resource__c != null) {
                if(!changesWithResourceIdMap2updateExtraFields.containsKey(String.valueOf(newExt.Resource__c).left(15))) {
                    changesWithResourceIdMap2updateExtraFields.put(
                        String.valueOf(newExt.Resource__c).left(15), 
                        new Extension__c[] {newExt}
                    );
                } else {
                    changesWithResourceIdMap2updateExtraFields.get(String.valueOf(newExt.Resource__c).left(15)).add(newExt);
                }
            } 

            /* Mateusz Pruszyński */
            // Trigger part that fills Construction Manager's email, investment name on handover failure records             
            if(newExt.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENTION_TYPE_HANDOVER_DEFECT)
                && newExt.Resource_Defect__c != null) {
                if(!handoverDefectsWithResourceIdMap2updateExtraFields.containsKey(String.valueOf(newExt.Resource_Defect__c).left(15))) {
                    handoverDefectsWithResourceIdMap2updateExtraFields.put(
                        String.valueOf(newExt.Resource_Defect__c).left(15), 
                        new Extension__c[] {newExt}
                    );
                } else {
                    handoverDefectsWithResourceIdMap2updateExtraFields.get(String.valueOf(newExt.Resource__c).left(15)).add(newExt);
                }
            }

            /* Mateusz Pruszyński */
            // Trigger part that fills Construction Manager's email, investment name on defect records             
            if(newExt.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_DEFECT)
                && newExt.Resource_Defect__c != null) {
                if(!defectsWithResourceIdMap2updateExtraFields.containsKey(String.valueOf(newExt.Resource_Defect__c).left(15))) {
                    defectsWithResourceIdMap2updateExtraFields.put(
                        String.valueOf(newExt.Resource_Defect__c).left(15), 
                        new Extension__c[] {newExt}
                    );
                } else {
                    defectsWithResourceIdMap2updateExtraFields.get(String.valueOf(newExt.Resource__c).left(15)).add(newExt);
                }
            }         

        }
    }

	public override void afterInsert(Map<Id, sObject> o){
		Map<Id, Extension__c> extNs = (Map<Id, Extension__c>)o;

		for(Id key : extNs.keySet()) {
			Extension__c extN = extNs.get(key);
            //Marta Malinowska - add roll up for Change Elements
            if(extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE_ELEMENT)){
            	if(String.isNotBlank(extN.Change__c)){
            		changeIdsForQuotationSumUp.add(extN.Change__c);
            	}
            }

            /* Mateusz Wolak-Ksiazek */
            //update vat value on after sales (add/subtract quotation from changes)
            if(extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE) 
                && extN.Change_Agreement_Type__c == 'Annex'
                && (extN.Status__c == CommonUtility.EXTENSION_STATUS_ACCEPTED
                    || extN.Status__c == CommonUtility.EXTENSION_STATUS_COMPLETED
                    || extN.Status__c == CommonUtility.EXTENSION_STATUS_IN_PROGRESS)
                && extN.Quotation__c != null
            ) {
                changeToUpdateValueOnAfterSales.add(extN.After_sales_Service__c);
            }
	    }
	}

	public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
		Map<Id, Extension__c> resOld = (Map<Id, Extension__c>)old;
        Map<Id, Extension__c> resNew = (Map<Id, Extension__c>)o;

    	for(Id key : resNew.keySet()) {
    		Extension__c extN = resNew.get(key);
    		Extension__c extO = resOld.get(key);

    		for(Id keyOld : resOld.keySet()) {
		        /* Mateusz Pruszyński */
		        // Trigger part that does not allow to edit particular lookups (see CannotEditLookupsSalesProcess.cls description)
	            if(
	            	(
	            		extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE) 
		            	&& extO.After_sales_Service__c != null 
		            	&& extO.After_sales_Service__c != extN.After_sales_Service__c
	            	) || (
	            		extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_DEFECT) 
	            		&& extO.Handover_Protocol__c != null 
	            		&& extO.Handover_Protocol__c != extN.Handover_Protocol__c
	            	)
	            ) {
	            	cannotEditLookups.put(extO, extN);
	            }

		    }
	    } 
	}

	public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
		Map<Id, Extension__c> extNewMap = (Map<Id, Extension__c>)old;
        Map<Id, Extension__c> extOldMap = (Map<Id, Extension__c>)o;

        for(Id key : extNewMap.keySet()) { 
            Extension__c extNew = extNewMap.get(key);
            Extension__c extOld = extOldMap.get(key);

            //Marta Malinowska - add roll up for Change Elements
            if(extNew.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE_ELEMENT)){
            	if(String.isNotBlank(extNew.Change__c)){
            		changeIdsForQuotationSumUp.add(extNew.Change__c);
            	}
            }

            /* Mateusz Wolak-Ksiazek */
            if(extNew.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE) 
                && extNew.Change_taken__c &&  extNew.Change_taken__c == extOld.Change_taken__c
                && extNew.Last_date_of_changed_change__c == extOld.Last_date_of_changed_change__c
                && (
                    extNew.Comment__c != extOld.Comment__c
                    || extNew.Description__c != extOld.Description__c
                    )
            ) {
                if(!changedFieldOnChanges2updateDate.contains(extNew.Id)) {
                    changedFieldOnChanges2updateDate.add(extNew.Id);
                }
            }

            /* Mateusz Wolak-Ksiazek */
            //update vat value on after sales (add/subtract quotation from changes)
            if(extNew.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE)
                && extNew.Change_Agreement_Type__c == 'Annex'
                && extNew.Quotation__c != null
                && extNew.After_sales_Service__c != null
                && 
                (
                    (extNew.Quotation__c != extOld.Quotation__c
                        && (extNew.Status__c == CommonUtility.EXTENSION_STATUS_ACCEPTED
                            || extNew.Status__c == CommonUtility.EXTENSION_STATUS_COMPLETED
                            || extNew.Status__c == CommonUtility.EXTENSION_STATUS_IN_PROGRESS)
                    )
                    || 
                    (extNew.Status__c != extOld.Status__c
                    )
                )
            ) {
               changeToUpdateValueOnAfterSales.add(extNew.After_sales_Service__c);
            }

        }
	}

	public override void afterDelete(Map<Id, sObject> old) {
		Map<Id, Extension__c> extOldMap = (Map<Id, Extension__c>)old;
        
        for(Id key : extOldMap.keySet()) {
        	Extension__c extOld = extOldMap.get(key);

        	//Marta Malinowska - add roll up for Change Elements
            if(extOld.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE_ELEMENT)){
            	if(String.isNotBlank(extOld.Change__c)){
            		changeIdsForQuotationSumUp.add(extOld.Change__c);
            	}
            }

            //Mateusz Wolak-Ksiazek update agreement price, vat amount, net agreement price on after sales after change deletion
            if(extOld.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE)
                && extOld.Quotation__c != null 
                && (
                    extOld.Status__c == CommonUtility.EXTENSION_STATUS_ACCEPTED
                    || extOld.Status__c == CommonUtility.EXTENSION_STATUS_COMPLETED
                    || extOld.Status__c == CommonUtility.EXTENSION_STATUS_IN_PROGRESS
                )
            ) {
                changeToUpdateValueOnAfterSales.add(extOld.After_sales_Service__c);
            }
        }
	}

	public override void finish() {
        /* Mateusz Pruszyński */
        // Trigger part that does not allow to edit particular lookups (see CannotEditLookupsSalesProcess.cls description)
        if(cannotEditLookups != null && cannotEditLookups.size() > 0) {
            CannotEditLookupsSalesProcess.LookupsExtension(cannotEditLookups);
        }

        /* Mateusz Pruszyński */
        // Trigger part that fills Construction Manager's email, investment name on changes records
        if(changesWithResourceIdMap2updateExtraFields != null && changesWithResourceIdMap2updateExtraFields.size() > 0) {
            ChangesAndDefectsExtraFieldsManager.updateFieldsForChangesOrDefects(changesWithResourceIdMap2updateExtraFields);
        }

        /* Mateusz Pruszyński */
        // Trigger part that fills Construction Manager's email, investment name on handover failure records
        if(handoverDefectsWithResourceIdMap2updateExtraFields != null && handoverDefectsWithResourceIdMap2updateExtraFields.size() > 0) {
            ChangesAndDefectsExtraFieldsManager.updateFieldsForChangesOrDefects(handoverDefectsWithResourceIdMap2updateExtraFields);
        }

        /* Mateusz Pruszyński */
        // Trigger part that fills Construction Manager's email, investment name on defect records
        if(defectsWithResourceIdMap2updateExtraFields != null && defectsWithResourceIdMap2updateExtraFields.size() > 0) {
            ChangesAndDefectsExtraFieldsManager.updateFieldsForChangesOrDefects(defectsWithResourceIdMap2updateExtraFields);
        }        
        
		//Marta Malinowska - add roll up for Change Elements
		if(changeIdsForQuotationSumUp != null && changeIdsForQuotationSumUp.size() > 0) {
			ExtensionManager.updateQuotationOnChanges(changeIdsForQuotationSumUp);
		}

        /* Mateusz Wolak-Ksiazek */
        if(changedFieldOnChanges2updateDate != null && changedFieldOnChanges2updateDate.size() > 0) {
            ExtensionManager.updateDateOfChangedChange(changedFieldOnChanges2updateDate);
        }

        /* Mateusz Wolak-Ksiazek */
        //update agreement value, vat value and net agreement price on after sales (add/subtract quotation from changes)
        if(changeToUpdateValueOnAfterSales != null && changeToUpdateValueOnAfterSales.size() > 0) {
            ExtensionManager.updateValueOnAfterSales(changeToUpdateValueOnAfterSales);
        }

	}
}