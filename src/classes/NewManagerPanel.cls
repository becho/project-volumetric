/**
* @author       Mateusz Pruszyński
* @description  Override for Manager Panel object "new" action
**/

public with sharing class NewManagerPanel {
    
    public Manager_Panel__c currentRecord {get; set;}

    public NewManagerPanel(ApexPages.StandardController stdController) {
        currentRecord = (Manager_Panel__c)stdController.getRecord();
    }
 
    // Redirect:
    // - rt Sales Target -> goes to custom page NewSalesTarget
    // - rt Promotion -> goes to standard edit mode page
    public PageReference redirect() {
        PageReference p;
        Set<String> allowedRTs = new Set<String>{CommonUtility.getRecordTypeId('Manager_Panel__c', CommonUtility.MANAGER_PANEL_TYPE_PROMOTION), CommonUtility.getRecordTypeId('Manager_Panel__c', CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS), CommonUtility.getRecordTypeId('Manager_Panel__c', CommonUtility.MANAGER_PANEL_TYPE_REWARD_THRESHOLD), CommonUtility.getRecordTypeId('Manager_Panel__c', CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD)};
        if(allowedRTs.contains(currentRecord.RecordTypeId)) {
            String prefix = CommonUtility.getManagerPanelPrefix();
            p = new PageReference('/' + prefix + '/e?RecordType=' + currentRecord.RecordTypeId + '&nooverride=1&retURL=/' + prefix + '/o');
        } else {
            p = Page.NewSalesTarget;
        }
        return p;
    }
}