/**
* @author 		Mateusz Pruszyńsi
* @description 	Test class for ChangeConfiguratorController
**/

@isTest
private class TestClassChangeConfiguratorController {
	
	@isTest static void createChangeElements() {
		Integer nrOfItems = 100;

		Extension__c change = TestHelper.createExtensionChange(
            new Extension__c(
            ), 
            true
        );
		List<Resource__c> priceListItems = TestHelper.createResourcePriceListItems(
            new Resource__c(
            ), 
            nrOfItems,
            true
        );

        PageReference changeConfigurator = Page.ChangeConfigurator;
        changeConfigurator.getParameters().put('id', change.Id);
        changeConfigurator.getParameters().put('retURL', '/' + change.id);
        Test.setCurrentPage(changeConfigurator);

        ApexPages.StandardController stdController;
        ChangeConfiguratorController controller;

        Test.startTest();

		stdController = new ApexPages.StandardController(change);
		controller = new ChangeConfiguratorController(stdController);

		Test.stopTest();

		System.assertEquals(controller.changeData.size(), nrOfItems+1);
	}

	
	@isTest static void updateChangeElements() {
		Integer nrOfItems = 1;

		Extension__c change = TestHelper.createExtensionChange(
            new Extension__c(
            ), 
            true
        );
		List<Resource__c> priceListItems = TestHelper.createResourcePriceListItems(
            new Resource__c(
            ), 
            nrOfItems,
            true
        );
        List<Extension__c> changeElements = new List<Extension__c>();
        for(Resource__c priceListItem : priceListItems){
        	Extension__c changeElementTemplate = new Extension__c(
				Change__c = change.Id,
            	Price_list_item__c = priceListItem.Id
			);
        	changeElements.add(TestHelper.createExtensionChangeElement(changeElementTemplate, false));
        }
        insert changeElements;

        PageReference changeConfigurator = Page.ChangeConfigurator;
        changeConfigurator.getParameters().put('id', change.Id);
        changeConfigurator.getParameters().put('retURL', '/' + change.id);
        Test.setCurrentPage(changeConfigurator);

        Test.startTest();

		ApexPages.StandardController stdController = new ApexPages.StandardController(change);
		ChangeConfiguratorController controller = new ChangeConfiguratorController(stdController);

		Test.stopTest();
	}

	@isTest static void saveChangeElements() {
		Integer nrOfItems = 1;

		Extension__c change = TestHelper.createExtensionChange(
            new Extension__c(
            ), 
            true
        );
		List<Resource__c> priceListItems = TestHelper.createResourcePriceListItems(
            new Resource__c(
            ), 
            nrOfItems,
            true
        );
        List<Extension__c> changeElements = new List<Extension__c>();
        for(Resource__c priceListItem : priceListItems){
        	Extension__c changeElement = TestHelper.createExtensionChangeElement(
            	new Extension__c(
            		Price_list_item__c = priceListItem.Id
            	), 
            	false
        	);
        	changeElements.add(changeElement);
        }

        String changeElementsJSON = JSON.serialize(changeElements);


        Test.startTest();

		ChangeConfiguratorController.Result_Wrapper result = ChangeConfiguratorController.saveData(changeElementsJSON);

		Test.stopTest();
	}
}