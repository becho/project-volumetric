/**
* @author 		Mateusz Pruszynski
* @description 	Test class for OffersPDFController
*/
@isTest
private class TestClassOffersPDFController {
    
    @testsetup
    static void setuptestdata() {
        Map<Integer, Id> idList = new Map<Integer, Id>();
        Resource__c invtemp = new Resource__c(Name = 'TestInvestment');
        Resource__c inv = TestHelper.createInvestment(invtemp, true);
        Id invId = inv.Id;
        Resource__c restemp = new Resource__c(Name = 'TestResource',Investment_Flat__c = invId);
        Resource__c resflat = TestHelper.createResourceFlatApartment(restemp, true);
        Contact customer = TestHelper.createContactPerson(null, true);
        //idList.put(1, customer.Id);
        //
        //Sales_Process__c temploffernoline = new Sales_Process__c(Contact__c = customer.id);
        Sales_Process__c offernoline = TestHelper.createSalesProcessProposal(null, true);
        Sales_Process__c temploffer = new Sales_Process__c(Contact__c = customer.id);
        Sales_Process__c offer = TestHelper.createSalesProcessProposal(temploffer, true);
        Sales_Process__c templprline = new Sales_Process__c(Product_Line_to_Proposal__c = offer.id, Price_With_Discount__c = 180000, Offer_Line_Resource__c = resflat.Id);
        Sales_Process__c prline = TestHelper.createSalesProcessProductLine(templprline, true);
        //idList.put(2, offer.Id);
        List<Extension__c> constrs = new List<Extension__c>();
        //RecordType rect = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :CommonUtility.RECORDTYPE_RESOURCE_EXTENSION LIMIT 1];
        Extension__c scheduletempl1 = new Extension__c(Investment__c = invId, Progress__c = 25.00, Name = 'Test_Etap_1', Start_Date__c = Date.today() + 1, End_Date__c = Date.today() + 2, Planned_End_Date__c = Date.today() - 1);
 		Extension__c scheduletempl2 = new Extension__c(Investment__c = invId, Progress__c = 40.00, Name = 'Test_Etap_2', Start_Date__c = Date.today() + 3, End_Date__c = Date.today() + 4, Planned_End_Date__c = Date.today() + 4);
		Extension__c scheduletempl3 = new Extension__c(Investment__c = invId, Progress__c = 35.00, Name = 'Test_Etap_3', Start_Date__c = Date.today() + 5, End_Date__c = Date.today() + 6, Planned_End_Date__c = Date.today() + 6);
        Extension__c schedule1 = TestHelper.createExtensionConstructionStage(scheduletempl1, false);
        Extension__c schedule2 = TestHelper.createExtensionConstructionStage(scheduletempl2, false);
        Extension__c schedule3 = TestHelper.createExtensionConstructionStage(scheduletempl3, false);
        constrs.add(schedule1);
        constrs.add(schedule2);
        constrs.add(schedule3);
        insert constrs;      
    }
    static testmethod void OffersPDFretNoOfflTest() {
        Contact contoff = [SELECT id FROM Contact LIMIT 1];
        String proposaltype = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER);
        Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Contact__c <> :contoff.Id AND RecordTypeId = :proposaltype LIMIT 1];
        PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put('Id', offer.Id);
		//System.debug(pageRef);
		Test.setCurrentPage(pageRef);  
        String expUrl = '/apex/OffersPDF';
        Test.startTest();
        OffersPDFController testclass = new OffersPDFController();
        Pagereference actPageref = testclass.ret();
        String actUrl = actPageref.getUrl();
        String expErrtype = 'NoOffl';
        Test.stopTest();
        Boolean errorfound = false;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(ApexPages.Message message : msgs) {
            if (message.getDetail().contains(Label.NoOfferLineHasBeenSelected))
                errorfound = true;      
        }
        System.assert(errorfound);
        System.assertEquals(expUrl,actUrl);
        System.assertEquals(true, testclass.allowEdit);
        System.assertEquals(false, testclass.isRet);
        System.assertEquals(expErrtype, testclass.errorType);
    }
    
    static testmethod void OffersPDFretTest() {
        Contact contoff = [SELECT id FROM Contact LIMIT 1];
        String proposaltype = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER);
        Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Contact__c = :contoff.Id AND RecordTypeId = :proposaltype LIMIT 1];
        PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put('Id', offer.Id);
		Test.setCurrentPage(pageRef);
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Test.startTest();
        OffersPDFController testclass = new OffersPDFController();
        Pagereference actPageref = testclass.ret();
        Test.stopTest();
        System.debug('PAY'+testclass.payments);
        Resource__c inv = [SELECt id, Name FROM Resource__c WHERE Name = 'TestResource' LIMIT 1];
        //List <Extension__c> construction = [SELECT Id, End_Date__c, Planned_End_Date__c, Progress__c, Planned_Start_Date__c, Investment__c, Start_Date__c, Status__c, RecordType.DeveloperName FROM Extension__c];
        /*List <Extension__c> construction = [SELECT Id, End_Date__c, Planned_End_Date__c, Progress__c, Planned_Start_Date__c, Investment__c, Start_Date__c, Status__c, RecordType.DeveloperName FROM Extension__c WHERE Investment__c = :inv.Id AND RecordType.DeveloperName =: CommonUtility.RECORDTYPE_RESOURCE_EXTENSION  ORDER BY Progress__c];
        System.debug('TESTTTTTTTTTTTTTTTTTT');
        System.debug('CONSTR'+construction);*/
        //System.assertEquals(3, construction.size());
        List<Payment__c> exppays = new List<Payment__c>();
        Payment__c exppay1 = new Payment__c();
        exppay1.Installment__c= 1;
        exppay1.of_contract_value__c = 25;
        Payment__c exppay2 = new Payment__c();
        exppay2.Installment__c= 2;
        exppay2.of_contract_value__c = 40;
        Payment__c exppay3 = new Payment__c();
        exppay3.Installment__c= 3;
        exppay3.of_contract_value__c = 35;
        
    }
    static testmethod void OfferPDFSecondaryretTest() {
        Contact contoff = [SELECT id FROM Contact LIMIT 1];
        String proposaltype = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER);
        Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Contact__c = :contoff.Id AND RecordTypeId = :proposaltype LIMIT 1];
        offer.Market__c = 'Secondary';
        update offer;
        /*Resource__c inv = [SELECt id, Name FROM Resource__c WHERE Name = 'TestResource' LIMIT 1];
        Contact contlandlrd = TestHelper.createContactPerson(null, true);
        inv.Property_Owner_Landlord__c = contlandlrd.Id;*/
        PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put('Id', offer.Id);
		Test.setCurrentPage(pageRef);
        Test.startTest();
        OffersPDFController testclass = new OffersPDFController();
        Pagereference actpageref = testclass.ret();
        Test.stopTest();
        Pagereference exppageref = new PageReference('/apex/OffersPDF');
        Boolean errorfound = false;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(ApexPages.Message message : msgs) {
            if (message.getDetail().contains(Label.PropertyHasNotBeenAssignedToLandlord))
                errorfound = true;      
        }
        System.assertequals(false, testclass.isRet);
        System.assertequals(exppageref.getUrl(), actpageref.getUrl());
        System.assert(errorfound);
    }
    static testmethod void savePdfNonameTest() {
        Contact contoff = [SELECT id FROM Contact LIMIT 1];
        String proposaltype = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER);
        Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Contact__c = :contoff.Id AND RecordTypeId = :proposaltype LIMIT 1];
        PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put('Id', offer.Id);
		Test.setCurrentPage(pageRef);
        
        OffersPDFController testclass = new OffersPDFController();
        //Pagereference actPageref = testclass.ret();
        testclass.pdfName = null;
        Test.startTest();
        PageReference actres = testclass.savePdf();
        Test.stopTest();
        Boolean errorfound = false;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(ApexPages.Message message : msgs) {
            if (message.getDetail().contains(Label.ErrorNoPdfNameProvidedError))
                errorfound = true;      
        }
        System.assert(errorfound);
        System.assertEquals(null, actres);
    }
    
    static testmethod void savePdfest() {
        Contact contoff = [SELECT id FROM Contact LIMIT 1];
        String proposaltype = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER);
        Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Contact__c = :contoff.Id AND RecordTypeId = :proposaltype LIMIT 1];
        PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offer.Id);
        
		Test.setCurrentPage(pageRef);
        OffersPDFController testclass = new OffersPDFController();
        testclass.pdfName = 'TestPDFName';
        //Pagereference actPageref = testclass.ret();
        String expRes = '/' + String.valueOf(offer.Id);
        Test.startTest();
        	PageReference actRes = testclass.savePdf();
        Test.stopTest();

        Attachment attach = [SELECT Name, IsPrivate, ParentId FROM Attachment WHERE Name = 'TestPDFName.pdf'];

        System.assertEquals(expRes, actRes.getUrl());
        System.assertEquals('TestPDFName.pdf', attach.Name);
        System.assertEquals(false, attach.IsPrivate);
        System.assertEquals(offer.Id, attach.ParentId);
        
    }

    static testmethod void cancelTest() {
        Contact contoff = [SELECT id FROM Contact LIMIT 1];
        String proposaltype = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER);
        Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Contact__c = :contoff.Id AND RecordTypeId = :proposaltype LIMIT 1];
        PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put(CommonUtility.URL_PARAM_ID, offer.Id);
        
		Test.setCurrentPage(pageRef);
        OffersPDFController testclass = new OffersPDFController();
        String expRes = '/' + String.valueOf(offer.Id);
        Test.startTest();
        	PageReference actRes = testclass.Cancel();
        Test.stopTest();

        System.assertEquals(expRes, actRes.getUrl());
        
    }

    static testmethod void reloadTest() {
        Contact contoff = [SELECT id FROM Contact LIMIT 1];
        String proposaltype = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER);
        Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Contact__c = :contoff.Id AND RecordTypeId = :proposaltype LIMIT 1];
        PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put('Id', offer.Id);
		Test.setCurrentPage(pageRef);
        
        OffersPDFController testclass = new OffersPDFController();
        testclass.allowEdit = true;
        Test.startTest();
        	PageReference actres = testclass.Reload();
        Test.stopTest();

        System.assertEquals(null, actres);
    }

    static testmethod void reloadNoAllowEditTest() {
        Contact contoff = [SELECT id FROM Contact LIMIT 1];
        String proposaltype = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER);
        Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Contact__c = :contoff.Id AND RecordTypeId = :proposaltype LIMIT 1];
        PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put('Id', offer.Id);
		Test.setCurrentPage(pageRef);
        
        OffersPDFController testclass = new OffersPDFController();
        testclass.allowEdit = false;
        Test.startTest();
        	PageReference actres = testclass.Reload();
        Test.stopTest();

        Boolean errorfound = false;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(ApexPages.Message message : msgs) {
            if (message.getDetail().contains(Label.TheRecordIsLockedTextAreasCannotBeEdited))
                errorfound = true;      
        }

        System.assert(errorfound);
        System.assertEquals(null, actres);
    }

    // ret method invoked by setting previously errorType variable from controller
    static testmethod void retErrorsTest() {
        Contact contoff = [SELECT id FROM Contact LIMIT 1];
        String proposaltype = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER);
        Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Contact__c = :contoff.Id AND RecordTypeId = :proposaltype LIMIT 1];
        PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put('Id', offer.Id);
		Test.setCurrentPage(pageRef);
        
        OffersPDFController testclass = new OffersPDFController();
        Test.startTest();
        	testclass.errorType = 'noInv';
        	PageReference actres1 = testclass.ret();

        	testclass.errorType = 'noMarket';
        	PageReference actres2 = testclass.ret();

        	testclass.errorType = 'NoAccountMachingOrgName';
        	PageReference actres3 = testclass.ret();

        	testclass.errorType = 'noListing';
        	PageReference actres4 = testclass.ret();
        Test.stopTest();

        PageReference exppageref = new PageReference('/apex/OffersPDF');
        Boolean errorNoInv = false;
        Boolean errorNoMarket = false;
        Boolean errorNoAccountMachingOrgName = false;
        Boolean errorNoListing = false;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for (ApexPages.Message message : msgs) {
            if (message.getDetail().contains(Label.PropertyHasNotBeenAssignedToInvestment)) {
                errorNoInv = true;      
            }
            if (message.getDetail().contains(Label.PropertyNoMarket)) {
                errorNoMarket = true;      
            }
            if (message.getDetail().contains(Label.ReviewContactAccountData)) {
                errorNoAccountMachingOrgName = true;      
            }
            if (message.getDetail().contains(Label.NoListingSelected)) {
                errorNoListing = true;      
            }
        }

        System.assert(errorNoInv);
        System.assert(errorNoMarket);
        System.assert(errorNoAccountMachingOrgName);
        System.assert(errorNoListing);
        System.assertEquals(exppageref.getUrl(), actres1.getUrl());
        System.assertEquals(exppageref.getUrl(), actres2.getUrl());
        System.assertEquals(exppageref.getUrl(), actres3.getUrl());
        System.assertEquals(exppageref.getUrl(), actres4.getUrl());
        
    }
	
	/*static Map<Integer, Id> dataSetup(Boolean isOfferLine){
		Map<Integer, Id> idList = new Map<Integer, Id>();
		Account investor = testHelper.createInvestor('Test_investor');
		idList.put(1, investor.Id);			//1
		Resource__c building = testHelper.createBuilding('Test_building');
		idList.put(2, building.Id);			//2
		Resource__c investment = testHelper.createInvestment('Test_Investment', investor);
		idList.put(3, investment.Id);		//3
		Resource__c flat = testHelper.createFlat('Test_Flat', investment, building);
		idList.put(4, flat.Id);				//4
		Listing__c listing = testHelper.createListingSale(flat, 'Test_Listing', 145670);
		idList.put(5, listing.Id);			//5
		Contact customer = testHelper.createInddividualClient('Test_Customer');
		idList.put(6, customer.Id);			//6
		Sales_Process__c offer = testHelper.createOffer(customer);
		idList.put(7, offer.Id);			//7
		if(isOfferLine == true){
			//Sales_Process__c offerLine = testHelper.createOfferLine(offer, listing);
			//idList.put(8, offerLine.Id);	//8
		}
		else{
			Sales_Process__c offerLine = null;
		}
		System.debug(offer);		
		return idList;
	}

	static testMethod void allDataInserted() {

		Map<Integer, Id> idList = dataSetup(true);
		Id offerId = idList.get(7);

		Test.startTest();
		Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Id = :offerId LIMIT 1];
		PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put('Id', offer.Id);
		System.debug(pageRef);
		Test.setCurrentPage(pageRef);

		OffersPDFController opc = new OffersPDFController();
		PageReference rets = opc.ret();		
		PageReference reload = opc.Reload();
		test.stopTest();

		System.assertNotEquals(rets, null);
		System.assertEquals(reload, null);
	
	}

	static testMethod void noOfferLine() {

		Map<Integer, Id> idList = dataSetup(false);
		Id offerId = idList.get(7);

		Test.startTest();
		Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Id = :offerId LIMIT 1];
		PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put('Id', offer.Id);
		System.debug(pageRef);
		Test.setCurrentPage(pageRef);

		OffersPDFController opc = new OffersPDFController();
		PageReference rets = opc.ret();		
		PageReference cancel = opc.Cancel();
		test.stopTest();

		System.assertNotEquals(rets, null);
	}

	static testMethod void testPdfGeneratorNameProvided() {

		Map<Integer, Id> idList = dataSetup(true);
		Id offerId = idList.get(7);

		Test.startTest();
		Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Id = :offerId LIMIT 1];
	    PageReference pref = Page.OffersPDFGeneratorTemplate;
	    pref.getParameters().put('id', offer.Id);
	    Test.setCurrentPage(pref);

	    OffersPDFController con = new OffersPDFController();    

	    // populate the field with values
	    con.parentId = offer.id;
	    con.pdfName = 'My Test PDF';

	    // submit the record
	    pref = con.savePdf();

	    // assert that they were sent to the correct page
	    System.assertEquals(pref.getUrl(),'/'+offer.id);

	    // assert that an attachment exists for the record
	    System.assertEquals(1,[select count() from attachment where parentId = :offer.id]);

	    Test.stopTest(); 

  }

 	static testMethod void testPdfGeneratorNoNameProvided() {

		Map<Integer, Id> idList = dataSetup(true);
		Id offerId = idList.get(7);

		Test.startTest();
		Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Id = :offerId LIMIT 1];
	    PageReference pref = Page.OffersPDFGeneratorTemplate;
	    pref.getParameters().put('id', offer.Id);
	    Test.setCurrentPage(pref);

	    OffersPDFController con = new OffersPDFController();    

	    con.pdfName = null;
	    con.parentId = offer.id;
	    pref = con.savePdf();

	    System.assertEquals(pref ,null);

	    Test.stopTest(); 

    }

    static testMethod void noMarket(){

    	Map<Integer, Id> idList = dataSetup(true);
    	Id offerId = idList.get(7);

    	Test.startTest();
		Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Id = :offerId LIMIT 1];
		offer.Market__c = null;
		update offer;

	    PageReference pref = Page.OffersPDF;
	    pref.getParameters().put('id', offer.Id);
	    Test.setCurrentPage(pref);

	    OffersPDFController con = new OffersPDFController();    

		PageReference ret = con.ret();
	    Test.stopTest(); 
   		
		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.PropertyNoMarket)) b = true;
		}

   		System.assert(b);

   	}

   	static testMethod void marketSecondaryLandlordNull(){

    	Map<Integer, Id> idList = dataSetup(true);
    	Id offerId = idList.get(7);

    	Test.startTest();
		Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Id = :offerId LIMIT 1];
		//offer.Market__c = CommonUtility.LISTING_MARKET_SECONDARY;
		offer.Market__c = CommonUtility.SALES_PROCESS_MARKET_SECONDARY;
		update offer;

	    PageReference pref = Page.OffersPDF;
	    pref.getParameters().put('id', offer.Id);
	    Test.setCurrentPage(pref);

	    OffersPDFController con = new OffersPDFController();    

		PageReference ret = con.ret();
	    Test.stopTest(); 
   		
		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.PropertyHasNotBeenAssignedToLandlord)) b = true;
		}

   		System.assert(b);

   	}

   	@IsTest(SeeAllData=true) //since we want to see org data to proceed.
   	static void marketSecondaryLandlordSelected(){

    	Map<Integer, Id> idList = dataSetup(true);
    	Id accountId = idList.get(1);
    	Id flatId = idList.get(4);
    	Id offerId = idList.get(7);
    	Contact landlord = testHelper.createInddividualClient('Test_landlord');
		Profile testProfile = testHelper.getProfile(CommonUtility.PROFILE_PROPERTO_BACKOFFICE_NAME);
		User testUser = testHelper.createUser(testProfile, 'test', 'Test', 'test123_887@t1es1torg.com');
		//Organization org = new Organization();
		//org.Name = [SELECT Name FROM Account WHERE Id = :accountId].Name;
		//insert org;

	    System.runAs(testUser) {	
	    	Test.startTest();
			Resource__c flat = [SELECT Id, Property_Owner_Landlord__c FROM Resource__c WHERE Id = :flatId LIMIT 1];
			flat.Property_Owner_Landlord__c = landlord.Id;
			update flat;
			Sales_Process__c offer = [SELECT Id, Market__c FROM Sales_Process__c WHERE Id = :offerId LIMIT 1];
			//offer.Market__c = CommonUtility.LISTING_MARKET_SECONDARY;
			offer.Market__c = CommonUtility.SALES_PROCESS_MARKET_SECONDARY;
			update offer;

		    PageReference pref = Page.OffersPDF;
		    pref.getParameters().put('id', offer.Id);
		    Test.setCurrentPage(pref);

		    OffersPDFController con = new OffersPDFController();    

			PageReference ret = con.ret();
		    Test.stopTest(); 
	   		
			System.assertNotEquals(null, ret);
		}

   	}

   	static testMethod void paymentSchedule(){

   		Map<Integer, Id> idList = dataSetup(true);
   		Id investmentId = idList.get(3);
   		Extension__c schedule1 = testHelper.createConstructionStage(investmentId, 25.00, 'Test_Etap_1', Date.today() + 1, Date.today() + 2);
   		Extension__c schedule2 = testHelper.createConstructionStage(investmentId, 40.00, 'Test_Etap_2', Date.today() + 3, Date.today() + 4);
   		Extension__c schedule3 = testHelper.createConstructionStage(investmentId, 35.00, 'Test_Etap_3', Date.today() + 5, Date.today() + 6);
   		Id offerId = idList.get(7);

		Test.startTest();
		Sales_Process__c offer = [SELECT Id FROM Sales_Process__c WHERE Id = :offerId LIMIT 1];
		//offer.Market__c = CommonUtility.LISTING_MARKET_PRIMARY;
		offer.Market__c = CommonUtility.SALES_PROCESS_MARKET_PRIMARY;
		update offer;
		PageReference pageRef = Page.OffersPDF;
		pageRef.getParameters().put('Id', offer.Id);
		Test.setCurrentPage(pageRef);

		OffersPDFController opc = new OffersPDFController();
		opc.creteFromOffer();
		test.stopTest();

		Integer numberOfInstallments = opc.payments.size()/2;
		Integer numberOfConstrStages = opc.construction.size();
		System.assertEquals(numberOfConstrStages, numberOfInstallments);

   	}*/
}