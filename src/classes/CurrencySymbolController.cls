public with sharing class CurrencySymbolController {
	
	public String currencyISOsymbol {
	    get {
            return CommonUtility.getCurrencySymbolFromIso(UserInfo.getDefaultCurrency());
	    }
	    set;
	}

	public CurrencySymbolController() {
		
	}
}