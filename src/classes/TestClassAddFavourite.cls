/**
* @author       Mateusz Pruszyński
* @description  Test class for AddFavourite.cls (controller to AddFavourite.page).
**/
@isTest
private class TestClassAddFavourite {

  // Use case with Person Account
  static testMethod void testMethForAccountId() {
    PageReference pageRef = Page.AddFavourite;
    Test.setCurrentPageReference(pageRef); 

    Account personAccount = TestHelper.createAccountIndividualClient(null, true); 
    pageRef.getParameters().put(CommonUtility.URL_PARAM_ACCOUNTID, personAccount.Id);

    AddFavourite controller = new AddFavourite();

    Resource__c parkingSpace = TestHelper.createResourceParkingSpace(null, true);

    // check data
    Test.startTest();
      // get contact related to PA
      Contact paContact = [
        SELECT Id FROM Contact WHERE AccountId = :personAccount.Id
      ];
      Boolean favouriteInserted = AddFavourite.massAddToFavorite(
        new String[] {String.valueOf(parkingSpace.Id)}, 
        String.valueOf(paContact.Id)
      );
    	List<Sales_Process__c> favourite = [
        SELECT Id FROM Sales_Process__c 
        WHERE Customer_Favorite__c = :paContact.Id 
        AND Resource_Favourite__c = :parkingSpace.Id
        AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FAVOURITE)
      ];
    Test.stopTest();
    System.assertEquals(1, favourite.size());
  }

  // Use case with Contact
  static testMethod void testMethForContactId() {
    PageReference pageRef = Page.AddFavourite;
    Test.setCurrentPageReference(pageRef); 

    Contact contactPerson = TestHelper.createContactPerson(null, true);

    pageRef.getParameters().put(CommonUtility.URL_PARAM_CONTACTID, contactPerson.Id);

    AddFavourite controller = new AddFavourite();

    Resource__c parkingSpace = TestHelper.createResourceParkingSpace(null, true);

    // check data
    Test.startTest();
      Boolean favouriteInserted = AddFavourite.massAddToFavorite(
        new String[] {String.valueOf(parkingSpace.Id)}, 
        String.valueOf(contactPerson.Id)
      );
      List<Sales_Process__c> favourite = [
        SELECT Id FROM Sales_Process__c 
        WHERE Customer_Favorite__c = :contactPerson.Id 
        AND Resource_Favourite__c = :parkingSpace.Id
        AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FAVOURITE)
      ];
    Test.stopTest();
    System.assertEquals(1, favourite.size());
  }  
}