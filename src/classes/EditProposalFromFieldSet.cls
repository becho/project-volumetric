global with sharing class EditProposalFromFieldSet {

	private Map<String, Map<String,object>> sObjectWithAtt;
	private  Map<String, List<Schema.PicklistEntry>> optionsForPicklists;
	public Sales_Process__c process;
	public static final String FIELDTYPE_PICKLIST ='PICKLIST';
	public static final String FIELDTYPE_REFERENCE ='REFERENCE';

	public List<String> getJSONwithAdditionalFields(String mainEditProcessId){
		List<String> returnList = new List<String>();
		sObjectWithAtt = new Map<String,Map<String,object>>();
		optionsForPicklists = new Map<String, List<Schema.PicklistEntry>>();
        getValuesForJSON(mainEditProcessId);
        returnList.add(JSON.serialize(sObjectWithAtt));
        if(!optionsForPicklists.isEmpty()){
            returnList.add(JSON.serialize(optionsForPicklists));
        }
        return returnList;
    }

    private void getValuesForJSON(String mainEditProcessId){
    	process = Database.query(queryBuilder(mainEditProcessId));
        for(String fieldPath : sObjectWithAtt.keySet()){
            if(sObjectWithAtt.get(fieldPath).get('type') == FIELDTYPE_REFERENCE && process.get(fieldPath) != null && process.get(fieldPath) != ''){
                String queryName = 'SELECT Name FROM ';
                queryName += sObjectWithAtt.get(fieldPath).get('obj');
                queryName += ' WHERE Id = \'' + process.get(fieldPath) + '\'';
                queryName = queryName.replaceAll('[()]' , '');
                SObject objSaved = Database.query(queryName);
                sObjectWithAtt.get(fieldPath).put('values', objSaved);
            } else {
                sObjectWithAtt.get(fieldPath).put('values', process.get(fieldPath));
            }
        }
    }

    public String queryBuilder(String mainEditProcessId){
        List<Schema.FieldSetMember> fields = getFields();
    	String query = 'SELECT ';
        for(Schema.FieldSetMember field : fields){
            Map<String,object> atts = new Map<String,object>();
            query += field.getFieldPath() + ', ';
            atts.put('label',field.getLabel());
            atts.put('type', String.valueOf(field.getType()));
            if(String.valueOf(field.getType()) == FIELDTYPE_PICKLIST){
                optionsForPicklists.put(field.getFieldPath(), getOptionsList(field.getFieldPath()));
            }
            if(String.valueOf(field.getType()) == FIELDTYPE_REFERENCE){
                atts.put('obj', getReferencedObjAPIname(field.getFieldPath()));
            }
            atts.put('values','');
            sObjectWithAtt.put(field.getFieldPath(),atts);
        }
        query += 'Rejection_Reason__c FROM Sales_Process__c WHERE id =: mainEditProcessId';
        return query;
    }

    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Sales_Process__c.FieldSets.Edit_Proposal_Fields.getFields();
    }

    public List<Schema.PicklistEntry> getOptionsList(String describedField){
        Schema.sObjectType objType = Sales_Process__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();      
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        list<Schema.PicklistEntry> values = fieldMap.get(describedField).getDescribe().getPickListValues();
        if(describedField == 'Status__c'){
           values = onlyValidStatusValues(values);
        }
        return values;
    }

    public List<Schema.PicklistEntry> onlyValidStatusValues(List<Schema.PicklistEntry> valuesSet){
        list<Schema.PicklistEntry> values = new List<Schema.PicklistEntry>();
        for(Schema.PicklistEntry val : valuesSet){
            String currentVal = String.valueOf(val.getValue());
            if(currentVal == CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_CUSTOMER_APPROVAL|| currentVal == CommonUtility.SALES_PROCESS_STATUS_ACCEPTED_CUSTOMER|| currentVal == CommonUtility.SALES_PROCESS_STATUS_OUT_OFF_DATE || currentVal == CommonUtility.SALES_PROCESS_STATUS_REJECTED )
            values.add(val);
        }
        return values;
    }

    public String getReferencedObjAPIname(String field){
        Map<String, Schema.SObjectType> GlobalMap = Schema.getGlobalDescribe();
        Schema.DescribeSObjectResult obj = GlobalMap.get(CommonUtility.SOBJECT_NAME_SALESPROCESS).getDescribe();
        Schema.DescribeFieldResult currentField = obj.Fields.getMap().get(field).getDescribe();
        String value = String.valueOf(currentField.getReferenceTo());
        return value;
    }

    public static String getTypeByAPIname(String field){
        Map<String, Schema.SObjectType> GlobalMap = Schema.getGlobalDescribe();
        Schema.DescribeSObjectResult obj = GlobalMap.get(CommonUtility.SOBJECT_NAME_SALESPROCESS).getDescribe();
        Schema.DescribeFieldResult currentField = obj.Fields.getMap().get(field).getDescribe();
        String value = String.valueOf(currentField.getType());
        return value;
    }

    public static Map<String, String> deserializer(String jsonObj){
        jsonObj = jsonObj.substring(1);
        List<String> elements = jsonObj.split('},');
        Map<String, String> allFields = new Map<String, String>();
        String fieldName;
        for(String field : elements){
            List<String> currItems = field.split('=\\{');
            for(String item : currItems){  
                String elementDiff = item.substring(item.length()-3);
                if( elementDiff.equals('__c') ){
                  fieldName = item;
                } else if(item.contains('values=')) {
                    List<String> currValues = item.split('values=');
                    List<String> val = currValues[1].split(',');                    
                    allFields.put(fieldName, val[0]);
                } else if(item.contains(CommonUtility.URL_PARAM_ID)){
                    System.debug(item);
                    List<String> currValues = item.split('Id=');
                    List<String> val = currValues[1].split(',');  
                    allFields.put(fieldName, val[0]); 
                }
            }
        }
        return allFields;
    }

}