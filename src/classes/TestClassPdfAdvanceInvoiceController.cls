/**
* @author 		Mateusz Pruszynski
* @description 	Test class for PdfAdvanceInvoiceController
*/

@isTest
private class TestClassPdfAdvanceInvoiceController {
	
	//static Map<Integer, Id> dataSetup(){
	//	Map<Integer, Id> idList = new Map<Integer, Id>();

	//	Contact customer = testHelper.createInddividualClient('Test_Customer');
	//	idList.put(1, customer.Id);					//1		
	//	Account investor = testHelper.createInvestor('Test_investor');
	//	idList.put(2, investor.Id);					//2
	//	Resource__c building = testHelper.createBuilding('Test_building');
	//	idList.put(3, building.Id);					//3
	//	Resource__c investment = testHelper.createInvestment('Test_Investment', investor);
	//	idList.put(4, investment.Id);				//4
	//	Resource__c flat = testHelper.createFlat('Test_Flat', investment, building);
	//	idList.put(5, flat.Id);						//5
	//	Listing__c listing = testHelper.createListingSale(flat, 'Test_Listing', 345654);
	//	idList.put(6, listing.Id);					//6
	//	Sales_Process__c offer = testHelper.createOffer(customer);
	//	idList.put(7, offer.Id);					//7
	//	Sales_Process__c offerLine = testHelper.createOfferLine(offer, listing);
	//	idList.put(8, offerLine.Id);				//8
	//	Sales_Process__c preliminaryAgreement = testHelper.createPreliminaryAgreement(offer);
	//	idList.put(9, preliminaryAgreement.Id);		//9
	//	Payment__c installment = testHelper.newPayment(345.6, preliminaryAgreement);
	//	idList.put(10, installment.Id);				//10

	//	return idList;
	//}

	//static List<SelectOption> getSelectVAT(){
	//List<SelectOption> options = new List<SelectOption>();
	//options.add(new SelectOption('', Label.ResSearchSelect));        
	//Schema.DescribeFieldResult fieldResult =
	//Invoice__c.VAT__c.getDescribe();
	//List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	        
	//    for( Schema.PicklistEntry f : ple){
	//       options.add(new SelectOption(f.getLabel(), f.getValue()));
	//    }       
	//    return options;
	//}

	//static testMethod void installmentNotPaid(){
	//	Map<Integer, Id> idList = dataSetup();
	//	Id paymentID = idList.get(10);

	//	Test.startTest();
	//	PageReference pageRef = Page.AdvanceInvoice;
	//	pageRef.getParameters().put('Id', paymentID);
	//	System.debug(pageRef);
	//	Test.setCurrentPage(pageRef);
	//	PdfAdvanceInvoiceController opc = new PdfAdvanceInvoiceController();
	//	PageReference reload = opc.reload();
	//	Test.stopTest();

	//	List<Apexpages.Message> msgs = ApexPages.getMessages();
	//	boolean b = false;
	//	for(Apexpages.Message msg:msgs){
	//	    if (msg.getDetail().contains(Label.InstallmentNotPaidYet)) b = true;
	//	}

 //  		System.assert(b);
	//}
	
	//static testMethod void installmentPaidNoMarket(){
	//	Map<Integer, Id> idList = dataSetup();
	//	Id paymentID = idList.get(10);
	//	Payment__c installment = [SELECT Id, Name, Paid__c, Payment_Date__c FROM Payment__c WHERE Id = :paymentID LIMIT 1];
	//	installment.Paid__c = true;
	//	installment.Payment_Date__c = Date.today();
	//	update installment;

	//	Test.startTest();
	//	PageReference pageRef = Page.AdvanceInvoice;
	//	pageRef.getParameters().put('Id', paymentID);
	//	System.debug(pageRef);
	//	Test.setCurrentPage(pageRef);
	//	PdfAdvanceInvoiceController opc = new PdfAdvanceInvoiceController();
	//	PageReference reload = opc.reload();
	//	Test.stopTest();
	
	//	List<Apexpages.Message> msgs = ApexPages.getMessages();
	//	boolean b = false;
	//	for(Apexpages.Message msg:msgs){
	//	    if (msg.getDetail().contains(Label.ErrorMarketNotSelected)) b = true;
	//	}

 //  		System.assert(b);
	//}

	//static testMethod void installmentPaidMarketPrimaryNoNIP(){
	//	Map<Integer, Id> idList = dataSetup();
	//	Id paymentID = idList.get(10);
	//	Id offerID = idList.get(7);
	//	Payment__c installment = [SELECT Paid__c, Payment_Date__c FROM Payment__c WHERE Id = :paymentID LIMIT 1];
	//	installment.Paid__c = true;
	//	installment.Payment_Date__c = Date.today();
	//	update installment;
	//	Sales_Process__c offer = [SELECT Market__c FROM Sales_Process__c WHERE Id = :offerID LIMIT 1];
	//	offer.Market__c = CommonUtility.LISTING_MARKET_PRIMARY;
	//	update offer;

	//	Test.startTest();
	//	PageReference pageRef = Page.AdvanceInvoice;
	//	pageRef.getParameters().put('Id', paymentID);
	//	System.debug(pageRef);
	//	Test.setCurrentPage(pageRef);
	//	PdfAdvanceInvoiceController opc = new PdfAdvanceInvoiceController();
	//	PageReference reload = opc.reload();
	//	Test.stopTest();

	//	List<Apexpages.Message> msgs = ApexPages.getMessages();
	//	boolean b = false;
	//	for(Apexpages.Message msg:msgs){
	//	    if (msg.getDetail().contains(Label.NoNameNIPprovidedAccount)) b = true;
	//	}

 //  		System.assert(b);
	//}

	//static testMethod void installmentPaidMarketPrimaryNIPprovided(){
	//	Map<Integer, Id> idList = dataSetup();
	//	Id paymentID = idList.get(10);
	//	Id offerID = idList.get(7);
	//	Id accountID = idList.get(2);
	//	Payment__c installment = [SELECT Paid__c, Payment_Date__c FROM Payment__c WHERE Id = :paymentID LIMIT 1];
	//	installment.Paid__c = true;
	//	installment.Payment_Date__c = Date.today();
	//	update installment;
	//	Sales_Process__c offer = [SELECT Market__c FROM Sales_Process__c WHERE Id = :offerID LIMIT 1];
	//	offer.Market__c = CommonUtility.LISTING_MARKET_PRIMARY;
	//	update offer;
	//	Account investor = [SELECT NIP__c FROM Account WHERE Id = :accountID LIMIT 1];
	//	investor.NIP__c = '6245057728';
	//	update investor;

	//	Test.startTest();
	//	PageReference pageRef = Page.AdvanceInvoice;
	//	pageRef.getParameters().put('Id', paymentID);
	//	System.debug(pageRef);
	//	Test.setCurrentPage(pageRef);
	//	PdfAdvanceInvoiceController opc = new PdfAdvanceInvoiceController();
	//	PageReference reload = opc.reload();
	//	PageReference cancel = opc.cancel();
	//	List<SelectOption> paymentMethod = opc.getSelectMethod();
	//	Test.stopTest();

	//	System.assertEquals(null, reload);
	//	System.assertNotEquals(null, cancel);
	//	System.assertEquals(3, paymentMethod.size());
	//}
	
	//static testMethod void installmentPaidMarketSecondary(){
	//	Map<Integer, Id> idList = dataSetup();
	//	Id paymentID = idList.get(10);
	//	Id offerID = idList.get(7);
	//	Id accountID = idList.get(2);
	//	Payment__c installment = [SELECT Paid__c, Payment_Date__c FROM Payment__c WHERE Id = :paymentID LIMIT 1];
	//	installment.Paid__c = true;
	//	installment.Payment_Date__c = Date.today();
	//	update installment;
	//	Sales_Process__c offer = [SELECT Market__c FROM Sales_Process__c WHERE Id = :offerID LIMIT 1];
	//	offer.Market__c = CommonUtility.LISTING_MARKET_SECONDARY;
	//	update offer;
	//	Account investor = [SELECT NIP__c FROM Account WHERE Id = :accountID LIMIT 1];
	//	investor.NIP__c = '6245057728';
	//	update investor;

	//	Test.startTest();
	//	PageReference pageRef = Page.AdvanceInvoice;
	//	pageRef.getParameters().put('Id', paymentID);
	//	System.debug(pageRef);
	//	Test.setCurrentPage(pageRef);
	//	PdfAdvanceInvoiceController opc = new PdfAdvanceInvoiceController();
	//	PageReference reload = opc.reload();
	//	Test.stopTest();

	//	List<Apexpages.Message> msgs = ApexPages.getMessages();
	//	boolean b = false;
	//	for(Apexpages.Message msg:msgs){
	//	    if (msg.getDetail().contains(Label.ErrorWrongMarket)) b = true;
	//	}

 //  		System.assert(b);
	//}

	//static testMethod void upsertInvoiceEmptyFields(){
	//	Map<Integer, Id> idList = dataSetup();
	//	Id paymentID = idList.get(10);
	//	Id offerID = idList.get(7);
	//	Id accountID = idList.get(2);
	//	Payment__c installment = [SELECT Paid__c, Payment_Date__c FROM Payment__c WHERE Id = :paymentID LIMIT 1];
	//	installment.Paid__c = true;
	//	installment.Payment_Date__c = Date.today();
	//	update installment;
	//	Sales_Process__c offer = [SELECT Market__c FROM Sales_Process__c WHERE Id = :offerID LIMIT 1];
	//	offer.Market__c = CommonUtility.LISTING_MARKET_PRIMARY;
	//	update offer;
	//	Account investor = [SELECT NIP__c FROM Account WHERE Id = :accountID LIMIT 1];
	//	investor.NIP__c = '6245057728';
	//	update investor;

	//	Test.startTest();
	//	PageReference pageRef = Page.PdfAdvanceInvoice;
	//	pageRef.getParameters().put('Id', paymentID);
	//	System.debug(pageRef);
	//	Test.setCurrentPage(pageRef);
	//	PdfAdvanceInvoiceController opc = new PdfAdvanceInvoiceController();
	//	PageReference upsertInvoice = opc.upsertInvoice();
	//	Test.stopTest();

	//	List<Apexpages.Message> msgs = ApexPages.getMessages();
	//	boolean b = false;
	//	for(Apexpages.Message msg:msgs){
	//	    if (msg.getDetail().contains(Label.EmptyFieldsRemaining)) b = true;
	//	}

 //  		System.assert(b);
	//}

	//static testMethod void upsertInvoiceNoPdfName(){
	//	Map<Integer, Id> idList = dataSetup();
	//	Id paymentID = idList.get(10);
	//	Id offerID = idList.get(7);
	//	Id accountID = idList.get(2);
	//	Payment__c installment = [SELECT Paid__c, Payment_Date__c FROM Payment__c WHERE Id = :paymentID LIMIT 1];
	//	installment.Paid__c = true;
	//	installment.Payment_Date__c = Date.today();
	//	update installment;
	//	Sales_Process__c offer = [SELECT Market__c FROM Sales_Process__c WHERE Id = :offerID LIMIT 1];
	//	offer.Market__c = CommonUtility.LISTING_MARKET_PRIMARY;
	//	update offer;
	//	Account investor = [SELECT NIP__c FROM Account WHERE Id = :accountID LIMIT 1];
	//	investor.NIP__c = '6245057728';
	//	update investor;

	//	Test.startTest();
	//	PageReference pageRef = Page.PdfAdvanceInvoice;
	//	pageRef.getParameters().put('Id', paymentID);
	//	System.debug(pageRef);
	//	Test.setCurrentPage(pageRef);
	//	PdfAdvanceInvoiceController opc = new PdfAdvanceInvoiceController();
	//	opc.pdfName = '';
	//	PageReference upsertInvoice = opc.upsertInvoice();
	//	Test.stopTest();

	//	List<Apexpages.Message> msgs = ApexPages.getMessages();
	//	boolean b = false;
	//	for(Apexpages.Message msg:msgs){
	//	    if (msg.getDetail().contains(Label.ErrorNoPdfNameProvidedError)) b = true;
	//	}

 //  		System.assert(b);
	//}

	//static testMethod void updateAmountsSelectVAT(){
	//	Map<Integer, Id> idList = dataSetup();
	//	Id paymentID = idList.get(10);
	//	List<SelectOption> vatRates = getSelectVAT();
	//	Integer vatRateSize = vatRates.size();
	//	String choosedVatRate = vatRates[vatRateSize - 1].getValue();

	//	Test.startTest();
	//	PageReference pageRef = Page.AdvanceInvoice;
	//	pageRef.getParameters().put('Id', paymentID);
	//	System.debug(pageRef);
	//	Test.setCurrentPage(pageRef);
	//	PdfAdvanceInvoiceController opc = new PdfAdvanceInvoiceController();
	//	opc.selectedVAT = choosedVatRate;
	//	List<SelectOption> vatList = getSelectVAT();
	//	PageReference upsertInvoice = opc.updateAmounts();
	//	Test.stopTest();

	//	System.assertEquals(vatRates, vatList);
	//	System.assertEquals(null, upsertInvoice);
	//}

	//static testMethod void upsertInvoiceAllDataInsertedNewInvoice(){
	//	Map<Integer, Id> idList = dataSetup();
	//	Id paymentID = idList.get(10);
	//	Id offerID = idList.get(7);
	//	Id accountID = idList.get(2);
	//	Id agreementID = idList.get(9);
	//	Payment__c installment = [SELECT Paid__c, Payment_Date__c FROM Payment__c WHERE Id = :paymentID LIMIT 1];
	//	installment.Paid__c = true;
	//	installment.Payment_Date__c = Date.today();
	//	update installment;
	//	Sales_Process__c offer = [SELECT Market__c FROM Sales_Process__c WHERE Id = :offerID LIMIT 1];
	//	offer.Market__c = CommonUtility.LISTING_MARKET_PRIMARY;
	//	update offer;
	//	Account investor = [SELECT NIP__c FROM Account WHERE Id = :accountID LIMIT 1];
	//	investor.NIP__c = '6245057728';
	//	update investor;

	//	List<SelectOption> vatRates = getSelectVAT();
	//	Integer vatRateSize = vatRates.size();
	//	String choosedVatRate = vatRates[vatRateSize - 1].getValue();
	//	System.debug(choosedVatRate);

	//	Test.startTest();
	//	PageReference pageRef = Page.PdfAdvanceInvoice;
	//	pageRef.getParameters().put('Id', paymentID);
	//	System.debug(pageRef);
	//	Test.setCurrentPage(pageRef);
	//	PdfAdvanceInvoiceController opc = new PdfAdvanceInvoiceController();
	//	opc.selectedMethod = CommonUtility.INVOICE_PAYMENT_METHOD_CASH;
	//	opc.selectedVAT = choosedVatRate; // VAT rates might get changed by admin!
	//	opc.newInvoiceToInsert.Invoice_City__c = 'Test_City';
	//	opc.newInvoiceToInsert.Invoice_Date__c = Date.today();
	//	PageReference upsertInvoice = opc.upsertInvoice();
	//	List<SelectOption> vat = opc.getSelectVAT();
	//	Test.stopTest();

	//	System.assertNotEquals(null, upsertInvoice);
	//	System.assertEquals(vatRates, vat);
	//}
}