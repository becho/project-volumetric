/**
* @author 		Mateusz Pruszyński
* @description 	Class with methods managing second way of bank account generation
**/

/* 
	BANK ACCOUNT SECOND WAY OF GENERATION:
 	- bank account number is stored on each resource as well as on Investment.
 	- flats can have Trust or Current bank accounts
 	- other Resource types always hold Current bank account
 	- bank account on Investment is always Current
 */

public without sharing class ManageBankAccountSecondWayOfGeneration {

    /* Copy incoming payments's After_sales_from_Incoming_Payment__c to After_sales_from_Incoming_Payment_Curren__c or After_sales_from_Incoming_Payment_Trust__c to differentiate records between related lists */
	public static void differentiateBetweenTrustAndCurrentIncomingPayments(Map<Id, List<Payment__c>> incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap) {

		// Incoimng payments must be matched with Resource object by BANK ACCOUNT and AFTER-SALES SERVICE

		List<Sales_Process__c> productLines = getProductLinesForMatching(incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap.keySet());

		if(!productLines.isEmpty()) {

			for(Id afterSalesId : incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap.keySet()) {

				for(Sales_Process__c productLine : productLines) {

					if(String.valueOf(afterSalesId).left(15) == String.valueOf(productLine.Offer_Line_to_After_sales_Service__c).left(15)) {

						List<Payment__c> incomingPayments = incomingPaymentsTrustVsCurrentSecondWayGenAfterSalesIdMap.get(afterSalesId);

						for(Payment__c incomingPayment : incomingPayments) {

							if(incomingPayment.Bank_Account_For_Resource__c == productLine.Offer_Line_Resource__r.Bank_Account_Number__c) {

								if(productLine.Offer_Line_Resource__r.Trust_Account__c) {
									incomingPayment.After_sales_from_Incoming_Payment_Trust__c = incomingPayment.After_sales_from_Incoming_Payment__c;
								} else {
									incomingPayment.After_sales_from_Incoming_Payment_Curren__c = incomingPayment.After_sales_from_Incoming_Payment__c;
								}

							}
							else {
									incomingPayment.After_sales_from_Incoming_Payment_Curren__c = incomingPayment.After_sales_from_Incoming_Payment__c;
							}

						}
					
					}

				}

			}

		}

	}

	public static List<Sales_Process__c> getProductLinesForMatching(Set<Id> afterSalesIds) {
		return [
			SELECT Id, Offer_Line_to_After_sales_Service__c, Offer_Line_Resource__c, Offer_Line_Resource__r.Trust_Account__c, Offer_Line_Resource__r.Bank_Account_Number__c
			FROM Sales_Process__c
			WHERE Offer_Line_to_After_sales_Service__c in :afterSalesIds
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
			AND Offer_Line_Resource__c <> null
			AND Offer_Line_Resource__r.Bank_Account_Number__c <> null
		];
	}

}