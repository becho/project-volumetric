/**
* @author       Dariusz Paszel, latest Fix: Mateusz Pruszyński
* @editor		Mateusz Wolak-Ksiazek
* @description  test class for Extension Trigger
**/

@isTest
private class TestClassExtensionTrigger {

	private static String randomText = 'randomText';
	private static Datetime todayMinus10Days = Datetime.now().addDays(-10);


	@isTest static void testBeforeUpdateExtensionChange() {

		Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);

		Sales_Process__c saleTermsTemplate = new Sales_Process__c(
			Offer__c = productLine.Product_Line_to_Proposal__c,
			Contact__c = productLine.Contact_from_Offer_Line__c
		);
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(saleTermsTemplate, true);

		Sales_Process__c afterSalesTemplate = new Sales_Process__c(
			Agreement__c = saleTerms.Id,
			Contact__c = productLine.Contact_from_Offer_Line__c
		);
		Sales_Process__c afterSalesService = TestHelper.createSalesProcessAfterSalesService(afterSalesTemplate, false);


		Test.startTest();

			insert afterSalesService;

			Extension__c changeTemplate = new Extension__c(
				Resource_Flat__c = productLine.Offer_Line_Resource__c,
				After_sales_Service__c = afterSalesService.Id
			);
			Extension__c eChange = TestHelper.createExtensionChange(changeTemplate, true);

			eChange.After_sales_Service__c = saleTerms.Id;

			Boolean b = false;
			try {
				update eChange;
			} catch (Exception e){
				List<Apexpages.Message> msgs = ApexPages.getMessages();
				for(Apexpages.Message msg:msgs){
					if (msg.getDetail().contains(Label.CannotModifyReference)) b = true;
				}
			}

		Test.stopTest();

		System.assert(b);
	}

	@isTest static void testBeforeUpdateExtensionDefect() {

		Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);

		Sales_Process__c saleTermsTemplate = new Sales_Process__c(
			Offer__c = productLine.Product_Line_to_Proposal__c,
			Contact__c = productLine.Contact_from_Offer_Line__c
		);
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(saleTermsTemplate, true);

		Sales_Process__c afterSalesTemplate = new Sales_Process__c(
			Agreement__c = saleTerms.Id,
			Contact__c = productLine.Contact_from_Offer_Line__c
		);
		Sales_Process__c afterSalesService = TestHelper.createSalesProcessAfterSalesService(afterSalesTemplate, false);


		Test.startTest();

			insert afterSalesService;

			Sales_Process__c finalAgreementTemplate = new Sales_Process__c(
				Preliminary_Agreement__c = saleTerms.Id,
				Contact__c = productLine.Contact_from_Offer_Line__c,
				Handover_Protocol__c = afterSalesService.Id
			);
			Sales_Process__c finalAgreement = TestHelper.createSalesProcessFinalAgreement(finalAgreementTemplate, true);
			
			Extension__c defectTemplate = new Extension__c(
				Resource_Defect__c = productLine.Offer_Line_Resource__c,
				Handover_Protocol__c = finalAgreement.Id
			);
			Extension__c dChange = TestHelper.createExtensionDefect(defectTemplate, true);

			dChange.Handover_Protocol__c = saleTerms.Id;

			Boolean b = false;
			try {
				update dChange;
			} catch (Exception e){
				List<Apexpages.Message> msgs = ApexPages.getMessages();
				for(Apexpages.Message msg:msgs){
					if (msg.getDetail().contains(Label.CannotModifyReference)) b = true;
				}
			}

		Test.stopTest();

		System.assert(b);		
	}

		/* Mateusz Wolak-Ksiazek */
	@isTest static void testBeforeInsertExtensionChange() {

		Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);

		Sales_Process__c saleTermsTemplate = new Sales_Process__c(
			Offer__c = productLine.Product_Line_to_Proposal__c,
			Contact__c = productLine.Contact_from_Offer_Line__c
		);
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(saleTermsTemplate, true);

		Sales_Process__c afterSalesTemplate = new Sales_Process__c(
			Agreement__c = saleTerms.Id,
			Contact__c = productLine.Contact_from_Offer_Line__c
		);
		Sales_Process__c afterSalesService = TestHelper.createSalesProcessAfterSalesService(afterSalesTemplate, false);


		Test.startTest();

			insert afterSalesService;

			Extension__c changeTemplate = new Extension__c(
				Resource_Flat__c = productLine.Offer_Line_Resource__c,
				After_sales_Service__c = afterSalesService.Id
			);
			Extension__c eChange = TestHelper.createExtensionChange(changeTemplate, false);

			eChange.After_sales_Service__c = saleTerms.Id;
			eChange.Resource__c = productLine.Offer_Line_Resource__c;

			insert eChange;

		Test.stopTest();

		productLine = [
			SELECT Offer_Line_Resource__r.Investment_Flat__c
			FROM Sales_Process__c
			WHERE Id =: productLine.Id
			LIMIT 1
		];

		eChange = [
			SELECT Investment_for_Report__c 
			FROM Extension__c
			WHERE Id =: eChange.Id
		];

		System.assert(eChange.Investment_for_Report__c.contains(productLine.Offer_Line_Resource__r.Investment_Flat__c));

	}

	/* Mateusz Wolak-Ksiazek */
	@isTest static void testBeforeInsertExtensionDefect() {

		Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);

		Sales_Process__c saleTermsTemplate = new Sales_Process__c(
			Offer__c = productLine.Product_Line_to_Proposal__c,
			Contact__c = productLine.Contact_from_Offer_Line__c
		);
		Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(saleTermsTemplate, true);

		Sales_Process__c afterSalesTemplate = new Sales_Process__c(
			Agreement__c = saleTerms.Id,
			Contact__c = productLine.Contact_from_Offer_Line__c
		);
		Sales_Process__c afterSalesService = TestHelper.createSalesProcessAfterSalesService(afterSalesTemplate, false);


		Test.startTest();

			insert afterSalesService;

			
			
			Extension__c defectTemplate = new Extension__c(
				Resource_Defect__c = productLine.Offer_Line_Resource__c,
				RecordtypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENTION_TYPE_HANDOVER_DEFECT)
			);
			Extension__c defect = TestHelper.createExtensionDefect(defectTemplate, false);

			defect.Resource_Defect__c = productLine.Offer_Line_Resource__c;

			insert defect;

		Test.stopTest();

		productLine = [
			SELECT Offer_Line_Resource__r.Investment_Flat__c
			FROM Sales_Process__c
			WHERE Id =: productLine.Id
			LIMIT 1
		];

		defect = [
			SELECT Investment_for_Report__c 
			FROM Extension__c
			WHERE Id =: defect.Id
		];

		System.assert(defect.Investment_for_Report__c.contains(productLine.Offer_Line_Resource__r.Investment_Flat__c));


	}

	@isTest static void testAfterInsertExtensionChangeElement() {

		Integer nrOfItems = 100;

		Extension__c change = TestHelper.createExtensionChange(
            new Extension__c(
            	Status__c = CommonUtility.EXTENSION_STATUS_ACCEPTED,
            	Quotation__c = 1000
            ), 
            true
        );
		List<Resource__c> priceListItems = TestHelper.createResourcePriceListItems(
            new Resource__c(
            ), 
            nrOfItems,
            true
        );

        List<Extension__c> changeElements = new List<Extension__c>();
        Extension__c changeElementTemplate = new Extension__c(
			Change__c = change.Id
		);
        for(Resource__c priceListItem : priceListItems){
        	changeElementTemplate.Price_list_item__c = priceListItem.Id;
			Extension__c changeElement = TestHelper.createExtensionChangeElement(changeElementTemplate, false);
        	changeElements.add(changeElement);
        }


		Test.startTest();

		TestHelper.CreateAreaAndVatPercentageDistribution_CurrentDistributionChanges();

		insert changeElements;

		Test.stopTest();

		//Calculate Quotation
        Decimal quotation = 0;
        for(Extension__c ext : [SELECT Id, Value_of_change__c FROM Extension__c WHERE Change__c = :change.Id]){
        	quotation += ext.Value_of_change__c;
        }
        //Check Quotation
		Extension__c changeAfterUpdate = [SELECT Id, Quotation__c FROM Extension__c WHERE Id = :change.Id];
		System.assertEquals(changeAfterUpdate.Quotation__c, quotation);
	}

	@isTest static void testAfterUpdateExtensionChangeElement() {

		Integer nrOfItems = 100;

		Extension__c change = TestHelper.createExtensionChange(
            new Extension__c(
            ), 
            true
        );

		List<Resource__c> priceListItems = TestHelper.createResourcePriceListItems(
            new Resource__c(
            ), 
            nrOfItems,
            true
        );

        List<Extension__c> changeElements = new List<Extension__c>();
        Extension__c changeElementTemplate = new Extension__c(
			Change__c = change.Id
		);
        for(Resource__c priceListItem : priceListItems){
        	changeElementTemplate.Price_list_item__c = priceListItem.Id;
			Extension__c changeElement = TestHelper.createExtensionChangeElement(changeElementTemplate, false);
        	changeElements.add(changeElement);
        }
        insert changeElements;

		for(Extension__c ext : changeElements){
			ext.Selected__c = false;
		}

		Test.startTest();

		update changeElements;
		

		//Calculate Quotation
        Decimal quotation = 0;
        for(Extension__c ext : [SELECT Id, Value_of_change__c FROM Extension__c WHERE Change__c = :change.Id]){
        	quotation += ext.Value_of_change__c;
        }
        //Check Quotation
        Extension__c changeAfterUpdate = [SELECT Id, Quotation__c FROM Extension__c WHERE Id = :change.Id];

        Test.stopTest();
		System.assertEquals(changeAfterUpdate.Quotation__c, quotation);

	}

		/* Mateusz Wolak-Ksiazek */
	@isTest static void testAfterUpdateExtensionChange() {

		Integer nrOfItems = 100;

		Extension__c change = TestHelper.createExtensionChange(
            new Extension__c(
            	Quotation__c = 1000,
            	Change_taken__c = true,
            	Change_Agreement_Type__c = 'Annex',
            	Last_date_of_changed_change__c  = todayMinus10Days,
            	Comment__c = randomText,
            	Description__c  = randomText,
            	After_sales_Service__c = TestHelper.createSalesProcessAfterSalesService(null, true).Id
            ), 
            true
        );
	
		Test.startTest();
			TestHelper.CreateAreaAndVatPercentageDistribution_CurrentDistributionChanges();

			change.Comment__c = randomText + randomText;
			change.Description__c  = randomText + randomText;
			change.Quotation__c = 1200;
			change.Status__c = CommonUtility.EXTENSION_STATUS_COMPLETED;
			update change;

		Test.stopTest();

        Extension__c changeAfterUpdate = [SELECT Last_date_of_changed_change__c FROM Extension__c WHERE Id = :change.Id];
		
		System.assertNotEquals(changeAfterUpdate.Last_date_of_changed_change__c, change.Last_date_of_changed_change__c);		

	}

	@isTest static void testAfterDeleteExtensionChangeElement() {

		Integer nrOfItems = 100;

		Extension__c change = TestHelper.createExtensionChange(
            new Extension__c(
            	Status__c = CommonUtility.EXTENSION_STATUS_ACCEPTED
            ), 
            true
        );
		List<Resource__c> priceListItems = TestHelper.createResourcePriceListItems(
            new Resource__c(
            ), 
            nrOfItems,
            true
        );

        List<Extension__c> changeElements = new List<Extension__c>();
        Extension__c changeElementTemplate = new Extension__c(
			Change__c = change.Id
		);
        for(Resource__c priceListItem : priceListItems){
        	changeElementTemplate.Price_list_item__c = priceListItem.Id;
			Extension__c changeElement = TestHelper.createExtensionChangeElement(changeElementTemplate, false);
        	changeElements.add(changeElement);
        }
        insert changeElements;


		Test.startTest();

		delete changeElements;
        //Calculate Quotation
        Decimal quotation = 0;
        for(Extension__c ext : [SELECT Id, Value_of_change__c FROM Extension__c WHERE Change__c = :change.Id]){
        	quotation += ext.Value_of_change__c;
        }
        Extension__c changeAfterUpdate = [SELECT Id, Quotation__c FROM Extension__c WHERE Id = :change.Id];

		delete change;

		Test.stopTest();

        //Check Quotation
		System.assertEquals(changeAfterUpdate.Quotation__c, quotation);

	}

}