public without sharing class TH_Resource extends TriggerHandler.DelegateBase {

   /* public List<Resource__c> resourceWithOwnerLandlord;*/
    public List<Resource__c> resourcesToUpdateVatRates; 
    public List<Resource__c> copyResourcesInvestment;
    public List<Resource__c> externalLinesToInsert;
    public List<Resource__c> potentialDuplicates;
    public List<Resource__c> retriveBuildings;

    public Map<String, List<Resource__c>> resourcesWithDefinedPropomotion2calculatePrices;

    public List<List<String>> parsedCSV;
    public Set<Id> resourceIds2UpdateSalesProcessBindFields;

    /* Mateusz Wolak-Ksiazek
    inserting/updating land surface on builds, flats, parking spaces etc.
    whenever land surface on stage is updated/created*/
    public Map<String, List<Resource__c>> resourceLandSurface; //after update - after updating field land_surface on stage
    public Map<Id, List<Resource__c>> resourceLandSurfaceAfterUpdatingStageField; //after update - after updating stage field on resource
    public Map<Id, List<Resource__c>> resourceLandSurfaceBeforeInsert; //before insert - before inserting resource to datebase it fills the land surface field
    public Map<Id, List<Resource__c>> resourceLandSurfaceWithNullStage;
    public Map<Id, List<Resource__c>> resourceWithConnectedCollectiveSpace;
    public Map<Id, Resource__c> resourceAfterUpdatingCollectiveSpace; // after update - after updating collective
    public Map<Id, List<Resource__c>> resourceWithConnectedCollectiveSpaceBeforeInsert; //before inserting resource to datebase it fills the common surface from collective space
    public Map<Id, Resource__c> afterUpdateBankAccountChangedToChangeAccountInProcess;
    /* Mateusz Wolak-Ksiazek 
    cummulating area of all resources and putting it in field - common_surface__c */
    public Map<Id, List<Resource__c>> resourcesAreaOnAfter;

    public Set<String> invalidStatuses;
    public Set<Id> validResourcesId;

    public boolean authData;

    public String profileName;

    //xxx-semeko
    public Map<Id, Resource__c> resourceIdsToProcessMeasurementChangesMap;

    /* Mateusz Pruszyński */
    // List of inserted/updated Resources - to check their uniqueness (by name field)
    public Map<Id,Resource__c> resourcesToRestrictUniqueness;

    public override void prepareBefore(){
            Id profileId = userinfo.getProfileId();
            profileName = [Select Id,Name from Profile where Id=:profileId].Name;
         /*   resourceWithOwnerLandlord = new List<Resource__c>();*/
         /*   ownerLandlord = new List<Id>();*/
            resourceLandSurfaceBeforeInsert = new Map<Id, List<Resource__c>>();
            resourceWithConnectedCollectiveSpaceBeforeInsert = new Map<Id, List<Resource__c>>();
            invalidStatuses = new Set<String>{CommonUtility.RESOURCE_STATUS_SOLD, CommonUtility.RESOURCE_STATUS_SOLD_FINAL_AGR, CommonUtility.RESOURCE_STATUS_SOLD_TRANSFERED};
            resourcesToRestrictUniqueness = new Map<Id,Resource__c>();
            resourcesToUpdateVatRates = new List<Resource__c>();
            copyResourcesInvestment = new List<Resource__c>();
            resourcesWithDefinedPropomotion2calculatePrices = new Map<String, List<Resource__c>>();
    }

    public override void prepareAfter(){
        //create External Lines (with data from static resource) for new Resources (investments)
        parsedCSV = new List<List<String>>();
        externalLinesToInsert = new List<Resource__c>();
        List<StaticResource> srAuthData = [ SELECT id, Body From StaticResource WHERE Name = 'ExternalPortalAuthData'];
        if (srAuthData.size() == 1) {
            string srBody = srAuthData[0].Body.toString();
            parsedCSV = CommonUtility.parseCSV(srBody, false);
            authData = true;
        } else {
            authData = false;
        }
       /* resourcesToIntegrationServiceUpdate = new List<Id>();*/
       /* resourcesToIntegrationServiceDelete = new List<Id>();*/
        resourceLandSurface = new Map<String, List<Resource__c>>();
        resourceLandSurfaceAfterUpdatingStageField = new Map<Id, List<Resource__c>>();
        resourceLandSurfaceWithNullStage = new Map<Id, List<Resource__c>>();
        resourcesAreaOnAfter = new Map<Id, List<Resource__c>>();
        resourceWithConnectedCollectiveSpace = new Map<Id, List<Resource__c>>();
        resourceAfterUpdatingCollectiveSpace = new Map<Id, Resource__c>();
        retriveBuildings = new List<Resource__c>();
        validResourcesId = new Set<Id>{CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE),
            CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY),
            CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE),
            CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_HOUSE),
            CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
        };
        
        //xxx-semeko
        resourceIdsToProcessMeasurementChangesMap = new Map<Id, Resource__c>();
        afterUpdateBankAccountChangedToChangeAccountInProcess = new Map<Id, Resource__c>(); 

        resourceIds2UpdateSalesProcessBindFields = new Set<Id>();

    }


    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o){
        Map<Id, Resource__c> resOld = (Map<Id, Resource__c>)old;
        Map<Id, Resource__c> resNew = (Map<Id, Resource__c>)o;
        for(Id key : resNew.keySet()){
            Resource__c resN = resNew.get(key);
            Resource__c resO = resOld.get(key);

            /* Mateusz Pruszyński */
            // fill Investment__c field
            copyResourcesInvestment.add(resN);

            /* Mateusz Pruszyński */
            // Only admin can edit construction number
            //The part of the code is commented to solve the task PS-116
            /*if(resN.Name != resO.Name && userinfo.getProfileId() != CommonUtility.getProfileId(Label.SystemAdministrator)) {
                resN.addError(Label.CannotEditConstructionNumber);
            }*/

            //disallow editing SOLD resources if user is not SysAdmin
            //if (String.isNotBlank(resO.Status__c) && invalidStatuses.contains(resO.Status__c) && profileName != Label.SystemAdministrator) {
                //resN.addError(Label.ErrorCannotModifyRecordOtherThanActive);
            //}

            
            /* Mateusz Pruszyński */
            // Check Resources uniqueness (by name field)
            if (resO.Name != resN.Name || resO.Investment__c != resN.Investment__c) {
                if(String.isNotBlank(resN.Investment__c)) resourcesToRestrictUniqueness.put(resN.Investment__c, resN);
            }

            /*
            / Code part made by Mateusz Pruszynski
            / Trigger part that sets "Property Owner/Landlord as Company" field to true
            */
/*            if(resN.Property_Owner_Landlord__c != null){ 
                if(resN.Property_Owner_Landlord__c != resO.Property_Owner_Landlord__c){
                    resourceWithOwnerLandlord.add(resN);
                    ownerLandlord.add(resN.Property_Owner_Landlord__c);
                }
            } */
            
            /* Mateusz Pruszyński */
            // Trigger part to update low and high vat rates
            if((resN.Total_Area_Planned__c != null && resN.Total_Area_Planned__c != resO.Total_Area_Planned__c) || (resN.Usable_Area_Planned__c != null && resN.Usable_Area_Planned__c != resO.Usable_Area_Planned__c)) {
                resourcesToUpdateVatRates.add(resN);
            } 

            /* Mateusz Pruszyński */
            // method to recalculate promotional price on Resource after adding lookup to defined promotion
            if(resN.Defined_Promotion__c != resO.Defined_Promotion__c 
                && (
                    resN.Status__c != CommonUtility.RESOURCE_STATUS_SOLD_RESERVATION_AGREEMENT
                    && resN.Status__c != CommonUtility.RESOURCE_STATUS_SOLD_TRANSFERED
                    && resN.Status__c != CommonUtility.RESOURCE_STATUS_SOLD_FINAL_AGR
                    && resN.Status__c != CommonUtility.RESOURCE_STATUS_SOLD
                )
            ) { // CALCULATE

                if(String.isNotBlank(resN.Defined_Promotion__c)) {
                    if(!resourcesWithDefinedPropomotion2calculatePrices.containsKey(String.valueOf(resN.Defined_Promotion__c).left(15))) {
                        resourcesWithDefinedPropomotion2calculatePrices.put(
                            String.valueOf(resN.Defined_Promotion__c).left(15),
                            new Resource__c[] {resN}
                        );
                    } else {
                        resourcesWithDefinedPropomotion2calculatePrices.get(String.valueOf(resN.Defined_Promotion__c).left(15)).add(resN);
                    }

                } else {

                    if(!resourcesWithDefinedPropomotion2calculatePrices.containsKey(ManageDefinedPromotions.DEFINED_PROMO_NULL)) {
                        resourcesWithDefinedPropomotion2calculatePrices.put(ManageDefinedPromotions.DEFINED_PROMO_NULL, new Resource__c[] {resN});
                    } else {
                        resourcesWithDefinedPropomotion2calculatePrices.get(ManageDefinedPromotions.DEFINED_PROMO_NULL).add(resN);
                    }

                }

            } else if(resN.Defined_Promotion__c != resO.Defined_Promotion__c
                && (
                    resN.Status__c == CommonUtility.RESOURCE_STATUS_SOLD_RESERVATION_AGREEMENT
                    || resN.Status__c == CommonUtility.RESOURCE_STATUS_SOLD_TRANSFERED
                    || resN.Status__c == CommonUtility.RESOURCE_STATUS_SOLD_FINAL_AGR
                    || resN.Status__c == CommonUtility.RESOURCE_STATUS_SOLD
                )                
            ) { // VALIDATE
                
                if(!resourcesWithDefinedPropomotion2calculatePrices.containsKey(ManageDefinedPromotions.DEFINED_PROMO_ADD_VALIDATION_ERROR)) {
                    resourcesWithDefinedPropomotion2calculatePrices.put(ManageDefinedPromotions.DEFINED_PROMO_ADD_VALIDATION_ERROR, new Resource__c[] {resN});
                } else {
                    resourcesWithDefinedPropomotion2calculatePrices.get(ManageDefinedPromotions.DEFINED_PROMO_ADD_VALIDATION_ERROR).add(resN);
                }

            }                                     
        }
    }


    public override void beforeInsert(List<sObject> o ){
        List<Resource__c> res = (List<Resource__c>)o;
        for(Resource__c resT : res){
            
            /* Mateusz Pruszyński */
            // fill Investment__c field
            copyResourcesInvestment.add(resT);

            //Allow to insert records only with active or inactive status

            /* Mateusz Pruszyński */
            // Check Resources uniqueness (by name field)
            if(String.isNotBlank(resT.Investment__c)) resourcesToRestrictUniqueness.put(resT.Investment__c, resT);

            /*
            / Code part made by Mateusz Pruszynski
            / Trigger part that sets "Property Owner/Landlord as Company" field to true
            */
/*            if(resT.Property_Owner_Landlord__c != null){ 
                resourceWithOwnerLandlord.add(resT);
                ownerLandlord.add(resT.Property_Owner_Landlord__c);
            } */
            
            /* Mateusz Pruszyński */
            // Trigger part to update low and high vat rates
            if(resT.Total_Area_Planned__c != null || resT.Usable_Area_Planned__c != null) {
                resourcesToUpdateVatRates.add(resT);
            }

            /* Mateusz Wolak-Ksiazek */
            // Updating field Common_Surface__c on resource(if stage is connected)
            if(resT.Stage__c != null && (resT.Collective_Space_parking__c == null && resT.Collective_Space_storage__c == null ) && ( 
                    resT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
                    || resT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
                    || resT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)
                    || resT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_HOUSE)
                )
            ) {
                if(resourceLandSurfaceBeforeInsert.containsKey(resT.Stage__c)) {
                    resourceLandSurfaceBeforeInsert.get(resT.Stage__c).add(resT);
                } else {
                    resourceLandSurfaceBeforeInsert.put(resT.Stage__c, new List<Resource__c> {resT});
                }
            }

            if( (resT.Collective_Space_parking__c != null || resT.Collective_Space_storage__c != null) && (
                     resT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
                    || resT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)
                )
            ) {
                if(resT.Collective_Space_parking__c != null ) {
                    if(resourceWithConnectedCollectiveSpaceBeforeInsert.containsKey(resT.Collective_Space_parking__c)) {
                        resourceWithConnectedCollectiveSpaceBeforeInsert.get(resT.Collective_Space_parking__c).add(resT);
                    } else {
                        resourceWithConnectedCollectiveSpaceBeforeInsert.put(resT.Collective_Space_parking__c, new List<Resource__c> {resT});
                    } 
                }
                if(resT.Collective_Space_storage__c != null ) {
                    if(resourceWithConnectedCollectiveSpaceBeforeInsert.containsKey(resT.Collective_Space_storage__c)) {
                        resourceWithConnectedCollectiveSpaceBeforeInsert.get(resT.Collective_Space_storage__c).add(resT);
                    } else {
                        resourceWithConnectedCollectiveSpaceBeforeInsert.put(resT.Collective_Space_storage__c, new List<Resource__c> {resT});
                    } 
                }
            }

            /* Mateusz Pruszyński */
            // method to recalculate promotional price on Resource after adding lookup to defined promotion
            if(resT.Defined_Promotion__c != null 
                && (
                    resT.Status__c != CommonUtility.RESOURCE_STATUS_SOLD_RESERVATION_AGREEMENT
                    && resT.Status__c != CommonUtility.RESOURCE_STATUS_SOLD_TRANSFERED
                    && resT.Status__c != CommonUtility.RESOURCE_STATUS_SOLD_FINAL_AGR
                    && resT.Status__c != CommonUtility.RESOURCE_STATUS_SOLD
                )                
            ) { // CALCULATE

                if(String.isNotBlank(resT.Defined_Promotion__c)) {
                    
                    if(!resourcesWithDefinedPropomotion2calculatePrices.containsKey(String.valueOf(resT.Defined_Promotion__c).left(15))) {
                        resourcesWithDefinedPropomotion2calculatePrices.put(
                            String.valueOf(resT.Defined_Promotion__c).left(15),
                            new Resource__c[] {resT}
                        );
                    } else {
                        resourcesWithDefinedPropomotion2calculatePrices.get(String.valueOf(resT.Defined_Promotion__c).left(15)).add(resT);
                    }

                } else {

                    if(!resourcesWithDefinedPropomotion2calculatePrices.containsKey(ManageDefinedPromotions.DEFINED_PROMO_NULL)) {
                        resourcesWithDefinedPropomotion2calculatePrices.put(ManageDefinedPromotions.DEFINED_PROMO_NULL, new Resource__c[] {resT});
                    } else {
                        resourcesWithDefinedPropomotion2calculatePrices.get(ManageDefinedPromotions.DEFINED_PROMO_NULL).add(resT);
                    }

                }

            } else if(resT.Defined_Promotion__c != null 
                && (
                    resT.Status__c == CommonUtility.RESOURCE_STATUS_SOLD_RESERVATION_AGREEMENT
                    || resT.Status__c == CommonUtility.RESOURCE_STATUS_SOLD_TRANSFERED
                    || resT.Status__c == CommonUtility.RESOURCE_STATUS_SOLD_FINAL_AGR
                    || resT.Status__c == CommonUtility.RESOURCE_STATUS_SOLD
                )                
            ) { // VALIDATE
                
                if(!resourcesWithDefinedPropomotion2calculatePrices.containsKey(ManageDefinedPromotions.DEFINED_PROMO_ADD_VALIDATION_ERROR)) {
                    resourcesWithDefinedPropomotion2calculatePrices.put(ManageDefinedPromotions.DEFINED_PROMO_ADD_VALIDATION_ERROR, new Resource__c[] {resT});
                } else {
                    resourcesWithDefinedPropomotion2calculatePrices.get(ManageDefinedPromotions.DEFINED_PROMO_ADD_VALIDATION_ERROR).add(resT);
                }

            }             
        }
            
    }

    public override void afterInsert(Map<Id, sObject> o){
        Map<Id, Resource__c> res = (Map<Id, Resource__c>)o;
        for(Id key : res.keySet()){
            Resource__c resT = res.get(key);

            //create External Lines (with data from static resource) for new Resources (investments)
            Id invId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT);
            Id externalLineId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_EXTERNAL_LINE);

            if (resT.recordTypeId == invId && authData) {
                for(List<string> line:parsedCSV) {
                    Resource__c tempRes = new Resource__c();
                    tempRes.Name__c = line[0];
                    tempRes.Status__c = 'Active';
                    tempRes.Resource_EL__c = resT.Id;
                    tempRes.RecordTypeId = externalLineId;
                    externalLinesToInsert.add(tempRes);
                }
            }

            /* Mateusz Wolak-Ksiazek */
            // updating field common surface on stage
            if(resT.Usable_Area_Planned__c != null && resT.Stage__c != null) {
                if(resT.Collective_Space_parking__c == null && resT.Collective_Space_storage__c == null ) {
                   if(resourcesAreaOnAfter.containsKey(resT.Stage__c)) {
                        resourcesAreaOnAfter.get(resT.Stage__c).add(resT);
                    } else {
                        resourcesAreaOnAfter.put(resT.Stage__c, new List<Resource__c> {resT});
                    } 
                }
            }        

        }
    }

    public override void afterUpdate(Map<Id, sObject>old, Map<Id, sObject> o){
        Map<Id, Resource__c> resOld = (Map<Id, Resource__c>)old;
        Map<Id, Resource__c> res = (Map<Id, Resource__c>)o;

        for(Id key : res.keySet()){
            Resource__c resN = res.get(key);
            Resource__c resO = resOld.get(key);
            
            if(resN.Usable_Area_Planned__c != resO.Usable_Area_Planned__c && resN.Stage__c != null) {
                if(resN.Collective_Space_parking__c == null && resN.Collective_Space_storage__c == null) {
                   if(resourcesAreaOnAfter.containsKey(resN.Stage__c)) {
                        resourcesAreaOnAfter.get(resN.Stage__c).add(resN);
                    } else {
                        resourcesAreaOnAfter.put(resN.Stage__c, new List<Resource__c> {resN});
                    } 
                }
            }
            /* Mateusz Wolak-Ksiazek */
            // whenever field common_surface on stage is updated, updated values on all resources connected to this stage
            if(resN.Common_Surface__c != resO.Common_Surface__c && resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE) && resN.Investment_stage__c != null) {
                if(resourceLandSurface.containsKey(resN.Investment_stage__c)) {
                    resourceLandSurface.get(resN.Investment_stage__c).add(resN);
                } else {
                    resourceLandSurface.put(String.valueOf(resN.Investment_stage__c).LEFT(15), new List<Resource__c> {resN});
                }
            }
            //after updating stage field on resource, get the new's stage common_surface
            if(resN.Stage__c != resO.Stage__c && resN.Stage__c != null && resN.InvestmentId__c != null && ( 
                    resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
                    || resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
                    || resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)
                    || resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_HOUSE)
                    || resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COLLECTIVE_SPACE)
                    )
            ) {
                if(resourceLandSurfaceAfterUpdatingStageField.containsKey(resN.Stage__c)) {
                    resourceLandSurfaceAfterUpdatingStageField.get(resN.Stage__c).add(resN);
                } else {
                    resourceLandSurfaceAfterUpdatingStageField.put(resN.Stage__c, new List<Resource__c> {resN});
                }
            }

            if(resN.Stage__c != resO.Stage__c && resN.Stage__c == null && resN.Collective_Space_parking__c == null && resN.Collective_Space_storage__c == null) {
                if(resourceLandSurfaceWithNullStage.containsKey(resN.Stage__c)) {
                    resourceLandSurfaceWithNullStage.get(resN.Stage__c).add(resN);
                } else {
                    resourceLandSurfaceWithNullStage.put(resN.Stage__c, new List<Resource__c> {resN});
                }

            }

            if( (resN.Collective_Space_parking__c != resO.Collective_Space_parking__c || resN.Collective_Space_storage__c != resO.Collective_Space_storage__c) && ( 
                    resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
                    || resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)
                    ) 
                ){
                if(resN.Collective_Space_parking__c != null) {
                    if(resourceWithConnectedCollectiveSpace.containsKey(resN.Collective_Space_parking__c)) {
                        resourceWithConnectedCollectiveSpace.get(resN.Collective_Space_parking__c).add(resN);
                    } else {
                        resourceWithConnectedCollectiveSpace.put(resN.Collective_Space_parking__c, new List<Resource__c> {resN});
                    }
                }
                if(resN.Collective_Space_storage__c != null ) {
                    if(resourceWithConnectedCollectiveSpace.containsKey(resN.Collective_Space_storage__c)) {
                        resourceWithConnectedCollectiveSpace.get(resN.Collective_Space_storage__c).add(resN);
                    } else {
                        resourceWithConnectedCollectiveSpace.put(resN.Collective_Space_storage__c, new List<Resource__c> {resN});
                    }   
                }
                if(resN.Stage__C != null) {
                    if(resourcesAreaOnAfter.containsKey(resN.Stage__c)) {
                    resourcesAreaOnAfter.get(resN.Stage__c).add(resN);
                } else {
                    resourcesAreaOnAfter.put(resN.Stage__c, new List<Resource__c> {resN});
                }
                }
            }

            if(resN.Stage__c != resO.Stage__c && resN.Usable_Area_Planned__c != null) {
                if(resN.Stage__c != null ) {
                    if(resourcesAreaOnAfter.containsKey(resN.Stage__c)) {
                        resourcesAreaOnAfter.get(resN.Stage__c).add(resN);
                    } else {
                        resourcesAreaOnAfter.put(resN.Stage__c, new List<Resource__c> {resN});
                    }
                }
                if(resO.Stage__c != null ) {
                    if(resourcesAreaOnAfter.containsKey(resO.Stage__c)) {
                        resourcesAreaOnAfter.get(resO.Stage__c).add(resN);
                    } else {
                        resourcesAreaOnAfter.put(resO.Stage__c, new List<Resource__c> {resN});
                    }
                }
            }

            if(resN.Usable_Area_Planned__c != resO.Usable_Area_Planned__c && resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COLLECTIVE_SPACE)) {
                if(!resourceAfterUpdatingCollectiveSpace.containsKey(resN.Id)) {
                    resourceAfterUpdatingCollectiveSpace.put(resN.Id, resN);
                }
            }

            //xxx - semeko
            /* Mateusz Pruszyński */
            // manage changes in sales process and payment schedule for accepted measurement changes
            if(resN.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED 
                && resO.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_DIRECTED_FOR_APPROVAL
                && (resN.Usable_Area_Planned__c != null || resN.Total_Area_Planned__c != null)
                && resN.Total_Area_Measured__c != null) {

                resourceIdsToProcessMeasurementChangesMap.put(resN.Id, resN);
            }  

            /* Mateusz Pruszynski */
            // copy bank account from Resource to sales process and payments 
            // if bank account on resource gets null, it sould become null on sp and schedule as well
            if(resN.Bank_Account_Number__c != resO.Bank_Account_Number__c) {
                afterUpdateBankAccountChangedToChangeAccountInProcess.put(resN.Id, resN);
            }   

            /* Mateusz Pruszynski */
            // update bind fields on SP
            if(
                (
                    resN.With_Storage__c != resO.With_Storage__c
                    || resN.With_Parking_Space__c != resO.With_Parking_Space__c
                ) && (
                    resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT) 
                    || resN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY)
                )
            ) {
                resourceIds2UpdateSalesProcessBindFields.add(resN.Id);
            }                         
        }
    }

    public override void afterDelete(Map<Id, sObject> old) {
        Map<Id, Resource__c> resOld = (Map<Id, Resource__c>)old;

        for(Id key : resOld.keySet()){
            Resource__c resO = resOld.get(key);

            /* Mateusz Wolak-Ksiazek */
            //after deleting resource, recalculate the common_surface field on stage
            if(resO.Usable_Area_Planned__c != null && resO.Stage__c != null) {
                if(resO.Collective_Space_parking__c == null && resO.Collective_Space_storage__c == null) {
                    if(resourcesAreaOnAfter.containsKey(resO.Stage__c)) {
                        resourcesAreaOnAfter.get(resO.Stage__c).add(resO);
                    } else {
                        resourcesAreaOnAfter.put(resO.Stage__c, new List<Resource__c> {resO});
                    }
                }
            }

        }

    }



    public override void finish(){

        /* Mateusz Pruszyński */
        // fill Investment__c field
        if(copyResourcesInvestment != null && copyResourcesInvestment.size() > 0) {
            CommonUtility.makeInvestmentIdForResources(copyResourcesInvestment);
        }

        /* Mateusz Pruszyński */
        // Trigger part to update low and high vat rates        
        if(resourcesToUpdateVatRates != null && resourcesToUpdateVatRates.size() > 0) {
            ResourceManager.updateVatRates(resourcesToUpdateVatRates);
        }
        
        /* Mateusz Pruszyński */
        // update resources for developer portal integration    
/*        // - publish/update
        if (resourcesToIntegrationServiceUpdate != null && resourcesToIntegrationServiceUpdate.size() > 0) {
            //IntegrationService.updateAppartmentsAtFuture(resourcesToIntegrationServiceUpdate);                 
        }*/
/*        // - remove/delete
        if (resourcesToIntegrationServiceDelete != null && resourcesToIntegrationServiceDelete.size() > 0) {
            //IntegrationService.removeAppartmentsAtFuture(resourcesToIntegrationServiceDelete);                 
        }*/

        if(externalLinesToInsert != null && externalLinesToInsert.size()>0){
            try {
                insert externalLinesToInsert;
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }

        /*
        / Code part made by Mateusz Pruszynski
        / Trigger part that sets "Property Owner/Landlord as Company" field to true
        */
/*        if(resourceWithOwnerLandlord != null && resourceWithOwnerLandlord.size() > 0){
            if(ownerLandlord != null && ownerLandlord.size() > 0){
                Id companyId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_TYPE_COMPANY);
                Map<Id, Contact> companyContacts = new Map<Id, Contact>([SELECT RecordTypeId FROM Contact WHERE Id in: ownerLandlord]);
                for(Resource__c res : resourceWithOwnerLandlord){
                    Contact owner = companyContacts.get(res.Property_Owner_Landlord__c);
                    if(owner != null && owner.RecordTypeId == companyId){
                        res.Property_Owner_Company__c = true;
                    }
                }   
            }
        }*/

        /* Mateusz Pruszyński */
        // Check Resources uniqueness (by name field) 
        if(resourcesToRestrictUniqueness != null && resourcesToRestrictUniqueness.size() > 0) {
            ResourceManager.checkResourcesUniqueness(resourcesToRestrictUniqueness);
        }

        if(resourcesAreaOnAfter != null && resourcesAreaOnAfter.size() > 0) {
            ResourceManager.checkResourcesAreaOnAfter(resourcesAreaOnAfter);
        }

        if(resourceWithConnectedCollectiveSpace != null && resourceWithConnectedCollectiveSpace.size() > 0) {
            ResourceManager.checkResourceWithConnectedCollectiveSpace(resourceWithConnectedCollectiveSpace);
        }

        if(resourceAfterUpdatingCollectiveSpace != null && resourceAfterUpdatingCollectiveSpace.size() > 0) {
            ResourceManager.checkResourceAfterUpdatingCollectiveSpace(resourceAfterUpdatingCollectiveSpace);
        }

        if(resourceLandSurfaceAfterUpdatingStageField != null && resourceLandSurfaceAfterUpdatingStageField.size() > 0) {
            ResourceManager.checkResourceLandSurfaceAfterUpdatingStageField(resourceLandSurfaceAfterUpdatingStageField);
        }
   
        if(resourceLandSurface != null && resourceLandSurface.size() > 0) {
            ResourceManager.checkResourceLandSurface(resourceLandSurface);
        }

        if(resourceLandSurfaceBeforeInsert != null && resourceLandSurfaceBeforeInsert.size() > 0) {
            ResourceManager.checkResourceLandSurfaceBeforeInsert(resourceLandSurfaceBeforeInsert);
        }


        if(resourceWithConnectedCollectiveSpaceBeforeInsert != null && resourceWithConnectedCollectiveSpaceBeforeInsert.size() > 0) {
            ResourceManager.checkResourceWithConnectedCollectiveSpaceBeforeInsert(resourceWithConnectedCollectiveSpaceBeforeInsert);
        }    


        if(resourceLandSurfaceWithNullStage != null && resourceLandSurfaceWithNullStage.size() > 0) {
            ResourceManager.checkResourceLandSurfaceWithNullStage(resourceLandSurfaceWithNullStage);
        }

        //xxx - semeko
        /* Mateusz Pruszyński */
        // manage changes in sales process and payment schedule for accepted measurement changes
        if(resourceIdsToProcessMeasurementChangesMap != null && resourceIdsToProcessMeasurementChangesMap.size() > 0) {
            // Execute batch apex
            Database.executeBatch(new MeasurementsAcceptanceProcessBatch(resourceIdsToProcessMeasurementChangesMap));
        }

        /* Mateusz Pruszynski */
        // copy bank account from Resource to sales process and payments 
        if(afterUpdateBankAccountChangedToChangeAccountInProcess != null && afterUpdateBankAccountChangedToChangeAccountInProcess.size() > 0) {
            ManageBankAccountForSalesProcess.copyBankAccountToSalesProcessAndPayments(afterUpdateBankAccountChangedToChangeAccountInProcess);
        }

        /* Mateusz Pruszyński */
        // method to recalculate promotional price on Resource after adding lookup to defined promotion
        if(resourcesWithDefinedPropomotion2calculatePrices != null && resourcesWithDefinedPropomotion2calculatePrices.size() > 0) {
            ManageDefinedPromotions.updatePromotionPriceOnResource(resourcesWithDefinedPropomotion2calculatePrices);
        }

        /* Mateusz Pruszynski */
        // update bind fields on SP
        if(resourceIds2UpdateSalesProcessBindFields != null && resourceIds2UpdateSalesProcessBindFields.size() > 0) {
            ManageBindProducts.updateSaleTermsBindingFields(null, resourceIds2UpdateSalesProcessBindFields);
        }   
             
    }

}