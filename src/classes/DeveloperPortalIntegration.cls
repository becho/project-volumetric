/**
* @author       Maciej Jóźwiak
* @editor       Mateusz Pruszyński
* @description  class used for the mapping data from SF to requests to the Developer Portal 
*/

global class DeveloperPortalIntegration {

    static List<StaticResource> srAuthData = [ SELECT id, Body From StaticResource WHERE Name = 'DeveloperPortalAuthData'];
    static string devPortaAccessKey = ''; 
    static string devPortalUsername = '';
    static string devPortalPassword = '';
    static boolean authDataExists = false;
    static DeveloperPortalESB devPortal;
    static DeveloperPortalESB.PortalWebServicePort devPortalService;

    /*All variables below are used only within this class, and are sent to ESB as values, thus they do not need to be translated, nor referenced from CommonUtility.*/
    public static string SUCCESS = 'SUCCESS';
    public static string AVAILABLE_UPPER_CASE = 'AVAILABLE';
    public static string AVAILABLE_LOWER_CASE = 'Available';
    public static string OPTIONAL = 'OPTIONAL';
    public static string ACTIVE = 'Active';
    public static string RESERVED = 'RESERVED';
    public static string INVESTMENT_TYPE = 'INVESTMENT';
    public static string APPARTMENT = 'APARTMENT';
    public static string ADJUSTED = 'Adjusted';
    public static string YES = 'Yes';
    public static string NO = 'NO';
    public static string OBLIGATORY_UPPER_CASE = 'OBLIGATORY';
    public static string OBLIGATORY_LOWER_CASE = 'Obligatory';
    public static string ACCOUNT_TYPE_PROPERTY_BROKER = 'Property Broker';
    public static string STATUS_COMPLETED = 'Completed';

    public class MyException extends Exception{}  

    public DeveloperPortalIntegration() {
        getAuthData();
    }

    public static void getAuthData(){
        if (srAuthData.size() == 1) {
            string srBody = srAuthData[0].Body.toString();
            string[] authData = srBody.split(';', 4);
            devPortaAccessKey = authData[0];
            devPortalUsername = authData[1];
            devPortalPassword = authData[2];
            devPortal = new DeveloperPortalESB();
            devPortalService = new DeveloperPortalESB.PortalWebServicePort();
            authDataExists = true;
        }       
    }

    // sending investment
    public string updateInvestment(Id investmentId) {
        Resource__c investment = [ SELECT 
                                    Id, Website__c, Status__c, Country__c, City__c, Street__c, House_number__c, ZIP_code__c, Name,
                                    Planned_Construction_End_Date__c, Building_Permit_Authorization_Date__c, Building_Permit_Number__c,
                                    Land_Ownership_Type__c, Program__c, Development_Act__c, Other__c, Sale_Begin_Date__c,
                                    Heating__c, Building_Material__c, Flats__c, Number_of_Flats_on_the_Floor__c,
                                    Commercial_Properties__c, Storages__c, Storages_Average_Price__c, Parking_Spaces_Garages__c,
                                    Parking_Space_Average_Price__c, Number_of_Elevators_in_Building__c, 
                                    Gated_Community__c, Monitoring__c, Building_Adapted_for_the_Disabled__c, Balconies_Terraces__c,
                                    Garden__c, Playground__c, Public_Parking__c, Description__c, Distance_from_Underground_Station__c,
                                    Distance_from_Tram_Stop__c, Distance_from_Bus_Stop__c, Distance_from_Train_Station__c, Distance_from_Green_Areas__c,
                                    Distance_from_Recreational_Areas__c, Distance_from_School__c, Distance_from_Shops_Markets__c
                                                FROM Resource__c 
                                                WHERE Id = :investmentId LIMIT 1 ];

        if (investment != null) {
            Set<Id> resourceIds = new Set<Id>();
            resourceIds.add(investment.Id);
            
            Map<Id, Metadata__c> files = new Map<Id, Metadata__c>([ SELECT Id, RecordID__c, Metadata__c FROM Metadata__c WHERE ObjectID__c = :CommonUtility.SOBJECT_NAME_RESOURCE AND RecordID__c IN :resourceIds ]);
            List<string> metaDatas = new List<string>();    
    
            // create investment data
            DeveloperPortalESB.investmentData investmentData = new DeveloperPortalESB.investmentData();
            investmentData.externalId = investment.Id;
            investmentData.developerExternalId = UserInfo.getOrganizationId();
            investmentData.websiteUrl = investment.Website__c;
            investmentData.status = investment.Status__c;
            investmentData.country = investment.Country__c;
            investmentData.city = investment.City__c;
            investmentData.street = investment.Street__c;
            if (investment.House_number__c == null) {
                investmentData.streetNumber = CommonUtility.EMPTY_PART0;
            }
            else { 
                investmentData.streetNumber = investment.House_number__c;
            }
            investmentData.postalCode = investment.ZIP_code__c;
            investmentData.name = investment.Name;
            investmentData.completionDate = investment.Planned_Construction_End_Date__c;
            investmentData.salesStartDate = investment.Sale_Begin_Date__c;
            investmentData.buildingPermissionDate = investment.Building_Permit_Authorization_Date__c;
            investmentData.buildingPermissionNumber = investment.Building_Permit_Number__c;
            investmentData.landPosessionType = investment.Land_Ownership_Type__c;
            investmentData.program = investment.Program__c;
            investmentData.subjectOfRegulation = investment.Development_Act__c;
            investmentData.heatingType = investment.Heating__c;
            investmentData.buildingMaterial = investment.Building_Material__c;
            
            investmentData.metroStationDistance = investment.Distance_from_Underground_Station__c;
            investmentData.tramStopDistance = investment.Distance_from_Tram_Stop__c;
            investmentData.busStopDistance = investment.Distance_from_Bus_Stop__c;
            investmentData.trainStationDistance = investment.Distance_from_Train_Station__c;
            investmentData.greenAreasDistance = investment.Distance_from_Green_Areas__c;
            investmentData.recreationAreasDistance = investment.Distance_from_Recreational_Areas__c;
            investmentData.schoolDistance = investment.Distance_from_School__c;
            investmentData.shoppingDistance = investment.Distance_from_Shops_Markets__c;
            
            investmentData.neighbourhoodDescription = investment.Other__c;
            investmentData.numberOfApartments = integer.valueOf(investment.Flats__c);
            investmentData.averageNumberOfApartmentsPerFloor = integer.valueOf(investment.Number_of_Flats_on_the_Floor__c);
            investmentData.numberOfPremises = integer.valueOf(investment.Commercial_Properties__c);
            investmentData.numberOfStorageBoxes = integer.valueOf(investment.Storages__c);
            investmentData.averagePricePerStorageBox = investment.Storages_Average_Price__c;
            investmentData.numberOfParkingPlaces = integer.valueOf(investment.Parking_Spaces_Garages__c);
            investmentData.averagePricePerParkingPlace = investment.Parking_Space_Average_Price__c;
            investmentData.numberOfElevators = integer.valueOf(investment.Number_of_Elevators_in_Building__c);
            investmentData.fenced = investment.Gated_Community__c;
            investmentData.monitored = investment.Monitoring__c;
            investmentData.accessibleForDisabled = investment.Building_Adapted_for_the_Disabled__c;
            investmentData.balconiesAvailable = investment.Balconies_Terraces__c;
            investmentData.gardensAvailable = investment.Garden__c;
            investmentData.playgroundsAvailable = investment.Playground__c;
            investmentData.sharedParkingAvailable = investment.Public_Parking__c;
            investmentData.description = investment.Description__c;

            metaDatas.clear();
            for (Metadata__c f: files.values()) {
                if (f.RecordID__c == investment.Id) {
                    metaDatas.add(f.Metadata__c);
                }
            }
            investmentData.metaData = metaDatas;
            string result;
            if(Test.isRunningTest() == false){
                try {
                    result = devPortalService.updateInvestment(UserInfo.getOrganizationId(), devPortaAccessKey, devPortalUsername, devPortalPassword, investmentData);
                } catch(Exception e) {
                    ErrorLogger.log(e);
                }
            }
            else{
               result = SUCCESS; 
            }

            if (result == SUCCESS) {
                Resource__c resUpdate = new Resource__c();
                resUpdate.Id = investment.Id;
                resUpdate.Publish_Date__c = Datetime.now();
                update resUpdate;               
            }           

            return result;
        }
        else {
            return Label.ErrorIntegrationDeveloperPortal;           
        }
        return '';
    }

    public string updateApartments(List<Id> resourceIds) {
        
        List<Resource__c> resources = [ SELECT Id, Name, Investment__c, Status__c, Building__c, 
                                        Dont_Show_Price_on_Portal__c, Flat_number__c,
                                        Area__c, Price__c, Floor__c, Number_of_Rooms__c,
                                        Number_of_Bathrooms__c, Flat_Levels__c,
                                        Well_Planned_Flat__c, Seperate_Kitchen__c, 
                                        Attic__c, toLabel(Saloon_Location__c), Can_be_Connected_with_Other_Flat__c, 
                                        External_Roller_Blinds__c, Security_doors__c, 
                                        Air_Conditioning__c, Parking_Space__c, Storage__c,
                                        Description__c, Building__r.Name, 
                                        RecordType.DeveloperName, Garden__c,
                                        Investment_Commercial_Property__c, Garden_Area__c,
                                        Investment_Flat__c, Balcony__c, Entresol__c,                            
                                        Investment_House__c, Terrace__c,
                                        Investment_Parking_Space__c, Terrace_Area__c,
                                        Investment_Storage__c, Balcony_Area__c,
                                        Investment_Commercial_Property__r.Website__c, 
                                        Investment_Flat__r.Website__c,
                                        Investment_House__r.Website__c, Investment__r.Name,
                                        Investment_Parking_Space__r.Website__c, 
                                        Investment_Storage__r.Website__c, Investment__r.Website__c
                                        FROM Resource__c 
                                        WHERE Id IN :resourceIds ];
                            
        
        Map<Id, Metadata__c> files = new Map<Id, Metadata__c>([ SELECT Id, RecordID__c, Metadata__c FROM Metadata__c WHERE ObjectID__c = :CommonUtility.SOBJECT_NAME_RESOURCE AND RecordID__c in :resourceIds ]);
        DeveloperPortalESB.apartmentData appartmentData = new DeveloperPortalESB.apartmentData();
        List<DeveloperPortalESB.apartmentData> appartments = new List<DeveloperPortalESB.apartmentData>();
        Id investmentId;
        List<Resource__c> resourcesToUpdate = new List<Resource__c>();
        List<Task> tasksToInsert = new List<Task>();
        List<string> metaDatas = new List<string>();
        
        for (Resource__c res : resources) {
            appartmentData.externalId = res.Id;
            appartmentData.resourceId = res.Name;
            if (res.Status__c == ACTIVE) {
                appartmentData.status = AVAILABLE_UPPER_CASE;            
            }
            else {      
                appartmentData.status = RESERVED;         
            }
            if(res.Building__r.Name!=null){
                appartmentData.buildingName = res.Building__r.Name;
            } else {
                appartmentData.buildingName = res.Investment__r.Name;
            }            
            appartmentData.totalArea = res.Area__c;
            if(!res.Dont_Show_Price_on_Portal__c){
                appartmentData.totalPrice = res.Price__c;
            }
            appartmentData.floorNumber = integer.valueOf(res.Floor__c);
            appartmentData.numberOfRooms = integer.valueOf(res.Number_of_Rooms__c);
            appartmentData.numberOfBathrooms = integer.valueOf(res.Number_of_Bathrooms__c);
            appartmentData.numberOfFloors = integer.valueOf(res.Flat_Levels__c);
            appartmentData.wellPlanned = res.Well_Planned_Flat__c;
            appartmentData.separateKitchen = res.Seperate_Kitchen__c;
            appartmentData.attic = res.Attic__c;
            appartmentData.livingRoomExposure = res.Saloon_Location__c;
            appartmentData.flatNumber = res.Flat_number__c;
            appartmentData.combinationPossible = res.Can_be_Connected_with_Other_Flat__c;
            appartmentData.externalBlindsInstalled = res.External_Roller_Blinds__c;
            appartmentData.securedDoorInstalled = res.Security_doors__c;
            appartmentData.airConditioned = res.Air_Conditioning__c;            
                            
            if (res.Parking_Space__c) {
                appartmentData.parkingPlace = OPTIONAL;               
            }
            else {
                appartmentData.parkingPlace = NO;                             
            }
            
            if (res.Storage__c == AVAILABLE_LOWER_CASE) {
                appartmentData.storageBox = OPTIONAL;             
            }
            else {
                appartmentData.storageBox = NO;                               
            }

            if(res.Balcony__c){
                appartmentData.balconyArea = res.Balcony_Area__c;
            }   

            if(res.Terrace__c){
                appartmentData.terraceArea = res.Terrace_Area__c;
            }

            if(res.Garden__c){
                appartmentData.gardenArea = res.Garden_Area__c;
            }           

            appartmentData.description = res.Description__c;
            
            metaDatas.clear();
            for (Metadata__c f: files.values()) {
                if (f.RecordID__c == res.Id) {
                    metaDatas.add(f.Metadata__c);
                }
            }
            appartmentData.metaData = metaDatas;
            appartments.add(appartmentData);
            investmentId = res.Investment__c;
        } // end of for
        
        string result = '';
        if (appartments.size()>0) {
            if(Test.isRunningTest() == false){      
                result = devPortalService.updateApartments(UserInfo.getOrganizationId(), devPortaAccessKey, devPortalUsername, devPortalPassword, investmentId, appartments); 
            }
            else{
                result = SUCCESS;
            }
        }
        List<Resource__c> resToUpdate = new List<Resource__c>();
            
        if (result == SUCCESS) {
            string resLabel = Schema.SObjectType.Resource__c.getLabel();
            // update Publish Date
            for (Resource__c res : resources) {
                Resource__c resUpdate = new Resource__c();
                resUpdate.Id = res.Id;
                resUpdate.Publish_Date__c = datetime.now();
                resUpdate.Website__c = res.Investment__r.Website__c;
                resUpdate.Website__c += '/mieszkanie/-/oferta/'+res.Name+'';
                resToUpdate.add(resUpdate);
                    
                // create Activity history
                Task t = new Task();
                t.Subject = resLabel +' ' +Label.HasBeenPublished;
                t.WhatId = res.Id;
                t.IsReminderSet = False;
                t.Status = STATUS_COMPLETED;
                t.ActivityDate = date.today();
                tasksToInsert.add(t);
            } // end of for
            update resToUpdate;
            insert tasksToInsert;

            return result;
        } // end of if result == SUCCESS        
        else {
            return Label.ErrorIntegrationDeveloperPortal;
       }
    }
    
    public string updateResourcesStatuse(List<Id> resourcesIds, String resourceType) {
        String result = '';

        List<DeveloperPortalESB.resourceStatusData> statuses = new List<DeveloperPortalESB.resourceStatusData>();
        DeveloperPortalESB.resourceStatusData statustData;
        for(Resource__c resource : [SELECT Id, Status__c FROM Resource__c WHERE Id in :resourcesIds]) {
            statustData = new DeveloperPortalESB.resourceStatusData();
            statustData.externalId = String.valueOf(resource.Id);
            statustData.status = resource.Status__c == CommonUtility.RESOURCE_STATUS_ACTIVE ? AVAILABLE_UPPER_CASE : RESERVED;
            statuses.add(statustData);
        }

        result = devPortalService.updateResourcesStatuses(UserInfo.getOrganizationId(), devPortaAccessKey, devPortalUsername, devPortalPassword, resourceType, statuses);

        if (result != SUCCESS) {
          return Label.ErrorIntegrationDeveloperPortal;
        }

        return result;
    }   
    
    public string deleteResources(String resourceType, List<Id> resourcesIds) {                
        List<Resource__c> resources = new List<Resource__c>();          
        if (resourceType == INVESTMENT_TYPE) {
            resources = [ SELECT Id FROM Resource__c WHERE Id IN :resourcesIds ];
        }
        List<Task> tasks = new List<Task>();
             
        string result = '';
        if(Test.isRunningTest() == false){      
            try {
                result = devPortalService.deleteResources(UserInfo.getOrganizationId(), devPortaAccessKey, devPortalUsername, devPortalPassword, resourceType, resourcesIds);
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }
        else{
            result = SUCCESS;   
        }
        if (result == SUCCESS) {          
            if (resourceType == INVESTMENT_TYPE) {
                // clear Publish Date       
                for (Resource__c r : resources) {
                    r.Publish_Date__c = null;

                    Task t = new Task();
                    string resourceLabel = Schema.SObjectType.Resource__c.getLabel();
                    t.Subject = resourceLabel +' ' +Label.HasBeenUnpublished;
                    t.WhatId = r.Id;
                    t.IsReminderSet = False;
                    t.Status = STATUS_COMPLETED;
                    t.ActivityDate = date.today();
                    tasks.add(t);
                }
                try {                
                    update resources;
                    insert tasks;
                } catch(Exception e) {
                    ErrorLogger.log(e);
                }                    
            }
        }
        else {
            return Label.ErrorIntegrationDeveloperPortal;
        }
        return result;
    }   
}