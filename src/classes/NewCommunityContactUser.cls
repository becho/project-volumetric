/**
* @author 		Przemysław Tustanowski
* @description 	Class for creating new users of customer community 
*/


public with sharing class NewCommunityContactUser {

//public Id contactId {get; set;}
//public Contact contactData{get; set;}
//public User newUser {get;set;}
//public Boolean render {get; set;}

//	public  NewCommunityContactUser() {

//		contactId = Apexpages.currentPage().getParameters().get('contactId');

//		if(contactId != null){
//			Contact contactData = [SELECT Name, Email, LastName, FirstName, Language__c FROM Contact WHERE Id = :contactId];

//			if(contactData.Email == null){
//				render = false;
//				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.EmailNotProvided));	
//			}
//			else{
//				render = true;
//				List<User> countName = [SELECT Name FROM User WHERE Name = : contactData.Name];
//				newUser = new User();
//				newUser.FirstName = contactData.FirstName;
//				newUser.LastName = contactData.LastName;
//				newUser.Username = contactData.Email;
//				newUser.Contact_Hidden_Id__c = '' + contactId;
//				//newUser.Alias = contactData.Alias;
//				newUser.Email = contactData.Email;
//				newUser.CommunityNickname = contactData.Name + '123' + countName.size();
//				//newUser.LicenseDefinitionKey = 'PID_Customer_Community_Login';
//				Profile   p = [ SELECT Id FROM  Profile where Name = 'Properto Customer Community User' Limit 1 ];
//				newUser.ProfileId = p.Id;

//				newUser.alias = 'alias';

//	        	newUser.emailencodingkey = 'UTF-8';

//	        	if(contactData.Language__c == 'Polish'){newUser.languagelocalekey = 'pl';}
//	        	else{newUser.languagelocalekey = 'en_US';}
	        	
//	        	newUser.localesidkey = 'pl';
//	        	newUser.timezonesidkey = 'Europe/Berlin';

//		        newUser.UserPermissionsMarketingUser = false;
//		        newUser.IsActive = true;
//		    }
//	    }


//	}

//	public PageReference ret(){
//		Organization org = [SELECT Name FROM Organization];
//		System.debug(org);
		
//		List<Account> baseAcc = [SELECT Name FROM Account WHERE Name = : org.Name];
//		System.debug(baseAcc);
//		if(baseAcc.size() == 0 || baseAcc == null){
//			baseAcc = new List<Account>();
//			Account acc = new Account();
//			Id accRT = CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_STANDARD_COMPANY_TYPE);
//			User owner = [SELECT Id, Name FROM User WHERE UserRole.DeveloperName = :CommonUtility.ROLE_NAME_CEO LIMIT 1];
//			acc.Name = org.Name;
//			acc.RecordTypeId = accRT;
//			acc.OwnerId = owner.Id;
//			baseAcc.add(acc);
//			try{
//				insert baseAcc;
				
//			}
//			catch(Exception e){
//				System.debug(e);
//            	ErrorLogger.log(e);
//			}
//		}
//		List<Contact> hostContact = [SELECT LastName, AccountId FROM Contact Where Id = : contactId LIMIT 1];
//		if(hostContact != null && hostContact[0].AccountId != baseAcc[0].Id){
//			hostContact[0].AccountId = baseAcc[0].Id;
//			try{
//			update hostContact;
//			}
//			catch(Exception e){
//				System.debug(e);
//            	ErrorLogger.log(e);
//			}
//		}
//		newUser.ContactId = hostContact[0].Id;
//		List<User> currentUser = [SELECT Id, Contact_Hidden_Id__c  FROM User WHERE Contact_Hidden_Id__c = : contactId LIMIT 1];
//		if(currentUser.size() > 0 && currentUser[0] != null){
//			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.UserExists));	
//			return null;	
//		}
//		else{
//			try{				
//				insert newUser;
//				return new PageReference('/' + contactId);
//			}
//			catch(Exception e){
//				System.debug(e);
//            	ErrorLogger.log(e);
//				return null;
//			}
//		}
//	}

//	public PageReference back(){
//		return new PageReference('/' + contactId);
//	}
}