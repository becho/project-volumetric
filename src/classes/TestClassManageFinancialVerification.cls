@isTest
private class TestClassManageFinancialVerification {

	private static testMethod void financialVerificationTrueForContactPersonTest() {
	    Contact con = TestHelper.createContactPerson(null, true);
	    Set<Id> conIds = new Set<Id>();
	    conIds.add(con.Id);
	    Test.startTest();
        ManageFinancialVerification.financialVerificationTrueForContactPerson(conIds);
        Test.stopTest();
	}
	
	private static testMethod void financialVerificationFalseForContactPersonTest() {
	    Contact con = TestHelper.createContactPerson(null, true);
	    Set<Id> conIds = new Set<Id>();
	    conIds.add(con.Id);
	    Test.startTest();
        ManageFinancialVerification.financialVerificationFalseForContactPerson(conIds);
        Test.stopTest();
	}

    private static testMethod void financialVerificationSaleTermsIdsTrueTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(null, true);
	    Sales_Process__c customerGroup = TestHelper.createSalesProcessCustomerGroup(new Sales_Process__c(Developer_Agreement_from_Customer_Group__c = sp.Id), true);
	    Set<Id> spIds = new Set<Id>();
	    spIds.add(sp.Id);
	    Test.startTest();
        ManageFinancialVerification.financialVerificationSaleTermsIdsTrue(spIds);
        Test.stopTest();
	}
	
	private static testMethod void financialVerificationSaleTermsIdsFalseTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(null, true);
	    Sales_Process__c customerGroup = TestHelper.createSalesProcessCustomerGroup(new Sales_Process__c(Developer_Agreement_from_Customer_Group__c = sp.Id), true);
	    Set<Id> spIds = new Set<Id>();
	    spIds.add(sp.Id);
	    Test.startTest();
        ManageFinancialVerification.financialVerificationSaleTermsIdsFalse(spIds);
        Test.stopTest();
	}
	
	private static testMethod void financialVerificationProposalIdsTrueTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessProposal(null, true);
	    Sales_Process__c customerGroup = TestHelper.createSalesProcessCustomerGroup(new Sales_Process__c(Proposal_from_Customer_Group__c = sp.Id), true);
	    Set<Id> spIds = new Set<Id>();
	    spIds.add(sp.Id);
	    Test.startTest();
        ManageFinancialVerification.financialVerificationProposalIdsTrue(spIds);
        Test.stopTest();
	}
	
	private static testMethod void financialVerificationProposalIdsFalseTest() {
	    Sales_Process__c sp = TestHelper.createSalesProcessProposal(null, true);
	    Sales_Process__c customerGroup = TestHelper.createSalesProcessCustomerGroup(new Sales_Process__c(Proposal_from_Customer_Group__c = sp.Id), true);
	    Set<Id> spIds = new Set<Id>();
	    spIds.add(sp.Id);
	    Test.startTest();
        ManageFinancialVerification.financialVerificationProposalIdsFalse(spIds);
        Test.stopTest();
	}
}