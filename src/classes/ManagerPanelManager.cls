/**
* @author       Mateusz Pruszyński
* @description  Manager class for various methods called from triggers. Mostly for update of SalesTarget caused by amount changes on offer/agreement
*/

public without sharing class ManagerPanelManager {

	private static List<Manager_Panel__c> getSalesTargets(Set<Id> userIds) {
		return [
			SELECT Id, Start_Date__c, End_Date__c, User__c, Current_Quantity__c, Potential_Quantity__c, Name
			FROM Manager_Panel__c 
			WHERE User__c in :userIds 
			AND (
				RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_TARGET) 
				OR RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_MONTHLY_SALES_TARGET)
			)
		];
	}

	private static Map<Id, List<Payment__c>> getInstallments(Set<Id> saleTermsIds, Set<Id> rtIds) {

		Map<Id, List<Payment__c>> resultMap = new Map<Id, List<Payment__c>>();
		
		List<Payment__c> payments = [
			SELECT Id, Paid_Formula__c, Paid_Value__c, Agreements_Installment__c, After_sales_Service__c, Payment_date__c,
				   Payment_For__c
			 FROM Payment__c 
			 WHERE Agreements_Installment__c in :saleTermsIds 
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)
			AND Paid_Value__c > 0
			AND Payment_For__r.RecordTypeId in :rtIds
		];

		for(Payment__c payment : payments) {

			if(resultMap.containsKey(String.valueOf(payment.Agreements_Installment__c).left(15))) {
				resultMap.get(String.valueOf(payment.Agreements_Installment__c).left(15)).add(payment);
			} else {
				resultMap.put(String.valueOf(payment.Agreements_Installment__c).left(15), new Payment__c[] {payment});
			}

		}

		return resultMap;
	}

	private static List<Sales_Process__c> getProductLines(Set<Id> userIds, Set<Id> rtIds) {
		return [
			SELECT Id, Product_Line_to_Proposal__c, Offer_Line_to_Sale_Term__c, Offer_Line_to_Sale_Term__r.Status__c,
				   Product_Line_to_Proposal__r.OwnerId, Offer_Line_to_Sale_Term__r.OwnerId, Price_With_Discount__c,
				   Offer_Line_to_Sale_Term__r.CreatedDate, Product_Line_to_Proposal__r.CreatedDate, Offer_Line_to_Sale_Term__r.Date_of_signing__c 
			FROM Sales_Process__c
			WHERE (Product_Line_to_Proposal__r.OwnerId in :userIds OR Offer_Line_to_Sale_Term__r.OwnerId in :userIds)
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
			AND Queue__c > 0
			AND Product_Line_to_Proposal__c <> null
			AND Offer_Line_Resource__c <> null
			AND Offer_Line_Resource__r.RecordTypeId in :rtIds
			AND Price_With_Discount__c <> null
		];
	}

	/* Mateusz Pruszyński */
	// update sales targets' potential and current amounts and quantities
	public static void updateSalesTargets(Set<Id> userIds, SalesTargetConfig__c currentConfig) {
		
		if(currentConfig == null) {

			if(Test.isRunningTest() && SalesTargetConfig__c.getInstance('CurrentConfig') == null) {
				SalesTargetConfig__c cc = new SalesTargetConfig__c();
				cc.Name = 'CurrentConfig';
				cc.Count_Amounts_For_Flats__c = true;
				cc.Count_Amounts_For_Houses__c = true;
				cc.Count_Amounts_For_Commercial_P__c = true;
				cc.Count_Amounts_For_Storages__c = false;
				cc.Count_Amounts_For_Parking_S__c = false;
				cc.Include_Payments__c = false;
				insert cc;
			}

			currentConfig = SalesTargetConfig__c.getInstance('CurrentConfig');

		}

		// 0 - only sales processes with active reservation on product lines can be taken under concideration, thus we need to find product lines with queue__c > 0
		// we also have to check in custom settings for types of resources that are to be taken under account in terms of current and potential quentity sales target fields
		Set<Id> rtIds = new Set<Id>();

		if(currentConfig.Count_Amounts_For_Commercial_P__c) {
			rtIds.add(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY));				
		}

		if(currentConfig.Count_Amounts_For_Flats__c) {
			rtIds.add(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT));
		}

		if(currentConfig.Count_Amounts_For_Houses__c) {
			rtIds.add(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_HOUSE));
		}

		if(currentConfig.Count_Amounts_For_Parking_S__c) {
			rtIds.add(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE));
		}

		if(currentConfig.Count_Amounts_For_Storages__c) {
			rtIds.add(CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE));
		}

		// get all sales targets for specific sales reps
		List<Manager_Panel__c> salesTargets = getSalesTargets(userIds);

		// get product lines
		List<Sales_Process__c> productLines = getProductLines(userIds, rtIds);

		// get all related to Sale Terms or After-sales Service Payment Schedule Installments
		Set<Id> saleTermsIds = new Set<Id>();
		for(Sales_Process__c productLine : productLines) {
			if(productLine.Offer_Line_to_Sale_Term__c != null) {
				saleTermsIds.add(productLine.Offer_Line_to_Sale_Term__c);
			}
		}
		Map<Id, List<Payment__c>> installments;
		installments = currentConfig.Include_Payments__c? getInstallments(saleTermsIds, rtIds) : new Map<Id, List<Payment__c>>();

		// count Potential and Current Quantities
		for(Manager_Panel__c salesTarget : salesTargets) {

			// Potential
			salesTarget.Potential_Amount__c = 0;
			salesTarget.Potential_Quantity__c = 0;	

			// Current
			salesTarget.Current_Amount__c = 0;
			salesTarget.Current_Quantity__c = 0;	

			for(Sales_Process__c productLine : productLines) {

				if(productLine.Offer_Line_to_Sale_Term__c != null) {

					if(productLine.Offer_Line_to_Sale_Term__r.OwnerId == salesTarget.User__c) {

						if(
							productLine.Offer_Line_to_Sale_Term__r.Status__c == CommonUtility.SALES_PROCESS_STATUS_DEVELOPER_SIGNED
							&& productLine.Offer_Line_to_Sale_Term__r.Date_of_signing__c >= salesTarget.Start_Date__c 
							&& productLine.Offer_Line_to_Sale_Term__r.Date_of_signing__c <= salesTarget.End_Date__c
						) {

							if(currentConfig.Include_Payments__c) {

								if(installments.get(String.valueOf(productLine.Offer_Line_to_Sale_Term__c).left(15)).size() > 0) {

									salesTarget.Current_Amount__c += productLine.Price_With_Discount__c;
									salesTarget.Current_Quantity__c++;

								} else {

									salesTarget.Potential_Amount__c += productLine.Price_With_Discount__c;
									salesTarget.Potential_Quantity__c++;

								}

							} else {

								salesTarget.Current_Amount__c += productLine.Price_With_Discount__c;
								salesTarget.Current_Quantity__c++;

							}							

						} else {

							if(
								productLine.Offer_Line_to_Sale_Term__r.CreatedDate >= salesTarget.Start_Date__c 
								&& productLine.Offer_Line_to_Sale_Term__r.CreatedDate <= salesTarget.End_Date__c
							) {
							
								salesTarget.Potential_Amount__c += productLine.Price_With_Discount__c;
								salesTarget.Potential_Quantity__c++;							
							
							}

						}

					}

				} else {

					if(
						productLine.Product_Line_to_Proposal__r.OwnerId == salesTarget.User__c
						&& productLine.Product_Line_to_Proposal__r.CreatedDate >= salesTarget.Start_Date__c 
						&& productLine.Product_Line_to_Proposal__r.CreatedDate <= salesTarget.End_Date__c						
					) {

						salesTarget.Potential_Amount__c += productLine.Price_With_Discount__c;
						salesTarget.Potential_Quantity__c++;

					}					

				}

			}		

		}		

		try{
			update salesTargets;
		}
		catch (Exception e ){
			ErrorLogger.log(e);
		}

	}
}