/**
* @author 		Krystian Bednarek
* @description 	Test class for TH_Metadata
**/


@isTest
private class TestClassMetadataTrigger {
	
	@testsetup
	static void setuptestData(){
		Resource__c flat = TestHelper.createResourceFlatApartment(null, true);

		Metadata__c metaDataSheet = new Metadata__c(
			Metadata_type__c = CommonUtility.METADATA_TYPE_DATASHEET,
			RecordID__c = flat.Id,
			ObjectID__c = CommonUtility.SOBJECT_NAME_RESOURCE

		);
		insert metaDataSheet;

		Metadata__c metaDataFile = new Metadata__c(
			Metadata_type__c = CommonUtility.METADATA_TYPE_FILE_IMG,
			RecordID__c = flat.Id
		);
		insert metaDataFile;

	}

	@isTest static void testSwitchDataSheetOnInsert() {

		List<Metadata__c> metaList = [
			SELECT Id, Name, Metadata_type__c 
			FROM Metadata__c 
			WHERE Metadata_type__c = :CommonUtility.METADATA_TYPE_DATASHEET
		];

		Test.startTest();
			System.assertEquals(1, metaList.size());
		Test.stopTest();
	}


	@isTest static void testSwitchDataSheetOnUpdate() {
		Metadata__c metaDataFile = [
			SELECT Metadata_type__c 
			FROM Metadata__c 
			WHERE Metadata_type__c = :CommonUtility.METADATA_TYPE_FILE_IMG LIMIT 1
		];

		metaDataFile.Metadata_type__c = CommonUtility.METADATA_TYPE_DATASHEET;
		Test.startTest();
			update metaDataFile;

			Metadata__c metaAfterUpdate = [
				SELECT Metadata_type__c 
				FROM Metadata__c 
				WHERE Metadata_type__c = :CommonUtility.METADATA_TYPE_DATASHEET LIMIT 1
			];

			System.assertEquals(CommonUtility.METADATA_TYPE_DATASHEET, metaAfterUpdate.Metadata_type__c);
		Test.stopTest();

	}

}