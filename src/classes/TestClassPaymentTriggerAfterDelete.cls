/**
* @author      Krystian Bednarek
* @description  Test class for th_payment
*/

@isTest
private class TestClassPaymentTriggerAfterDelete {
    
    @isTest static void paymentDelete() {
        // Trigger part to update Schedule_Generated__c (related Sales_Process__c record, rt: Sale Terms) checkbox
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
            ), 
            true
        );

        Payment__c newPayment = new Payment__c (
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            Agreements_Installment__c  = saleTerms.Id,
            Due_Date__c = Date.today(),
            Paid_Value__c = -12341,
            Amount_to_pay__c = 12341
        );

        Payment__c somePayment = new Payment__c (
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            Agreements_Installment__c  = saleTerms.Id,
            Due_Date__c = Date.today(),
            Paid_Value__c = -12341,
            Amount_to_pay__c = 12341
        );

        Test.startTest();
            insert somePayment;
            insert newPayment;
            delete newPayment;
        Test.stopTest();

        //  scheduleInstallmentsDel.add(oldPayment);
        //  paymentIdsToUpdateSalesTargets.add(oldPayment.Id);
    }

    @isTest static void customerBalanceOverpayment() {
        //Calculate customer balance PAYMENT_TYPE_OVERPAYMENT

        Payment__c somePayment = new Payment__c(Due_Date__c = Date.today(), Paid_Value__c = 350000);
        insert somePayment;

        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_OVERPAYMENT),
            Incoming_Payment_from_Overpayment__c = somePayment.Id,
            Paid_Value__c = 34535
        );

        Test.startTest();
            insert newPayment;
            delete newPayment;
        Test.stopTest();    
        // test   afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(oldPayment.After_sales_from_Incoming_Payment__c);
    }

    @isTest static void customerBalanceInterestNote() {
        //Calculate customer balance PAYMENT_TYPE_INTEREST_NOTE

        Payment__c somePayment = new Payment__c(Due_Date__c = Date.today(), Paid_Value__c = 350000);
        insert somePayment;
        
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE),
            Incoming_Payment_from_Overpayment__c = somePayment.Id,
            Paid_Value__c = 34535
        );

        Test.startTest();
        insert newPayment;
        delete newPayment;
        Test.stopTest();    
        // test  afterSalesServiceToCalculateBalanceAfterPaymentsInsertUpdateDelete.add(oldPayment.After_sales_Service__c);
    }

    @isTest static void paymentDeleteMarketingCampain() {
        // Calculate expenses for Marketing campaigns

        Marketing_Campaign__c marketingCampaign = TestHelper.createMarketingCampaign(NULL, true);
        Payment__c newPayment = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_EXPENSE),
            Marketing_Campaign__c = marketingCampaign.Id,
            Amount_to_pay__c = 2500
        );

        Test.startTest();
            insert newPayment;  
            delete newPayment;
        Test.stopTest();
        // test  marketingCampaignIdsToCountExpenses.add(oldPayment.Marketing_Campaign__c);
    }
}