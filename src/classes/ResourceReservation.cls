/**
* @author       Mateusz Pruszyński
* @description  Class used to manage resource reservation line
**/

global without sharing class ResourceReservation {

    public Boolean fromResource {get; set;}
    public Boolean fromSalesProcess {get; set;}
    public Sales_Process__c myOffer {get; set;}
    public static final String ALL_ALREADY_CANCELLED = 'all_already_cancelled';
    public static final String ALL_ALREADY_IN_QUEUE = 'all_already_in_queue';

    public ResourceReservation() {
        if(Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_PROPOSALID) != null) {
            myOffer = [
                SELECT Id, Contact__c, Offer_Line_Reservation_Expiration_Date__c
                FROM Sales_Process__c 
                WHERE Id = :Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_PROPOSALID)
            ];
            fromResource = false;
            fromSalesProcess = false;
        } else if(Apexpages.currentPage().getParameters().get('productLine') != null) {
        	myOffer = new Sales_Process__c();
        	fromSalesProcess = true;
        	fromResource = false;
        } else {
            myOffer = new Sales_Process__c(); 
            fromResource = true;
            fromSalesProcess = false;
        }
		// get default reservation date from custom settings      
		DefaultReservationDays2Add__c days2Add = DefaultReservationDays2Add__c.getInstance('currentConfig');        
        myOffer.Offer_Line_Reservation_Expiration_Date__c = Date.today() + Integer.valueOf(days2Add.Number_Of_Days__c);
    }

    /** Webservice methods
    * --------------------
    */

    //check if reservation is allowed
    webservice static Boolean allowMassReservation(String[] resourceIds) {
        List<Resource__c> resources = [
            SELECT Id, Price_After_Measurement__c, Acceptance_Status__c, Promotion_IsActive__c, Promotional_Price__c, Defined_Promotion__c FROM Resource__c 
            WHERE Id in: resourceIds 
            AND (Status__c = :CommonUtility.RESOURCE_STATUS_RESERVATION OR Status__c = :CommonUtility.RESOURCE_STATUS_ACTIVE) 
            AND Price__c > 0
        ];
        return resources.size() == resourceIds.size() ? true : false;
    }

    // reserve
    // reserve
    webservice static Boolean massResourceReservation(String[] resourceIds, String contactId, String reservationExpDate) {
        //first, check if there is any reservation created for this resource and for this contact person
        List<Sales_Process__c> proposalsInQueue = [
            SELECT  Id, OwnerId, RecordTypeId, Contact__c, Reservation_Expiration_Date__c, Status__c 
            FROM Sales_Process__c 
            WHERE Contact__c = :contactId 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
        ];

        List<Sales_Process__c> salesLinesInQueue;

        if(!proposalsInQueue.isEmpty()) {  

            List<Id> proposalsInQueueIds = new List<Id>();

            for(Sales_Process__c piq : proposalsInQueue) {
                proposalsInQueueIds.add(piq.Id);
            }

            salesLinesInQueue = [
                SELECT Id, Offer_Line_Resource__c, Offer_Line_Resource__r.Name, Contact__c, 
                       Product_Line_to_Proposal__r.Contact__c, Queue__c, Offer_Line_Reservation_Date__c 
                FROM Sales_Process__c 
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
                AND Product_Line_to_Proposal__c in :proposalsInQueueIds 
                AND Offer_Line_Resource__c in :resourceIds 
                AND Queue__c > 0
            ];
        }
        //reservation has been made
        if(salesLinesInQueue != null && salesLinesInQueue.size() > 0) {
            return false;
        }
        //if no reservation has been made for this contact and these resources, create new
        else {
            List<Resource__c> resToReserve = [SELECT Id, Price__c, Defined_Promotion__c, Promotion_IsActive__c, Usable_Area_Planned__c, Acceptance_Status__c, Total_Area_Planned__c, Price_After_Measurement__c, Promotional_Price__c 
            FROM Resource__c WHERE Id in :resourceIds];
            Sales_Process__c proposalToQueue = new Sales_Process__c(
                RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER),
                Contact__c = contactId,
                Market__c = CommonUtility.SALES_PROCESS_MARKET_PRIMARY
            );
            // sum price from all resources
            proposalToQueue.Agreement_Value__c = 0;

            for(Resource__c res : resToReserve) {
                if(res.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED) { 
                    proposalToQueue.Agreement_Value__c += res.Price_After_Measurement__c;
                } else {
                    if(!res.Promotion_IsActive__c && (res.Promotional_Price__c == null || res.Promotional_Price__c <= 0)) {
                        proposalToQueue.Agreement_Value__c += res.Price__c;
                    } else {
                        proposalToQueue.Agreement_Value__c += res.Promotional_Price__c;
                    }
                }
            }

            Savepoint sp = Database.setSavepoint();         
            try {

		        // get reservation date from parameter
				List<String> reservationDateTimeSplitted = ((reservationExpDate.replaceAll(' ','\\.')).replaceAll(':','\\.')).split('\\.');  

                insert proposalToQueue; // insert proposal to obtain its Id
                List<Sales_Process__c> salesLinesToQueue = new List<Sales_Process__c>();
                Sales_Process__c slToQ;
                Map<Id, String> resourceIdsMapWithStatus = new Map<Id, String>();
                for(Resource__c res : resToReserve) {
                    slToQ = new Sales_Process__c(
                        RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE),
                        Contact_from_Offer_Line__c = contactId,
                        Offer_Line_Resource__c = res.Id,
                        Product_Line_to_Proposal__c = proposalToQueue.Id,
                        Offer_Line_Reservation_Date__c = System.now(),
                        Offer_Line_Reservation_Expiration_Date__c = Date.newInstance(
			                Integer.valueOf(reservationDateTimeSplitted[2]), 
			                Integer.valueOf(reservationDateTimeSplitted[1]), 
			                Integer.valueOf(reservationDateTimeSplitted[0])
			            ),
                        Discount__c = 0,
                        Discount_Amount_Offer__c = 0,
                        Price_With_Discount__c = (res.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED ? res.Price_After_Measurement__c : 
                            ((!res.Promotion_IsActive__c && (res.Promotional_Price__c == null || res.Promotional_Price__c <= 0)) ? res.Price__c : res.Promotional_Price__c)
                        ),
                        Resource_Area__c = res.Total_Area_Planned__c != null ?  res.Usable_Area_Planned__c : res.Total_Area_Planned__c

                    );

                    if(res.Price__c != slToQ.Price_With_Discount__c) {
                        Decimal resPrice = res.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED ? res.Price_After_Measurement__c : res.Price__c;
                        slToQ.Discount_Amount_Offer__c = resPrice - slToQ.Price_With_Discount__c;
                        slToQ.Discount__c = (100 / resPrice) * slToQ.Discount_Amount_Offer__c;
                    }

                    if(res.Defined_Promotion__c != null) {
                        slToQ.Promotion_Definition__c = res.Defined_Promotion__c;
                    }

                    salesLinesToQueue.add(slToQ);
                    resourceIdsMapWithStatus.put(res.Id, CommonUtility.RESOURCE_STATUS_RESERVATION);
                }
                updateQueue(salesLinesToQueue, resourceIds);
                updateResourceStatuse(resourceIdsMapWithStatus);
                return true;
            } catch(Exception e) {
                Database.rollback(sp);
                ErrorLogger.log(e);
                return null;
            }
        }
    }

    // reserve or return resources that are already reserved for this customer
    webservice static DetailsResponse massResourceReservationWithDetails(String[] resourceIds, String contactId, String reservationExpDate) {
        DetailsResponse resp = new DetailsResponse();
        //first, check if there is any reservation created for this resource and for this contact person
        List<Sales_Process__c> proposalsInQueue = [
            SELECT Id, OwnerId, RecordTypeId, Contact__c, Reservation_Expiration_Date__c, 
                   Reservation_Date__c, Status__c 
            FROM Sales_Process__c 
            WHERE Contact__c = :contactId 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
        ];

        List<Sales_Process__c> salesLinesInQueue;

        if(!proposalsInQueue.isEmpty()) {       
            List<Id> proposalsInQueueIds = new List<Id>();

            for(Sales_Process__c piq : proposalsInQueue) {
                proposalsInQueueIds.add(piq.Id);
            }

            salesLinesInQueue = [
                SELECT Id, Offer_Line_Resource__c, Offer_Line_Resource__r.Name, Contact__c, 
                       Product_Line_to_Proposal__r.Contact__c, Queue__c, Offer_Line_Reservation_Date__c 
                FROM Sales_Process__c 
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
                AND Product_Line_to_Proposal__c in :proposalsInQueueIds 
                AND Offer_Line_Resource__c in :resourceIds AND Queue__c > 0
            ];
        }
        //reservation has been made
        if(salesLinesInQueue != null && salesLinesInQueue.size() > 0) {
            List<String> alreadyReservedResList = new List<String>();

            for (Sales_Process__c resrv : salesLinesInQueue) {
                alreadyReservedResList.add(resrv.Offer_Line_Resource__r.Name);
            }

            resp.alreadyReservedList = alreadyReservedResList;
            return resp;
        }
        //if no reservation has been made for this contact and these resources, create new
        else {
            List<Resource__c> resToReserve = [SELECT Id, Price__c, Price_After_Measurement__c, Promotion_IsActive__c, Promotional_Price__c, Acceptance_Status__c, Usable_Area_Planned__c, Total_Area_Planned__c, Defined_Promotion__c FROM Resource__c WHERE Id in :resourceIds];
            Sales_Process__c proposalToQueue = new Sales_Process__c(
                RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER),
                Contact__c = contactId,
                Market__c = CommonUtility.SALES_PROCESS_MARKET_PRIMARY
            );
            // sum price from all resources
            proposalToQueue.Agreement_Value__c = 0;

            for(Resource__c res : resToReserve) {
                if(res.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED) { 
                    proposalToQueue.Agreement_Value__c += res.Price_After_Measurement__c;
                } else {
                    if(!res.Promotion_IsActive__c && (res.Promotional_Price__c == null || res.Promotional_Price__c <= 0)) {
                        proposalToQueue.Agreement_Value__c += res.Price__c;
                    } else {
                        proposalToQueue.Agreement_Value__c += res.Promotional_Price__c;
                    }
                }
            }

            Savepoint sp = Database.setSavepoint();
            try {

                // get reservation date from parameter
                List<String> reservationDateTimeSplitted = ((reservationExpDate.left(10).replaceAll(' ','\\.')).replaceAll('-','\\.')).split('\\.');  

                insert proposalToQueue; // insert proposal to obtain its Id
                Map<Id, String> resourceIdsMapWithStatus = new Map<Id, String>();
                List<Sales_Process__c> salesLinesToQueue = new List<Sales_Process__c>();
                Sales_Process__c slToQ;
                for(Resource__c res : resToReserve) {
                    slToQ = new Sales_Process__c(
                        RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE),
                        Contact_from_Offer_Line__c = contactId,
                        Offer_Line_Resource__c = res.Id,
                        Product_Line_to_Proposal__c = proposalToQueue.Id,
                        Offer_Line_Reservation_Date__c = System.now(),
                        Offer_Line_Reservation_Expiration_Date__c = Date.newInstance(
                            Integer.valueOf(reservationDateTimeSplitted[0]), 
                            Integer.valueOf(reservationDateTimeSplitted[1]), 
                            Integer.valueOf(reservationDateTimeSplitted[2])
                        ),
                        Discount__c = 0,
                        Discount_Amount_Offer__c = 0,
                        Price_With_Discount__c = (res.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED ? res.Price_After_Measurement__c : 
                            ((!res.Promotion_IsActive__c && (res.Promotional_Price__c == null || res.Promotional_Price__c <= 0)) ? res.Price__c : res.Promotional_Price__c)
                        ),
                        Resource_Area__c = res.Total_Area_Planned__c != null ?  res.Usable_Area_Planned__c : res.Total_Area_Planned__c
                    );
                    
                    if(res.Price__c != slToQ.Price_With_Discount__c) {
                        Decimal resPrice = res.Acceptance_Status__c == CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED ? res.Price_After_Measurement__c : res.Price__c;
                        slToQ.Discount_Amount_Offer__c = resPrice - slToQ.Price_With_Discount__c;
                        slToQ.Discount__c = (100 / resPrice) * slToQ.Discount_Amount_Offer__c;
                    }

                    if(res.Defined_Promotion__c != null) {
                        slToQ.Promotion_Definition__c = res.Defined_Promotion__c;
                    }

                    salesLinesToQueue.add(slToQ);
                    resourceIdsMapWithStatus.put(res.Id, CommonUtility.RESOURCE_STATUS_RESERVATION);
                }
                updateQueue(salesLinesToQueue, resourceIds);
                updateResourceStatuse(resourceIdsMapWithStatus);

                resp.reservation = [SELECT Id, Name FROM Sales_Process__c WHERE Id = :proposalToQueue.Id];
                return resp;
            } catch(Exception e) {
                Database.rollback(sp);
                ErrorLogger.log(e);
                return null;
            }
        }
    }

    global class DetailsResponse {
        @TestVisible public List<String> alreadyReservedList;
        @TestVisible public Sales_Process__c reservation;
    }

    // called from resource Offers&Reservations related list
    webservice static String massCancelReservation(String[] spIdList, String resourceId) {
        // get all offer lines
        List<Sales_Process__c> offerLinesToUpdateQueue = [
            SELECT Id, Product_Line_to_Proposal__c 
            FROM Sales_Process__c 
            WHERE Id in :spIdList 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
            AND Offer_Line_Resource__c = :resourceId
        ];

        for(Sales_Process__c aol : offerLinesToUpdateQueue) {
            aol.Offer_Line_Reservation_Date__c = null;
            aol.Offer_Line_Reservation_Expiration_Date__c = null;
            aol.Queue__c = null;
            aol.Without_Reservation__c = true;
        }

        Savepoint sp = Database.setSavepoint();
        try {
            renumberQueue(spIdList, resourceId, offerLinesToUpdateQueue);
            return 'true';
        } catch(Exception e) {
            Database.rollback(sp);      
            ErrorLogger.log(e);
            return 'false';
        }
    }

    // renumbers other sales product in queue - always in case of one resource!
    public static void renumberQueue(String[] offerLinesIdList, String resourceId, List<Sales_Process__c> offerLinesToUpdateQueue) {
        List<Sales_Process__c> offerLines2update = new List<Sales_Process__c>();

        if(offerLinesToUpdateQueue != null) {
            offerLines2update.addAll(offerLinesToUpdateQueue);
        }

        List<Sales_Process__c> offerLinesToRenumber = [
            SELECT Id, Queue__c, Product_Line_to_Proposal__c
            FROM Sales_Process__c 
            WHERE Id not in:offerLinesIdList 
            AND Offer_Line_Resource__c = :resourceId 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
            AND Queue__c > 0 AND Without_Reservation__c <> true ORDER BY Queue__c asc
        ];

        if(!offerLinesToRenumber.isEmpty()) {
            Decimal queue = 1;

            for(Sales_Process__c oltr : offerLinesToRenumber) {
                oltr.Queue__c = queue++;
                offerLines2update.add(oltr);
            }

        } else {
            Map<Id, String> resourceIdsMapWithStatus = new Map<Id, String>();
            // change resource status
            resourceIdsMapWithStatus.put(resourceId, CommonUtility.RESOURCE_STATUS_ACTIVE);
            updateResourceStatuse(resourceIdsMapWithStatus);                        
        }
        update offerLines2update;
    }

    // Called from Person Account (Custromer is only one)
    webservice static Boolean massTransformFavsToReservations(String[] favoritesIds) {
        List<Sales_Process__c> favorites = [
            SELECT Id, Customer_Favorite__c, Resource_Favourite__c, Resource_Favourite__r.Price__c, 
                   Resource_Favourite__r.Usable_Area_Planned__c, Resource_Favourite__r.Total_Area_Planned__c 
            FROM Sales_Process__c 
            WHERE Id in :favoritesIds
        ];
        // proposal to insert
        Sales_Process__c proposal = new Sales_Process__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER),
            Contact__c = favorites[0].Customer_Favorite__c,
            Market__c = CommonUtility.SALES_PROCESS_MARKET_PRIMARY,
            Agreement_Value__c = 0 // initialize value
        );

        for(Sales_Process__c fav : favorites) {
            // update proposal price with summed resources price
            proposal.Agreement_Value__c += fav.Resource_Favourite__r.Price__c != null ? fav.Resource_Favourite__r.Price__c : 0;
        }

        Savepoint sp = Database.setSavepoint();
        try {
            insert proposal;
            // favorites to transform into offer lines (update)
            Map<Id, String> resourceIdsMapWithStatus = new Map<Id, String>();
            Set<Id> resourceIds = new Set<Id>();

            for(Sales_Process__c fav : favorites) {
                resourceIds.add(fav.Resource_Favourite__c);
                resourceIdsMapWithStatus.put(fav.Resource_Favourite__c, CommonUtility.RESOURCE_STATUS_RESERVATION);
                fav.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE);
                fav.Price_With_Discount__c = fav.Resource_Favourite__r.Price__c;                
                fav.Product_Line_to_Proposal__c = proposal.Id;
                fav.Offer_Line_Reservation_Date__c = System.now();
                fav.Offer_Line_Reservation_Expiration_Date__c = Date.today() + 3;
                fav.Discount__c = 0;
                fav.Discount_Amount_Offer__c = 0;
                fav.Contact_from_Offer_Line__c = fav.Customer_Favorite__c;  /* -> clear field characteristic for favorites */   fav.Customer_Favorite__c = null;
                fav.Offer_Line_Resource__c = fav.Resource_Favourite__c;     /* -> clear field characteristic for favorites */   fav.Resource_Favourite__c = null;
                fav.Resource_Area__c = fav.Resource_Favourite__r.Total_Area_Planned__c != null ?  fav.Resource_Favourite__r.Usable_Area_Planned__c : fav.Resource_Favourite__r.Total_Area_Planned__c;
                fav.Without_Reservation__c = false;
            }

            List<Id> resIds = new List<Id>();
            resIds.addAll(resourceIds);
            updateResourceStatuse(resourceIdsMapWithStatus);            
            updateQueue(favorites, resIds); // transformation - has to be split into 2 dmls
            return true;
        } catch(Exception e) {
            Database.rollback(sp);
            ErrorLogger.log(e);
            return false;
        }
    }

    // checks if there has been created sale terms to bulk of proposals on cancellation of reservation
    webservice static String allowCancel(String[] salesLinesIds) {
        String result = '';
        List<Sales_Process__c> salesLines = [
            SELECT Id, Product_Line_to_Proposal__c 
            FROM Sales_Process__c 
            WHERE Id in :salesLinesIds 
            AND Product_Line_to_Proposal__c != null
        ];

        List<Id> proposalIds = new List<Id>();

        for(Sales_Process__c sl : salesLines) {
            proposalIds.add(sl.Product_Line_to_Proposal__c);
        }

        List<Sales_Process__c> saleTerms = [
            SELECT Id, Name, Offer__r.Name 
            FROM Sales_Process__c 
            WHERE Offer__c in :proposalIds 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
        ];

        if(saleTerms.isEmpty()) {
            result = 'true';
        } else {
            for(Sales_Process__c st : saleTerms) {
                result += '<br />'+Label.copfp+' <h1>' + String.valueOf(st.Offer__r.Name) + '</h1> '+Label.cbcbthbscftp+': <h1>' + String.valueOf(st.Name) + '</h1>';
            }
        }
        return result;
    }

    // update quque
    public static void updateQueue(List<Sales_Process__c> salesLinesToQueue, List<String> resourceIds) {
        // first, find all offer lines for provided resources
        List<Sales_Process__c> linesToReassignQueue = [
            SELECT Id, Queue__c, Offer_Line_Resource__c 
            FROM Sales_Process__c 
            WHERE Offer_Line_Resource__c in :resourceIds 
            AND Queue__c > 0 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
            AND Without_Reservation__c <> true ORDER BY Queue__c ASC
        ];

        for(Sales_Process__c sltq : salesLinesToQueue) {
            Decimal q = 1;

            for(Sales_Process__c strq : linesToReassignQueue) {
                if(sltq.Offer_Line_Resource__c == strq.Offer_Line_Resource__c) {
                    q++;
                }
            }

            sltq.Queue__c = q;
        }
        upsert salesLinesToQueue;
    }

    /* Mateusz Pruszyński */
    // updates Resource status
    public static void updateResourceStatuse(Map<Id, String> resourceIdsMapWithStatus) { // Map<Resource Id, Status to change>
        List<Resource__c> resources2update = new List<Resource__c>();

        for(Id resId : resourceIdsMapWithStatus.keySet()) {
            Resource__c resource = new Resource__c(
                Id = resId,
                Status__c = resourceIdsMapWithStatus.get(resId)
            );
            resources2update.add(resource);
        }

        if(!resources2update.isEmpty()) {
            update resources2update;
        }
    }

    /* Mateusz Pruszyński */
    // renew reservations for product lines
    webservice static String massRenewReservation(String[] productLineIds, String reservationExpDate) {
        String result;
        List<Sales_Process__c> productLinesToRenumberQueue = new List<Sales_Process__c>();
        Set<String> resIds = new Set<String>();

        // get reservation date from parameter
		List<String> reservationDateTimeSplitted = ((reservationExpDate.replaceAll(' ','\\.')).replaceAll(':','\\.')).split('\\.');  

		// get default reservation date from custom settings      
		DefaultReservationDays2Add__c days2Add = DefaultReservationDays2Add__c.getInstance('currentConfig');

        for(Sales_Process__c prodLine : [
            SELECT Id, Queue__c, Offer_Line_Resource__c, Product_Line_to_Proposal__c 
            FROM Sales_Process__c 
            WHERE Id in :productLineIds
        ]) {
            if(prodLine.Queue__c == null || prodLine.Queue__c <= 0) {
                prodLine.Offer_Line_Reservation_Date__c = System.now();
                prodLine.Offer_Line_Reservation_Expiration_Date__c = Date.newInstance(
	                Integer.valueOf(reservationDateTimeSplitted[2]), 
	                Integer.valueOf(reservationDateTimeSplitted[1]), 
	                Integer.valueOf(reservationDateTimeSplitted[0])
	            );
                prodLine.Without_Reservation__c = false;
                productLinesToRenumberQueue.add(prodLine);
                resIds.add(prodLine.Offer_Line_Resource__c);
            }
        }

        if(!productLinesToRenumberQueue.isEmpty()) {
            Savepoint sp = Database.setSavepoint();
            try {
                List<String> resourceIds = new List<String>();
                resourceIds.addAll(resIds);
                Map<Id, String> resourceIdsMapWithStatus = new Map<Id, String>();

                for(String i : resourceIds) {
                    resourceIdsMapWithStatus.put(i, CommonUtility.RESOURCE_STATUS_RESERVATION);
                }

                updateResourceStatuse(resourceIdsMapWithStatus);        
                updateQueue(productLinesToRenumberQueue, resourceIds);
                result = 'true';
            } catch(Exception e) {
                Database.rollback(sp);
                ErrorLogger.log(e);
                result = String.valueOf(e);
            }
        } else { // already in queue msg
            result = ALL_ALREADY_IN_QUEUE;
        }
        return result;
    }

    // called from Contact Offers&Reservations related list
    webservice static String massCancelReservationFromContact(String[] spIdList, String contactId) {
        // get all offer lines
        List<Sales_Process__c> offerLinesToUpdateQueue = [
            SELECT Id, Product_Line_to_Proposal__c, Queue__c, Offer_Line_Resource__c 
            FROM Sales_Process__c 
            WHERE Id in :spIdList 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
            AND Product_Line_to_Proposal__r.Contact__c = :contactId AND Queue__c > 0
        ];

        if(!offerLinesToUpdateQueue.isEmpty()) {
            Set<Id> resourceIds = new Set<Id>();

            for(Sales_Process__c aol : offerLinesToUpdateQueue) {
                aol.Offer_Line_Reservation_Date__c = null;
                aol.Offer_Line_Reservation_Expiration_Date__c = null;
                aol.Queue__c = null;
                aol.Without_Reservation__c = true;
                resourceIds.add(aol.Offer_Line_Resource__c); 
            }

            Savepoint sp = Database.setSavepoint();
            try {
                renumberQueueFromProposal(spIdList, offerLinesToUpdateQueue, resourceIds);
                return 'true';
            } catch(Exception e) {
                Database.rollback(sp);      
                ErrorLogger.log(e);
                return 'false';
            }
        } else {
            //already cancelled
            return ALL_ALREADY_CANCELLED;
        }           
    }

    /* Mateusz Pruszyński */
    // cancel reservations from proposal layout (called from rl button)
    webservice static String massCancelReservationFromProposal(String[] prodLinesIds) {
        String result = '';
        List<Sales_Process__c> prodLines = [
            SELECT Id, Queue__c, Offer_Line_Resource__c 
            FROM Sales_Process__c 
            WHERE Id in :prodLinesIds AND Queue__c > 0
        ];

        if(!prodLines.isEmpty()) {
            Set<Id> resourceIds = new Set<Id>();

            for(Sales_Process__c pl : prodLines) {
                pl.Offer_Line_Reservation_Expiration_Date__c = null;
                pl.Queue__c = null; 
                pl.Without_Reservation__c = true;
                resourceIds.add(pl.Offer_Line_Resource__c);         
            }

            Savepoint sp = Database.setSavepoint();
            try {
                renumberQueueFromProposal(prodLinesIds, prodLines, resourceIds);
                return 'true';
            } catch(Exception e) {
                Database.rollback(sp);          
                ErrorLogger.log(e);
                return 'false';
            }
        } else {
            //already cancelled
            return ALL_ALREADY_CANCELLED;
        }
    }

    // renumbers queue
    public static void renumberQueueFromProposal(String[] prodLinesIds, List<Sales_Process__c> prodLines, Set<Id> resourceIds) {
        List<Sales_Process__c> productLines2update = new List<Sales_Process__c>();

        if(prodLines != null) {
            productLines2update.addAll(prodLines);
        }

        List<Sales_Process__c> productLinesToRenumberQueue = [
            SELECT Id, Offer_Line_Resource__c
            FROM Sales_Process__c 
            WHERE Id not in :prodLinesIds 
            AND Offer_Line_Resource__c in :resourceIds 
            AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE) 
            AND Queue__c > 0 AND Without_Reservation__c <> true ORDER BY Queue__c asc
        ];

        Map<Id, String> resourceIdsMapWithStatus = new Map<Id, String>();
        if(!productLinesToRenumberQueue.isEmpty()) {
            Map<Id, List<Sales_Process__c>> resIdANDProdLinesMap = new Map<Id, List<Sales_Process__c>>();
            List<Sales_Process__c> prodL;

            for(Id resId : resourceIds) {
                prodL = new List<Sales_Process__c>();
                for(Sales_Process__c pltrq : productLinesToRenumberQueue) {
                    if(pltrq.Offer_Line_Resource__c == resId) {
                        prodL.add(pltrq);
                    }
                }
                
                if(!resIdANDProdLinesMap.containsKey(resId)) {
                    resIdANDProdLinesMap.put(resId, prodL);
                } else {
                    resIdANDProdLinesMap.get(resId).addAll(prodL);
                }
            }

            for(Id resId : resIdANDProdLinesMap.keySet()) {
                List<Sales_Process__c> productLinez = resIdANDProdLinesMap.get(resId);
                Decimal queue = 1;

                if(!productLinez.isEmpty()) {
                    for(Sales_Process__c plz : productLinez) {
                        plz.Queue__c = queue++;
                    }
                } else {
                    resourceIdsMapWithStatus.put(resId, CommonUtility.RESOURCE_STATUS_ACTIVE);
                }
                productLines2update.addAll(productLinez);
            }
        } else {
            for(Id resId : resourceIds) {
                resourceIdsMapWithStatus.put(resId, CommonUtility.RESOURCE_STATUS_ACTIVE);
            }
        }
        if(resourceIdsMapWithStatus.size() > 0) {
            updateResourceStatuse(resourceIdsMapWithStatus);
        }
        update productLines2update;
    }

    // comes from th_salesProcessTrigger - updates queue after product lines deletion
    public static void updateQueueAfterProductLineDeletion(List<Sales_Process__c> deletedProductLines) {
        Set<Id> resourceIds = new Set<Id>();
        List<String> prodLinesIds = new List<String>();
        for(Sales_Process__c dpl : deletedProductLines) {
            resourceIds.add(dpl.Offer_Line_Resource__c);
            prodLinesIds.add(dpl.Id);
        }
        renumberQueueFromProposal(prodLinesIds, null, resourceIds);
    }

    /* Mateusz Pruszyński */
    // Called from th_SalesProcessTrigger
    // Code part to update "Reservation_Quque_First_Place__c" on Sale Temrs based on "Queue__c" on related Product Lines
    public static void checkQueueCondition(List<Sales_Process__c> productLines, List<Sales_Process__c> deletedProductLines) {
        List<Sales_Process__c> saleTerms2Update = new List<Sales_Process__c>();
        // 1 - case when a Sale Terms record is created and all existing Product Lines are updated with lookup to created Sale Terms
        // AND
        // 2 - case when a new Product is attached to existing Sale Terms
        // AND
        // 3 - case when a queue__c gets changer - this means that either a user cancelled a reservation or a record was deleted
        if(productLines != null) {
            Set<Id> saleTermsIds = new Set<Id>();
            Set<Id> productLinesIds = new Set<Id>();
            for(Sales_Process__c productLine : productLines) {
                saleTermsIds.add(productLine.Offer_Line_to_Sale_Term__c);
                productLinesIds.add(productLine.Id);
            }
            List<Sales_Process__c> otherProductLines = [
                SELECT Id, Queue__c, Offer_Line_to_Sale_Term__c 
                FROM Sales_Process__c 
                WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds 
                AND Without_Reservation__c <> true
                AND Id not in :productLinesIds
            ];

            if(!otherProductLines.isEmpty()) {
                productLines.addAll(otherProductLines);
            }

            List<Sales_Process__c> saleTerms = [
                SELECT Id, Reservation_Quque_First_Place__c FROM Sales_Process__c WHERE Id in :saleTermsIds
            ];

            Boolean reservationQueueFirstPlace;
            
            for(Sales_Process__c saleTerm : saleTerms) {
                reservationQueueFirstPlace = true;
                
                for(Sales_Process__c productLine : productLines) {
                    if(productLine.Offer_Line_to_Sale_Term__c == saleTerm.Id) {
                        if(productLine.Queue__c != 1) {
                            reservationQueueFirstPlace = false;
                            break;
                        }
                    }
                }

                if(saleTerm.Reservation_Quque_First_Place__c != reservationQueueFirstPlace) {
                    saleTerm.Reservation_Quque_First_Place__c = reservationQueueFirstPlace;
                    saleTerms2Update.add(saleTerm);
                }
            }

        }

        // 4 - case when a record is directly deleted from Sale Terms
        if(deletedProductLines != null) {
            Set<Id> saleTermsIds = new Set<Id>();
            Set<Id> productLinesIds = new Set<Id>();

            for(Sales_Process__c productLine : deletedProductLines) {
                saleTermsIds.add(productLine.Offer_Line_to_Sale_Term__c);
                productLinesIds.add(productLine.Id);
            }

            List<Sales_Process__c> otherProductLines = [
                SELECT Id, Queue__c, Offer_Line_to_Sale_Term__c 
                FROM Sales_Process__c 
                WHERE Offer_Line_to_Sale_Term__c in :saleTermsIds 
                AND Without_Reservation__c <> true
                AND Id not in :productLinesIds
            ];

            List<Sales_Process__c> saleTerms = [
                SELECT Id, Reservation_Quque_First_Place__c FROM Sales_Process__c WHERE Id in :saleTermsIds
            ];
            
            Boolean reservationQueueFirstPlace;
            
            for(Sales_Process__c saleTerm : saleTerms) {
                reservationQueueFirstPlace = true;

                for(Sales_Process__c productLine : otherProductLines) {
                    if(productLine.Offer_Line_to_Sale_Term__c == saleTerm.Id) {
                        if(productLine.Queue__c != 1) {
                            reservationQueueFirstPlace = false;
                            break;
                        }
                    }
                }

                if(saleTerm.Reservation_Quque_First_Place__c != reservationQueueFirstPlace) {
                    saleTerm.Reservation_Quque_First_Place__c = reservationQueueFirstPlace;
                    saleTerms2Update.add(saleTerm);
                }
            }

        }
        // -----------------
        // update sale terms
        if(!saleTerms2Update.isEmpty()) {
            try {
                update saleTerms2Update;
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }
    }

}