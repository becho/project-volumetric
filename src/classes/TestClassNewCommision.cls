@isTest
private class TestClassNewCommision {

	private static testMethod void initTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        Test.stopTest();
	}
	
	private static testMethod void initNullTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        // Apexpages.currentPage().getParameters().put('commision', payment.Id);
        Apexpages.currentPage().getParameters().put('Id', sp.Id);
        
        NewCommision controller = new NewCommision();
        Test.stopTest();
	}


    private static testMethod void getTypeTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        controller.getType();
        Test.stopTest();
	}
	
	private static testMethod void getContactTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        controller.getContact();
        Test.stopTest();
	}
	
	private static testMethod void updateAmountTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        controller.updateAmount();
        Test.stopTest();
	}
	
	private static testMethod void saveTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        controller.save();
        
        // controller.selectedType = NewCommision.OUT;
        
        // controller.save();
        
        // controller.selectedContact = NewCommision.COMMISSION_SELECTED_CONTACT_USER;
        
        // controller.save();
        
        // controller.selectedContact = NewCommision.COMMISSION_SELECTED_CONTACT_ACCOUNT;
        
        // controller.save();
        
        controller.selectedType = NewCommision.INCOMING;
        Test.stopTest();
	}
	
	private static testMethod void saveContactUserOutTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id, Agreement_Value__c = 1), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id, Type__c = NewCommision.OUT, User__c = TestHelper.createUser(new User(Maximum_Commission__c = 100), true).Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        
        controller.save();
        
        Test.stopTest();
	}
	
	private static testMethod void saveContactUserOut2Test() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id, Agreement_Value__c = 1), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id, Type__c = NewCommision.OUT, Value__c = 10000, User__c = TestHelper.createUser(new User(Maximum_Commission__c = 100), true).Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        
        controller.save();

        Test.stopTest();
	}
	
	private static testMethod void saveContactUserOut3Test() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id, Agreement_Value__c = 1), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id, Type__c = NewCommision.OUT, Value__c = 10000, User__c = TestHelper.createUser(new User(Maximum_Commission__c = null), true).Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        
        controller.save();

        Test.stopTest();
	}
	
	private static testMethod void saveContactAccountTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id, Agreement_Value__c = 1), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id, Type__c = NewCommision.OUT, Value__c = 10000, Account__c = TestHelper.createAccountStandard(new Account(MaxCommission__c = 100), true).Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        controller.save();
        
        Test.stopTest();
	}
	
	private static testMethod void saveContactAccount2Test() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id, Type__c = NewCommision.OUT, Value__c = 10000, Account__c = TestHelper.createAccountStandard(new Account(MaxCommission__c = null), true).Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        controller.getContact();
        controller.save();
        
        Test.stopTest();
	}
	
	private static testMethod void saveIncommingAccountTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id, Type__c = NewCommision.INCOMING, Value__c = 10000, Account__c = TestHelper.createAccountStandard(new Account(MaxCommission__c = null), true).Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        controller.getContact();
        controller.save();
        
        Test.stopTest();
	}
	
	private static testMethod void saveIncommingContactTest() {
        Test.startTest();
	    Resource__c investment = TestHelper.createInvestment(new Resource__c(Name = 'test'), true);
	    Resource__c res = TestHelper.createResourceFlatApartment(new Resource__c(Investment_Flat__c = investment.Id), true);
	    Sales_Process__c sp = TestHelper.createSalesProcessSaleTerms(new Sales_Process__c(Discount__c = 1, Offer_Line_Resource__c = res.Id), true);
	    Payment__c payment = TestHelper.createPaymentsInstallment(new Payment__c(Agreements_Comission__c = sp.Id, Type__c = NewCommision.INCOMING, Value__c = 10000, Contact__c = TestHelper.createContactPerson(new Contact(), true).Id), true);
	    PageReference pageRef = new PageReference('/apex/NewCommision');
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put(CommonUtility.URL_PARAM_SPID, sp.Product_Line_to_Proposal__c);
        Apexpages.currentPage().getParameters().put('commision', payment.Id);
        
        NewCommision controller = new NewCommision();
        controller.save();
        
        Test.stopTest();
	}
}