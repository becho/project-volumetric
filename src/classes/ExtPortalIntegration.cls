/**
* @author       Maciej Jóźwiak
* @description  interface used for external lines. Gratka, otodom etc
*/

public interface ExtPortalIntegration{

    String[] createInvestment(Id investmentId);
    String[] deleteInvestment(Id investmentId);
    String[] createListing(Id listingId);
    String[] deleteListing(Id listingId);
}