/**
* @author      Krystian Bednarek
* @description  Test class for th_payment
*/

@isTest
private class TestClassPaymentTriggerBeforeInsert {

    private final static String FLAT_BANK_ACCOUNT_2ND_WAY = '00000000000000000000000000';

    /* Mateusz Pruszynski */
    // method to prepare common for all methods data
    @testSetup static void prepareCommonData() {
        // custom settings - bank account generation way - default value - THIRD WAY (bank account is generated automatically with customers Id)
        BankAccountGenWay__c currentWay = new BankAccountGenWay__c();
        currentWay.Name = ManageBankAccountForSalesProcess.CURRENT_WAY;
        currentWay.Choose_Way__c = 3;
        insert currentWay;
    }

    /* Mateusz Pruszynski */
    // Copy incoming payments's After_sales_from_Incoming_Payment__c to After_sales_from_Incoming_Payment_Curren__c or After_sales_from_Incoming_Payment_Trust__c to differentiate records between related lists for second bank account generation way
    @isTest static void differentiateBetweenTrustAndCurrentIncomingPaymentsSecondWay() {
        // change the custom settings config
        BankAccountGenWay__c currentWay = BankAccountGenWay__c.getInstance(ManageBankAccountForSalesProcess.CURRENT_WAY);
        currentWay.Choose_Way__c = 2;
        update currentWay;

        System.debug('currentWay: ' + currentWay);

        // insert data

        // investment
        Resource__c investment = TestHelper.createInvestment(null, true);

        // flat
        Resource__c flat = TestHelper.createResourceFlatApartment(              
            new Resource__c(
                Investment_Flat__c = investment.Id,
                Price__c = 10000000,
                Bank_Account_Number__c = FLAT_BANK_ACCOUNT_2ND_WAY
            ), 
            true
        );

        // proposal
        Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);

        // product line
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(
            new Sales_Process__c(
                Product_Line_to_Proposal__c = proposal.Id,
                Contact_from_Offer_Line__c = proposal.Contact__c,
                Offer_Line_Resource__c = flat.Id        
            ), 
            true
        );

        // sale terms
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = proposal.Contact__c,
                Offer__c = proposal.Id,
                Agreement_Value__c = flat.Price__c
            ),
            true
        );        

        Test.startTest();
    

            // create payment schedule
            Payment__c installment = TestHelper.createPaymentsInstallment(
                new Payment__c(
                    Amount_to_pay__c = flat.Price__c,
                    of_contract_value__c = 100,
                    Due_Date__c = Date.today() + 30,
                    Contact__c = proposal.Contact__c
                ),
                true
            );

            // after-sales service
            Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
                new Sales_Process__c(
                    Agreement__c = saleTerms.Id, 
                    Contact__c = saleTerms.Contact__c
                ), 
                true
            );  

            // insert incoming payment
            Payment__c incomingPayment = TestHelper.createPaymentIncomingPayment(
                new Payment__c(
                    After_sales_from_Incoming_Payment__c = afterSales.Id,
                    Bank_Account_For_Resource__c = flat.Bank_Account_Number__c,
                    Paid_Value__c = flat.Price__c / 3
                ),
                true
            );

        Test.stopTest();  

        //System.assertEquals();
              
    }

  /*  @isTest static void TestPaymentForChange(){
    // test if  newly created payments for changes, get automatically assigned investment current bank account
        Resource__c investment = TestHelper.createInvestment(
            new Resource__c(
                Current_Bank_Account__c = '14249000050000452080437211'
            ), 
            true
        );

        Resource__c flat = TestHelper.createResourceFlatApartment(              
            new Resource__c(
                Investment_Flat__c = investment.Id,
                Price__c = 10000000
            ), 
            true
        );
        Sales_Process__c proposal = TestHelper.createSalesProcessProposal(null, true);

        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(
            new Sales_Process__c(
                Product_Line_to_Proposal__c = proposal.Id,
                Contact_from_Offer_Line__c = proposal.Contact__c,
                Offer_Line_Resource__c = flat.Id        
            ), 
            true
        );

        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = proposal.Contact__c,
                Offer__c = proposal.Id,
                Agreement_Value__c = flat.Price__c
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        
        Extension__c extChange =  TestHelper.createExtensionChange(
            new Extension__c(
                Resource__c = productLine.Offer_Line_Resource__c,
                After_sales_Service__c = afterSales.Id
            ), 
            true
        );
        Payment__c paymentForChange = TestHelper.createPaymentForChange(
            new Payment__c(
                Change__c = extChange.Id,
                After_sales_Service__c = afterSales.Id
            ),
            false
        );
        insert paymentForChange;
        Test.stopTest();
    }*/

    @isTest static void TestPaymentInterestNote(){
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
                new Sales_Process__c(
                    Contact__c = productLine.Contact_from_Offer_Line__c,
                    Offer__c = productLine.Product_Line_to_Proposal__c,
                    Agreement_Value__c = 987
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        

        Extension__c extChange =  TestHelper.createExtensionChange(
            new Extension__c(
                Resource__c = productLine.Offer_Line_Resource__c,
                After_sales_Service__c = afterSales.Id
            ), 
            true
        );

        Payment__c paymentInterestNote = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INTEREST_NOTE),
            After_sales_Service__c = afterSales.Id
        );
        insert paymentInterestNote;
        Test.stopTest();
    }

    @isTest static void TestPaymentDeposit(){
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
            
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        

        Extension__c extChange =  TestHelper.createExtensionChange(
            new Extension__c(
                Resource__c = productLine.Offer_Line_Resource__c,
                After_sales_Service__c = afterSales.Id
            ), 
            true
        );
   
        Payment__C paymentsDeposits = TestHelper.createPaymentsDeposit(
            new Payment__c(
            ),
            false
        );
        insert paymentsDeposits;
        Test.stopTest();
    }

    @isTest static void TestPaymentInstallments(){
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
            
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
                ), 
                true
            );        

            Extension__c extChange =  TestHelper.createExtensionChange(
            new Extension__c(
                Resource__c = productLine.Offer_Line_Resource__c,
                After_sales_Service__c = afterSales.Id
                ), 
                true
            );

        Payment__C paymentsDeposits = TestHelper.createPaymentsInstallment(new Payment__c(), true );
        Test.stopTest();
    }

    @isTest static void TestPaymentRelatedToSaleTerms(){
    // Obtain bank account for regular payment that is related to sale terms and is not an installment nor a deposit record
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        

        Extension__c extChange =  TestHelper.createExtensionChange(
            new Extension__c(
                Resource__c = productLine.Offer_Line_Resource__c,
                After_sales_Service__c = afterSales.Id
            ), 
            true
        );
        // Selected Resource does not match any Product Line attached to the same Sale Terms

        Payment__c paymentInterestNote = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT),
            After_sales_Service__c = afterSales.Id,
            Agreements_Installment__c = saleTerms.Id,
            Payment_For__c = productLine.Offer_Line_Resource__c,
            Due_Date__c = Date.today()
        );
        insert paymentInterestNote;
        Test.stopTest();
    }
   
    @isTest static void TestPaymentIncomingIsEscrowAccount(){
        // Copy incoming payments's After_sales_from_Incoming_Payment__c to After_sales_from_Incoming_Payment_Curren__c or 
        // After_sales_from_Incoming_Payment_Trust__c to differentiate records between related lists 
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        

        Extension__c extChange =  TestHelper.createExtensionChange(
            new Extension__c(
                Resource__c = productLine.Offer_Line_Resource__c,
                After_sales_Service__c = afterSales.Id
            ), 
            true
        );
        // Selected Resource does not match any Product Line attached to the same Sale Terms
        Payment__c paymentInterestNote = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT),
                // Bank account contains 4952 which is ESCROW_ACCOUNT_PART
                Bank_Account_For_Resource__c = '85114010104952120100100006', 
                Paid_Value__c = 250000
            );
        insert paymentInterestNote;

        List<Payment__c> result = [SELECT After_sales_from_Incoming_Payment_Trust__c , 
                                          After_sales_from_Incoming_Payment__c 
                                FROM Payment__c WHERE Id = :paymentInterestNote.Id];
        System.assertEquals(result[0].After_sales_from_Incoming_Payment_Trust__c, result[0].After_sales_from_Incoming_Payment__c);
        Test.stopTest();
    }
                   
    @isTest static void TestPaymentIncomingIsWorkingAccount(){
        // Copy incoming payments's After_sales_from_Incoming_Payment__c to After_sales_from_Incoming_Payment_Curren__c or
        // After_sales_from_Incoming_Payment_Trust__c to differentiate records between related lists 
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        

        Extension__c extChange =  TestHelper.createExtensionChange(
            new Extension__c(
                Resource__c = productLine.Offer_Line_Resource__c,
                After_sales_Service__c = afterSales.Id
            ), 
            true
        );
        // Selected Resource does not match any Product Line attached to the same Sale Terms
        Payment__c paymentInterestNote = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT),
            // Bank account contains 3679 which is WORKING_ACCOUNT_PART
                Bank_Account_For_Resource__c = '94114010103679140400100006', 
                Paid_Value__c = 250000
            );
        insert paymentInterestNote;

        List<Payment__c> result = [SELECT After_sales_from_Incoming_Payment_Curren__c, 
                                          After_sales_from_Incoming_Payment__c 
                                FROM Payment__c WHERE Id = :paymentInterestNote.Id];
        System.assertEquals(result[0].After_sales_from_Incoming_Payment_Curren__c, result[0].After_sales_from_Incoming_Payment__c);
        Test.stopTest();
    }
    @isTest static void TestPaymentIncomingIsUnknownAccount(){
        // Copy incoming payments's After_sales_from_Incoming_Payment__c to After_sales_from_Incoming_Payment_Curren__c or 
        // After_sales_from_Incoming_Payment_Trust__c to differentiate records between related lists 
        Sales_Process__c productLine = TestHelper.createSalesProcessProductLine(null, true);
        Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
            new Sales_Process__c(
                Contact__c = productLine.Contact_from_Offer_Line__c,
                Offer__c = productLine.Product_Line_to_Proposal__c,
                Agreement_Value__c = 987
            ),
            true
        );

        Test.startTest();
        Sales_Process__c afterSales = TestHelper.createSalesProcessAfterSalesService(
            new Sales_Process__c(
                Agreement__c = saleTerms.Id, 
                Contact__c = saleTerms.Contact__c
            ), 
            true
        );        

        Extension__c extChange =  TestHelper.createExtensionChange(
            new Extension__c(
                Resource__c = productLine.Offer_Line_Resource__c,
                After_sales_Service__c = afterSales.Id
            ), 
            true
        );
        // Selected Resource does not match any Product Line attached to the same Sale Terms
        Payment__c paymentInterestNote = new Payment__c(
            RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT),
            // Bank account does not contain 3679 or 4952 so it's not 
            Bank_Account_For_Resource__c = '94114010104567140400100006', 
            Paid_Value__c = 250000
            );
        insert paymentInterestNote;

        List<Payment__c> result = [SELECT After_sales_from_Incoming_Payment_Curren__c, 
                                          After_sales_from_Incoming_Payment__c 
                                    FROM Payment__c WHERE Id = :paymentInterestNote.Id];
        System.assertEquals(result[0].After_sales_from_Incoming_Payment_Curren__c, result[0].After_sales_from_Incoming_Payment__c);
        Test.stopTest();
    }
}