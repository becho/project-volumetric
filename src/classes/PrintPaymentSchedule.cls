/**
* @author       Mateusz Pruszyński
* @description  Controller for PrintPaymentSchedule.page used to print payment schedule into pdf
**/

global class PrintPaymentSchedule {

    // records to display on page
    public Sales_Process__c process {get; set;}
    public List<Sales_Process__c> customers {get; set;}
    public PaymentScheduleWrapper scheduleWrapper {get; set;}

    // helper records
    public List<Sales_Process__c> productLines {get; set;}
    public List<Payment__c> installments {get; set;}

    // logo to be displayed on page
    public String logo {get; set;}

    // flags for displaying on the page
    public Boolean displayCurrentAccount {get; set;}
    public Boolean displayTrustAccount {get; set;}

    // Current bank account
    public String currentBankAccount {get; set;}

    // Trust bank account
    public String trustBankAccount {get; set;}

    // currency
    public String isoCode { get {return CommonUtility.getCurrencySymbolFromIso(UserInfo.getDefaultCurrency());} set; }

    public PrintPaymentSchedule(ApexPages.StandardController stdController) {
        process = (Sales_Process__c)stdController.getRecord();
        process = [SELECT Id, Agreement_Value__c, Name, Customer_Company__c FROM Sales_Process__c WHERE Id = :process.Id];
        displayCurrentAccount = false;
        displayTrustAccount = false;
        installments = getInstallments();
        customers = getCustomers();
        productLines = getProductLines();


        for(Sales_Process__c productLine : productLines) {
            if(productLine.Offer_Line_Resource__r.Trust_Account__c == true) {
                displayTrustAccount = true;
                /*trustBankAccount = String.valueOf(productLine.Bank_Account_Trust__c);*/
            }
            if(productLine.Offer_Line_Resource__r.Trust_Account__c == false) {
                displayCurrentAccount = true;
                /*currentBankAccount = String.valueOf(productLine.Bank_Account_General__c);*/
            }           
        }        
        logo = getLogo();
        scheduleWrapper = getscheduleWrapper();
    }

    // search for schedule installments
    public List<Payment__c> getInstallments() {
        return [SELECT Id, Name, Of_contract_value__c, Installment__c, Amount_to_pay__c, Payment_date__c, 
                       Due_Date__c, Payment_For__c, Bank_Account_For_Resource__c, Payment_For__r.Trust_Account__c
                FROM Payment__c 
                WHERE Agreements_Installment__c = :process.Id 
                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)];
    }

    // search for related customers to agreement
    public List<Sales_Process__c> getCustomers() {
        return [SELECT Id, Name, Account_from_Customer_Group__c, Account_from_Customer_Group__r.Name 
                FROM Sales_Process__c 
                WHERE Developer_Agreement_from_Customer_Group__c = :process.Id 
                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)];
    }

    // search for product lines
    public List<Sales_Process__c> getProductLines() {
        return [SELECT Id, Offer_Line_Resource__r.InvestmentId__c, Offer_Line_Resource__c, Offer_Line_Resource__r.Name, Offer_Line_Resource__r.RecordTypeId, 
                       Offer_Line_Resource__r.Area__c, Offer_Line_Resource__r.Price_per_Square_Meter__c, Discount_Amount_Offer__c,
                       Price_With_Discount__c, Offer_Line_Resource__r.Parking_Space_Type__c, Bank_Account_General__c, Bank_Account_Number__c, Bank_Account_Trust__c,
                       Offer_Line_Resource__r.Trust_Account__c 
                FROM Sales_Process__c 
                WHERE Offer_Line_to_Sale_Term__c = :process.Id 
                AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)];
    }

    // search for logo image
    public String getLogo() {
        Id investmentId;
        for(Sales_Process__c productLine : productLines) {           
            if(String.isNotBlank(productLine.Offer_Line_Resource__r.InvestmentId__c)) {
                investmentId = productLine.Offer_Line_Resource__r.InvestmentId__c;
                break;
            }
        }
        if(String.isNotBlank(investmentId)) {
            Resource__c investment = [SELECT Id, Account__c FROM Resource__c WHERE Id = :investmentId];
            try {
                return '/servlet/servlet.FileDownload?file=' + [SELECT Id FROM Attachment WHERE Parent.Type='Account' AND ParentId = :investment.Account__c AND Name like 'logo%' LIMIT 1][0].Id;
            } catch(Exception e) {
                ErrorLogger.log(e);
                return 'no_logo_found';
            }
        } else {
            return 'no_logo_found';
        }
    }

    // create wrapper structure to display fields on page
    public PaymentScheduleWrapper getscheduleWrapper() {
        PaymentScheduleWrapper psw = new PaymentScheduleWrapper();
        for(Sales_Process__c productLine : productLines) {
            if(productLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)) {
                psw.productLines.productLines_flat.add(productLine);
            } else if(productLine.Offer_Line_Resource__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)) {
                if(productLine.Offer_Line_Resource__r.Parking_Space_Type__c == CommonUtility.RESOURCE_PARKING_SPACE_TYPE_BICECLES) {
                    psw.productLines.productLines_parkingSpace_bike.add(productLine);
                } else if(productLine.Offer_Line_Resource__r.Parking_Space_Type__c == CommonUtility.RESOURCE_PARKING_SPACE_TYPE_MOTO) {
                    psw.productLines.productLines_parkingSpace_moto.add(productLine);
                } else {
                    psw.productLines.productLines_parkingSpace_car.add(productLine);
                }   
            }
        }

        psw.productLines.getPricePerSquareMeter();

        for(Payment__c installment : installments) {
            installment.Amount_to_pay__c.setScale(2, RoundingMode.HALF_UP);
            /*if(displayCurrentAccount && String.valueOf(installment.Bank_Account_For_Resource__c) == currentBankAccount) {*/
            if(installment.Payment_For__r.Trust_Account__c == false ) {
                psw.installmentsCurrent.add(installment);
            }
            /*if(displayTrustAccount && String.valueOf(installment.Bank_Account_For_Resource__c) == trustBankAccount) {*/
            if(installment.Payment_For__r.Trust_Account__c == true) {
                psw.installmentsTrust.add(installment);
            }
        }
        return psw;
    }

    // Wrapper structure for payment schedule
    public class PaymentScheduleWrapper {
        public ProductLinesWrapper productLines {get; set;}
        public List<Payment__c> installmentsCurrent {get; set;}
        public List<Payment__c> installmentsTrust {get; set;}
        public PaymentScheduleWrapper() {
            productLines = new ProductLinesWrapper();
            installmentsCurrent = new List<Payment__c>();
            installmentsTrust = new List<Payment__c>();
        }
    }

    // Wrapper structure for product lines
    public class ProductLinesWrapper {
        public List<Sales_Process__c> productLines_flat {get; set;}
        public List<Sales_Process__c> productLines_parkingSpace_car {get; set;}
        public List<Sales_Process__c> productLines_parkingSpace_moto {get; set;}
        public List<Sales_Process__c> productLines_parkingSpace_bike {get; set;}

        public Map<Id, Decimal> flat_sqrMeterPrice_map {get; set;}

        public ProductLinesWrapper() {
            productLines_flat = new List<Sales_Process__c>();
            productLines_parkingSpace_car = new List<Sales_Process__c>();
            productLines_parkingSpace_moto = new List<Sales_Process__c>();
            productLines_parkingSpace_bike = new List<Sales_Process__c>();
            flat_sqrMeterPrice_map = new Map<Id, Decimal>();
        }

        public void getPricePerSquareMeter() {
            for(Sales_Process__c pl : productLines_flat) {
                Decimal flatSqrMtrPriceAfterDiscount = pl.Offer_Line_Resource__r.Price_per_Square_Meter__c - (pl.Discount_Amount_Offer__c != null ? (pl.Discount_Amount_Offer__c / ((pl.Offer_Line_Resource__r.Area__c != null && pl.Offer_Line_Resource__r.Area__c > 0) ? pl.Offer_Line_Resource__r.Area__c : pl.Offer_Line_Resource__r.Area__c)) : 0);    
                flat_sqrMeterPrice_map.put(pl.Id, flatSqrMtrPriceAfterDiscount.setScale(2, RoundingMode.HALF_UP));
            }                                   
        }
    }

    // Method called from Print Payment Schedule button (Sale Terms layout)
    // This method is used to print content into pdf and save it as an attachment to the current sale terms record
    Webservice static String printSchedule(String saleTermsId) {
        ApexPages.StandardController sc = new ApexPages.StandardController(new Sales_Process__c(Id = saleTermsId));
        PrintPaymentSchedule pps = new PrintPaymentSchedule(sc);
        PageReference p = Page.PrintPaymentSchedule;
        p.getParameters().put(CommonUtility.URL_PARAM_ID, saleTermsId);
        
        Attachment attach = new Attachment();
        Blob body;

        try {
            body = p.getContent();

        // testing bug!
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }

        attach.Body = body;
        attach.ContentType = CommonUtility.FILETYPE_PDF;
        attach.Name = Label.PaymentSchedule + '.pdf';
        attach.IsPrivate = false;
        attach.ParentId = saleTermsId;
        try {
            insert attach;
            return 'true';
        } catch(Exception e) {
            ErrorLogger.log(e);
            return String.valueOf(e);
        }

    }
}