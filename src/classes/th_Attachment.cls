public without sharing class th_Attachment extends TriggerHandler.DelegateBase {

	public Set<Id> saleTermsIdsIns;
	public Set<Id> saleTermsIdsDel;
    public List<Attachment> attachedDeclarations;
    public Set<Id> changeIds;

	public override void prepareAfter() {
    	saleTermsIdsIns = new Set<Id>();  
    	saleTermsIdsDel = new Set<Id>();
        attachedDeclarations = new List<Attachment>();
        changeIds = new Set<Id>();
	}

	public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Attachment> newAttachments = (Map<Id, Attachment>)o;
        for(Id key : newAttachments.keySet()) {
            Attachment newAttachment = newAttachments.get(key);

            // Mateusz Pruszyński
            /* attaching sale terms -> Sale_Terms_Printed__c = true */
            if(newAttachment.Name != null && newAttachment.Name.containsIgnoreCase(Label.docgen_SaleTerms) 
                && newAttachment.ParentId != null 
                && newAttachment.ParentId.getSobjectType() == Schema.Sales_Process__c.SObjectType) {
                saleTermsIdsIns.add(newAttachment.ParentId);
            }

            // Mateusz Pruszyński
            /* attaching reservation declaration -> increment CurrentDeclarationNumber__c AND Local_Declaration_Number__c */
            if(newAttachment.Name != null 
                && newAttachment.Name.containsIgnoreCase(Label.docgen_ReservationDeclaration) 
                && newAttachment.ParentId != null 
                && newAttachment.ParentId.getSobjectType() == Schema.Sales_Process__c.SObjectType) {
                attachedDeclarations.add(newAttachment);
            }

            /* Mateusz Wolak-Ksiazek */
            // attaching any document to change -> email alert to construction manager
            if(newAttachment.ParentId.getSobjectType() == Schema.Extension__c.SObjectType) {
                changeIds.add(newAttachment.ParentId);
            }
        }
	}

	public override void afterDelete(Map<Id, sObject> old) {
		Map<Id, Attachment> oldAttachments = (Map<Id, Attachment>)old;
        for(Id key : oldAttachments.keySet()) {
            Attachment oldAttachment = oldAttachments.get(key);

            // Mateusz Pruszyński
            /* attaching sale terms -> Sale_Terms_Printed__c = true */
            if(oldAttachment.Name != null && oldAttachment.Name.containsIgnoreCase(Label.docgen_SaleTerms) 
                && oldAttachment.ParentId != null 
                && oldAttachment.ParentId.getSobjectType() == Schema.Sales_Process__c.SObjectType) {
                saleTermsIdsDel.add(oldAttachment.ParentId);
            }          
        }
	}

	public override void finish() {
        // Mateusz Pruszyński
        /* attaching reservation declaration -> increment CurrentDeclarationNumber__c AND Local_Declaration_Number__c */
        if(attachedDeclarations != null && attachedDeclarations.size() > 0) {
            ManageDeclarationReservationNumber.renumberFromAttachment(attachedDeclarations);
        }

        // Mateusz Pruszyński
        /* attaching sale terms -> Sale_Terms_Printed__c = true */
        // - insert
        if(saleTermsIdsIns != null && saleTermsIdsIns.size() > 0) {
        	List<Sales_Process__c> saleTerms = [SELECT Id, Name FROM Sales_Process__c WHERE Id in :saleTermsIdsIns AND Sale_Terms_Printed__c = false];
        	if(!saleTerms.isEmpty()) {
        		for(Sales_Process__c st : saleTerms) {
        			st.Sale_Terms_Printed__c = true;
        		}
        		try {
        			update saleTerms;
        		} catch(Exception e) {
        			ErrorLogger.log(e);
        		}
        	}
        }
        // - delete
        if(saleTermsIdsDel != null && saleTermsIdsDel.size() > 0) {
        	List<Sales_Process__c> saleTerms = [SELECT Id, Name FROM Sales_Process__c WHERE Id in :saleTermsIdsDel AND Sale_Terms_Printed__c = true];
        	if(!saleTerms.isEmpty()) {
        		for(Sales_Process__c st : saleTerms) {
        			st.Sale_Terms_Printed__c = false;
        		}
        		try {
        			update saleTerms;
        		} catch(Exception e) {
        			ErrorLogger.log(e);
        		}
        	}
        }
        // end - attaching sale terms

        /* Mateusz Wolak-Ksiazek */
        if(changeIds != null && changeIds.size() > 0) {
            List<Extension__c> changes = [SELECT Last_date_of_changed_change__c FROM Extension__c WHERE Id in: changeIds AND Change_taken__c =: true];
            if(!changes.isEmpty()) {
                List<Extension__c> changesToUpdate = new List<Extension__c>();
                for(Extension__c change : changes) {
                    change.Last_date_of_changed_change__c = Datetime.now();
                    changesToUpdate.add(change);
                }
                if(!changesToUpdate.isEmpty()) {
                    update changesToUpdate;
                }
            }
        }
	}

}