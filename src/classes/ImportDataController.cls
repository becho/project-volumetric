public class ImportDataController {
	/*
		Author: Wojciech Słodziak
		Class that is responsible for preventing tab from switcing between actions
	*/
	private static final String SELECT_TAB1 = 'tab1';
	
	public String selectedTab { get; set; }

	public ImportDataController() {
		selectedTab = SELECT_TAB1; 
	}
}