public without sharing class NewLine {

    public String newLine {get; set;}

    public NewLine(){
      newLine = '\r\n';
    }

}