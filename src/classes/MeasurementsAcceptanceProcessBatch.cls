/**
* @author 		Mateusz Pruszynski
* @description 	Class with methods to manage acceptance process of measured resource area 
**/

global class MeasurementsAcceptanceProcessBatch implements Database.Batchable<Sales_Process__c>, Database.Stateful {
	
	Set<Id> resourcesIds = new Set<Id>();	
	
	// Constructor initialization - parameters from TH_Resource
	global MeasurementsAcceptanceProcessBatch(Map<Id, Resource__c> resources) {

		for(Id key : resources.keySet()) {

			Resource__c res = resources.get(key);
            if(res.Total_Area_Measured__c - res.Usable_Area_Planned__c < 0) {
                resourcesIds.add(res.Id);
            }	

        }	

	}
	
	// Query method
	global Iterable<Sales_Process__c> start(Database.BatchableContext BC) {
		return [
			SELECT Id, Discount_Amount_Offer__c, Offer_Line_Resource__c, Offer_Line_Resource__r.Price_After_Measurement__c, Offer_Line_Resource__r.Price__c,
				   Offer_Line_to_Sale_Term__c, Product_Line_to_Proposal__c, Offer_Line_to_After_sales_Service__c, Price_With_Discount__c, 
				   Offer_Line_Resource__r.The_Difference_After_Measurement__c, Offer_Line_Resource__r.Usable_Area_Planned__c,
				   Offer_Line_to_After_sales_Service__r.Balance__c, Offer_Line_Resource__r.Price_Difference__c, Queue__c, Offer_Line_to_Sale_Term__r.Status__c
			FROM Sales_Process__c 
			WHERE Offer_Line_Resource__c in :resourcesIds
			AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
			AND Product_Line_to_Proposal__c <> null
		];
	}

	// Execute apex logic for acceptance process
   	global void execute(Database.BatchableContext BC, List<Sales_Process__c> scope) {

   		MeasurementsAcceptanceProcess.manageSalesProcessAndPaymentSchedule(scope);

	}
	
	// Does nothing
	global void finish(Database.BatchableContext BC) {}
	
}