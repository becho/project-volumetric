/**
* @author      Krystian Bednarek
* @description  This class is used to generate invoice pdf file by creating new record from a single payment record. A new invoice object is than created
*               with pdf attached to it.
*/

public with sharing class PdfAdvanceInvoiceController {


    
    public Invoice__c advancedInvoice {get; set;}
    public List<Invoice__c> invoiceLines {get; set;}
    public Resource__c investment{get; set;}

    public PdfAdvanceInvoiceController() {

    

            Id invoiceId = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_ID);

            advancedInvoice = [SELECT   Id, 
                                Name,
                                RecordType.Name, 
                                Invoice_number__c, 
                                Invoice_For__c,
                                Invoice_For__r.InvestmentId__c,
                                Payment__r.Payment_date__c, 
                                Payment__r.Bank_Account_For_Resource__c, 
                                Invoice_Date__c,
                                Client__r.Name,
                                Client__r.BillingAddress,
                                Client__r.BillingStreet,
                                Client__r.BillingPostalCode,
                                Client__r.BillingCity,
                                Client__r.BillingCountry,
                                Client__r.NIP__c,
                                Client__r.PESEL__c,
                                Client__r.IsPersonAccount,
                                Gross_Value__c
                                  
                               FROM 
                                Invoice__c 
                               WHERE 
                                Id = :invoiceId];
 
            invoiceLines = [SELECT 
                            Invoice_From_Invoice_Line__r.Invoice_For__r.Name,
                            Description__c,
                            Net_Value__c, 
                            Gross_Value__c,                          
                            VAT__c, 
                            VAT_Amount__c,
                            Invoice_For__c
                        FROM 
                            Invoice__c 
                        WHERE  
                        Invoice_From_Invoice_Line__c = :invoiceId];
                

        

         investment = [SELECT Id, 
                            Account__r.Name,
                            Account__r.NIP__c,
                            Account__r.BillingAddress,
                            Account__r.BillingStreet,
                            Account__r.BillingPostalCode,
                            Account__r.BillingCountry,
                            Account__r.BillingCity
     
                    FROM 
                        Resource__c 
                    WHERE 
                        Id = :advancedInvoice.Invoice_For__r.InvestmentId__c];

            


}}