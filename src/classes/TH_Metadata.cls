public without sharing class TH_Metadata extends TriggerHandler.DelegateBase {


	Set<Id> metadataIdsToUpdateType { get; set; }
	public override void prepareBefore(){
		
	}

	public override void prepareAfter(){
		metadataIdsToUpdateType = new Set<Id>();

	}

	public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o){
		Map<Id, Metadata__c> metaOld = (Map<Id, Metadata__c>)old;
		Map<Id, Metadata__c> metaNew = (Map<Id, Metadata__c>)o;
		for(Id key : metaNew.keySet()){
			Metadata__c metaN = metaNew.get(key);
			Metadata__c metaO = metaOld.get(key);

			/* 
			/ Code Part made by Wojciech Słodziak
			/ Trigger part prevents possibility of multiple datasheets on one resource
			*/
			if (metaO.Metadata_type__c != metaN.Metadata_type__c && metaN.Metadata_type__c == CommonUtility.METADATA_TYPE_DATASHEET) {
				metadataIdsToUpdateType.add(metaN.Id);
			}
		}
	}


	public override void afterInsert(Map<Id, sObject> o){
		Map<Id, Metadata__c> meta = (Map<Id, Metadata__c>)o;
		for(Id key : meta.keySet()){
			Metadata__c metaT = meta.get(key);

			/* 
			/ Code Part made by Wojciech Słodziak
			/ Trigger part prevents possibility of multiple datasheets on one resource
			*/
			if (metaT.Metadata_type__c == CommonUtility.METADATA_TYPE_DATASHEET) {
				metadataIdsToUpdateType.add(metaT.Id);
			}
		}
	}

	public override void finish(){

		/* 
		/ Code Part made by Wojciech Słodziak
		/ Trigger part prevents possibility of multiple datasheets on one resource
		*/
		if (metadataIdsToUpdateType != null && metadataIdsToUpdateType.size() > 0) {
			List<Metadata__c> metadataList = [SELECT RecordID__c FROM Metadata__c WHERE Id in :metadataIdsToUpdateType AND ObjectID__c = :CommonUtility.SOBJECT_NAME_RESOURCE];
			Set<String> resIdList = new Set<String>();
			for (Metadata__c metadata : metadataList) {
				resIdList.add(metadata.RecordID__c);
			}
			List<Metadata__c> otherMetadataList = [SELECT Metadata_type__c FROM Metadata__c WHERE RecordID__c in :resIdList AND Metadata_type__c = :CommonUtility.METADATA_TYPE_DATASHEET AND ID not in :metadataList];
			for (Metadata__c otherMetadata : otherMetadataList) {
				otherMetadata.Metadata_type__c = CommonUtility.METADATA_TYPE_FILE_PDF;
			}
			try {
				update otherMetadataList;
			} catch (Exception e) {
				ErrorLogger.log(e);
			}
		}
	}

}