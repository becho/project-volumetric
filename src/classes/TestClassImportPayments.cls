@isTest
private class TestClassImportPayments {
	
	/*
		Author: Wojciech Słodziak
		Class that is responsible for testing loading Incoming Payments from Bank csv file
	*/
/*
	private static String testData1 = 'Data operacji;Data ksiegowania;Opis operacji;Tytul;Nadawca/Odbiorca;Numer konta;Kwota;\r\n2015-04-21;2015-04-22;"PRZELEW MULTITRANSFER WYCHODZACY";"Splata raty";"Klient";"\'121010101010100000111122224007\'";39 640,00;\r\n2015-05-27;2015-05-28;"PRZELEW MULTITRANSFER WYCHODZACY";"Splata raty";"Klient";"\'12 1010 1010 1010 0000 1111 2222 4007\'";70 000,00;';
	private static String testData2 = 'Data operacji;Data ksiegowania;Opis operacji;Tytul;Nadawca/Odbiorca;Numer konta;Kwota;\r\n2015-04-21;2015-04-22;"PRZELEW MULTITRANSFER WYCHODZACY";"Splata raty";"Klient";"\'121010101010100000111122224007\'";-39 640,00;\r\n2015-05-27;2015-05-28;"PRZELEW MULTITRANSFER WYCHODZACY";"Splata raty";"Klient";"\'12 1010 1010 1010 0000 1111 2222 4007\'";70 000,00;';


	private static Map<Integer, Id> prepareData() {
		Map<Integer, Id> idList = new Map<Integer, Id>();
		idList.put(0, TestHelper.createBankAccount(new Extension__c(Name = '121010101010100000111122224007', Trust__c = true), true).Id);
		idList.put(1, TestHelper.createContact(null, true).Id);
		idList.put(2, TestHelper.createPreliminaryAgreement(new Sales_Process__c(Bank_Account__c = idList.get(0), Contact__c = idList.get(1)), true).Id);
		idList.put(3, TestHelper.createPayment(new Payment__c(Agreements_Installment__c = idList.get(2), Amount_to_pay__c = 39640, Due_Date__c = System.today() + 1), true).Id);
		idList.put(4, TestHelper.createPayment(new Payment__c(Agreements_Installment__c = idList.get(2), Amount_to_pay__c = 80000, Due_Date__c = System.today() + 2), true).Id);

		return idList;
	}


	private static Map<Integer, Id> prepareData2() {
		Map<Integer, Id> idList = new Map<Integer, Id>();
		idList.put(0, TestHelper.createBankAccount(new Extension__c(Name = '121010101010100000111122224007', Trust__c = true), true).Id);
		idList.put(1, TestHelper.createContact(null, true).Id);
		idList.put(2, TestHelper.createPreliminaryAgreement(new Sales_Process__c(Bank_Account__c = idList.get(0), Contact__c = idList.get(1)), true).Id);
		idList.put(3, TestHelper.createPayment(new Payment__c(RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT), Bank_Account__c = idList.get(0), Amount_to_pay__c = 70000, Payment_Date__c = Date.newInstance(2015,5,28)), true).Id);

		return idList;
	}

	@isTest static void testNoDataInSystem() {
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(testData1);
		System.debug('TestDataString: ' + testData1);		
		System.debug('ParsedBlob: ' + ImportPaymentsController.blobToString(ipc.csvFileBody, 'UTF-8'));

		Test.startTest();
		ipc.import();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportPaymentsNoRelatedRecords)) b = true;
		}

		System.assert(b);
	}

	@isTest static void testNegativeAmount() {
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(testData2);
		System.debug('TestDataString: ' + testData2);		
		System.debug('ParsedBlob: ' + ImportPaymentsController.blobToString(ipc.csvFileBody, 'UTF-8'));

		Test.startTest();
		ipc.import();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportPaymentsNegativeAmount)) b = true;
		}

		System.assert(b);
	}

	@isTest static void testMatchBankAccAndAgreementAndInsert() {
		prepareData();
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(testData1);
		System.debug('TestDataString: ' + testData1);		
		System.debug('ParsedBlob: ' + ImportPaymentsController.blobToString(ipc.csvFileBody, 'UTF-8'));

		Test.startTest();
		ipc.import();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportPaymentsNoRelatedRecords)) b = true;
		}


		List<Payment__c> incPayments = [SELECT Id, Unaccounted__c FROM Payment__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT)];
		System.debug('Inc Installments: ' + incPayments);
		List<Payment__c> installments = [SELECT Id, Paid__c, Installment_Unpaid_Amount__c FROM Payment__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT) ORDER BY Due_Date__c ASC];
		System.debug('Paid Installments: ' + installments);

		System.assert(!b);
		System.assertEquals(2, incPayments.size());
		System.assertEquals(2, installments.size());
		System.assertEquals(10000, installments.get(1).Installment_Unpaid_Amount__c);
	}

	@isTest static void testDuplicates() {
		prepareData2();

		List<Payment__c> incPayments = [SELECT Id, Unaccounted__c, Amount_to_pay__c, Payment_Date__c, Bank_Account__c FROM Payment__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT)];
		System.debug('Inc Installments: ' + incPayments);

		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(testData1);
		System.debug('TestDataString: ' + testData1);		
		System.debug('ParsedBlob: ' + ImportPaymentsController.blobToString(ipc.csvFileBody, 'UTF-8'));

		Test.startTest();
		ipc.import();
		Test.stopTest();


		List<Payment__c> incPayments2 = [SELECT Id, Unaccounted__c, Amount_to_pay__c, Payment_Date__c, Bank_Account__c FROM Payment__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT)];
		System.debug('Inc Installments2: ' + incPayments2);


		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportPaymentsPossibleDuplicates)) b = true;
		}

		System.assert(b);
		System.assert(ipc.verify);
	}

	@isTest static void testNoFile() {
		ImportPaymentsController ipc = new ImportPaymentsController();

		Test.startTest();
		ipc.import();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportPaymentsFileNotProvided )) b = true;
		}

		System.assert(b);
		System.assert(ipc.errors);

		ipc.cancel();
	}

	@isTest static void testDummyImportDataCntrl() {
		ImportDataController idc = new ImportDataController();

		System.assertEquals('tab1', idc.selectedTab);
	}

	*/

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

/* Grzegorz Murawiński */

/* 	Class that is responsible for testing loading Incoming Payments from Bank csv file */ 

private static String testData1 = '"payment_id";"payment_amount";"payment_date";"contractor_number";"remarks";"invoice_number";"invoice_issue_date";"payment_bank_account"\r\n"B01001412345    0014";444,00;14.10.2016;"0037801";"#5618/RP30/M/2";"U000019/16/10-01";17.10.2016;"12345612345612345612345600";';
private static String testDataZeroAmount = '"payment_id";"payment_amount";"payment_date";"contractor_number";"remarks";"invoice_number";"invoice_issue_date";"payment_bank_account"\r\n"B01001412345    0014";0;14.10.2016;"0037801";"#5618/RP30/M/2";"U000019/16/10-01";17.10.2016;"12345612345612345612345600";';
private static String nullTestData = '"payment_id";"payment_amount";"payment_date";"contractor_number";"remarks";"invoice_number";"invoice_issue_date";"payment_bank_account"\r\n';
private static String wrongTestData = '"payment_id";"payment_amount";"payment_date";"contractor_number";"remarks";"invoice_number";"invoice_issue_date";"payment_bank_account"\r\n"B01001412345    0014";434;14.10.2016;"0037801";"#5618/RP30/M/2";"U000019/16/10-01";"17.10.2016";"12345612345612345612345600";';


	@isTest static void testNoDataInSystem() {

	//Prepare test csv file body
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(testData1);
	

		Test.startTest();

		ipc.import();
	
		Test.stopTest();


		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportPaymentsNoRelatedRecords)) b = true;
		}

		System.assertEquals(true,b);
	}

	@isTest static void testZeroAmount() {

	//Prepare test csv file body
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(testDataZeroAmount);
	

		Test.startTest();

		ipc.import();
	
		Test.stopTest();


		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
			System.debug(msg);
		    if (msg.getDetail().contains(Label.ImportPaymentsNegativeAmount+'1')) b = true;
		}

		System.assert(b);
	}

	@isTest static void testCancel() {
		ImportPaymentsController ipc = new ImportPaymentsController();

		Test.startTest();
		ipc.cancel();
		ipc.import();
		Test.stopTest();

		System.assertEquals(null, ipc.payments);
		System.assertEquals(null, ipc.paymentsInstallment);
		System.assertEquals(false, ipc.verify);
//		System.assertEquals(false, ipc.byImportButton);
	}

	@isTest static void testNoFile() {
		ImportPaymentsController ipc = new ImportPaymentsController();

		Test.startTest();
		ipc.import();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportContractsFileNotProvided )) b = true;
		}

		System.assert(b);
	}

	@isTest static void testNoRecords() {
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(nullTestData);

		Test.startTest();
		ipc.import();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportContractsNoRecords )) b = true;
		}
		System.assert(b);
	}

	@isTest static void testWrongCSV(){
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(wrongTestData);

		Test.startTest();
		ipc.import();
		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportDataInsertingError )) b = true;
		}
		System.assert(b);

	}

	@isTest static void testImportPayment(){
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(testData1);

		Test.startTest();
		Sales_Process__c aftSale = TestHelper.createSalesProcessAfterSalesService(
			new Sales_Process__c(
				Bank_Account_Number__c = '12345612345612345612345600',
				
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL) 
				), 
			true
		); 
		aftSale.Agreement_Number__c = '5618/RP30/M/2"';
		update aftSale;
		
		

		

		ipc.import();

		Test.stopTest();

		List<Payment__c> saleTerms2Assert = [SELECT Id, Amount_to_pay__c
											 FROM Payment__c
											 ];

		System.assertEquals(444, saleTerms2Assert[0].Amount_to_pay__c);
	}


	@isTest static void testImportPaymentDuplicate(){
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(testData1);
		
		Sales_Process__c aftSale = TestHelper.createSalesProcessAfterSalesService(
			new Sales_Process__c(
				Bank_Account_Number__c = '12345612345612345612345600'
				), 
			true
		); 
		
		Payment__c income = TestHelper.createPaymentIncomingPayment(
			new Payment__c(
			After_sales_from_Incoming_Payment__c = aftSale.Id,
			Bank_Account_For_Resource__c = '12345612345612345612345600',
			Payment_Id__c = '"B01001412345    0014"'
			),
			true
		);

		

		Test.startTest();
System.debug(aftSale);
		ipc.import();

		Test.stopTest();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.ImportPaymentsPossibleDuplicates )) b = true;
		}

		System.assert(b);
	}

	@isTest static void testDataVerification(){
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.verify = true;
		ipc.byImportButton = true;		
		Test.startTest();
		ipc.load();
		System.assertEquals(false, ipc.verify);
		Test.stopTest();
	}

	/*@isTest static void testImportUnaccounted(){
		ImportPaymentsController ipc = new ImportPaymentsController();
		ipc.csvFileBody = Blob.valueOf(testData1);
System.debug('##$$ Insert ProductLine $$##');
		
		Sales_Process__c aftSale = TestHelper.createSalesProcessAfterSalesService(
			new Sales_Process__c(
				Bank_Account_Number__c = '12345612345612345612345600',
				Offer_Line_to_After_sales_Service__c = 'a0K4E000000tW0bUAE'
				), 
			true
		); 
		
		Payment__c income = TestHelper.createPaymentIncomingPayment(
			new Payment__c(
			After_sales_from_Incoming_Payment__c = aftSale.Id,
			Bank_Account_For_Resource__c = '12345612345612345612345600',
			Payment_Id__c = '"B01001412345    0015"',
			Agreements_Installment__c = aftSale.Id,
			Paid__c = false,
			Amount_to_pay__c = 445,
			Paid_Value__c = 1
			),
			true
		);

System.debug('####INCOME#### '+income);
		

		Test.startTest();
System.debug(aftSale);
		ipc.import();

		Test.stopTest();

		List<Payment__c> saleTerms2Assert = [SELECT Paid__c, After_sales_from_Incoming_Payment__c, Agreements_Installment__c, Amount_to_pay__c, contractor_number__c, Bank_Account_For_Resource__c, Payment_Id__c, Installment_Unpaid_Amount__c
									  		 FROM Payment__c
									   		 WHERE Payment_Id__c = :income.Payment_Id__c];

System.debug(saleTerms2Assert);
		
		System.assertEquals(true, saleTerms2Assert[0].Paid__c);
	}*/

}