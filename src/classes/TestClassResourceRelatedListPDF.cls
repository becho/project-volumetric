/**
* @author		Mariia Dobzhanska
* @description  Test class for resourcePDF.cls
*/
@isTest
private class TestClassResourceRelatedListPDF {

    public final static String testflname = 'TestFlatName';
    public final static String testflname2 = 'TestFlatName2';
    public final static String inact = 'inactive';
    
	@testsetup
    static void setuptestdata() {
        List<Resource__c> res = new List<Resource__c> ();
        //Creating the Resource with two attachments
        Resource__c testflat = TestHelper.createResourceFlatApartment(null, false);
        testflat.Name = testflname;
        res.add(testflat);
        //Creating the Resource with 13 attachments
        Resource__c testflat2 = TestHelper.createResourceFlatApartment(null, false);
        testflat2.Name = testflname2;
		res.add(testflat2);  
        insert res;
        //Creating the metadata attachments
        List<Metadata__c> metadata = new List<Metadata__c>();
        Metadata__c md1 = new Metadata__c();
        md1.Metadata__c = '{"Category":"listing","originalFilename":"kitchen-673733_1280.jpg","URL":"http://static-mennica.properto-portal.enxoo.com/C.LU.I.01/fzzQyrnzaHSNSu4X_30Jun2016090238GMT_1467277358026.jpg","Main":"false","FileType":"IMG"}';
        md1.Metadata_type__c = CommonUtility.METADATA_TYPE_DATASHEET;
        md1.ObjectID__c = CommonUtility.SOBJECT_NAME_RESOURCE;
        md1.RecordID__c = testflat.Id;
        
        Metadata__c md2 = new Metadata__c();
        md2.Metadata__c = '{"Category":"listing","originalFilename":"kitchen-673733_1280.jpg","URL":"http://static-mennica.properto-portal.enxoo.com/C.LU.I.01/fzzQyrnzaHSNSu4X_30Jun2016090238GMT_1467277358026.jpg","Main":"false","FileType":"IMG"}';
        md2.Metadata_type__c = 'File PDF';
        md2.ObjectID__c = CommonUtility.SOBJECT_NAME_RESOURCE;
        md2.RecordID__c = testflat.Id;
        metadata.add(md1);
        metadata.add(md2);
        
        for (integer i = 0; i < 13; i++) {
            Metadata__c md = new Metadata__c();
            md.Metadata__c = '{"Category":"listing","originalFilename":"kitchen-673733_1280'+i+'.jpg","URL":"http://static-mennica.properto-portal.enxoo.com/C.LU.I.01/fzzQyrnzaHSNSu4X_30Jun2016090238GMT_1467277358026.jpg","Main":"false","FileType":"IMG"}';
            md.ObjectID__c = CommonUtility.SOBJECT_NAME_RESOURCE;
            md.Metadata_type__c = CommonUtility.METADATA_TYPE_DATASHEET;
            md.RecordID__c = testflat2.Id;
            metadata.add(md);
        }
        insert metadata;
    }
    
    /**
	**Testing if the appropriate list with metafiles is received for the resource that has less attachments than the groupsize
	**/  
    
    static testmethod void getMetafilesListTest() {
        //Choosing the resource that has 2 attachments
        String resname = testflname;
        Resource__c resource = [SELECT Id, Name From Resource__c WHERE Name = :resname LIMIT 1];
        
        List<Metadata__c> atts = [
        SELECT FileID__c, Metadata__c, Metadata_type__c, ObjectID__c , RecordID__c, StaticResourceRowID__c, CreatedById, CreatedDate
                                    FROM Metadata__c
                                    WHERE RecordID__c = :resource.Id
                                    ORDER BY CreatedDate ASC
        ];
        
        //Creating an instance of the class for the actual results
        ApexPages.StandardController stdController = new ApexPages.StandardController(resource);
        ResourceRelatedListPDFs testclass = new ResourceRelatedListPDFs(stdController);
        
        //Creating the list with expexted size and values
        List<ResourceRelatedListPDFs.Metafile>expmetafiles = new List<ResourceRelatedListPDFs.Metafile>();
        ResourceRelatedListPDFs.Metafile mf1 = new ResourceRelatedListPDFs.Metafile();
        mf1.fileName = CommonUtility.getMetadataValue(atts[0].Metadata__c, CommonUtility.METADATA_FILE_NAME_ORIGINALFILENAME);
        mf1.meta = atts[0];
        mf1.index = 0;
        ResourceRelatedListPDFs.Metafile mf2 = new ResourceRelatedListPDFs.Metafile();
        mf2.fileName = CommonUtility.getMetadataValue(atts[1].Metadata__c, CommonUtility.METADATA_FILE_NAME_ORIGINALFILENAME);
        mf2.meta = atts[1];
        mf2.index = 1;
        expmetafiles.add(mf1);
        expmetafiles.add(mf2);
        Integer expsize = expmetafiles.size();
        
        Test.startTest();
            List<ResourceRelatedListPDFs.Metafile>actmetafiles = testclass.getMetafilesList();
        Test.stopTest();    
        
        //Checking if the list size and values are as expected
        System.assertEquals(expsize, actmetafiles.size());
        
        for (Integer i = 0; i < expsize ;i++) {
            System.assertEquals(expmetafiles[i].filename, actmetafiles[i].filename);
            System.assertEquals(expmetafiles[i].index, actmetafiles[i].index);
            System.assertEquals(expmetafiles[i].meta, actmetafiles[i].meta);
        }      
    }
    
    /**
	**Testing if the appropriate list with metafiles is received for the resource that has more attachments than the groupsize
	**/  
    
    static testmethod void getMetafilesListTestMore() {
        //Getting the resource and attachments (13)
        String resname = testflname2;
        Resource__c resource = [
                                    SELECT Id, Name
                                    FROM Resource__c
                                    WHERE Name = :resname
                                    LIMIT 1
        ];
        
        List<Metadata__c> atts = [
        SELECT FileID__c, Metadata__c, Metadata_type__c, ObjectID__c , RecordID__c, StaticResourceRowID__c,CreatedById, CreatedDate 
                                    FROM Metadata__c
                                    WHERE RecordID__c = :resource.Id
                                    ORDER BY CreatedDate ASC
        ];
        
        //Creating the controller and a class instance for actual results
        ApexPages.StandardController stdController = new ApexPages.StandardController(resource);
        ResourceRelatedListPDFs testclass = new ResourceRelatedListPDFs(stdController);
        
        Test.startTest();
            testclass.getMetafilesList();
            testclass.nextgroup();
            List<ResourceRelatedListPDFs.Metafile>actmetafiles = testclass.getMetafilesList();
        Test.stopTest();
        
        //Creating the list with expexted results
        List<ResourceRelatedListPDFs.Metafile> expreslist = new List<ResourceRelatedListPDFs.Metafile>(); 
                
        for (Integer i = 6; i < 12; i++) {
        	ResourceRelatedListPDFs.Metafile mf = new ResourceRelatedListPDFs.Metafile();
            mf.fileName = CommonUtility.getMetadataValue(atts[i].Metadata__c, CommonUtility.METADATA_FILE_NAME_ORIGINALFILENAME);
            mf.meta = atts[i];
            mf.index = i;  
            expreslist.add(mf);
        }
        Integer expsize = expreslist.size();
        
        //Checking if the list size and values are as expected
        System.assertEquals(expsize, actmetafiles.size());
        
        for (Integer i = 0; i < expsize ;i++){
            System.assertEquals(expreslist[i].filename, actmetafiles[i].filename);
            System.assertEquals(expreslist[i].index, actmetafiles[i].index);
            System.assertEquals(expreslist[i].meta, actmetafiles[i].meta);
        } 
    }
    
    /**
	**Testing if the appropriate class properties are installed 
	**/     
    
    static testmethod void verifyClassesAndIndexesTest() {
        //Getting the resource and attachments
        String resname = testflname;
        Resource__c resource = [
                                SELECT Id, Name
                                FROM Resource__c
                                WHERE Name = :resname
                                LIMIT 1
        ];
        
        List<Metadata__c> atts = [
                                    SELECT FileID__c, Metadata__c, Metadata_type__c, ObjectID__c , RecordID__c, StaticResourceRowID__c,CreatedById, CreatedDate
                                    FROM Metadata__c
                                    WHERE RecordID__c = :resource.Id
                                    ORDER BY CreatedDate ASC
                                    ];
        
        //Creating the controller and a class instance for actual results
        ApexPages.StandardController stdController = new ApexPages.StandardController(resource);
        ResourceRelatedListPDFs testclass = new ResourceRelatedListPDFs(stdController);
        
        //Creating the expected values
        String expnextclass = inact;
        String exppreviousClass = inact;
        Integer expminindex = 1;
        Integer expmaxindex = 2;
        
        Test.startTest();
            testclass.getMetafilesList();
        Test.stopTest();
        
        //Comparing if the properties are as expected
        System.assertEquals(expnextclass, testclass.nextclass);
        System.assertEquals(exppreviousClass, testclass.previousClass);
        System.assertEquals(expminindex, testclass.minindex);
        System.assertEquals(expmaxindex, testclass.maxindex);     
    }
    
    /**
	**Testing if the appropriate last group is received while the attachment size is 13
	**/
    
    static testmethod void lastGroupTest() {
       //Getting the resource and attachments
        String resname = testflname2;
        Resource__c resource = [
                                SELECT Id, Name
                                FROM Resource__c
                                WHERE Name = :resname
                                LIMIT 1
        ];
        
        List<Metadata__c> atts = [
        SELECT FileID__c, Metadata__c, Metadata_type__c, ObjectID__c , RecordID__c, StaticResourceRowID__c,CreatedById, CreatedDate
                                    FROM Metadata__c
                                    WHERE RecordID__c = :resource.Id
                                    ORDER BY CreatedDate ASC
        ];
        
        //Creating the controller and a class instance for actual results
        ApexPages.StandardController stdController = new ApexPages.StandardController(resource);
        ResourceRelatedListPDFs testclass = new ResourceRelatedListPDFs(stdController); 
        Integer explastgroup = 3;
        
        Test.startTest();
            testclass.getMetafilesList();
            testclass.lastGroup();
        Test.stopTest();
        
        //Checking if the received last group value is as expected
        System.assertEquals(explastgroup, testclass.groupNumber);
    }
    
    /**
	**Testing if the appropriate chosen attachment is deleted
	**/  
    
    static testmethod void deletefileTest() {
        //Getting the resource and attachments
        String resname = testflname;
        Resource__c resource = [
                                SELECT Id, Name
                                FROM Resource__c
                                WHERE Name = :resname
                                LIMIT 1
        ];
        
        List<Metadata__c> atts = [
        SELECT FileID__c, Metadata__c, Metadata_type__c, ObjectID__c , RecordID__c, StaticResourceRowID__c,CreatedById, CreatedDate
                                    FROM Metadata__c
                                    WHERE RecordID__c = :resource.Id
                                    ORDER BY CreatedDate ASC
        ];
        
        //Creating the controller and a class instance for actual results
        ApexPages.StandardController stdController = new ApexPages.StandardController(resource);
        ResourceRelatedListPDFs testclass = new ResourceRelatedListPDFs(stdController);
        //Choosing the second metadata element out of two
        testclass.choice = 1;
        
        Test.startTest();
            testclass.getMetafilesList();
            testclass.deleteFile();
        Test.stopTest();
        
        //Getting the attachments after deleting
        List<Metadata__c> attsdel = [
        SELECT FileID__c, Metadata__c, Metadata_type__c, ObjectID__c , RecordID__c, StaticResourceRowID__c, CreatedById, CreatedDate
                                        FROM Metadata__c
                                        WHERE RecordID__c = :resource.Id
                                        ORDER BY CreatedDate ASC
        ];
                                        
        Integer expsize = atts.size() - 1;
        
        //Checking if the metadata list has one element less and the expected element was removed
        System.assertEquals(expsize, attsdel.size());
        System.assertNotEquals(attsdel[0], testclass.metafiles[testclass.choice].meta);
    }
    
    /**
	**Testing if the property gets the appropriate value with an error text while trying to delete an 
	**attachment that does not exist anymore
	**/
    
    static testmethod void deletefileTestErr() {
        //Getting the resource and attachments
        String resname = testflname;
        Resource__c resource = [
                                SELECT Id, Name
                                FROM Resource__c
                                WHERE Name = :resname
                                LIMIT 1
        ];
        
        List<Metadata__c> atts = [
        SELECT FileID__c, Metadata__c, Metadata_type__c, ObjectID__c , RecordID__c, StaticResourceRowID__c,CreatedById, CreatedDate
                                    FROM Metadata__c
                                    WHERE RecordID__c = :resource.Id
                                    ORDER BY CreatedDate ASC
        ];
        
        //Creating the controller and a class instance for actual results
        ApexPages.StandardController stdController = new ApexPages.StandardController(resource);
        ResourceRelatedListPDFs testclass = new ResourceRelatedListPDFs(stdController);
        Integer delchoice = 1;
        //Choosing the second metadata element out of two
        testclass.choice = delchoice;
        Test.startTest();
            testclass.getMetafilesList();
            //Removing the chosen metadata from the database
            delete atts[delchoice];
            testclass.deleteFile();
        Test.stopTest();
        String experror = System.Label.InsufficientDelPDF;
        //Checking if the property with an error is as expected
        System.assertequals(experror, testclass.deleteInfoMsg);
    }
    
    /**
	**Testing if the groupnumber property is changes to the previous one
	**/ 
    
    static testmethod void previousgroupTest() {
    	//Getting the resource and attachments
        String resname = testflname2;
        Resource__c resource = [
                                SELECT Id, Name
                                FROM Resource__c
                                WHERE Name = :resname
                                LIMIT 1
        ];
        
        List<Metadata__c> atts = [
        SELECT FileID__c, Metadata__c, Metadata_type__c, ObjectID__c , RecordID__c, StaticResourceRowID__c,CreatedById, CreatedDate
                                    FROM Metadata__c
                                    WHERE RecordID__c = :resource.Id
                                    ORDER BY CreatedDate ASC
        ];
        
        
        //Creating the controller and a class instance for actual results, where the groupnumber will be first increased
        //by nextgroup method and then decreased after by previousgroup method
        ApexPages.StandardController stdController = new ApexPages.StandardController(resource);
        ResourceRelatedListPDFs testclass = new ResourceRelatedListPDFs(stdController);
        Test.startTest();
            testclass.getMetafilesList();
            testclass.nextgroup();
            testclass.previousGroup();
        Test.stopTest();
        
        Integer expgroupnumb = 1;
        System.assertEquals(expgroupnumb, testclass.groupNumber);
    }
    
    /**
	**Testing if the groupnumber property is changed to the first
	**/   
    
    static testmethod void firstgroupTest() {
        //Getting the resource and attachments
        String resname = testflname2;
        Resource__c resource = [
                                SELECT Id, Name
                                FROM Resource__c
                                WHERE Name = :resname
                                LIMIT 1
        ];
        
        List<Metadata__c> atts = [
        SELECT FileID__c, Metadata__c, Metadata_type__c, ObjectID__c , RecordID__c, StaticResourceRowID__c,CreatedById, CreatedDate
                                    FROM Metadata__c
                                    WHERE RecordID__c = :resource.Id
                                    ORDER BY CreatedDate ASC
        ];
        
        //Creating the controller and a class instance for actual results where the groupnumber will be first increased
        //by nextgroup method and then set to the first by firstgroup method
        ApexPages.StandardController stdController = new ApexPages.StandardController(resource);
        ResourceRelatedListPDFs testclass = new ResourceRelatedListPDFs(stdController);
        Test.startTest();
            testclass.getMetafilesList();
            testclass.nextgroup();
            testclass.firstGroup();
        Test.stopTest();
        Integer expgroupnumb = 1;
        System.assertEquals(expgroupnumb, testclass.groupNumber);
    }

}