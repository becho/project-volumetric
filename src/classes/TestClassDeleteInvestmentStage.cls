/**
* @author 		Joanna Stępińska
* @description 	test class for DeleteInvestmentStage.cls
**/
@isTest
private class TestClassDeleteInvestmentStage {
	
    //public method for initializing controller and its selected Stages
    public static DeleteInvestmentStage getController(List<Extension__c> selectedStages) {
        ApexPages.StandardSetController pageController = new ApexPages.StandardSetController(selectedStages);
        pageController.setSelected(selectedStages);
        return new DeleteInvestmentStage(pageController);
    }
    
    //public method for intitializing page reference and its parameter retUrl
    public static PageReference getPageReference(String investmentId) {
        PageReference pageRef = Page.DeleteInvestmentStage;
        Test.setCurrentPageReference(pageRef);
        pageRef.getParameters().put(CommonUtility.URL_PARAM_RETURL, investmentId);
        return pageRef;
    }

    //Test method for DeleteInvestmentStage with examplary filled selectedStages
    static testMethod void testDelInvestmentStage() {
        Resource__c investment = TestHelper.createInvestment(null, true);
        PageReference pageRef = getPageReference(String.valueOf(investment.Id));
        DeleteInvestmentStage controller = getController(new Extension__c[] {TestHelper.createExtensionConstructionStage(null, true)});
        Test.startTest();
            String nextPage = controller.yes().getUrl();
            System.assertEquals(String.valueOf(investment.Id), nextPage);
        Test.stopTest();
    }
    
    //Test method for DeleteInvestmentStage with empty selectedStages
    static testMethod void testWithEmptyStages() {
        Resource__c investment = TestHelper.createInvestment(null, true);
        PageReference pageRef = getPageReference(String.valueOf(investment.Id));
        List<Extension__c> emptyStages = new List<Extension__c>();
        DeleteInvestmentStage controller = getController(emptyStages);
        Test.startTest();
        	String nextPage = controller.yes().getUrl();
        	System.assertEquals(String.valueOf(investment.Id), nextPage);
          	List<Apexpages.Message> msgs = ApexPages.getMessages();
			boolean b = false;
			for(Apexpages.Message msg:msgs) {
                if (msg.getDetail().contains(Label.NoRecordsSelected)) {
                    b = true;
                }
            }
			System.assert(b);
        Test.stopTest();
    }
	
}