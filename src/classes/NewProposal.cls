/**
* @author       Przemysław Tustanowski
* @description  Controller for NewProposal.page. This class is used to create new proposal records from within Resource object records
*/

global with sharing class NewProposal {

    public String wrapperListJSON {get; set;}
    public String mainProcessJSON {get; set;}
    public String optionsListJSON {get; set;}
    public String rtJSON {get; set;}
    public String standardOfCompletionJSON {get; set;}
    public String availablePromotionJSON {get; set;}
    public String currencySymbol { get; set;}
    public Decimal maxDep {get; set;}
    public String statusWaiting {get {return CommonUtility.SALES_PROCESS_STATUS_WAITING_FOR_MANAGER_APPROVAL;}}
    public String statusRejected {get {return CommonUtility.SALES_PROCESS_STATUS_REJECTED;}}
    public String rejectedReason {get; set;}
    public String investment {get; set;}
    public String investmentName {get; set;}
    private Sales_Process__c mainProcess;
    private List<RecordType> rtList;

    public String rtFlat {get {return CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT);}}
    public String rtBuilding {get {return CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_BUILDING);}}
    public String rtComPro {get {return CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY);}}
    public NewProposal() {
        //get parameters from url
        currencySymbol = CommonUtility.getCurrencySymbolFromIso('PLN').unescapeHtml4();

        String mainEditProcessId = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_SPID);

        investment = findInvestment(mainEditProcessId);

        if(investment != '') {            
            investmentName = [SELECT Name FROM Resource__c WHERE Id =: investment].Name;
        }

        List<String> resourceIds = new List<String>();
        if(Apexpages.currentPage().getParameters().get('resourceIds') != null){
            resourceIds = String.valueOf(Apexpages.currentPage().getParameters().get('resourceIds')).split(',');
        }

        if(mainEditProcessId != null){
            editProposal(mainEditProcessId, resourceIds);
        } else {
            createProposal(resourceIds);
        }

        getUserMaxDisc();
        standardOfCompletionJSON = StandardsOfCompletionList();
        availablePromotionJSON = AvailablePromotionList();
        if(rtList != null){
            rtJSON = JSON.serialize(rtList);
        }
    }


    private void editProposal(String mainEditProcessId, List<String> resourceIds){
        String mode = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_MODE);
        rtList = mode == 'New' ? getAvailibleTypes() : null;
        String line = Apexpages.currentPage().getParameters().get(CommonUtility.URL_PARAM_LINE);
        EditProposalFromFieldSet editFields = new EditProposalFromFieldSet();
        if(String.valueOf(ApexPages.currentPage().getUrl()).contains('newproposal')){
            List<String> listOfJSON = editFields.getJSONwithAdditionalFields(mainEditProcessId);
            mainProcessJSON = listOfJSON[0];
            optionsListJSON = listOfJSON.size() > 1 ? listOfJSON[1] : null;
            mainProcess = editFields.process;
        } else{
            mainProcess = [SELECT Rejection_Reason__c, Approve_Reason__c FROM Sales_Process__c WHERE Id =: mainEditProcessId];
        }

        List<Sales_Process__c> relatedLines = [SELECT Offer_Line_Resource__c, Discount_Amount_Offer__c, Discount__c, Product_Line_to_Proposal__c, Offer_Line_to_Sale_Term__c, Price_With_Discount__c, Standard_of_Completion__c, Standard_of_Completion__r.Price__c, Standard_of_Completion__r.Name,Standard_of_Completion__r.Status__c, Standard_of_Completion__r.Description__c, Standard_of_Completion__r.Individual__c, Standard_of_Completion_Price__c, Promotion_Definition__c, Promotion_Definition__r.Discount_Value__c, Promotion_Definition__r.Discount_Type__c, Promotion_Definition__r.Discount_Kind__c, Promotion_Definition__r.Description__c, Promotion_Definition__r.Name, Bank_Account_Number__c FROM Sales_Process__c WHERE Product_Line_to_Proposal__c =: mainEditProcessId OR Offer_Line_to_Sale_Term__c  =: mainEditProcessId];
        if(!relatedLines.isEmpty()){
            resourceIds.clear();
            for(Sales_Process__c rl : relatedLines){
                resourceIds.add(rl.Offer_Line_Resource__c);
            }
            List<Resource__c> resources = [SELECT Id, Name, City__c, toLabel(RecordType.Name), Area__c, Street__c, House_Number__c, Price__c, RecordTypeId, Promotional_Price__c, Defined_Promotion__c FROM Resource__c WHERE Id in :resourceIds];
            List<ProposalWrapper> wrapperList = new List<ProposalWrapper>();
            if(mode != 'new'){
                for(Resource__c res : resources){
                    for(Sales_Process__c lin :relatedLines){
                        if(line != null){
                            if(lin.Offer_Line_Resource__c == res.Id && lin.Id == line){
                                ProposalWrapper wrap = new ProposalWrapper(res, lin);
                                rejectedReason = mainProcess.Rejection_Reason__c;
                                wrapperList.add(wrap);
                            }       
                        } else if(lin.Offer_Line_Resource__c == res.Id){
                            ProposalWrapper wrap = new ProposalWrapper(res, lin);
                            rejectedReason = mainProcess.Rejection_Reason__c;
                            wrapperList.add(wrap);
                        }
                    }
                }
            } else {
                ProposalWrapper wrap = new ProposalWrapper(new Resource__c(Name = ''));
                rejectedReason = mainProcess.Rejection_Reason__c;
                wrapperList.add(wrap);
                wrapperList[0].mainProcessId = mainEditProcessId;
            }
            wrapperListJSON = JSON.serialize(wrapperList);
        }
    }

    private void createProposal(List<String> resourceIds){
        List<Resource__c> resources = [SELECT Id, Name, City__c, RecordType.Name, Area__c, Street__c, House_Number__c, Price__c, Promotional_Price__c, Defined_Promotion__c FROM Resource__c WHERE Id in :resourceIds];
        if(!resources.isEmpty()){
            List<ProposalWrapper> wrapperList = new List<ProposalWrapper>();
            for(Resource__c res : resources){
                ProposalWrapper wrap = new ProposalWrapper(res);
                wrapperList.add(wrap);
            }
            wrapperListJSON = JSON.serialize(wrapperList);
        }
    }


    private void getUserMaxDisc(){
        User myUser = [SELECT Id, Maximum_Discount__c FROM User WHERE Id =: UserInfo.getUserId()];
        maxDep = myUser.Maximum_Discount__c != null ? myUser.Maximum_Discount__c : 100;
    }

    global class ProposalWrapper{
        public Resource__c resource {get; set;}
        public Sales_Process__c mainProcessFields {get; set;}
        public Decimal discount {get; set;}
        public Decimal discountAmount {get; set;}
        public String mainProcessId {get; set;}
        public Decimal saleLineValue {get; set;}
        public String saleLineId {get; set;}
        public Sales_Process__c saleLine {get; set;}


        public ProposalWrapper(Resource__c res, Sales_Process__c line){
            this.resource = res;
            this.discount = line.Discount__c.setScale(2, RoundingMode.HALF_UP);
            this.discountAmount = line.Discount_Amount_Offer__c;
            this.saleLineValue = line.Price_With_Discount__c;
            this.mainProcessId = String.isNotBlank(line.Offer_Line_to_Sale_Term__c) ? line.Offer_Line_to_Sale_Term__c : line.Product_Line_to_Proposal__c;
            this.saleLineId = line.Id;
            this.saleLine = line;
            //this is required to proper creation of JSON (with object Standard_of_Completion__r instead of Standard_of_Completion__c)
            //if(line.Standard_of_Completion__c == null){
            //    Resource__c std = new Resource__c(Price__c = 0);
            //    line.Standard_of_Completion__c = std.Id;
            //}
            if(line.Standard_of_Completion_Price__c != null){
                if(line.Standard_of_Completion__c != null){
                    line.Standard_of_Completion__r.Price__c = line.Standard_of_Completion_Price__c;
                } else {
                    line.Standard_of_Completion_Price__c = null;
                }
            }

        }

        public ProposalWrapper(Resource__c res){
            this.resource = res;
            this.discount = 0;
            this.discountAmount = 0;
            mainProcessId = '';
            saleLineValue = res.Price__c;
            this.saleLine = new Sales_Process__c(
                Standard_of_Completion__c = null, 
                Promotion_Definition__c = res.Defined_Promotion__c == null ? null : res.Defined_Promotion__c
            );
        }
    }

    @RemoteAction
    global static String saveLinesUp(String retId, String structuredData, String rejectReason){
        Map<String, String> additionalFields = EditProposalFromFieldSet.deserializer(structuredData);

        Sales_Process__c mainProcess = new Sales_Process__c(Id = retId);
        for(String fieldPlus : additionalFields.keySet()){
            String type = EditProposalFromFieldSet.getTypeByAPIname(fieldPlus.trim());
            if(type == 'boolean'){
                mainProcess.put(fieldPlus.trim(), Boolean.valueOf(additionalFields.get(fieldPlus)));
            } else if (type == 'CURRENCY' || type == 'DOUBLE'){        
                if(additionalFields.get(fieldPlus) == null || additionalFields.get(fieldPlus) == 'null'){
                    mainProcess.put(fieldPlus.trim(), null);
                } else{
                    mainProcess.put(fieldPlus.trim(), Decimal.valueOf(additionalFields.get(fieldPlus)));
                }
            } else if(type == 'REFERENCE' && additionalFields.get(fieldPlus) == 'null'){
                mainProcess.put(fieldPlus.trim(), null);
            } else if(type == 'DATE' && additionalFields.get(fieldPlus) != null) {
                mainProcess.put(fieldPlus.trim(), Date.valueOf(additionalFields.get(fieldPlus)));
            } else if(type == 'TEXTAREA' && additionalFields.get(fieldPlus) == 'null'){
                mainProcess.put(fieldPlus.trim(), null);
            } else {
                mainProcess.put(fieldPlus.trim(), additionalFields.get(fieldPlus));
            }
        }
        mainProcess.Rejection_Reason__c = mainProcess.Status__c == CommonUtility.SALES_PROCESS_STATUS_REJECTED ? rejectReason : null;
        try{
          upsert mainProcess;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
        return retId;
    }


    @RemoteAction
    global static List<SObject> getSObject(String query, String obj) {
        query = String.escapeSingleQuotes(query);
        List<String> splitQueries = query.split(' ');
        if (splitQueries.isEmpty()) {
            splitQueries.add(query);
        }
        obj = obj.replaceAll('[()]', ''); 
        String soqlQuery = 'SELECT Id, Name ';
        if(obj == CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN){
                soqlQuery += ', Title__c';
         }
        soqlQuery += ' FROM '+ obj;
        String whereString = ' WHERE';
        for (Integer i = 0; i < splitQueries.size(); i++) {
             whereString += (i != 0? ' AND ': ' ') + 'Name LIKE \'%' + splitQueries[i] + '%\'';
             if(obj == CommonUtility.SOBJECT_NAME_MARKETINGCAMPAIGN){
                whereString += ' OR Title__c LIKE \'%' + splitQueries[i] + '%\'';
             }
        }
        whereString += + ' LIMIT 5';
        soqlQuery += whereString ;
        try {
            return Database.query(soqlQuery);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            return null;
        }      
    }

    // creates map investment with all related standard of completion, to create new picklist
    public String StandardsOfCompletionList() {
        return JSON.serialize([
            SELECT Id, Name, Status__c, Price__c, Investment_Standard_of_Completion__c, Description__c, Individual__c 
            FROM Resource__c 
            WHERE RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMPLETION) 
            AND Investment_Standard_of_Completion__c =: investment
        ]);
    }

    // return all avaliable promotions for picklist in AngularJS
   public String AvailablePromotionList() {
        return JSON.serialize([SELECT Id, Name, Active__c, Description__c, Discount_Kind__c, Discount_Type__c, Discount_Value__c, Valid_From__c, Valid_To__c FROM Manager_Panel__c WHERE Active__c = true AND Valid_From__c <= TODAY AND Valid_To__c >= TODAY AND RecordTypeId =: CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_PROMOTION)]);
    }

    public static String findInvestment(String processId){
        return [
            SELECT Offer_Line_Resource__r.InvestmentId__c 
            FROM Sales_Process__c 
            WHERE Product_Line_to_Proposal__c =: processId 
            OR Offer_Line_to_Sale_Term__c =: processId 
            LIMIT 1
        ].Offer_Line_Resource__r.InvestmentId__c;     
    }

    @RemoteAction
    global static List<SObject> getResources(String query, String investment, String rtId ) {
        query = String.escapeSingleQuotes(query);
        List<String> splitQueries = query.split(' ');
        if (splitQueries.isEmpty()) {
            splitQueries.add(query);
        }
        String soqlQuery = 'SELECT Id, Name, City__c, toLabel(RecordType.Name), Floor__c, Area__c, Street__c, House_Number__c, Price__c, Status__c  FROM Resource__c ';
        String whereString = ' WHERE';
        for (Integer i = 0; i < splitQueries.size(); i++) {
             whereString += (i != 0? ' AND ': ' ') + 'Name LIKE \'%' + splitQueries[i] + '%\'';
        }
        String queryInv ='';
        if(investment != null){
            queryInv += ' AND ( Investment_Building__c =\'' + investment + '\' OR ';
            queryInv += 'Investment_Commercial_Property__c =\'' + investment + '\' OR ';
            queryInv += 'Investment_Flat__c =\'' + investment + '\' OR ';
            queryInv += 'Investment_Parking_Space__c =\'' + investment + '\' OR ';
            queryInv += 'Investment_Storage__c =\'' + investment + '\' OR ';
            queryInv += 'Investment__c =\'' + investment + '\')';
        }
        queryInv += ' AND RecordTypeId =\'' + rtId +'\'';
         queryInv += ' AND ( Status__c =\'' + CommonUtility.RESOURCE_STATUS_ACTIVE +'\'';
          queryInv += ' OR Status__c =\'' + CommonUtility.RESOURCE_STATUS_RESERVATION +'\')';
        whereString += queryInv;
        whereString += + ' LIMIT 5';
        soqlQuery += whereString ;
        try {
            return Database.query(soqlQuery);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            return null;
        }      
    }

    private List<RecordType> getAvailibleTypes(){
        return [SELECT Id, DeveloperName, toLabel(RecordType.Name) FROM RecordType WHERE DeveloperName =: CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY OR DeveloperName =: CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT OR DeveloperName =: CommonUtility.RESOURCE_TYPE_HOUSE OR DeveloperName =: CommonUtility.RESOURCE_TYPE_PARKING_SPACE OR DeveloperName =: CommonUtility.RESOURCE_TYPE_STORAGE ];
    }

    @RemoteAction
    global static String addLine(List<ProposalWrapper> wrappedList, String reason, String rejectReason, String stdId, String stdPrice, String promoId){

        String retId = wrappedList[0].mainProcessId;
        Sales_Process__c mainProcess;
        if(wrappedList[0].mainProcessId == ''){
            mainProcess = new Sales_Process__c();
        } else {
            mainProcess = new Sales_Process__c(Id = wrappedList[0].mainProcessId);
        }
        mainProcess.Approve_Reason__c = reason;
        mainProcess.Rejection_Reason__c = rejectReason;

        try{
          upsert mainProcess;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
        retId = mainProcess.Id;

        List<Sales_Process__c> linse2insert = new List<Sales_Process__c>();
        for(ProposalWrapper wrap : wrappedList){
            Sales_Process__c sp = new Sales_Process__c();
            if(wrap.saleLineId != ''){
                sp.Id = wrap.saleLineId;
            }
            sp.RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE);  

            sp.Discount__c = wrap.discount.setScale(2, RoundingMode.HALF_UP);
            sp.Discount_Amount_Offer__c = wrap.discountAmount;
            sp.Price_With_Discount__c = wrap.resource.Price__c - wrap.discountAmount;
            sp.Offer_Line_Resource__c = wrap.resource.id;
            sp.Bank_Account_Number__c = wrap.saleLine.Bank_Account_Number__c;
            if(stdPrice == null){
                stdPrice = '0';
            }
                sp.Standard_of_Completion_Price__c = Decimal.valueOf(stdPrice);
                sp.Price_With_Discount__c += (sp.Standard_of_Completion_Price__c * wrap.resource.Area__c);
             
            sp.Standard_of_Completion__c = stdId;
            sp.Promotion_Definition__c = promoId;
            sp.Approve_Reason__c = mainProcess.Approve_Reason__c;

            linse2insert.add(sp);
        }
        try{
           upsert linse2insert;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
        return retId;
    }
}