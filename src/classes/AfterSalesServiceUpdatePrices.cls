/**
* @author 		Mateusz Pruszynski
* @description 	class to store static methods (called from trigger), that calculate prices on After-sales Service stage
**/

public without sharing class AfterSalesServiceUpdatePrices {

	public static void changeFinalPricesOnSaleTerms(Map<Id, Sales_Process__c> afterSalesWithSaleTermsIdsMap) { // Map<sale terms Id, After-sales Service record>

		List<Sales_Process__c> saleTerms2update = new List<Sales_Process__c>();

		Sales_Process__c afterSalesService;
		for(Id saleTermsId : afterSalesWithSaleTermsIdsMap.keySet()) {

			afterSalesService = afterSalesWithSaleTermsIdsMap.get(saleTermsId);

			saleTerms2update.add(
				new Sales_Process__c(
					Id = saleTermsId,
					Final_Agreement_Value_Gross__c = afterSalesService.Agreement_Value__c,
					Final_Agreement_Value_Net__c = afterSalesService.Net_Agreement_Price__c,
					Final_Vat_Amount__c = afterSalesService.VAT_Amount__c
				)
			);	

		}

		try {
			update saleTerms2update;
		} catch(Exception e) {
			ErrorLogger.log(e);
		}

	}

}