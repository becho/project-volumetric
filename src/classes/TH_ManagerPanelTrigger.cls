public without sharing class TH_ManagerPanelTrigger extends TriggerHandler.DelegateBase {

    public class MyException extends Exception{}  

    public List<Manager_Panel__c> promotions2updateActiveCheckboxOnResources;
    public List<Manager_Panel__c> monthlySalesTargetNameFieldUpdate;
    public List<Manager_Panel__c> salesRepsRewardsDuplicateMaching;
    public List<Manager_Panel__c> salesRepsRewardChangeOwnerId;
    public List<Manager_Panel__c> salesTargetNameFieldUpdate;
    public List<Manager_Panel__c> salesRepsNameFieldUpdate; // only insert - edit is not allowed
    public List<Manager_Panel__c> monthlySalesTargets;
    public List<Manager_Panel__c> thresholds;
    public List<Manager_Panel__c> csl;

    public Set<Id> saleTargetUserIdsToUpdate;
    public Set<Id> users;

    public Id profileId;
    public Id roleId;

    public String profileName;
    public String roleName; 

    public override void prepareBefore(){
        monthlySalesTargetNameFieldUpdate = new List<Manager_Panel__c>();
        salesRepsRewardsDuplicateMaching = new List<Manager_Panel__c>();
        salesRepsRewardChangeOwnerId = new List<Manager_Panel__c>();
        salesTargetNameFieldUpdate = new List<Manager_Panel__c>();
        salesRepsNameFieldUpdate = new List<Manager_Panel__c>();
        monthlySalesTargets = new List<Manager_Panel__c>();
        csl = new List<Manager_Panel__c>();
        
        users = new Set<Id>();
    }

    public override void prepareAfter(){
        promotions2updateActiveCheckboxOnResources = new List<Manager_Panel__c>();
        saleTargetUserIdsToUpdate = new Set<Id>();
        profileId = userinfo.getProfileId();
        profileName = [Select Id,Name from Profile where Id=:profileId].Name;
        roleId = userinfo.getUserRoleId();
        system.debug(roleId);

        try{
            roleName = [SELECT Id, DeveloperName FROM UserRole WHERE Id=:roleId].DeveloperName;
        }
        catch(Exception e){
            roleName = '';
        }
    }

    public override void beforeInsert(List<sObject> o){
        List<Manager_Panel__c> managerPanels = (List<Manager_Panel__c>)o;
        Set<Id> usersId = new Set<Id>();
        for(Manager_Panel__c mpT : managerPanels){
            csl.add(mpT);
            users.add(mpT.User__c);

            /* Mateusz Pruszyński */
            // populates name for sales targets
            if(mpT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_TARGET) && mpT.Start_Date__c != null && mpT.User__c != null) {
                salesTargetNameFieldUpdate.add(mpT);
            }             

            /* Mateusz Pruszyński */
            // populates name for monthly sales targets
            if(mpT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_MONTHLY_SALES_TARGET) && mpT.Month__c != null && mpT.Year__c != null && mpT.User__c != null) {
                monthlySalesTargetNameFieldUpdate.add(mpT);
            }

            /* Mateusz Pruszyński */
            // 1 automatically populates Name field for sales reps (update not allowed)
            // 2 avoids creating two identical records
            if(mpT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS) && mpT.User__c != null) {
                salesRepsNameFieldUpdate.add(mpT);
            }

            /* Mateusz Pruszyński */
            // check for duplicates among sales reps rewards records
            if(mpT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD) && mpT.Sales_Representative__c != null && mpT.Threshold__c != null) {
                salesRepsRewardsDuplicateMaching.add(mpT);
            }

            if(mpT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD) && mpT.Sales_Representative__c != null){
                usersId.add(mpT.Sales_Representative__c);
            }
            
            if (mpT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_MONTHLY_SALES_TARGET)) {
                monthlySalesTargets.add(mpT);
            }

            /* Mateusz Pruszyński */
            /** Code part to automatically change record owner owner for:
            * Monthly Sales Target (user)
            * Sales Target (user)
            **/
            if((mpT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_MONTHLY_SALES_TARGET) || mpT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_TARGET)) && mpT.User__c != null) {
                mpT.OwnerId = mpT.User__c;
            }
            /* Mateusz Pruszyński */
            /** Code part to automatically change record owner owner for:
            * Sales Representative (Sales Representative)
            * Sales Reps Rewards (Sales Representative)
            **/
            if((mpT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS) || mpT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD)) && mpT.Sales_Representative__c != null) {
                salesRepsRewardChangeOwnerId.add(mpT);
            }
            
        }

        for(Manager_Panel__c threshold : [select id, RecordTypeId, Sales_Representative__c, Threshold_From_Formula__c, Threshold_To_Formula__c from Manager_Panel__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD) and Sales_Representative__c IN :usersId and Active_Formula__c = true]){
            for(Manager_Panel__c junction : managerPanels){
                if(junction.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD) && junction.Sales_Representative__c != null && junction.Threshold_From_Formula__c <= threshold.Threshold_To_Formula__c && junction.Threshold_To_Formula__c >= threshold.Threshold_From_Formula__c){
                    junction.addError('Podany zakres pokrywa się już z istniejącym dla tego przedstawiciela handlowego');
                }
            }
        }
    }

    public override void afterInsert(Map<Id, sObject> o){
        Map<Id, Manager_Panel__c> newManagerPanels = (Map<Id, Manager_Panel__c>)o;
        for(Id key : newManagerPanels.keySet()){
            Manager_Panel__c newMP = newManagerPanels.get(key);
            /* Beniamin Cholewa, fix: Mateusz Pruszyński */
            // Updates Sales Target
            if (newMP.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_TARGET) || newMP.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_MONTHLY_SALES_TARGET)) {
                saleTargetUserIdsToUpdate.add(newMP.User__c);
            }
        }
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o){
        Map<Id, Manager_Panel__c> resOld = (Map<Id, Manager_Panel__c>)old;
        Map<Id, Manager_Panel__c> resNew = (Map<Id, Manager_Panel__c>)o;
        Set<Id> usersId = new Set<Id>();
        for(Id key : resNew.keySet()){
            Manager_Panel__c extN = resNew.get(key);
            Manager_Panel__c extO = resOld.get(key);
            
            users.add(extN.User__c);
            csl.add(extN);

            if(extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD) && extN.Sales_Representative__c != null && extN.Sales_Representative__c != extO.Sales_Representative__c){
                usersId.add(extN.Sales_Representative__c);
            }

            /* Mateusz Pruszyński */
            // Sales Representative records can never be edited
            if(extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS)) {
                extN.addError(Label.SalesRepsCannotBeEdited);
            }

            /* Mateusz Pruszyński */
            /** Code part to automatically change record owner owner for:
            * Monthly Sales Target (user)
            * Sales Target (user)
            **/
            if((extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_MONTHLY_SALES_TARGET) || extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_TARGET)) && extN.User__c != null && extO.User__c != extN.User__c) {
                extN.OwnerId = extN.User__c;
            }
            /* Mateusz Pruszyński */
            /** Code part to automatically change record owner owner for:
            * Sales Representative (Sales Representative)
            * Sales Reps Rewards (Sales Representative)
            **/
            if((extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS) || extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD)) && extN.Sales_Representative__c != null && extO.Sales_Representative__c != extN.Sales_Representative__c) {
                salesRepsRewardChangeOwnerId.add(extN);
            }       

            /* Mateusz Pruszyński 
            // Does not allow to edit Reward Thresholds
            if(extN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_REWARD_THRESHOLD)) {
                extN.addError(Label.RewardThresholdsCannotBeEdited);
            }
            */
        }

        for(Manager_Panel__c threshold : [select id, RecordTypeId, Sales_Representative__c, Threshold_From_Formula__c, Threshold_To_Formula__c from Manager_Panel__c where RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD) and Sales_Representative__c IN :usersId and Active_Formula__c = true]){
            for(Id key : resNew.keySet()){
                if(resNew.get(key).RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD) && resNew.get(key).Sales_Representative__c != null && resNew.get(key).Threshold_From_Formula__c <= threshold.Threshold_To_Formula__c&& resNew.get(key).Threshold_To_Formula__c>= threshold.Threshold_From_Formula__c && threshold.Id != resNew.get(key).Id){
                    resNew.get(key).addError('Podany zakres pokrywa się już z istniejącym dla tego przedstawiciela handlowego');
                }
            }
        }
        
    }

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o){
        Map<Id, Manager_Panel__c> resOld = (Map<Id, Manager_Panel__c>)old;
        Map<Id, Manager_Panel__c> resNew = (Map<Id, Manager_Panel__c>)o;
        for(Id key : resNew.keySet()){
            Manager_Panel__c extT = resNew.get(key);
            Manager_Panel__c extO = resOld.get(key);
            /* Beniamin Cholewa, fix: Mateusz Pruszyński */
            // Updates Sales Target            
            if ((extT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_TARGET) || extT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_MONTHLY_SALES_TARGET)) && (extT.Start_Date__c != extO.Start_Date__c
               || extT.End_Date__c != extO.End_Date__c || extT.User__c != extO.User__c)) {
                saleTargetUserIdsToUpdate.add(extT.User__c);
            }

            if(extT.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_PROMOTION) && extT.Active__c != extO.Active__c) {
                promotions2updateActiveCheckboxOnResources.add(extT);
            }
        }
    }

    public override void finish() {
        /* Mateusz Pruszyński */
        // populates name for sales targets
        if(salesTargetNameFieldUpdate != null && salesTargetNameFieldUpdate.size() > 0) {
            Set<Id> userIds = new Set<Id>();
            for(Manager_Panel__c st : salesTargetNameFieldUpdate) {
                userIds.add(st.User__c);
            }
            List<User> users = [SELECT Id, Name FROM User WHERE Id in :userIds];
            for(Manager_Panel__c st : salesTargetNameFieldUpdate) {
                for(User u : users) {
                    if(st.User__c == u.Id) {
                        st.Name = 'C-' + st.Start_Date__c.Year() + '-' + st.Start_Date__c.Month() + '-' + u.Name;
                    }
                }
            }
        }            

        /* Mateusz Pruszyński */
        // populates name for monthly sales targets
        if(monthlySalesTargetNameFieldUpdate != null && monthlySalesTargetNameFieldUpdate.size() > 0) {
            Set<Id> userIds = new Set<Id>();
            for(Manager_Panel__c st : monthlySalesTargetNameFieldUpdate) {
                userIds.add(st.User__c);
            }
            List<User> users = [SELECT Id, Name FROM User WHERE Id in :userIds];
            for(Manager_Panel__c st : monthlySalesTargetNameFieldUpdate) {
                for(User u : users) {
                    if(st.User__c == u.Id) {
                        st.Name = 'CM-' + String.valueOf(st.Month__c) + '-' + String.valueOf(st.Year__c) + '-' + u.Name;
                    }
                }
            }
        }

        /* Beniamin Cholewa */
        // calculated date based on selected year and month
        if(monthlySalesTargets != null && monthlySalesTargets.size() > 0) {
            for(Manager_Panel__c monthlySalesTarget : monthlySalesTargets) {
                if(monthlySalesTarget.Year__c != null && monthlySalesTarget.Month__c != null) {
                    monthlySalesTarget.Start_Date__c = Date.newInstance(Integer.valueOf(monthlySalesTarget.Year__c), Integer.valueOf(monthlySalesTarget.Month__c), 1);
                    monthlySalesTarget.End_Date__c = Date.newInstance(Integer.valueOf(monthlySalesTarget.Year__c), Integer.valueOf(monthlySalesTarget.Month__c), Date.daysInMonth(Integer.valueOf(monthlySalesTarget.Year__c), Integer.valueOf(monthlySalesTarget.Month__c)));
                    System.debug(monthlySalesTarget.Start_Date__c);
                }
            }
        }
        
        /* Mateusz Pruszyński */
        // 1 automatically populates Name field for sales reps (update not allowed)
        // 2 avoids creating two identical records
        if(salesRepsNameFieldUpdate != null && salesRepsNameFieldUpdate.size() > 0) {
            Set<Id> userIds = new Set<Id>();
            for(Manager_Panel__c salesRep : salesRepsNameFieldUpdate) {
                userIds.add(salesRep.User__c);
            }
            if(!userIds.isEmpty()) {
                List<Manager_Panel__c> identicalRecords = [SELECT Id, User__c FROM Manager_Panel__c WHERE User__c in :userIds AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS)];
                if(!identicalRecords.isEmpty()) {
                    for(Manager_Panel__c identicalRecord : identicalRecords) { // checks for duplicates
                        for(Manager_Panel__c salesRep : salesRepsNameFieldUpdate) {
                            if(identicalRecord.User__c == salesRep.User__c) {
                                salesRep.addError(Label.DuplicateSalesReps + ' ' + identicalRecord.Id);
                            }
                        }
                    }
                }
                for(User u : [SELECT Id, FirstName, LastName FROM User WHERE Id in :userIds]) { // populates name
                    for(Manager_Panel__c salesRep : salesRepsNameFieldUpdate) {
                        if(salesRep.User__c == u.Id) {
                            salesRep.Name = Label.SalesRepName + ' - ' + (u.FirstName != null ? (u.FirstName + ' ') : '') + u.LastName;
                        }
                    }
                }
            }
        }

        /* Mateusz Pruszyński */
        /** Code part to automatically change record owner owner for:
        * Sales Representative (Sales Representative)
        * Sales Reps Rewards (Sales Representative)
        **/
        if(salesRepsRewardChangeOwnerId != null && salesRepsRewardChangeOwnerId.size() > 0) {
            Set<Id> salesRepsIds = new Set<Id>();
            for(Manager_Panel__c srrcoi : salesRepsRewardChangeOwnerId) {
                salesRepsIds.add(srrcoi.Sales_Representative__c);
            }
            for(Manager_Panel__c srrcoi : salesRepsRewardChangeOwnerId) {
                for(Manager_Panel__c salesRep : [SELECT Id, User__c FROM Manager_Panel__c WHERE Id in :salesRepsIds AND User__c != null]) {
                    if(srrcoi.Sales_Representative__c == salesRep.Id) {
                        srrcoi.OwnerId = salesRep.User__c;
                    }
                }
            }
        }

        /* Mateusz Pruszyński */
        // check for duplicates among sales reps rewards records
        if(salesRepsRewardsDuplicateMaching != null && salesRepsRewardsDuplicateMaching.size() > 0) {
            Set<Id> salesRepsIds = new Set<Id>();
            Set<Id> thressholdIds = new Set<Id>();
            for(Manager_Panel__c salesRep : salesRepsRewardsDuplicateMaching) {
                salesRepsIds.add(salesRep.Sales_Representative__c);
                thressholdIds.add(salesRep.Threshold__c);
            }
            for(Manager_Panel__c salesRep : salesRepsRewardsDuplicateMaching) {
                for(Manager_Panel__c identicalSalesRep : [SELECT Id, Threshold__c, Sales_Representative__c FROM Manager_Panel__c WHERE Threshold__c in :thressholdIds AND Sales_Representative__c in :salesRepsIds AND RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_REPS_REWARD)]) {
                    if(salesRep.Threshold__c == identicalSalesRep.Threshold__c && salesRep.Sales_Representative__c == identicalSalesRep.Sales_Representative__c) {
                        salesRep.addError(Label.DuplicateSalesRepReward + ' ' + identicalSalesRep.Id);
                    }
                }
            }
        }
        /* Beniamin Cholewa, fix: Mateusz Pruszyński */
        // Trigger part updates potential amount and current amount on Sales Target on insert
        if (saleTargetUserIdsToUpdate != null && saleTargetUserIdsToUpdate.size() > 0) {
            ManagerPanelManager.updateSalesTargets(saleTargetUserIdsToUpdate, null);
        }

        // Beniamin Cholewa
        if(csl != null && csl.size() > 0 && users != null){
            for(Manager_Panel__c cslTemp : csl){
                if (cslTemp.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_MANAGERPANEL, CommonUtility.MANAGER_PANEL_TYPE_SALES_TARGET)) {
                    string userId = String.valueOf(cslTemp.User__c);
                    userId = userId.substring(0,15);
                    userId = userId.toUpperCase();
                }
            }
        }

        if(promotions2updateActiveCheckboxOnResources != null && promotions2updateActiveCheckboxOnResources.size() > 0) {
            ManageDefinedPromotions.updateIsActivePromotionOnResource(promotions2updateActiveCheckboxOnResources);
        }
    }
}