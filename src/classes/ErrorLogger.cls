public without sharing class ErrorLogger {
    /**
    *   @author         Wojciech Słodziak
    *   @description    Class used for sending emails on error - mostly used in try catch
    **/

    static User u;
    static Profile uProfile;
    public static final String EMAIL_DARIUSZ = 'dariusz.paszel@enxoo.com'; 
    public static final String EMAIL_MATEUSZ = 'mateusz.pruszynski@enxoo.com';
    public static final String EMAIL_KAMIL = 'kamil.baluc@enxoo.com';
    public static final String EMAIL_PRZEMYSLAW = 'przemyslaw.tustanowski@enxoo.com';
    public static final String EMAIL_WOJCIECH = 'wojciech.slodziak@enxoo.com';
    public static final String EMAIL_BENIAMIN = 'beniamin.cholewa@enxoo.com';
    //public static final String TARGET_1 = '00524000000Wsyh';
    public static final String TARGET_2 = '0052400000113Av';
    public static final String TARGET_3 = '00524000001T1Dt';
    public static final String ERROR_OCCURRED = 'Error Occurred - PROPERTO - ';
    public static final String FOLLOWING_IN_SYSTEM = 'The following error occurred in system: ';

    public static void log(Exception e) {
        if(e != null) {
            if (u == null) {
                Id uId = UserInfo.getUserId();
                ErrorLogger.u = [SELECT Id, Name FROM User WHERE Id = :uId];
                Id pId = UserInfo.getProfileId();
                ErrorLogger.uProfile = [SELECT Id, Name FROM Profile WHERE Id = :pId];
            }

            String exc = '';
            exc = 'EXCEPTION TYPE: ' + e.getTypeName() + '\n';
            exc += '...CAUSE:   ' + e.getCause() + '\n';
            exc += '...LINE NO: ' + e.getLineNumber() + '\n';
            exc += '...MESSAGE: ' + e.getMessage() + '\n';
            exc += '...STACK TRACE: ' + e.getStackTraceString() + '\n'; 
            exc += '...USER NAME: ' + ErrorLogger.u.Name + '\n'; 
            exc += '...USER PROFILE: ' + ErrorLogger.uProfile.Name + '\n'; 

            if(!Test.isRunningTest()) {
                sendMessage(exc);
            }
        }
    }

    public static void sendMessage(String errorMessage) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        //address list
        List<String> toAddress = new List<String>();
        //toAddress.add(EMAIL_DARIUSZ);
        //toAddress.add(EMAIL_MATEUSZ);
        //toAddress.add(EMAIL_KAMIL);
        //toAddress.add(EMAIL_PRZEMYSLAW);
        //toAddress.add(EMAIL_WOJCIECH);
        //toAddress.add(EMAIL_BENIAMIN);
        //mail.setToAddresses(toAddress);


        //workaround for limit - 15 mails daily
        mail.setTargetObjectId('0050Y000000EfPT'); // MS
       // mail.setTargetObjectId(TARGET_2); //MP
        //mail.setTargetObjectId(TARGET_3); //WS
        mail.saveAsActivity = false;

        mail.setSubject(ERROR_OCCURRED + [Select Name From Organization].Name);
        mail.setPlainTextBody(FOLLOWING_IN_SYSTEM + errorMessage);
        //Messaging.SendEmailResult [] r =  Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    }
}