/**
* @author 		Krystian Bednarek
* @description 	Test class for TranslationLabel class
**/

@isTest
private class TestClassTranslationLabel {

    static testMethod void myUnitTest() {
    	User u = TestHelper.createUser(null, true);
		
		System.runAs(u){
			
	    	test.startTest();
	    	String[] myList = new List<String>{'Name'};
	    	
	    	String[] tab = TranslationLabel.getLabelForField('Listing__c', myList);
	    	
	    	System.assertEquals('Listing Number' , tab[0]);
	    	test.stopTest();
		}
    
    }

}