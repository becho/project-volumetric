/**
* @author 		Mariia Dobzhanska
* @description 	Test class for RemindDates
**/

@isTest
private class TestClassRemindDates {
    
    public static final Integer befcount = 7;
    public static final Integer aftcount = 5;
    
@testsetup
    static void setuptestdata() {
   
        //Creating days counts to send remind Emails - before and after - which have to be extracted from the custom settings
        List<RemindEmailDayCount__c> beforeafter= new List<RemindEmailDayCount__c>();
        
        RemindEmailDayCount__c datebefore = new RemindEmailDayCount__c();
		datebefore.Count__c = befcount;
        datebefore.Name = 'REMIND_DAYS_COUNT';
		RemindEmailDayCount__c dateafter = new RemindEmailDayCount__c();
		dateafter.Count__c = aftcount;
        dateafter.Name = 'REMIND_DAYS_COUNT_AFT';
        
		beforeafter.add(datebefore);
        beforeafter.add(dateafter);
        insert beforeafter;
    }
    /*
    * Checking if the method returns the appropriate Dates for sending reminders
	*/
    static testmethod void getremdatesTest() {
        
        //Creating expected list values
        Date tod = Date.today();
        Date datebefore = tod + befcount;
        Date dateafter = tod - aftcount;
        
        Test.startTest();
        	List<Date> actdates = RemindDates.getremdates();
        Test.stopTest();
        
        //Checking if the list size and its elements are as expected
        System.assertEquals(2, actdates.size());
        System.assertEquals(datebefore, actdates[0]);
        System.assertEquals(dateafter, actdates[1]);
    }
}