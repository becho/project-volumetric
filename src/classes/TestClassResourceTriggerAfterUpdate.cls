/**
* @author 		Krystian Bednarek
* @description 	Test class for AfterUpdate part of TH_Resource class
**/

@isTest 
private class TestClassResourceTriggerAfterUpdate {
	public final static Integer CONFIG_DISTRIBUTION_THRESHOLD_AREA = 150;

	@testsetup
	static void setuptestData() {
		TestHelper.CreateAreaAndVatPercentageDistribution_CurrentDistributionFlat();
		
		Account personAccount = TestHelper.createAccountIndividualClient(null, true);
		
		Resource__c investment = TestHelper.createInvestment(
			new Resource__c (
				Common_Surface__c = 250
			),
			true
		);

		Resource__c building = TestHelper.createResourceBuilding(new Resource__c(Investment_Building__c = investment.Id), true);

		Resource__c stage = new Resource__c(
			Name__c = 'KW 123/23442',
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE),
			Investment_stage__c = investment.Id
		);
		insert stage;

		Resource__c flat = TestHelper.createResourceFlatApartment(
			new Resource__c(
				Investment__c = investment.Id,
				Investment_Flat__c = investment.Id, 
				Building__c = building.Id, 
				Flat_Number__c = '123',
				Stage__c = stage.Id,
			    Price__c = 293571,
		    	Acceptance_Status__c = CommonUtility.RESOURCE_ACCEPTANCE_STATUS_DIRECTED_FOR_APPROVAL,
		    	Bank_Account_Number__c = '00000000000000000000000000'
			), 
			true
		);

		Resource__c parking = TestHelper.createResourceParkingSpace(
			new Resource__c(				
				Stage__c = stage.Id,
				Investment_Parking_Space__c = investment.Id
			), 
			true
		);

		Resource__c storage = TestHelper.createResourceStorage(
			new Resource__c(				
				Stage__c = stage.Id,
				Investment_Storage__c = investment.Id         
			), 
			true
		);

		Resource__c comercialProperty = TestHelper.createResourceFlatApartment(
			new Resource__c(
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY),
				Stage__c = stage.Id,
				Investment_Commercial_Property__c = investment.Id
			), 
			true
		);

		Resource__c commercialProperty = TestHelper.createResourceStorage(
			new Resource__c(				
				Stage__c = stage.Id,
				Investment_House__c = investment.Id,
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_HOUSE)       
			), 
			true
		);

		Contact contactPerson = [SELECT Id FROM Contact LIMIT 1];

		Sales_Process__c proposal = TestHelper.createSalesProcessProposal(
			new Sales_Process__c(
				Contact__c = contactPerson.Id
			), 
			true
		);
	}

	@isTest 
	static void testUpdateCommonSurfaceAfterUpdate() {
		// method should trigger ResourceManager.checkResourcesAreaOnAfter
		// 															pytanie  czy da się wjesc do 357 w th_Resource  if(resourcesAreaOnAfter.containsKey(resN.Stage__c)) {
		Resource__c parking = [SELECT Id, Usable_Area_Planned__c
								 FROM Resource__c 
								WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
								LIMIT 1];
		Test.startTest();
			parking.Usable_Area_Planned__c = 250;

			update parking;
			Resource__c stage = [SELECT Id, Common_Surface__c 
								   FROM Resource__c 
								  WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE)
								  LIMIT 1];
			// checking if Common_Surface__c has been updated
			System.assertEquals(stage.Common_Surface__c, parking.Usable_Area_Planned__c);
		Test.stopTest();
	}

	// whenever field common_surface on stage is updated, updated values on all resources connected to this stage
	@isTest 
	static void testUpdateCommonSurfaceOnStage() {
	 // method should trigger ResourceManager.checkResourceLandSurface
	 // after updating stage field on resource, get the new's stage common_surface
		Resource__c stage = [SELECT Id, Common_Surface__c 
							   FROM Resource__c 
							  WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE)
							  LIMIT 1];

		Test.startTest();
			stage.Common_Surface__c  = 550;
			update stage;
			Resource__c parking = [SELECT Id, Common_Surface__c
									 FROM Resource__c 
									WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
									LIMIT 1];

			// checking if Common_Surface__c has been updated
			System.assertEquals(stage.Common_Surface__c, parking.Common_Surface__c);
		Test.stopTest();
	}

	@isTest 
	static void testUdpateStageOnResourceFlat() {
	// method should trigger ResourceManager.checkResourceLandSurface
	 //after updating stage field on Flat, get the new's stage common_surface

		Resource__c stageToUpdate = new Resource__c(
			Name__c = 'KW 123/23442',
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE),
			Common_Surface__c = 300
		);

		Resource__c flat = [SELECT Id, Common_Surface__c
							  FROM Resource__c 
							 WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
							 LIMIT 1];

		Test.startTest();
			insert stageToUpdate;
			flat.Stage__c = stageToUpdate.Id;
			update flat;

			// checking if the new's stage common_surface has been asgined
			Resource__c flatAfterUpdate = [SELECT Id, Common_Surface__c
									 		 FROM Resource__c 
											WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
											LIMIT 1];
			// checking if the new's stage common_surface has been asgined
			System.assertEquals(stageToUpdate.Common_Surface__c, flatAfterUpdate.Common_Surface__c);

		Test.stopTest();
	}

	@isTest 
	static void testUdpateStageOnResourceParking() {
	// method should trigger ResourceManager.checkResourceLandSurface
	 //after updating stage field on Parking, get the new's stage common_surface
		Resource__c stageToUpdate = new Resource__c(
			Name__c = 'KW 123/23442',
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE),
			Common_Surface__c = 300
		);
		Resource__c parking = [SELECT Id, Common_Surface__c
								 FROM Resource__c 
								WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
								LIMIT 1];

		Test.startTest();
			insert stageToUpdate;
			parking.Stage__c = stageToUpdate.Id;
			update parking;

			// checking if the new's stage common_surface has been asgined
			Resource__c parkingAfterUpdate = [SELECT Id, Common_Surface__c
												FROM Resource__c 
											   WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
											   LIMIT 1];
			// checking if the new's stage common_surface has been asgined
			System.assertEquals(stageToUpdate.Common_Surface__c, parkingAfterUpdate.Common_Surface__c);

		Test.stopTest();
	}

	@isTest 
	static void testUdpateStageOnResourceStorage() {
	// method should trigger ResourceManager.checkResourceLandSurface
	 //after updating stage field on Storage, get the new's stage common_surface
		Resource__c stageToUpdate = new Resource__c(
			Name__c = 'KW 123/23442',
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE),
			Common_Surface__c = 300
		);
		Resource__c storage = [SELECT Id, Common_Surface__c
								 FROM Resource__c 
								WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)
								LIMIT 1];

		Test.startTest();
			insert stageToUpdate;
			storage.Stage__c = stageToUpdate.Id;
			update storage;

			// checking if the new's stage common_surface has been asgined
			Resource__c storageAfterUpdate = [SELECT Id, Common_Surface__c
												FROM Resource__c 
											   WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)
											   LIMIT 1];
			// checking if the new's stage common_surface has been asgined
			System.assertEquals(stageToUpdate.Common_Surface__c, storageAfterUpdate.Common_Surface__c);

		Test.stopTest();
	}

	@isTest 
	static void testUdpateStageOnResourceHouse() {
	// method should trigger ResourceManager.checkResourceLandSurface
 	//after updating stage field on House, get the new's stage common_surface
		Resource__c stageToUpdate = new Resource__c(
			Name__c = 'KW 123/23442',
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE),
			Common_Surface__c = 300
		);
		Resource__c house = [SELECT Id, Common_Surface__c
							   FROM Resource__c 
							  WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_HOUSE)
							  LIMIT 1];

		Test.startTest();
			insert stageToUpdate;
			house.Stage__c = stageToUpdate.Id;
			update house;

			// checking if the new's stage common_surface has been asgined
			Resource__c houseAfterUpdate = [SELECT Id, Common_Surface__c
											  FROM Resource__c 
											 WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_HOUSE)
											 LIMIT 1];
			// checking if the new's stage common_surface has been asgined
			System.assertEquals(stageToUpdate.Common_Surface__c, houseAfterUpdate.Common_Surface__c);

		Test.stopTest();
	}

	@isTest 
	static void testUdpateStageOnResourceColectiveSpace() {
	// method should trigger ResourceManager.checkResourceLandSurface
	//after updating stage field on ColectiveSpace, get the new's stage common_surface
		Resource__c investment = [SELECT Id
			 	     	    		FROM Resource__c 
					       		   WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
		                   		   LIMIT 1];

		Resource__c stage = [SELECT Id 
							   FROM Resource__c 
							  WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE) 
							  LIMIT 1];

		Resource__c colectiveSpace = new Resource__c(
				Name__c = 'KW 123/23442',
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COLLECTIVE_SPACE),
				Stage__c = stage.Id,
				Investment_Building__c = investment.Id,
				Construction_Status__c = CommonUtility.RESOURCE_CONSTRUCTION_STATUS_FINISHED
			);
		insert colectiveSpace;

		Resource__c stageToUpdate = new Resource__c(
			Name__c = 'KW 123/23442',
			RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE),
			Common_Surface__c = 300
		);

		Test.startTest();
			insert stageToUpdate;
			colectiveSpace.Stage__c = stageToUpdate.Id;
			update colectiveSpace;
			// checking if the new's stage common_surface has been asgined
			Resource__c colectiveSpaceAfterUpdate = [SELECT Id, Common_Surface__c
													   FROM Resource__c 
													  WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COLLECTIVE_SPACE)
													  LIMIT 1];						  
			// checking if the new's stage common_surface has been asgined
			System.assertEquals(stageToUpdate.Common_Surface__c, colectiveSpaceAfterUpdate.Common_Surface__c);

		Test.stopTest();
	}

	@isTest 
	static void testUpdateResourceNullStage() {
		// method should trigger ResourceManager.checkResourceLandSurfaceWithNullStage
		Resource__c parking = [SELECT Id, Stage__c
								 FROM Resource__c 
								WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
								LIMIT 1];

		Test.startTest();
			parking.Stage__c = null;
			update parking;	
			// checking if the new's stage common_surface has been asgined to 0
			Resource__c parkingQuerry = [SELECT Id, Common_Surface__c  FROM Resource__c WHERE Id = :parking.Id];
			System.assertEquals(0, parkingQuerry.Common_Surface__c);

		Test.stopTest();

	}

	@isTest 
	static void testUpdateColectiveSpaceParking() {
		Resource__c colectiveSpaceForUpdate = new Resource__c(
									Name__c = 'KW 123/234421',
									RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COLLECTIVE_SPACE),
									Usable_Area_Planned__c = 300
								);

		Resource__c parking = [SELECT Id, Stage__c
								 FROM Resource__c 
								WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
								LIMIT 1];

		Test.startTest();
		
			insert colectiveSpaceForUpdate;
			parking.Collective_Space_parking__c = colectiveSpaceForUpdate.Id;
			update parking;

			Resource__c parkingQuerry = [SELECT Id, Stage__c, Common_Surface__c
										   FROM Resource__c 
										  WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
							              LIMIT 1];
			// checking if Common_Surface__c has been asgined to new colectiveSpace.Usable_Area_Planned__c
			System.assertEquals(parkingQuerry.Common_Surface__c, colectiveSpaceForUpdate.Usable_Area_Planned__c);
		Test.stopTest();
	}

	@isTest 
	static void testUpdateColectiveSpaceStorage() {

		Resource__c storage = [SELECT Id 
								 FROM Resource__c 
								WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)
								LIMIT 1];

		Resource__c colectiveSpaceForUpdate = new Resource__c(
									Name__c = 'KW 123/234421',
									RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COLLECTIVE_SPACE),
									Usable_Area_Planned__c = 300,
									Collective_Space_storage__c = storage.Id
								);

		Test.startTest();
		
			insert colectiveSpaceForUpdate;
			storage.Collective_Space_storage__c = colectiveSpaceForUpdate.Id;
			update storage;

			Resource__c storageQuerry = [SELECT Id, Stage__c, Common_Surface__c
										   FROM Resource__c 
										  WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STORAGE)
							              LIMIT 1];
			// checking if Common_Surface__c has been asgined to new colectiveSpace.Usable_Area_Planned__c
			System.assertEquals(storageQuerry.Common_Surface__c, colectiveSpaceForUpdate.Usable_Area_Planned__c);
		Test.stopTest();
	}

	@isTest 
	static void testUpdateStageUsableAreaNotNull() {
		// Method should trigger Usable_Area_Planned__c != null in Th_Resource which will lead to call ResourceManager.resourcesAreaOnAfter
		Resource__c parking = [SELECT Id, Stage__c
								 FROM Resource__c 
								WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
					            LIMIT 1];

		Test.startTest();
			parking.Usable_Area_Planned__c = 300;
			update parking;

			Resource__c stageToUpdate = new Resource__c(
				Name__c = 'KW 123/23442',
				RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_STAGE),
				Common_Surface__c = 300
			);
			insert stageToUpdate;
			parking.Stage__c = stageToUpdate.Id;
			update parking;

			// checking if the new's stage common_surface has been recalucalted
			Resource__c parkingQuerry = [SELECT Id, Stage__c, Common_Surface__c
									 	   FROM Resource__c 
									      WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_PARKING_SPACE)
						                  LIMIT 1];
			System.assertEquals(stageToUpdate.Common_Surface__c, parkingQuerry.Common_Surface__c);
		Test.stopTest();
	}

	@isTest 
	static void testUpdateAcceptanceStatus() {
		// 452 in Th_Resource 
		// should trigger MeasurementsAcceptanceProcessBatch 
		Resource__c investment = [SELECT Id
					 	     	    FROM Resource__c 
							       WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_INVESTMENT)
				                   LIMIT 1];

		Resource__c flat = [SELECT Id, Total_Area_Planned__c, Usable_Area_Planned__c
							  FROM Resource__c 
							 WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
							 LIMIT 1];

		flat.Total_Area_Planned__c = 42;
		flat.Usable_Area_Planned__c = 42;
		update flat;

		Test.startTest();
			flat.Acceptance_Status__c = CommonUtility.RESOURCE_ACCEPTANCE_STATUS_APPROVED;
			
			// setting Total_Area_Measured__c so that: if(res.Total_Area_Measured__c - res.Usable_Area_Planned__c < 0) equals true
			flat.Total_Area_Measured__c  = 41;
			update flat;
		Test.stopTest();
	}

	@isTest 
	static void testUpdateBankAccount() {
		// 460  in Th_Resource 
		// testing MeasurementsAcceptanceProcessBatch 
		// copy bank account from Resource to sales process and payments 
		Contact contactPerson = [SELECT Id FROM Contact LIMIT 1];

		Sales_Process__c proposal = [SELECT Id, Contact__c 
									   FROM Sales_Process__c 
									  WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
									  LIMIT 1];

		Test.startTest();
			Resource__c flat = [SELECT Id, Bank_Account_Number__c
						  		  FROM Resource__c 
						 		 WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
						 		 LIMIT 1];

			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
				new Sales_Process__c(
					Offer__c = proposal.Id,
					Contact__c = proposal.Contact__c,
					Status__c = CommonUtility.SALES_PROCESS_STATUS_REJECTED
				),
				true
			);
	
			Sales_Process__c prline = TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Contact_from_Offer_Line__c = contactPerson.Id,
					Product_Line_to_Proposal__c = proposal.Id,
					Offer_Line_Resource__c = flat.Id,
					Offer_Line_to_Sale_Term__c = saleTerms.Id
				), true
			);

			Payment__c paymentInstallment = TestHelper.createPaymentsInstallment(
					new Payment__c(
						Agreements_Installment__c = saleTerms.Id,
						Contact__c = proposal.Contact__c,
						Payment_For__c = flat.Id
					),
					true
				);	

			flat.Bank_Account_Number__c = null;
			update flat;
			Payment__c paymentInstallmentQuerry = [SELECT Id, Bank_Account_For_Resource__c FROM Payment__c WHERE Id = :paymentInstallment.Id];
			System.assertEquals(paymentInstallmentQuerry.Bank_Account_For_Resource__c, flat.Bank_Account_Number__c);

		Test.stopTest();
	}

	@isTest 
	static void testBindFieldsOnSalesProcessFlat() {
	 	// update bind fields on SP
 		Contact contactPerson = [SELECT Id FROM Contact LIMIT 1];

		Sales_Process__c proposal = [SELECT Id, Contact__c 
									   FROM Sales_Process__c 
									  WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
									  LIMIT 1];

  		Resource__c flat = [SELECT Id, With_Storage__c, Bank_Account_Number__c
					  		  FROM Resource__c 
					 		 WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_FLAT_APARTMENT)
					 		 LIMIT 1];

 		Test.startTest();

			Sales_Process__c prline = TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Contact_from_Offer_Line__c = contactPerson.Id,
					Product_Line_to_Proposal__c = proposal.Id,
					Offer_Line_Resource__c = flat.Id
				), true
			);

			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
					new Sales_Process__c(
						Offer__c = proposal.Id,
						Contact__c = proposal.Contact__c,
						Status__c = CommonUtility.SALES_PROCESS_STATUS_REJECTED
					),
					true
				);

			flat.With_Storage__c = true;

			Sales_Process__c saleTermsQuerry = 
				[SELECT Id, BindParkingSpace__c, 
							BindParkingSpace_ok__c, 
							BindStorage__c, 
							BindStorage_ok__c 
				FROM Sales_Process__c 
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1];

			update flat;
		
			Sales_Process__c saleTermsQuerryAfterUpdate = 
				[SELECT Id, BindParkingSpace__c, 
							BindParkingSpace_ok__c, 
							BindStorage__c, 
							BindStorage_ok__c 
				FROM Sales_Process__c 
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1];

				System.assertEquals(true, saleTermsQuerryAfterUpdate.BindStorage__c);
			Test.stopTest();
			// 363 ManageBindProducts.updateSaleTermsBindingFields 
		
	}

	@isTest 
	static void testBindFieldsOnSalesProcessCommercialProperty() {
	 	// update bind fields on SP
 		Contact contactPerson = [SELECT Id FROM Contact LIMIT 1];

		Sales_Process__c proposal = [SELECT Id, Contact__c 
									   FROM Sales_Process__c 
									  WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_OFFER)
									  LIMIT 1];

  		Resource__c commercialProperty = [SELECT Id, With_Storage__c
					  		  				FROM Resource__c 
					 		 			   WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_RESOURCE, CommonUtility.RESOURCE_TYPE_COMMERCIAL_PROPERTY)
										   LIMIT 1];

 		Test.startTest();
			Sales_Process__c saleTerms = TestHelper.createSalesProcessSaleTerms(
					new Sales_Process__c(
						Offer__c = proposal.Id,
						Contact__c = proposal.Contact__c,
						Status__c = CommonUtility.SALES_PROCESS_STATUS_REJECTED
					),
					true
				);

			Sales_Process__c prline = TestHelper.createSalesProcessProductLine(
				new Sales_Process__c(
					Contact_from_Offer_Line__c = contactPerson.Id,
					Product_Line_to_Proposal__c = proposal.Id,
					Offer_Line_Resource__c = commercialProperty.Id,
					Offer_Line_to_Sale_Term__c = saleTerms.Id
				), true
			);

			commercialProperty.With_Parking_Space__c = true;
			update commercialProperty;
			// 363 ManageBindProducts.updateSaleTermsBindingFields 
			Sales_Process__c saleTermsQuerryAfterUpdate = 
				[SELECT Id, BindParkingSpace__c, 
							BindParkingSpace_ok__c, 
							BindStorage__c, 
							BindStorage_ok__c 
				FROM Sales_Process__c 
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
				LIMIT 1];

				System.assertEquals(true, saleTermsQuerryAfterUpdate.BindParkingSpace__c);

		Test.stopTest();
	}
}