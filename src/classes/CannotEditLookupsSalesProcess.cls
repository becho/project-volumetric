/**
* @author 		Mateusz Pruszyński
* @description 	Trigger part that does not allow to edit following lookups (references to Sales Process):
*
* 				1) Sales Process
* 				 1.1) RT [Sale Terms] 				LOOKUPS [Offer__c]
* 				 1.2) RT [After-sales Service] 		LOOKUPS [Agreement__c]
* 				 1.3) RT [Final Agreement] 			LOOKUPS [Preliminary_Agreement__c, 	Handover_Protocol__c]
* 				 1.4) RT [Promotion] 				-> Validation rule: "PromotionsCannotEditLookups"
* 				 1.5) RT [Customer Group] 			-> Validation rule: "CustomerGroupCannotEditLookups"
*
* 				2) Payment
* 				 2.1) RT [Installment]				LOOKUPS [Agreements_Installment__c, After_sales_Service__c, Contact__c, Payment_For__c]
*
* 				3) Extension
*				 3.1) RT [Change]					LOOKUPS [After_sales_Service__c]
* 				 3.2) RT [Handover Defect] 			LOOKUPS [Handover_Protocol__c]
**/

public without sharing class CannotEditLookupsSalesProcess {

	/**
	* @SALES_PROCESS
	**/

	public static void LookupsSalesProcess(Map<Sales_Process__c, Sales_Process__c> salesProcessesMap) { // as a parameter, we get Map<old, new>
		for(Sales_Process__c oldSP : salesProcessesMap.keySet()) {
			Sales_Process__c newSP = salesProcessesMap.get(oldSP);
			/* Sale Terms */
			if(newSP.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)) {
				// Offer__c
				if(oldSP.Offer__c != null && newSP.Offer__c != oldSP.Offer__c) {
					newSP.addError(Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_SALESPROCESS).getDescribe().fields.getMap().get('Offer__c').getDescribe().getLabel());
				}
			}
			/* After-sales Service */
			else if(newSP.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)) {
				// Agreement__c
				if(oldSP.Agreement__c != null && newSP.Agreement__c != oldSP.Agreement__c) {
					newSP.addError(Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_SALESPROCESS).getDescribe().fields.getMap().get('Agreement__c').getDescribe().getLabel());
				}
			}
			/* Final Agreement */
			else if(newSP.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_SALESPROCESS, CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)) {
				// Preliminary_Agreement__c
				if(oldSP.Preliminary_Agreement__c != null && newSP.Preliminary_Agreement__c != oldSP.Preliminary_Agreement__c) {
					newSP.addError(Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_SALESPROCESS).getDescribe().fields.getMap().get('Preliminary_Agreement__c').getDescribe().getLabel());
				}
				// Handover_Protocol__c
				else if(oldSP.Handover_Protocol__c != null && newSP.Handover_Protocol__c != oldSP.Handover_Protocol__c) {
					newSP.addError(Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_SALESPROCESS).getDescribe().fields.getMap().get('Handover_Protocol__c').getDescribe().getLabel());
				}				
			}			
		}
	}

	/**
	* @PAYMENT
	**/	

	public static void LookupsPayment(Map<Payment__c, Payment__c> paymentsMap) { // as a parameter, we get Map<old, new>
		for(Payment__c oldP : paymentsMap.keySet()) {
			Payment__c newP = paymentsMap.get(oldP);
			/* Installment */
			if(newP.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_PAYMENT, CommonUtility.PAYMENT_TYPE_PAYMENT)) {
				// Agreements_Installment__c
				if(oldP.Agreements_Installment__c != null && newP.Agreements_Installment__c != oldP.Agreements_Installment__c) {
					newP.addError(Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('Agreements_Installment__c').getDescribe().getLabel());
				}
				// After_sales_Service__c
				else if(oldP.After_sales_Service__c != null && newP.After_sales_Service__c != oldP.After_sales_Service__c) {
					newP.addError(Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('After_sales_Service__c').getDescribe().getLabel());
				}
				// Contact__c
				else if(oldP.Contact__c != null && newP.Contact__c != oldP.Contact__c) {
					newP.addError(Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('Contact__c').getDescribe().getLabel());
				}
				// Payment_For__c
				else if(oldP.Payment_For__c != null && newP.Payment_For__c != oldP.Payment_For__c) {
					newP.addError(Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_PAYMENT).getDescribe().fields.getMap().get('Payment_For__c').getDescribe().getLabel());
				}								
			}
		}
	}

	/**
	* @EXTENSION
	**/	

	public static void LookupsExtension(Map<Extension__c, Extension__c> paymentsMap) { // as a parameter, we get Map<old, new>
		for(Extension__c oldE : paymentsMap.keySet()) {
			Extension__c newE = paymentsMap.get(oldE);
			/* Change */
			if(newE.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_CHANGE)) {
				// After_sales_Service__c
				if(oldE.After_sales_Service__c != null && newE.After_sales_Service__c != oldE.After_sales_Service__c) {
					newE.addError(Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_EXTENSION).getDescribe().fields.getMap().get('After_sales_Service__c').getDescribe().getLabel());
				}				
			}
			/* Handover Defect */
			else if(newE.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_EXTENSION, CommonUtility.EXTENSION_TYPE_DEFECT)) {
				// Handover_Protocol__c
				if(oldE.Handover_Protocol__c != null && newE.Handover_Protocol__c != oldE.Handover_Protocol__c) {
					newE.addError(Label.CannotModifyReference + ': ' + Schema.getGlobalDescribe().get(CommonUtility.SOBJECT_NAME_EXTENSION).getDescribe().fields.getMap().get('Handover_Protocol__c').getDescribe().getLabel());
				}				
			}			
		}
	}	
}