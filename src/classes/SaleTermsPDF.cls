public with sharing class SaleTermsPDF {

	public List<Sales_Process__c> customersGroup {get; set;} // customers from related list
	public List<Sales_Process__c> offerLines {get; set;}
	public Sales_Process__c saleTerms {get; set;}
	public String attName {get; set;}

	public SaleTermsPDF(ApexPages.StandardController stdController) {
		saleTerms = (Sales_Process__c)stdController.getRecord();
		customersGroup = getCustomersInfo();
		offerLines = getOfferLines();
	}

	// query customers from related list
	public List<Sales_Process__c> getCustomersInfo() {
		return [SELECT Id, Account_from_Customer_Group__c, Account_from_Customer_Group__r.Name, Share_Type__c, Share__c, Account_from_Customer_Group__r.Phone, Account_from_Customer_Group__r.Email__c, Ownership_Type__c FROM Sales_Process__c WHERE Developer_Agreement_from_Customer_Group__c = :saleTerms.Id];
	}

	// query offer lines from related lisd (contain information about prices, discounts and resource parameters ass well)
	public List<Sales_Process__c> getOfferLines() {
		return [SELECT Id, Standard_of_Completion__c, Standard_of_Completion__r.Price__c, Offer_Line_Resource__r.Name, Offer_Line_Resource__c, Offer_Line_Resource__r.Total_Area_Planned__c, Price_With_Discount__c, Offer_Line_Resource__r.City__c, Offer_Line_Resource__r.House_number__c, Offer_Line_Resource__r.Street__c, Offer_Line_Resource__r.Investment__c, Offer_Line_Resource__r.Investment__r.Name, Offer_Line_Resource__r.Building__c, Offer_Line_Resource__r.Building__r.Name, toLabel(Offer_Line_Resource__r.RecordType.Name) FROM Sales_Process__c WHERE Offer_Line_to_Sale_Term__c = :saleTerms.Id];
	}


	// SAVE meth.
    public PageReference savePdf() {  
		Attachment attach = new Attachment();
	    Date todaysDate = Date.today();
		attName = Label.SaleTermsName + '_' + saleTerms.Name;
    	String version = chceckForVersion();
	    PageReference pdf = Page.SaleTermsPDFGeneratorTemplate;
	    pdf.getParameters().put('id', saleTerms.Id);
	    Blob body;

	    try {
	        body = pdf.getContent();
	    // testing bug!
	    } catch (VisualforceException e) {
	        body = Blob.valueOf('Some Text');
	    }

	    attach.Body = body;
		attach.ContentType = 'application/pdf';
	    // add the user entered name
	    attach.Name = attName + '_' + todaysDate.year() + '/' + todaysDate.month() + '/' + todaysDate.day() + '_v' + version + '.pdf';
	    attach.IsPrivate = false;
	    // attach the pdf to the offer
	    attach.ParentId = saleTerms.Id;
	    try {
	    	insert attach;
	    	return new PageReference('/' + saleTerms.Id);
	    } catch(Exception e) {
	    	ErrorLogger.log(e);
	    	return null;
	    }
  	}

  	public String chceckForVersion() {
  		String attNameCheck = attName + '%';
  		List<Attachment> existingA = [SELECT Id FROM Attachment WHERE ParentId = :saleTerms.Id AND Name LIKE :attNameCheck];
  		if(!existingA.isEmpty()) {
  			return String.valueOf(existingA.size() + 1);
		} else {
			return '1';
		}
  	}
}